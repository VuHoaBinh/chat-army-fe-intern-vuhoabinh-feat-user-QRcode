/* eslint-disable require-await */
/** @type {import('next').NextConfig} */
const nextConfig = {
    experimental: {
        serverActions: true
    },
    output: "standalone",
    images: {
        domains: [
            "cdn.imagin.studio",
            "storage.stechvn.org",
            "avatar.com",
            "test3.stechvn.org",
            "test4.stechvn.org",
            "fe05.stechvn.org",
            "vapa.vn",
            "fe01.stechvn.org",
            "fe04.stechvn.org",
            "fe03.stechvn.org",
            "test1.irics.vn",
            "mdac.irics.vn",
            "https://fe04.stechvn.org"
        ]
    },
    async redirects() {
        return [
            {
                source: "/",
                destination: "/chat",
                permanent: true
            }
        ];
    },
    reactStrictMode: false
};

module.exports = nextConfig;
