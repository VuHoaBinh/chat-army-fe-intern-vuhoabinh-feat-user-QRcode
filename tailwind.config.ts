/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
        "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
        "./src/app/**/*.{js,ts,jsx,tsx,mdx}"
    ],
    mode: "jit",
    darkMode: "class",

    theme: {
        extend: {
            fontFamily: {
                inter: ["Inter", "sans-serif"]
            },

            colors: {
                "black-100": "#2B2C35",
                "primary-blue": {
                    DEFAULT: "#2B59FF",
                    100: "#F5F8FF"
                },
                "secondary-orange": "#f79761",
                "light-white": {
                    DEFAULT: "rgba(59,60,152,0.03)",
                    100: "rgba(59,60,152,0.02)"
                },
                grey: "#747A88",
                thPrimary: "#FCD535",
                "thDark-background": "#000000",
                thNewtral1: "#222426",
                thNewtral2: "#55595F",
                thWhite: "#FDFDFD",
                thNewtral: "#18191B",
                thRed: "#B6061D",
                thPrimaryHover: "#FCD5350D",

                //===================LIGHT MODE===================//
                // Light mode colors
                "light-black-100": "#FFFFFF",
                "light-primary-blue": {
                    DEFAULT: "#3B3C98",
                    100: "#F2F3FE"
                },
                "light-secondary-orange": "#FFA584",
                "light-light-white": {
                    DEFAULT: "rgba(59,60,152,0.03)",
                    100: "rgba(59,60,152,0.02)"
                },
                "light-grey": "#A5A8B0",
                "light-thPrimary": "#f0b90b",
                "light-thDark-background": "#FFFFFF",
                "light-thNewtral1": "#E1E2E4",
                "light-thNewtral2": "#B6B8BE",
                "light-thWhite": "#333333",
                "light-thNewtral": "#EDEDED",
                "light-thRed": "#FF6B78",
                "light-thPrimaryHover": "#FCD5350D"
            },
            backgroundImage: {
                pattern: "url('/pattern.png')",
                "hero-bg": "url('/hero-bg.png')",
                "login-bg": "url('/logoDAK.jpg')"
            },
            keyframes: {
                showPopup: {
                    "0%": {
                        opacity: 0,
                        transform: "scale(0)"
                    },
                    "100%": {
                        opacity: 1,
                        transform: "scale(1)"
                    }
                }
            },
            animation: {
                "c-show-popup": "showPopup 0.2s linear"
            }
        }
    },
    plugins: []
};
