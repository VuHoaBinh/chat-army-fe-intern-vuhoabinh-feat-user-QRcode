import { render } from '@testing-library/react';
// import Home from '../src/app/components/Home';
// import { Avatar } from '@mui/material';
import Home from '@/src/app/(chat)/chat/components/Home';

it('renders homepage unchanged', () => {
    const { container } = render(<Home />);
    expect(container).toMatchSnapshot();
});
