// import AdmireEmoji from "../../public/admire_love_emoji.png";
// import SadEmoji from "../../public/sad.png";
// import ThumbsUpEmoji from "../../public/thumbs-up.png";

export const MESSAGE_TYPE = {
    TEXT: 1,
    FILE: 2,
    IMAGE: 3,
    MEDIA: 4,
    LINK: 5,
    MISSED_CALL: 6,
    CALL: 7,
    BUSY_CALL: 8,
    END_CALL: 9,
    STICKER: 10
};

export const EMOJI = [
    {
        id: 1,
        src: "👍",
        alt: "like"
    },
    {
        id: 2,
        src: "🫡",
        alt: "saluting"
    },
    {
        id: 3,
        src: "😳",
        alt: "flushed"
    },
    {
        id: 4,
        src: "😢",
        alt: "sad"
    },
    {
        id: 5,
        src: "😡",
        alt: "Angry"
    }

];

export const CONVERSATION = {
    TYPE: {
        INDIVIDUAL: 1,
        GROUP: 2
    },
    NAME_LENGTH: {
        MIN: 1,
        MAX: 255
    },
    ROLE_TYPE: {
        OWNER: 1,
        ADMIN: 2,
        MEMBER: 3,
        MEMBER_WAITING_ACCEPT:4
    }
};

export const TIMEOUT = {
    CALL: 2000,
    WAITING_CALL: 30000
};

export const EVENT_NAMES = {
    CONVERSATION: {
        SEEN_MESSAGE: "SEEN_MESSAGE",
        SEENED_MESSAGE: "SEENED_MESSAGE",
        SEND_MESSAGE: "SEND_MESSAGE",
        SENDED: "SENDED",
        ACTIVE: "ACTIVE",
        SEND_FAIL: "SEND_FAIL",
        TYPING_ON: "TYPING_ON",
        TYPING_OFF: "TYPING_OFF",
        REACTION: "REACTION",
        RECEIVE_MESSAGE: "RECEIVE_MESSAGE",
        CALL_IS_READY: "CALL_IS_READY",
        FORWARD_MESSAGE: "FORWARD_MESSAGE"
    },
    INDIVIDUAL_CALL: {
        IN_COMING_CALL: "IN_COMING_CALL",
        IN_COMING_CANCEL: "IN_COMING_CANCEL",
        IN_COMING_CALL_START: "IN_COMING_CALL_START", // when receiver accept
        IN_COMING_CALL_END: "IN_COMING_CALL_END", // when receiver or sender end
        RECEIVER_DENY: "RECEIVER_DENY",
        ANSWER_CALL: "ANSWER_CALL",
        INDIVIDUAL_CALL_USER_OFFLINE: "INDIVIDUAL_CALL_USER_OFFLINE",
        CALL_INCOMING_DEMO:"CALL_INCOMING_DEMO",
        CALL_ACCEPT_DEMO:"CALL_ACCEPT_DEMO",
        CALL_MUTE_DEMO:"CALL_MUTE_DEMO",
        CALL_DENY_DEMO:"CALL_DENY_DEMO",
        CALL_OUT_DEMO:"CALL_OUT_DEMO",
        CALL_BUSY_DEMO:"CALL_BUSY_DEMO",
        DEMO_INCOMING_CANCEL:"DEMO_INCOMING_CANCEL"
    }
};

export const ERROR_CODE = {
    USER_IS_OFFLINE: "USER_IS_OFFLINE",
    USER_IS_BUSY: "USER_IS_BUSY"
};

export const DEVICE = {
    CAMERA: {
        FRONT: "FRONT",
        BACK: "BACK",
        INTEGRATED: "INTEGRATED"
    }
};


export const MEDIA_TYPE = {
    IMAGE: 2,
    FILE: 3,
    LINK: 5
};
export const  SUPPORTED_FILE_TYPES = [
    'jpg',
    'jpeg',
    'png',
    'gif',
    'doc',
    'docx',
    'pdf',
    'webm',
    'mp4',
    'zip',
    'txt',
    'rar',
    'xlsx',
    'xlsm',
    '7z',
    'xz',
    'gz',
    'psd',
    'dng',
    'heic',
    'avi',
    'mov',
    'mp3',
    'wav',
    'ogg'
];
export const SOCIAL_REGEX_LIST = [
    /facebook\.com\/.*/,
    /instagram\.com\/.*/,
    /twitter\.com\/.*/,
    /x\.com\/.*/,
    /tiktok\.com\/.*/,
    /youtube\.com\/(?:watch|shorts).*/,
    /fb\.watch\/.*/,
    /twitch\.tv\/.*/
];