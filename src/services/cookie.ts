"use server";

import { cookies } from "next/headers";


function setCookie(key: string, value: string, option: object = {}): Promise<boolean>  {
    return new Promise(resolve => {
        cookies().set(key, value, { sameSite: 'strict', httpOnly: true, secure: true, ...option });
        return resolve(true);
    });
}


function deleteCookie(key: string): Promise<boolean>  {
    return new Promise(resolve => {
        cookies().delete(key);
        return resolve(true);
    });
}



function getCookie(key: string): Promise<string | undefined> {
    return new Promise(resolve => {
        return resolve(cookies().get(key)?.value);
    });
}

export { getCookie, setCookie, deleteCookie }; 