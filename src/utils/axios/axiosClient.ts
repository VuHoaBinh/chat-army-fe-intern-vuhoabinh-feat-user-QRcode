import axios from "axios";
import { getLoginCookies } from "../auth/handleCookies";

const getAxiosClient = (): any => {
    const cookies = getLoginCookies();

    return axios.create({
        headers: {
            Authorization: `Bearer ${cookies?.token_chat}`
        }
    });
};

export default getAxiosClient;
// import axios from "axios";
// import { getLoginCookies } from "../auth/handleCookies";

// const cookies = getLoginCookies();
// const axiosClient = axios.create({
//     headers: {
//         Authorization: `Bearer ${cookies?.token_chat}`
//     }
// });

// export default axiosClient;
