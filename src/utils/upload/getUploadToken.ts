
import { SUPPORTED_FILE_TYPES } from "@/src/constants/chat";
import * as yup from "yup";
interface IUploadResult {
    type: number,
    link: string,
}
const MAX_FILES_SIZE = 100 * 1024 * 1024; // 100MB

export async function getUploadToken(): Promise<string> {
    try{
        const response = await fetch(`${process.env.NEXT_PUBLIC_DAK_API}/media/token`);
        const data = await response.json();
        return data.data.token;
    }catch(error){
        throw new Error("Can't get upload token!!");
    }
}


export async function uploadFile(formData: FormData, sendHd: boolean = false): Promise<IUploadResult[]>{
    try{
        const uploadToken = await getUploadToken();
        if(!uploadToken){
            throw new Error("Can't get upload token");
        }
        // Append sendHd to formData        
        formData.append("hd", sendHd.toString());
        const headers = new Headers();
        headers.append("Authorization", `Bearer ${uploadToken}`);
        const response = await fetch(`${process.env.NEXT_PUBLIC_DAK_UPLOAD_API}/api/upload`, {
            method:'POST',
            headers,
            body:formData
        });
        const data = await response.json();
        return data.data;
    }catch(error: any){
        throw new Error(error?.message);
    }
}

const fileSchema = yup.object().shape({
    fileSize: yup.number().min(1, 'Kích thước tệp không hợp lệ').max(MAX_FILES_SIZE, 'Vượt quá giới hạn kích thước tệp'),
    fileType: yup.string().oneOf(SUPPORTED_FILE_TYPES, 'Không hỗ trợ loại tệp')
});

const validateFile = async (file: File): Promise<string|null> => {
    try {
        await fileSchema.validate({
            fileSize: file.size,
            fileType: (file.name.split(".").pop()as string).toLowerCase()
        });
        return null;
    } catch (err: any) {
        return err.message;
    }
};
export const validateFiles = async (files: File[]): Promise<{
    invalidFiles: File[];
    errors: string[];
}> => {
    const invalidFiles: File[] = [];
    const errors: string[] = [];

    // Validate tổng số lượng file < 100MB
    if (files.reduce((totalSize, file) => totalSize + file.size, 0) > MAX_FILES_SIZE) {
        invalidFiles.push(...files);
        errors.push("Tổng số lượng file không vượt quá 100MB");
        return { invalidFiles, errors };
    }

    for (let i = 0; i < files.length; i++) {
        const file = files[i];
        const error = await validateFile(file);
        if (error) {
            invalidFiles.push(file);
            errors.push(error);
        }
    }

    return { invalidFiles, errors };
};

