const createImage = (url: any) =>
    new Promise((resolve, reject) => {
        const image: any = new Image();
        image.addEventListener("load", () => resolve(image));
        image.addEventListener("error", (error: any) => reject(error));
        image.setAttribute("crossOrigin", "anonymous"); 
        image.src = url;
    });

async function getCroppedImg(imageSrc: any, pixelCrop: any) {
    const image: any = await createImage(imageSrc);
    const canvas: any = document.createElement("canvas");
    const ctx: any = canvas.getContext("2d");

    const maxSize = Math.max(image.width, image.height);
    const safeArea = 2 * ((maxSize / 2) * Math.sqrt(2));

    canvas.width = safeArea;
    canvas.height = safeArea;

    ctx.drawImage(
        image,
        safeArea / 2 - image.width * 0.5,
        safeArea / 2 - image.height * 0.5
    );
    const data = ctx.getImageData(0, 0, safeArea, safeArea);

    canvas.width = pixelCrop.width;
    canvas.height = pixelCrop.height;

    ctx.putImageData(
        data,
        Math.round(0 - safeArea / 2 + image.width * 0.5 - pixelCrop.x),
        Math.round(0 - safeArea / 2 + image.height * 0.5 - pixelCrop.y)
    );

    return canvas.toDataURL("image/jpeg");
}

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const cropImage = async (image: any, croppedAreaPixels: any, onError: any) => {
    try {
        const croppedImage = await getCroppedImg(image, croppedAreaPixels);
        return croppedImage;
    } catch (err) {
        onError(err);
    }
};
