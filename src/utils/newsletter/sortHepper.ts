import { INote, IVote } from "@/src/redux/slices/newsletterSlice";

export function sortNotes(notes: INote[]): INote[] {
    return notes.slice().sort((a, b) => {
        const dateA = new Date(a.createdAt).getTime();
        const dateB = new Date(b.createdAt).getTime();
        // Sắp xếp từ mới nhất đến cũ nhất
        return dateB - dateA;
    });
}

export function sortVotes(votes: IVote[]): IVote[] {
    return votes.slice().sort((a, b) => {
        const dateA = new Date(a.createdAt).getTime();
        const dateB = new Date(b.createdAt).getTime();
        // Sắp xếp từ mới nhất đến cũ nhất
        return dateB - dateA;
    });
}