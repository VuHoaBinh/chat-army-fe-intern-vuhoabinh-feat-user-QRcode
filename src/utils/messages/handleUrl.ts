import { SUPPORTED_FILE_TYPES } from "@/src/constants/chat";

export const isUrl = (message: string): boolean => {
    const pattern = /^(https?:\/\/)?(www\.)?[a-zA-Z0-9\-]+(\.[a-zA-Z]{2,6})/;
    (pattern.test(message));
    return pattern.test(message);
};

export const getFileName = (message: string): string => {
    const parts = message.split('.');
    const fileName = `${parts[parts.length - 2]}.${parts[parts.length - 1]}`;
    return fileName;
};


// const extensions = ["txt", "docx", "rar", "zip", "xlsx"];
export const checkFile = (message: string): boolean => {
    return SUPPORTED_FILE_TYPES.some(e =>
        message.includes(e)
    );
};
// .doc,.docx,.pdf,.zip,.txt,.rar,.xlsx,.xlsm,,.psd,.dng,.heic,.mp3,.wav,.ogg
export const getFileIcon = (fileExtension: string): string => {
    switch (fileExtension) {
    case "docx":
    case "doc":
        return "https://png.pngtree.com/png-vector/20190411/ourmid/pngtree-docx-file-document-icon-png-image_927834.jpg";
    case "xlsx":
    case "xls":
    case "xlsm":
        return "https://png.pngtree.com/png-clipart/20190705/original/pngtree-xlsx-file-document-icon-png-image_4230191.jpg";
    case "pptx":
    case "ppt":
        return "https://png.pngtree.com/element_our/md/20180627/md_5b334611319f5.jpg";
    case "zip":
    case "rar":
    case "7z":
    case "xz":
    case "gz":
        return "https://png.pngtree.com/png-vector/20190406/ourlarge/pngtree-zip-file-document-icon-png-image_917269.jpg";
    case "txt":
        return "https://png.pngtree.com/png-vector/20190501/ourmid/pngtree-vector-txt-icon-png-image_1009883.jpg";
    case "pdf":
        return "https://png.pngtree.com/png-clipart/20220612/original/pngtree-pdf-file-icon-png-png-image_7965915.png";
    default:
        return "https://png.pngtree.com/png-vector/20190628/ourmid/pngtree-file-icon-for-your-project-png-image_1521170.jpg";
    }
};

  