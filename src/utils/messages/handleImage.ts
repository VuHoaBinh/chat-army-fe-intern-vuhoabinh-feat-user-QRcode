export const handleImage = (url: string): File => {
    const base64Image = url.split(',')[1];
    const binaryImg = atob(base64Image);
    const arrayBuffer = new ArrayBuffer(binaryImg.length);
    const uint8Array = new Uint8Array(arrayBuffer);
    for (let i = 0; i < binaryImg.length; i++) {
        uint8Array[i] = binaryImg.charCodeAt(i);
    }
    const blob = new Blob([arrayBuffer], { type: 'image/png' });
    return new File([blob], 'image.png', { type: 'image/png' });
};