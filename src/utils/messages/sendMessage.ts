import {
    IForwardMessageData,
    IMessageData,
    TMessageCallback
} from "@/src/types/Message";
import { Socket } from "socket.io-client";

// Kích  thước giới hạn để nén
const MAX_MESSAGE_SIZE_TO_COMPRESS = 2 * 1024; // 2MB
// Kích  thước giới hạn để thông báo người dùng
const MAX_MESSAGE_SIZE_TO_ALERT = 20 * 1024; // 20MB

const getMessageSize = (message: string): number => {
    // Return the length of the message in bytes
    return new TextEncoder().encode(message).length;
};

export const sendMessage = (
    event: string,
    conversationSocket: Socket<any, any>,
    cb: TMessageCallback,
    messageData: IMessageData | IForwardMessageData
): void => {
    const pako = require("pako");

    //temp check
    if (messageData.content === "") {
        alert("Can't send empty message");
        return;
    }

    const messageSize = getMessageSize(messageData.content);

    if (messageSize > MAX_MESSAGE_SIZE_TO_ALERT) {
        alert("Kích thước vượt ngưỡng tối đa (20MB)");
        return;
    }

    if (messageSize > MAX_MESSAGE_SIZE_TO_COMPRESS) {
    // Compress messageData
        const compressedContent = pako.deflate(messageData.content);
        const base64String = btoa(
            String.fromCharCode.apply(null, compressedContent)
        );
        // Create a new messageData with the compressed content
        const compressedMessageData = {
            ...messageData,
            content: base64String,
            isCompressed: true
        };
       

        conversationSocket.emit(event, compressedMessageData, (result: boolean) => {
            if (!result) { /* empty */ } else {
                cb();
            }
        });
    } else {
        conversationSocket.emit(event, messageData, (result: boolean) => {
            if (!result) { /* empty */ } else {
                cb();
            }
        });
    }
};
