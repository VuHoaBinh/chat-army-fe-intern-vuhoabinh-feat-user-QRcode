import { IMessage } from "@/src/types/Message";
import pako from "pako";

export const decompressMessage = (msg: IMessage): void => {
    try {
        const binaryMessage = atob(msg.content);
        const uint8Array = new Uint8Array(binaryMessage.length);
        for (let i = 0; i < binaryMessage.length; i++) {
            uint8Array[i] = binaryMessage.charCodeAt(i);
        }
        const decompressedMess = pako.inflate(uint8Array, {
            to: "string"
        });
        msg.content = decompressedMess;
    } catch (error) {
        console.error("Error decompressing message:", error);
        return;
    }
};

export const decompressListMessage = (listMessage: IMessage[]): void => {
    listMessage.forEach((msg) => {
        if (msg.content) {
            if (msg.isCompressed) {
                decompressMessage(msg);
            }
        }
    });
};
