import { IMessage } from "@/src/types/Message";

export function sortByTime(messages: IMessage[]): IMessage[] {
    return messages.sort((a, b) => {
        const aDate = new Date(a.createdAt);
        const bDate = new Date(b.createdAt);
        return aDate.getTime() - bDate.getTime();
    });
}