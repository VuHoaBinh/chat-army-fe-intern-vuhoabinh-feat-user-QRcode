import { useState, useEffect, cache } from 'react';
import axiosClient from '../axios/axiosClient';
import { filterConversation } from '../conversations/filterConversation';
import { useAppDispatch, useAppSelector } from '@/src/redux/hook';
import { removeIdToListIdConversationTurnOffNotify } from '@/src/redux/slices/NotifyOfConversationSlice';

const fetchConversations = cache(() =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations?offset=0&status=[2]&limit=10`
    )
);

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const useNotification = (idConversation: string, isOpenModalTurnOffNotification?: boolean) => {
    const { user } = useAppSelector(state => state.user);
    const dispatch = useAppDispatch();

    const [timeTurnOffNotify, setTimeTurnOffNotify] = useState<any>(() => {
        fetchConversations().then((res: any) => {
            const listConversationOfThisUser: any = filterConversation(res.data, user.id);
            const conversationSettings = (listConversationOfThisUser && listConversationOfThisUser.find((c: any) => c.id === idConversation)?.conversationSetting) || undefined;
            const turnOffNotifySetting = conversationSettings && conversationSettings.find((st: any) => {
                return st.type === 1;
            });
            const turnOffTime = turnOffNotifySetting && conversationSettings.find((st: any) => {
                return st.type === 1;
            }).value;
    
            return turnOffTime;
        });
    });

    useEffect(() => {
        const handleTurnOffTime = async () => {
            const { data } = await fetchConversations();
            const listConversationOfThisUser: any = filterConversation(data, user.id);
            const conversationSettings = (listConversationOfThisUser && listConversationOfThisUser.find((c: any) => c.id === idConversation)?.conversationSetting) || undefined;
            const turnOffNotifySetting = conversationSettings && conversationSettings.find((st: any) => {
                return st.type === 1;
            });
            const turnOffTime = turnOffNotifySetting && conversationSettings.find((st: any) => {
                return st.type === 1;
            }).value;

            
            if(turnOffTime !== undefined) {
                if(turnOffTime === null) {
                    setTimeTurnOffNotify(turnOffTime);
                    return;
                }
                setTimeTurnOffNotify(turnOffTime);

                const now = Date.now();
                const remainingTime = Math.max(0, turnOffTime - now);
    
                // Đặt timeout dựa trên thời gian còn lại
                setTimeout(async () => {
                    try {
                        const now = Date.now();
                        const result = await axiosClient().put(
                            `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/setting/notification/${idConversation}/on`, {
                                time: now.toString()
                            }
                        );
                        if (result) {
                            dispatch(removeIdToListIdConversationTurnOffNotify(idConversation));
                        }
                    } catch (err: any) {
                        return err;
                    }
                    setTimeTurnOffNotify(undefined);
                }, remainingTime);
            }
        };

        handleTurnOffTime();
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [idConversation, isOpenModalTurnOffNotification]);

    return { timeTurnOffNotify, setTimeTurnOffNotify };
};

export default useNotification;
