import { cache } from "react";
import getAxiosClient from "../axios/axiosClient";

export const fetchTunrOffSeened = cache((type: string) =>
    getAxiosClient().put(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/users/setting/status-message/${type}`
    )
);
export const fetchStatusSeened = cache(() =>
    getAxiosClient().get(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/users/setting`)
);
