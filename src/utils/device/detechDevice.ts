export function isMobile(): boolean {
    const isMobileMediaQuery =
    typeof window !== "undefined" &&
    window.matchMedia &&
    window.matchMedia("(max-width: 768px)"); // Thay đổi giới hạn kích thước tùy thuộc vào responsive design của bạn

    const checkMobile = () => {
        if (isMobileMediaQuery) {
            return isMobileMediaQuery.matches;
        }
        return false;
    };

    return checkMobile();
}
