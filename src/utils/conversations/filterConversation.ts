// import { IConversation } from "@/src/types/Conversation";

import { IConversation } from "@/src/types/Conversation";

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const filterConversation = (conversations: any, idUser: string): any => {
    if(!conversations) return [];
    const data = conversations.map((c: any) => {
        
        const otherUserId: any = c.directUserId === idUser ? c.createdBy : c.directUserId;
      
        const conversationName: any = c?.type === 1 ? (c.createdByUser.id === idUser
            ? c.directUser?.username
            : c.createdByUser?.username) : c?.name;
       
        const avatar = c?.type === 1 ? ( c.createdByUser.id === idUser
            ? c.directUser?.avatar
            : c.createdByUser?.avatar) : c?.avatar;
        
        const lastLoginOther = c?.type === 1 && c.createdByUser.id === idUser
            ? c.directUser?.lastLogin
            : c.createdByUser?.lastLogin;

        return {
            ...c,
            name: conversationName,
            avatar,
            idOther: otherUserId,
            lastLoginOther
        };
    });
   
    return data;
};

export function sortConversationByNewest(messages: IConversation[] ): IConversation[] {
    messages.sort((a: IConversation, b: IConversation) => {
        const dateA = new Date(a?.latestMessage?.[a.latestMessage?.length - 1]?.createdAt);
        const dateB = new Date(b?.latestMessage?.[b.latestMessage?.length - 1]?.createdAt);
        return dateB.getTime() - dateA.getTime();
    });
    return messages;
}
