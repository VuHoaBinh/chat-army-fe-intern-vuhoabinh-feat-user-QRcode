import { IUserCookie } from "@/src/types/User";


export const chatHeaders = (cookie: IUserCookie): any => {
   
    const { refresh_token, xsrf_token, chat_token, token, userAgent } = cookie; 

    if(!refresh_token || !xsrf_token || !chat_token || !token || !userAgent) return;
    return {
        chat: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${chat_token}`,
            'Refresh-Token': `${refresh_token}`,
            'Xsrf-Token': `${xsrf_token}`,
            'Chat-Token': `${chat_token}`,
            'ngrok-skip-browser-warning': 'any'
        },
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
            'Refresh-Token': `${refresh_token}`,
            'Xsrf-Token': `${xsrf_token}`,
            'Chat-Token': `${chat_token}`,
            "User-Agent": userAgent
        }
    };
};
 
 
 