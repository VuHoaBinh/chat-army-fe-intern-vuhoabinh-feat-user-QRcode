import axios from "axios";
import crypto from "crypto";

export async function getLoginPublicKey (username: string): Promise<string> {
    try {
        const result = await axios.get(`${process.env.NEXT_PUBLIC_DAK_API}/auth/login/${username}`, {
        });
        return result.data.data; 
    } catch (e) {
        throw new Error("Can't get login public key!!");
    }
}

export function hashLoginPublicKey (passwordRaw: string, publicKey: string): string {
    try {
        const key = crypto.publicEncrypt({
            key: publicKey,
            padding: crypto.constants.RSA_PKCS1_OAEP_PADDING
        }, Buffer.from(passwordRaw));
        return key.toString('base64');
    } catch (e) {
        throw new Error("Can't hash the public key!!");
    }
}