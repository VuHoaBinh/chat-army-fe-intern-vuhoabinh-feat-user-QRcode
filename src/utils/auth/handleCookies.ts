import { IUserSocket } from "@/src/types/User";
import Cookies from "universal-cookie";

const cookies = new Cookies();

export const setLoginCookies = (userData: IUserSocket): void => {
    cookies.set("userLogin", userData, { path: "/" });
};

export const getLoginCookies = (): IUserSocket | undefined => {
    const cookiesResult = cookies.get("userLogin");
    if (!cookiesResult) return;
    return cookiesResult;
};

export const deleteLoginCookies = (): void => {
    cookies.remove("userLogin", { path: "/" });
};
