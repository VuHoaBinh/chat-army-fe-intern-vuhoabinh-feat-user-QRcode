/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { IMessage, IMessageData } from "@/src/types/Message";
import { decompressListMessage } from "../messages/decompressMessage";

interface CustomWindow extends Window {
    indexedDB: IDBFactory;
    mozIndexedDB: IDBFactory;
    webkitIndexedDB: IDBFactory;
    msIndexedDB: IDBFactory;
    IDBTransaction: IDBTransaction;
    webkitIDBTransaction: IDBTransaction;
    msIDBTransaction: IDBTransaction;
    IDBVersionChangeEvent: IDBVersionChangeEvent;
}

const fileToBase64 = (file: File): Promise<string> => {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.onload = () => resolve(reader.result as string);
        reader.onerror = reject;
        reader.readAsDataURL(file);
    });
};

const base64ToFile = (base64: string, fileName: string, mimeType: string): File => {
    const byteCharacters = atob(base64);
    const byteNumbers = new Array(byteCharacters.length);

    for (let i = 0; i < byteCharacters.length; i++) {
        byteNumbers[i] = byteCharacters.charCodeAt(i);
    }

    const byteArray = new Uint8Array(byteNumbers);
    return new File([byteArray], fileName, { type: mimeType });
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const storeDataInIndexDBById = (id: string, data: any): void => {
    // const dbRequest = indexedDB.open('ChatDatabase', 1);
    const dbRequest = (window as unknown as CustomWindow).indexedDB.open('ChatDatabase', 1);

    dbRequest.onupgradeneeded = (event) => {
        const db = (event.target as IDBOpenDBRequest).result;

        if (!db.objectStoreNames.contains('conversations')) {
            db.createObjectStore('conversations', { keyPath: 'id' });
        }
    };

    dbRequest.onsuccess = async (event) => {
        const db = (event.target as IDBOpenDBRequest).result;

        if (db.objectStoreNames.contains('conversations')) {
            const transaction = db.transaction('conversations', 'readwrite');
            const store = transaction.objectStore('conversations');

            const existingData = await store.get(id);

            existingData.onsuccess = () => {
                const existingItem = existingData.result || { id: id, data: [] };
                
                if (existingData.result && existingData.result.id) {
                    // nếu đã tồn tại id thì duyệt cái dữ liệu để kiểm tra dữ liệu cũ có giống dữ liệu mới không ? => nếu không thì update
                    // kiểm tra để lấy ra cái dữ liệu đã lưu trên indexdb trùng với tham số data đang truyền vào 
                    if(data?._id === existingData.result.data?.id) {
                        // nếu trùng thì so sánh length 2 mảng tin nhắn cũ và mới nếu bằng nhau thì return còn không thì cập nhật
                        const lengthOfOldlatestMessage = existingData.result.data.latestMessage.length;
                        const lengthOfNewlatestMessage = data.latestMessage.length;

                        if(lengthOfOldlatestMessage === lengthOfNewlatestMessage) return existingItem;
                    };
                } 

                // lấy danh sách tin nhắn ra kiểm tra nếu là tin nhắn thì lưu bình thường còn nếu là file hay ảnh thì chuyển sang base64 rồi mới lưu
                const lastMessage: any = data?.latestMessage;
                const newLatestMessage = lastMessage.map((element: IMessageData) => {
                    let content: any = element.content;
                    const isFile = content instanceof File;

                    if (isFile) {
                        fileToBase64(content as File).then((c) => {
                            content = c;

                            return {
                                ...element,
                                content : content, 
                                fileName: content.fileName
                            };
                        }).catch(e => (e));                    
                    }

                    return {
                        ...element,
                        content : content 
                    };
                });

                const newData: { id: string, data: any } = { id: id, data: {
                    ...data,
                    // lastestMessage : newLatestMessage,
                    latestMessage : newLatestMessage
                } };

                store.put(newData);
            };

            existingData.onerror = () => {
                console.error(`Error getting data for conversation ${id} from IndexedDB.`);
            };
        } else {
            console.error("The 'conversations' object store was not found.");
        }
    };

    dbRequest.onerror = (event: any) => {
        console.error("Error opening the IndexedDB database:", event.target.error);
    };
};

export const getAllDataFromIndexDB = () => {
    return new Promise((resolve, reject) => {
        const dbRequest = (window as unknown as CustomWindow).indexedDB.open('ChatDatabase', 1);

        dbRequest.onupgradeneeded = (event) => {
            const db = (event.target as IDBOpenDBRequest).result;

            if (!db.objectStoreNames.contains('conversations')) {
                db.createObjectStore('conversations', { keyPath: 'id' });
            }
        };

        dbRequest.onsuccess = async (event) => {
            const db = (event.target as IDBOpenDBRequest).result;
            if (db.objectStoreNames.contains('conversations')) {
                const transaction = await db.transaction('conversations', 'readonly');
                const store = await transaction.objectStore('conversations');
                const getAllRequest = await store.getAll();

                getAllRequest.onsuccess = async () => {
                    const allData = getAllRequest.result;

                    const allDataAfterConverting = await Promise.all(allData.map(async (conversation: any) => {
                        const conversationData: any = conversation.data.latestMessage;
                        const newLastestMessage = await Promise.all(conversationData.map(async (message: IMessage) => {
                            const content: any = message.content;
                            if (content instanceof File) {
                                try {
                                    const base64File = await fileToBase64(content as File);
                                    const regularFile = base64ToFile(base64File, content.name, content.type);
                                    return {
                                        ...message,
                                        content: URL.createObjectURL(regularFile)
                                    };
                                } catch (error) {
                                    console.error('Error converting file from Base64:', error);
                                    return message;
                                }
                            }
                            return message;
                        }));

                        const newData = { ...conversation.data };
                        newData.latestMessage = newLastestMessage;
                        const updatedConversation = {
                            ...conversation,
                            data: newData
                        };
                        return updatedConversation;
                    }));

                    // giải mã tất cả tin nhắn bị mã hóa
                    allDataAfterConverting.forEach((alldata: any) => {
                        decompressListMessage(alldata.data.latestMessage);
                    });

                    // ("getAllDataFromIndexDB");
                    // (allDataAfterConverting);

                    resolve(allDataAfterConverting);
                };

                getAllRequest.onerror = () => {
                    console.error('Error retrieving data from IndexedDB.');
                    reject(null);
                };
            } else {
                console.error("The 'conversations' object store was not found.");
                reject(null);
            }
        };

        dbRequest.onerror = (event: any) => {
            console.error("Error opening the IndexedDB database:", event.target.error);
            reject(null);
        };
    });
};

export const clearDataInIndexDB = (): void => {
    const dbRequest = (window as unknown as CustomWindow).indexedDB.open('ChatDatabase', 1);

    dbRequest.onupgradeneeded = (event) => {
        const db = (event.target as IDBOpenDBRequest).result;

        if (db.objectStoreNames.contains('conversations')) {
            db.deleteObjectStore('conversations');
        }
    };

    dbRequest.onsuccess = (event) => {
        const db = (event.target as IDBOpenDBRequest).result;

        if (!db.objectStoreNames.contains('conversations')) {
            console.error("The 'conversations' object store was not found.");
            return;
        }

        const transaction = db.transaction('conversations', 'readwrite');
        const store = transaction.objectStore('conversations');

        const clearRequest = store.clear();

        clearRequest.onsuccess = () => {
            ("IndexedDB data cleared successfully.");
        };

        clearRequest.onerror = () => {
            console.error("Error clearing data from IndexedDB.");
        };
    };

    dbRequest.onerror = (event: any) => {
        console.error("Error opening the IndexedDB database:", event.target.error);
    };
};
