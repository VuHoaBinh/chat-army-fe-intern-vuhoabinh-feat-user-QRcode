// import { chatHeaders } from "@/src/utils/auth/createHeaders";
// import axios from "axios";
// import { cookies } from "next/headers";
import { chatHeaders } from "@/src/utils/auth/createHeaders";
import axios from "axios";
import { cookies } from "next/headers";
import { NextRequest, NextResponse } from "next/server";

export const dynamic = "force-dynamic";
export async function POST(req: NextRequest): Promise<unknown> {
    "use server";
    try {
        if(req) {
            const body = await req.formData();
            (body.get("files"));
            const cookie: string = cookies().get("cookiesData")?.value || "";
            if(cookie === "") return  NextResponse.error();
            const parsedHeader = JSON.parse(cookie);
            const { headers } = chatHeaders(parsedHeader);

            const getTokenUpload = await axios.get(`${process.env.NEXT_PUBLIC_DAK_API}/upload/token`, {
                headers
            });

            if(getTokenUpload.data.success) {
                const { data } = await axios.post(`${process.env.NEXT_PUBLIC_DOMAIN_UPLOAD}`, body, {
                    headers: 
                    {
                        'Authorization': `Bearer ${getTokenUpload.data.token}`
                    }
                });
                
                return NextResponse.json(data);

            }
        
        }
  
    } catch(error) {
        return error;
    }
}