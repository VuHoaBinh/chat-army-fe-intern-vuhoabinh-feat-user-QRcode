import { chatHeaders } from "@/src/utils/auth/createHeaders";
import axios from "axios";
import { cookies } from "next/headers";
import { NextResponse } from "next/server";

export const dynamic = "force-dynamic";
export async function GET(): Promise<unknown> {
    "use server";
    try {
        const cookie: string = cookies().get("cookiesData")?.value || "";
        if(cookie === "") return  NextResponse.error();
        const parsedHeader = JSON.parse(cookie);
        const { headers } = chatHeaders(parsedHeader);
      
        const { data } = await axios.get(`${process.env.NEXT_PUBLIC_DAK_API}/user/friendship/find?limit=10&page=1&keyword=a`, {
            headers
        });
        
        return NextResponse.json(data);
  
    } catch(error) {
        throw new Error("test");
    }
}