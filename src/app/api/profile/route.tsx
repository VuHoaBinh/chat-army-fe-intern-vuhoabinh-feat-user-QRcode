import { chatHeaders } from "@/src/utils/auth/createHeaders";
import axios from "axios";
import { cookies } from "next/headers";
import { NextRequest, NextResponse } from "next/server";

export const dynamic = "force-dynamic";
export async function GET(req: NextRequest): Promise<unknown> {
    "use server";
    try {
        const cookie: string = cookies().get("cookiesData")?.value || "";
        if(cookie === "") return  NextResponse.error();
        const parsedHeader = JSON.parse(cookie);
        const { headers } = chatHeaders(parsedHeader);
        const idUser = req.nextUrl.searchParams.get("id"); 
        const { data } = await axios.get(`${process.env.NEXT_PUBLIC_DAK_API}/user/profile/${idUser}`, {
            headers
        });
        return NextResponse.json(data);
  
    } catch(error) {
        throw new Error("test");
    }
}

export async function PUT(req: NextRequest): Promise<unknown> {
    "use server";
    try {
        if(req) {
            const { name, email, birthday, gender, bio, background_img } = await req.json();

            const updatedValue = {
                name: name,
                email: email,
                birthday: birthday,
                gender: gender,
                bio: bio,
                background_img: background_img
            };

            const cookie: string = cookies().get("cookiesData")?.value || "";
            if(cookie === "") return  NextResponse.error();
            const parsedHeader = JSON.parse(cookie);
            const { headers } = chatHeaders(parsedHeader);
        
            const { data } = await axios.put(`${process.env.NEXT_PUBLIC_DAK_API}/user/profile/`, updatedValue, {
                headers
            });
            return NextResponse.json(data);
        }
  
    } catch(error) {
        throw new Error("test");
    }
}