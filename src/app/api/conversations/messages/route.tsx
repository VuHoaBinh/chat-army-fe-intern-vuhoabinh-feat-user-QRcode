import { IMessage } from "@/src/types/Message";
import { chatHeaders } from "@/src/utils/auth/createHeaders";
import axios from "axios";
import { cookies } from "next/headers";
import { NextRequest, NextResponse } from "next/server";

export const dynamic = "force-dynamic";
export async function GET(req: NextRequest): Promise<unknown> {
    "use server";
    try {
        const cookie: string = cookies().get("cookiesData")?.value || "";
        if(cookie === "") return  NextResponse.error();
        const parsedHeader = JSON.parse(cookie);
        const { chat } = chatHeaders(parsedHeader);
        const idConversation = req.nextUrl.searchParams.get("id");
        const page = req.nextUrl.searchParams.get("page") || "0";
        const { data } = await axios.get(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${idConversation}/messages?sort=desc&limit=20&offset=${parseInt(page)}`, {
            headers: chat
        });
    
        const sortArr = data.sort((a: IMessage, b: IMessage) => {
            if (a.createdAt < b.createdAt) {
                return -1;
            }
            if (a.createdAt > b.createdAt) {
                return 1;
            }
            return 0;
        });
        return NextResponse.json(sortArr);
    } catch(error) {
        throw new Error("Can't get messages in conversation!!!");
    }
}