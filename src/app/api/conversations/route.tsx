//import { IConversation } from "../../../types/Conversation";

import { chatHeaders } from "@/src/utils/auth/createHeaders";
import axios from "axios";
import { NextResponse, NextRequest } from "next/server";
import { cookies } from "next/headers";

export const dynamic = "force-dynamic";
export async function GET(req: NextRequest): Promise<unknown> {
    "use server";
    try {
        if(req) {
            
            const cookie: string = cookies().get("cookiesData")?.value || "";
            if(cookie === "") return  NextResponse.json([]);
            const parsedHeader = JSON.parse(cookie);
            const { chat } = chatHeaders(parsedHeader);
            const { data } = await axios.get(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations`, {
                headers: chat
            });
         
            return NextResponse.json(data);
        }
    } catch(error) {
        throw new Error("Can't get conversation!!!");
    }
}
