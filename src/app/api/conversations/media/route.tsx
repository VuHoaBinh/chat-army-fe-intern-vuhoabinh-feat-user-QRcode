import { chatHeaders } from "@/src/utils/auth/createHeaders";
import axios from "axios";
import { cookies } from "next/headers";
import { NextRequest, NextResponse } from "next/server";

export const dynamic = "force-dynamic";
export async function GET(req: NextRequest): Promise<unknown> {
    "use server";
    try {
        const cookie: string = cookies().get("cookiesData")?.value || "";
        if(cookie === "") return  NextResponse.error();
        const parsedHeader = JSON.parse(cookie);
        const { chat } = chatHeaders(parsedHeader);
        const idConversation = req.nextUrl.searchParams.get("id");
       
        const { data } = await axios.get(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${idConversation}/medias?sort=asc`, {
            headers: chat
        });
        return NextResponse.json(data);
  
    } catch(error) {
        throw new Error("test");
    }
}