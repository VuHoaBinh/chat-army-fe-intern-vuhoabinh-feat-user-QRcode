'use server';
import { chatHeaders } from '@/src/utils/auth/createHeaders';
import axios from 'axios';
import { cookies } from 'next/headers';
import { NextRequest, NextResponse } from "next/server";

export async function POST(req: NextRequest): Promise<unknown> {
    try {
        if(req) {
            const cookie: string = req.cookies.get("cookiesData")?.value || "";
            if(cookie === "") return  NextResponse.json("Can't find cookies");
            const parsedHeader = JSON.parse(cookie);
            const { headers } = chatHeaders(parsedHeader);

            const { data } = await axios.post(`${process.env.NEXT_PUBLIC_DAK_API}/auth/logout`, {},
                {
                    headers
                });
            cookies().delete("cookiesData");    
            return NextResponse.json(data);
            
        }
    } catch (error) {
        return error;
    }
};
