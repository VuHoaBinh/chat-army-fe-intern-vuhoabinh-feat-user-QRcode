
import { IUserCookie } from "@/src/types/User";
import { getLoginPublicKey, hashLoginPublicKey } from "@/src/utils/auth/getApiKeys";
import axios from "axios";
import { cookies } from "next/headers";
import { NextResponse, NextRequest } from "next/server";

export const dynamic = "force-dynamic";
export async function POST(req: NextRequest): Promise<unknown> {
    'use server';
    try {
        if(req) {
            const { username, password, userAgent } = await req.json();
            const publicKey = await getLoginPublicKey(username);
            const hashedKey = hashLoginPublicKey(password, publicKey);

            const userData = {
                usernameOrEmail: username,
                password: hashedKey, 
                remember_me: false
            };
            const { data } = await axios.post(`${process.env.NEXT_PUBLIC_DAK_API}/auth/login?locale=en`, userData, {
                headers: {
                    "User-Agent": userAgent
                }
            });
            const cookiesData: IUserCookie | any = {
                xsrf_token: data.data.xsrf_token,
                chat_token: data.data.chat_token,
                refresh_token: data.data.refresh_token,
                token: data.data.token,
                userAgent: userAgent
            };
            cookies().set("cookiesData", JSON.stringify(cookiesData), {
                httpOnly: true
            });
        
            return NextResponse.json(JSON.stringify(data.data));
        }
    } catch(error) {
        throw new Error("Can't login to server");
    }
}


