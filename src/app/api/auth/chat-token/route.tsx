/* eslint-disable require-await */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { IUserCookie } from "@/src/types/User";
import { cookies } from "next/headers";
import { NextRequest, NextResponse } from "next/server";

export const dynamic = "force-dynamic";
export async function GET(req: NextRequest): Promise<unknown> {
    "use server";
    try {
        const data = cookies().get("cookiesData")?.value || "";
        if(data === "") return NextResponse.json("Can't find the cookies");
        const parsedData: IUserCookie = JSON.parse(data);
        return NextResponse.json(parsedData.chat_token);
    } catch (error: any) {
        throw new Error("Can't get chat token!!!");
    }
}