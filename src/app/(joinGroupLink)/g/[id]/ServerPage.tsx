import { IInforGroup } from "@/src/types/Conversation";
import getAxiosClient from '@/src/utils/axios/axiosClient';
import { ReactElement, cache } from 'react';

const fetchInforGroup = cache(async (id: string) => {
    try {
        const response = await getAxiosClient().get(
            `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/group/invite/${id}`
        );
        return response.data;
    } catch (err) {
        console.error(err);
        return null; // Return a default value or handle the error accordingly
    }
});

export default async function ServerComp({ params }: { params: { id: string } }): Promise<ReactElement> {
    const inforGroup: IInforGroup | null = await fetchInforGroup(params.id);

    if (!inforGroup) {
        // Handle the case where data fetching fails
        return (
            <>
                {/* Provide default metadata or handle the error */}
                <meta property="og:title" content="DAC" />
                <meta property="og:image" content={`${process.env.NEXT_PUBLIC_DAK_API_SHARE_GROUP}/logoDAK.jpg`} />
                <meta name="twitter:card" content="summary" />
            </>
        );
    }

    return (
        <>
            <meta property="og:title" content={inforGroup.name} />
            <meta property="og:image" content={inforGroup.avatar} />
            <meta name="twitter:card" content="summary" />
        </>
    );
}
