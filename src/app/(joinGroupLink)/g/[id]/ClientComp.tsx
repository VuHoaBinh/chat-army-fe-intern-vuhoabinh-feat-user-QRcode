"use client";
import React, { ReactElement, cache, useEffect, useState } from "react";

import Logo from "@/public/DP 1.png";
import { useAppDispatch } from "@/src/redux/hook";
import { setOtherUser } from "@/src/redux/slices/conversationSlice";
import { IInforGroup } from "@/src/types/Conversation";
import { getLoginCookies } from "@/src/utils/auth/handleCookies";
import getAxiosClient from "@/src/utils/axios/axiosClient";
import { QRCode } from "antd";
import Image from "next/image";
import Link from "next/link";
import { usePathname, useRouter } from "next/navigation";

import { BiMessageRoundedDetail } from "react-icons/bi";
import { toast } from "react-toastify";

const fetchInforGroup = cache(async (id: string) => {
  try {
    const response = await getAxiosClient().get(
      `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/group/invite/${id}`
    );
    return response.data;
  } catch (err) {
    console.log(err);
  }
});
const fetchConversations = cache((offset: number) =>
  getAxiosClient().get(
    `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations?offset=${offset}&status=[2]&limit=20&type=2`
  )
);
const fetchJoinGroup = cache(async (id: string) => {
  try {
    const response = await getAxiosClient().post(
      `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/group/invite/${id}`
    );
    return response;
  } catch (err: any) {
    return err?.response?.data;
  }
});

const cookies = getLoginCookies();

export default function ClientComp({
  children,
  params,
}: Readonly<{
  children: React.ReactNode;
  params: { id: string };
}>): ReactElement {
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const [reviewMember, setReviewMember] = useState<boolean>(false);
  const { push } = useRouter();
  const [cookie, setCookie] = useState<any>(cookies);
  const [inforGroup, setInforGroup] = useState<IInforGroup | null>(null);
  const dispatch = useAppDispatch();
  const url = usePathname();

  const handleJoinGroup = async () => {
    setIsLoading(true);
    if (!cookie) {
      toast.warning("Bạn cần đăng nhập để tham gia nhóm");
      localStorage.setItem("redirectUrlGroup", params.id);
      push("/login");
    } else {
      const res = await fetchJoinGroup(params.id);
      if (res) {
        setIsLoading(false);
      }
      if (!reviewMember && res?.status === 200) {
        toast.success("Bạn đã tham gia nhóm thành công!");
        const { data } = await fetchConversations(0);
        const listConversation = data.filter((conversation: any) => {
          return conversation?.inviteId === params?.id;
        });
        if (listConversation) {
          push(`/chat/conversation/${listConversation[0]?.id}`);
        }
        return;
      } else if (reviewMember && res?.status === 200) {
        toast.warning("Bạn cần chờ chủ nhóm xác nhận");
        return;
      }

      const { data } = await fetchConversations(0);
      const listConversation = data.filter((conversation: any) => {
        return conversation?.inviteId === params?.id;
      });
      if (listConversation.length == 0) {
        toast.warning("Bạn cần chờ chủ nhóm xác nhận");
        return;
      } else if (listConversation.length > 0) {
        toast.success("Bạn hiện đang là thành viên trong nhóm!");
        dispatch(
          setOtherUser({
            otherUsername: listConversation[0]?.name || "",
            otherAvatar: listConversation[0]?.avatar || "",
          })
        );
        push(`/chat/conversation/${listConversation[0]?.id}`);
        return;
      }
      toast.error(`${res?.message}`);
      return;
    }
  };
  useEffect(() => {
    setCookie(getLoginCookies());
  }, [cookies]);
  useEffect(() => {
    (async () => {
      const data: IInforGroup = await fetchInforGroup(params.id);
      setInforGroup(data);
      setReviewMember(data?.reviewMember);
    })();
  }, [params.id]);

  return (
    <div
      className="w-full h-[100vh]  bg-thDark-background "
      suppressHydrationWarning={true}
    >
      {children}
      <div className="w-full h-[8vh] bg-thNewtral2 flex items-center justify-center md:justify-start">
        <Link href="/chat">
          <Image
            src={Logo}
            alt={"Logo"}
            loading="eager"
            priority
            width={43.702}
            height={43.702}
            className="bg-thNewtral1 rounded-full object-cover md:ml-9"
          />
        </Link>
      </div>
      <div className="md:w-[65%] w-[80%] md:h-[320px] bg-thNewtral2 mx-auto my-8 rounded-2xl flex flex-col items-center md:flex-row md:justify-between p-10">
        <div className="md:w-[70%] w-full">
          <div className="flex flex-col md:flex-row mb-10 items-center">
            <div className="mb-4">
              <Image
                src={inforGroup?.avatar || ""}
                alt={"Logo"}
                loading="eager"
                priority
                width={80}
                height={80}
                className="bg-thNewtral1 rounded-full object-cover  md:mr-6 w-20 h-20"
              />
            </div>

            <div className="flex flex-col items-center ">
              <div className="text-2xl font-bold mb-2">{inforGroup?.name}</div>
              <div className="mb-4">Nhóm</div>
              <button
                className={`flex bg-[#29BD2E] px-5 py-3 rounded-lg items-center gap-x-2 hover:bg-[#23A027] justify-center ${
                  isLoading ? "cursor-not-allowed opacity-50" : ""
                }`}
                onClick={handleJoinGroup}
              >
                <BiMessageRoundedDetail size={20} /> Tham gia nhóm
              </button>
            </div>
          </div>
          <div>
            <div className="font-bold text-[22px]">Mô tả nhóm</div>
            <div className="opacity-80">Nhóm chưa có thông tin mô tả.</div>
          </div>
        </div>
        <div className="w-full md:max-w-[20%] flex items-center flex-col pt-10 md:pt-0 gap-2">
          <QRCode
            value={`${process.env.NEXT_PUBLIC_DAK_API_SHARE_GROUP}${url}`}
            size={200}
            icon="/DP 1.png"
            iconSize={40}
          />
          <div className={`text-[12px] text-center break-words`}>
            Mở QRCode, bấm quét QR để quét và xem trên điện thoại
          </div>
        </div>
      </div>
    </div>
  );
}
