import { ReactElement } from "react";
import ClientComp from "./ClientComp";
import ServerComp from "./ServerPage";


export default function Page({ params }: { params: { id: string } }):
    ReactElement {
    return (
        <ClientComp params={params} >
            <ServerComp params={params} />
        </ClientComp>
    );
}
