
"use client";
import { useAppDispatch } from "@/src/redux/hook";
import { addListIdToListIdConversationDecoded } from "@/src/redux/slices/endToEndEncryptionSlice";
import {
    getLoginPublicKey,
    hashLoginPublicKey
} from "@/src/utils/auth/getApiKeys";
import { setLoginCookies } from "@/src/utils/auth/handleCookies";
import { useRouter } from "next/navigation";
import { ReactElement, useEffect, useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { AiFillEye, AiFillEyeInvisible } from "react-icons/ai";
import { toast } from "react-toastify";

type Inputs = {
    username: string;
    password: string;
    checkbox: string;
};

export default function LoginForm(): ReactElement {
    const {
        register,
        handleSubmit,
        formState: { errors }
    } = useForm<Inputs>();
    const { push } = useRouter();
    const [type, setType] = useState<string>("password");
    const dispatch = useAppDispatch();

    useEffect(() => {
        // Mã hóa đầu cuối
        localStorage.removeItem("listEndToEndEncryptionConversation");

        dispatch(addListIdToListIdConversationDecoded([]));
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleLoginSubmit: SubmitHandler<Inputs> = async (data) => {
        try {
            // call API get public key
            const publicKey = await getLoginPublicKey(data.username);
            const hashedKey = hashLoginPublicKey(data.password, publicKey);

            const userData = {
                usernameOrEmail: data.username,
                password: hashedKey,
                remember_me: false
            };
            // call API login
            fetch(`${process.env.NEXT_PUBLIC_DAK_API}/auth/login?locale=vi`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(userData)
            })
                .then((res) => res.json())
                .then((data) => {
                    if (data.success === true) {
                        const parsedResult = data.data;
                        setLoginCookies({ ...parsedResult.user });
                        toast.success("Đăng nhập thành công");
                        // Sử dụng hàm để lấy giá trị của token
                        const ridirectInforGroup = localStorage.getItem('redirectUrlGroup');
                        if (ridirectInforGroup) {
                            localStorage.removeItem('redirectUrlGroup');
                            push(`/g/${ridirectInforGroup}`);
                        } else {

                            push("/chat");
                        }
                    } else {
                        toast.error(data.message);
                    }
                })
                .catch((error) => {
                    toast.error("Đăng nhập không thành công");
                    return error;
                });
        } catch (error) {
            console.error(error);
            toast.error("Đăng nhập không thành công");
        }
    };

    return (
        <form
            method="post"
            onSubmit={handleSubmit(handleLoginSubmit)}
            className="flex flex-col gap-y-4 pl-2"
        >
            {/* register your input into the hook by invoking the "register" function */}
            <div className="px-2 py-2 w-full border-2 border-thPrimary rounded-full">
                <input
                    key="username"
                    type="text"
                    {...register("username", {
                        required: true
                    })}
                    className="text-thNewtral h-[40px] rounded-full w-full outline-none border-none pl-4"
                />
            </div>
            {errors.username && (
                <span className="text-blue-400 font-semibold text-xs">
                    Bạn cần điền thông tin username
                </span>
            )}
            {/* include validation with required or other standard HTML validation rules */}
            <div className="px-2 py-2 w-full border-2 border-thPrimary rounded-full flex items-center gap-x-1">
                <input
                    key="password"
                    type={type}
                    {...register("password", {
                        required: true
                    })}
                    className="text-thNewtral h-[40px] rounded-full w-full outline-none border-none pl-4"
                />
                {type === "password" ? (
                    <AiFillEye
                        size={20}
                        className="text-thPrimary"
                        onClick={() => setType("text")}
                    />
                ) : (
                    <AiFillEyeInvisible
                        size={20}
                        className="text-thPrimary"
                        onClick={() => setType("password")}
                    />
                )}
            </div>
            {/* errors will return when field validation fails  */}
            {errors.password && (
                <span className="text-yellow-400 font-semibold text-xs">
                    Bạn cần điền thông tin password
                </span>
            )}
            <div className="flex justify-between items-center px-4">
                <div className="flex items-center gap-x-1">
                    <input
                        key="checkbox"
                        {...register("checkbox")}
                        type="checkbox"
                        value="A"
                    />
                    <span className="text-thWhite opacity-60">Remember me</span>
                </div>
                <p className="text-thPrimary">Forgot password?</p>
            </div>
            <input
                type="submit"
                className="cursor-pointer text-thNewtral font-semibold py-2 rounded-full  dark:bg-thPrimary bg-light-thPrimary"
                value={"Login"}
            />
        </form>
    );
}
