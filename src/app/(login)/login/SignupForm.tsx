"use client";
import { ErrorResponseRegister } from "@/src/types/Auth";
import * as bcrypt from "bcryptjs";
import { ReactElement, useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { AiFillEye, AiFillEyeInvisible } from "react-icons/ai";
enum phone {
    VN = "+84",
    ENG = "",
}

type Inputs = {
    userName: string;
    exampleRequired: string;
    fullName: string;
    email: string;
    password: string;
    checkbox: string;
    phone: phone;
    phoneNumber: string;
};
interface SignupFormProps {
    setIsLogin: (value: boolean) => void;
}

export default function SignupForm({
    setIsLogin
}: SignupFormProps): ReactElement {
    const [gender, setGender] = useState<string>("male");
    const [listError, setListError] = useState<{
        [field: string]: string;
    }>({});

    const {
        register,
        formState: { errors },
        handleSubmit
    } = useForm<Inputs>();
    const [type, setType] = useState<string>("password");
    const onSubmit: SubmitHandler<Inputs> = async (data) => {
        try {
            const salt = 10;
            const hashedPassword = await bcrypt.hash(data.password, salt);

            const userData = {
                name: data.fullName,
                username: data.userName,
                email: data.email,
                password: hashedPassword
            };
            // call API login
            setListError({});
            const response = await fetch(
                `${process.env.NEXT_PUBLIC_DAK_API}/auth/register?locale=en`,
                {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify(userData)
                }
            );
            if (!response.ok) {
                const errorResponse = await response.json();

                if (
                    errorResponse.error_code === "INVALID_INPUT" &&
                    errorResponse.error
                ) {
                    errorResponse.error.forEach((error: ErrorResponseRegister) => {
                        console.error(`${error.field}: ${error.message}`);
                        setListError((prev) => ({
                            ...prev,
                            [error.field]: error.message
                        }));
                    });
                } else {
                    console.error("Unexpected server error:", errorResponse);
                }

                throw new Error("Request failed");
            }

            // Xử lý response khi thành công
            const dataSuccess = await response.json();
            if (dataSuccess.success === true) {
                alert("Đăng ký thành công");
                setIsLogin(true);
            }
        } catch (error) {
            // Xử lý lỗi
            console.error("Fetch error:", error);
            alert("Đăng ký không thành công");
        }
    };
    return (
        <form
            className="flex flex-col gap-y-4 pl-2"
            onSubmit={handleSubmit(onSubmit)}
        >
            <div className="px-2 py-2 w-full border-2 border-thPrimary rounded-full">
                <input
                    {...register("fullName", { required: true })}
                    className="text-thNewtral h-[40px] rounded-full w-full outline-none border-none pl-4"
                    placeholder="fullname"
                />
            </div>
            {errors.fullName && (
                <span className="text-yellow-400 font-semibold text-xs">
                    Fullname is required
                </span>
            )}
            <div className="px-2 py-2 w-full border-2 border-thPrimary rounded-full">
                <input
                    {...register("userName", { required: true })}
                    className="text-thNewtral h-[40px] rounded-full w-full outline-none border-none pl-4"
                    placeholder="username"
                />
            </div>
            {listError.username ? (
                <span className="text-red-400 font-semibold text-xs">
                    {listError.username}
                </span>
            ) : (
                errors.userName && (
                    <span className="text-yellow-400 font-semibold text-xs">
                        Username is required
                    </span>
                )
            )}

            {/* register your input into the hook by invoking the "register" function */}
            <div className="px-2 py-2 w-full border-2 border-thPrimary rounded-full">
                <input
                    {...register("email", { required: true })}
                    className="text-thNewtral h-[40px] rounded-full w-full outline-none border-none pl-4"
                    placeholder="email"
                />
            </div>
            {listError.email ? (
                <span className="text-red-400 font-semibold text-xs">
                    {listError.email}
                </span>
            ) : (
                errors.email && (
                    <span className="text-yellow-400 font-semibold text-xs">
                        Email required
                    </span>
                )
            )}

            {/* include validation with required or other standard HTML validation rules */}
            <div className="px-2 py-2 w-full border-2 border-thPrimary rounded-full flex items-center gap-x-1">
                <input
                    type={type}
                    {...register("password", {
                        required: true,
                        pattern: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\W)/
                    })}
                    className="text-thNewtral h-[40px] rounded-full w-full outline-none border-none pl-4"
                    placeholder="password"
                />
                {type === "password" ? (
                    <AiFillEye
                        size={20}
                        className="text-thPrimary"
                        onClick={() => setType("text")}
                    />
                ) : (
                    <AiFillEyeInvisible
                        size={20}
                        className="text-thPrimary"
                        onClick={() => setType("password")}
                    />
                )}
            </div>
            {/* errors will return when field validation fails  */}
            {errors.password && (
                <span className="text-yellow-400 font-semibold text-xs">
                    Password field is required
                </span>
            )}
            <div className="flex gap-x-1 border-thPrimary border-2 px-2 py-2 rounded-full">
                <select
                    {...register("phone")}
                    className="bg-transparent outline-none border-none selecbox"
                >
                    <option value="+84">VN</option>
                    <option value="">ENG</option>
                </select>
                <input
                    type="text"
                    {...register("phoneNumber")}
                    className="bg-transparent text-thWhite h-[40px] rounded-full w-full outline-none border-none pl-4"
                    placeholder="phone number"
                />
            </div>
            <div className="flex justify-between items-center border-thPrimary border-2 px-4 py-2 rounded-full">
                <span className="opacity-60">Gender:</span>
                <div className="flex gap-x-12">
                    <span
                        className={`${gender === "male" && "text-thPrimary"
                        } cursor-pointer`}
                        onClick={() => setGender("male")}
                    >
                        Male
                    </span>
                    <span
                        className={`${gender === "female" && "text-thPrimary"
                        } cursor-pointer`}
                        onClick={() => setGender("female")}
                    >
                        Female
                    </span>
                    <span
                        className={`${gender === "other" && "text-thPrimary"
                        } cursor-pointer`}
                        onClick={() => setGender("other")}
                    >
                        Other
                    </span>
                </div>
            </div>
            <div className="flex justify-between items-center px-4">
                <div className="flex items-center gap-x-1">
                    <input
                        {...register("checkbox", { required: true })}
                        type="checkbox"
                        value="A"
                    />
                    <span className="text-thWhite opacity-60">
                        I agree with the{" "}
                        <span className="text-thPrimary font-medium opacity-100">Rule</span>{" "}
                        &{" "}
                        <span className="text-thPrimary font-medium opacity-100">
                            Privacy
                        </span>
                    </span>
                </div>
            </div>
            <input
                type="submit"
                className="text-thNewtral font-semibold py-2 rounded-full  dark:bg-thPrimary bg-light-thPrimary"
                value={"Register"}
            />
        </form>
    );
}
