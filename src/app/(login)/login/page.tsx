"use client";
import { ReactElement, useState } from "react";
import styles from "./layout.module.css";
import LoginForm from "./LoginForm";
import SignupForm from "./SignupForm";

export default function Page(): ReactElement {
    const [isLogin, setIsLogin] = useState<boolean>(true);
    return (
        <div className="flex items-center h-screen justify-center px-4 lg:px-0 bg-black text-white">
            <div className="bg-thNewtral px-8 py-8 rounded-2xl w-[500px] shadow_login">
                <h1 className={`${styles.login_title} text-center lg:text-left mb-10`}>
                    Welcome to DAC
                </h1>
                {isLogin ? <LoginForm /> : <SignupForm setIsLogin={setIsLogin} />}
                {isLogin ? (
                    <p className="text-thWhite text-center mt-6 font-semibold">
                        {" "}
                        Do not have an account?{" "}
                        <span
                            className="text-thPrimary hover:brightness-125 cursor-pointer"
                            onClick={() => setIsLogin(false)}
                        >
                            Register
                        </span>
                    </p>
                ) : (
                    <p className="text-thWhite text-center mt-6 font-semibold">
                        {" "}
                        Already have an account?{" "}
                        <span
                            className="text-thPrimary hover:brightness-125 cursor-pointer"
                            onClick={() => setIsLogin(true)}
                        >
                            Login
                        </span>
                    </p>
                )}
            </div>
            <div className="bg-login-bg bg-auto bg-no-repeat w-[600px] h-full hidden lg:block"></div>
        </div>
    );
}
