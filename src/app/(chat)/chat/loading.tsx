import styles from "../chat/conversation/[id]/conversation.module.css";
export default function Loading(): JSX.Element {
    // You can add any UI inside Loading, including a Skeleton.
    return (
        <div className="flex h-[90vh]">
            <div className="px-6 py-5 w-full mf:w-fit lg:w-fit flex flex-col gap-y-2 h-full ">
                <h1 className="opacity-60 text-xl font-semibold">Message</h1>
                <div className="flex items-center gap-x-2">
                    <div className={`w-14 h-14 hidden dark:block rounded-full ${styles.gradient} `}>
                    </div>
                    <div className={`w-14 h-14 rounded-full dark:hidden ${styles.gradient_light} `}>
                    </div>
                    <div className="flex flex-col gap-y-1">
                        <div className={`w-32 h-2 hidden dark:block rounded ${styles.gradient} `}></div>
                        <div className={`w-20 h-1.5 hidden dark:block rounded ${styles.gradient} `}></div>
                        <div className={`w-32 h-2 dark:hidden ${styles.gradient_light} rounded  `}></div>
                        <div className={`w-20 h-1.5 dark:hidden ${styles.gradient_light} rounded  `}></div>
                    </div>
                </div>
                <div className="flex items-center gap-x-2">
                    <div className={`w-14 h-14 hidden dark:block rounded-full ${styles.gradient}`}>
                    </div>
                    <div className={`w-14 h-14 dark:hidden ${styles.gradient_light} rounded-full `}>
                    </div>
                    <div className="flex flex-col gap-y-1">
                        <div className={`w-32 h-2 hidden dark:block rounded ${styles.gradient}`}></div>
                        <div className={`w-20 h-1.5 hidden dark:block rounded ${styles.gradient}`}></div>
                        <div className={`w-32 h-2 dark:hidden ${styles.gradient_light} rounded `}></div>
                        <div className={`w-20 h-1.5 dark:hidden ${styles.gradient_light} rounded `}></div>
                    </div>
                </div>
                <div className="flex items-center gap-x-2">
                    <div className={`w-14 h-14 hidden dark:block rounded-full ${styles.gradient}`}>
                    </div>
                    <div className={`w-14 h-14 dark:hidden rounded-full ${styles.gradient_light}`}>
                    </div>
                    <div className="flex flex-col gap-y-1">
                        <div className={`w-32 h-2 hidden dark:block rounded ${styles.gradient}`}></div>
                        <div className={`w-20 h-1.5 hidden dark:block rounded ${styles.gradient}`}></div>
                        <div className={`w-32 h-2 dark:hidden rounded ${styles.gradient_light}`}></div>
                        <div className={`w-20 h-1.5 dark:hidden rounded ${styles.gradient_light}`}></div>
                    </div>
                </div>
                <div className="flex items-center gap-x-2">
                    <div className={`w-14 h-14 hidden dark:block rounded-full ${styles.gradient}`}>
                    </div>
                    <div className={`w-14 h-14 dark:hidden rounded-full ${styles.gradient_light}`}>
                    </div>
                    <div className="flex flex-col gap-y-1">
                        <div className={`w-32 h-2 hidden dark:block rounded ${styles.gradient}`}></div>
                        <div className={`w-20 h-1.5 hidden dark:block rounded ${styles.gradient}`}></div>
                        <div className={`w-32 h-2 dark:hidden rounded ${styles.gradient_light}`}></div>
                        <div className={`w-20 h-1.5 dark:hidden rounded ${styles.gradient_light}`}></div>
                    </div>
                </div>
            </div>
            <div className="w-full h-full px-4 hidden md:flex lg:flex flex-col justify-between py-5">
                <div className="flex flex-col justify-center items-center gap-y-2">
                    <div className={`w-20 h-20 hidden dark:block rounded-full ${styles.gradient}`}></div>
                    <div className={`w-32 h-2 hidden dark:block rounded ${styles.gradient}`}></div>
                    <div className={`w-20 h-1.5 hidden dark:block rounded ${styles.gradient}`}></div>
                    <div className={`w-20 h-20 dark:hidden rounded-full ${styles.gradient_light}`}></div>
                    <div className={`w-32 h-2 dark:hidden rounded ${styles.gradient_light}`}></div>
                    <div className={`w-20 h-1.5 dark:hidden rounded ${styles.gradient_light}`}></div>
                </div>
                <div className="flex flex-col gap-y-2">
                    <div className="flex gap-x-1 items-end">
                        <div className={`w-4 h-4 rounded-full`}></div>
                        <div className={`w-56 h-12 hidden dark:block ${styles.gradient} ${styles.partner_box}`}></div>
                        <div className={`w-56 h-12 dark:hidden ${styles.gradient_light} ${styles.partner_box}`}></div>
                    </div>
                    <div className="flex gap-x-1 items-end">
                        <div className={`w-4 h-4 rounded-full`}></div>
                        <div className={`w-32 h-12 hidden dark:block ${styles.gradient} ${styles.partner_box}`}></div>
                        <div className={`w-32 h-12 dark:hidden ${styles.gradient_light} ${styles.partner_box}`}></div>
                    </div>
                    <div className="flex gap-x-1 items-end">
                        <div className={`w-4 h-4 rounded-full hidden dark:block ${styles.gradient}`}></div>
                        <div className={`w-72 h-24 hidden dark:block ${styles.gradient} ${styles.partner_box}`}></div>
                        <div className={`w-4 h-4 rounded-full dark:hidden ${styles.gradient_light}`}></div>
                        <div className={`w-72 h-24 dark:hidden ${styles.gradient_light} ${styles.partner_box}`}></div>
                    </div>
                    <div className="flex justify-end w-full">
                        <div className={`w-56 h-16 hidden dark:block ${styles.gradient} ${styles.owner_box}`}></div>
                        <div className={`w-56 h-16 dark:hidden ${styles.gradient_light} ${styles.owner_box}`}></div>
                    </div>
                </div>
            </div>
        </div>
    );
}