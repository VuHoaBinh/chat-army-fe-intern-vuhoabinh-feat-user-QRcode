"use client";
import React, { ReactElement, use, cache } from "react";
import useWindowDimensions from "../../../../../hook/useWindowDimension";
// import { IMessage } from '@/src/types/Message';
import axios from "axios";
import RequestBox from "./components/RequestBox";
import RequestBoxMbl from "./components/RequestBoxMbl";

const fetchMessages = cache((id: string) =>
    axios.get(`/api/conversations/messages?id=${id}&page=1`)
);

export default function Page({
    params
}: {
  params: { id: string };
}): ReactElement {
    const { width } = useWindowDimensions();
    const { data } = use(fetchMessages(params.id)) as { data: any };
    return (
        <>
            {width > 480 ? (
                <RequestBox idConversation={params.id} listMessage={data} />
            ) : (
                <RequestBoxMbl idConversation={params.id} listMessage={data} />
            )}
        </>
    );
}
