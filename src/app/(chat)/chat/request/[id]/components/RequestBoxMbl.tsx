/* eslint-disable react-hooks/exhaustive-deps */
"use client";
import React, { ReactElement, useEffect, useState, useRef, SyntheticEvent } from "react";
import phone from "@/public/call.svg";
import videoCall from "@/public/video-call.svg";
import info from "@/public/info-circle.svg";
import Slute from "@/public/slute.png";
import Send from "@/public/send-2.svg";
import Image from "next/image";
import styles from "../request.module.css";
import { AiOutlineSmile } from "react-icons/ai";
import { BsImage, BsFileEarmarkText } from "react-icons/bs";
import dynamic from "next/dynamic";
import { Theme } from "emoji-picker-react";
import PartnerText from "../../../conversation/[id]/components/PartnerText";
import OwnerText from "../../../conversation/[id]/components/OwnerText";
import Forward from "../../../conversation/[id]/components/Forward";
import InfoChatMbl from "../../../conversation/[id]/components/InfoChatMbl";
// import CallMbl from '../../../conversation/[id]/components/CallMbl';
import { IMessage, IMessageData } from "@/src/types/Message";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { EVENT_NAMES, MESSAGE_TYPE } from "@/src/constants/chat";
import axios from "axios";
import useCreateSocket from "@/src/hook/useCreateSocket";
import { Socket } from "socket.io-client";
import moment from "moment";
import { setLastMessage } from "@/src/redux/slices/conversationSlice";
import { sendMessage } from "@/src/utils/messages/sendMessage";
import { decompressMessage } from "@/src/utils/messages/decompressMessage";
import { ISeenUser } from "@/src/types/User";
import userAvatar from "@/public/user.png";
const Picker = dynamic(
    () => {
        return import("emoji-picker-react");
    },
    { ssr: false }
);
interface ChatBoxProps {
    listMessage: IMessage[];
    idConversation: string;
}
export default function RequestBoxMbl({
    listMessage,
    idConversation
}: ChatBoxProps): ReactElement {
    const { user, conversation } = useAppSelector((state) => state.user);
    const dispatch = useAppDispatch();
    const [page, setPage] = useState<number>(2);
    const [messages, setMessages] = useState<IMessage[]>(listMessage);
    const [isBottom, setIsBottom] = useState<boolean>(false);
    const [isFocused, setIsFocused] = useState<boolean>(false);
    const [isOpenEmoji, setIsOpenEmoji] = useState<boolean>(false);
    const [isOpenInfo, setIsOpenInfo] = useState<boolean>(false);
    const [isCalling, setIsCalling] = useState<boolean>(false);
    const [forwardContent, setForwardContent] = useState<IMessage | any>({});
    const divRef = useRef<HTMLDivElement>(null);
    const inputRef = useRef<HTMLInputElement>(null);
    const socket: Socket<any, any> = useCreateSocket(idConversation);
    const [reactionID, setReactionID] = useState<string>("");
    const [isAccept, setIsAccept] = useState<boolean>(false);
    const [isSubmitting, setIsSubmitting] = useState<boolean>(false);
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [isOpenMediaLibrary, setIsOpenMediaLibrary] = useState<boolean>(false);
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [idMediaModal, setIdMediaModal] = useState<string>("");
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [urlMediaModal, setUrlMediaModal] = useState<string>("");
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [heightVirtualList, setHeightVirtualList] = useState<number>(0);

    useEffect(() => {
        if (divRef.current && !isBottom) {
            divRef.current.scrollTop = divRef.current.scrollHeight;
        }
    }, [messages]);

    useEffect(() => {
        (async () => {
            try {
                if (!isBottom) return;
                const response = await axios.get(
                    `/api/conversations/messages?id=${idConversation}&page=${page}`
                );
                if (response.data) {
                    setMessages([...response.data, ...messages]);
                    setPage(page + 1);
                }
            } catch (error) {
                console.log(error);
            }
        })();
    }, [isBottom]);

    useEffect(() => {
        const handleAddMessages = () => {
            if (!divRef.current) return;
            const divEl = divRef.current;
            const isBottomScroll = divEl.scrollTop === 0;
            setIsBottom(isBottomScroll);
        };
        divRef.current?.addEventListener("scroll", handleAddMessages);
        return () => {
            divRef.current?.removeEventListener("scroll", handleAddMessages);
        };
    }, []);

    //add emoji to input
    const onEmojiClick = (emojiObject: any) => {
        if (inputRef.current !== null) {
            const value = inputRef.current.value;
            const selectionStart = inputRef.current.selectionStart;
            // Thêm emoji vào vị trí con trỏ bất kỳ
            if (selectionStart !== null) {
                const updatedValue =
                    value.substring(0, selectionStart) +
                    emojiObject.emoji +
                    value.substring(selectionStart);
                // Cập nhật giá trị và vị trí con trỏ mới
                inputRef.current.value = updatedValue;
                inputRef.current.setSelectionRange(
                    selectionStart + emojiObject.emoji.length,
                    selectionStart + emojiObject.emoji.length
                );
                inputRef.current.focus();
            }
        }
    };
    const handleSubmit = () => {
        if (isSubmitting) {
            alert("You are messaging too fast, take a break! ");
            return;
        }
        setIsSubmitting(true);
        const inputValue = inputRef.current?.value;
        if (!inputValue) return;

        const messageData: IMessageData = {
            content: inputValue || "",
            replyTo: null,
            conversationId: idConversation,
            type: MESSAGE_TYPE.TEXT
        };
        sendMessage(
            EVENT_NAMES.CONVERSATION.SEEN_MESSAGE,
            socket,
            () => {
                if (inputRef.current !== null) inputRef.current.value = "";
            },
            messageData
        );
        setTimeout(() => {
            setIsSubmitting(false);
        }, 300);
    };

    useEffect(() => {
        if (!socket) return;
        socket.on(EVENT_NAMES.CONVERSATION.SENDED, (msg: IMessage): any => {
            if (msg.isCompressed) {
                decompressMessage(msg);
            }
            setMessages((prevState) => [...prevState, msg]);
            dispatch(setLastMessage(msg));
        });

        return () => {
            socket.off(EVENT_NAMES.CONVERSATION.SENDED);
        };
    }, [socket]);

    useEffect(() => {
        if (!user.socket) return;
        user.socket.on(
            EVENT_NAMES.CONVERSATION.RECEIVE_MESSAGE,
            (msg: IMessage) => {
                if (msg.isCompressed) {
                    decompressMessage(msg);
                }
            }
        );
        return () => {
            user.socket.off(EVENT_NAMES.CONVERSATION.RECEIVE_MESSAGE);
        };
    }, [user.socket]);

    const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === "Enter") {
            handleSubmit();
        }
    };
    const handleSlute = () => {
        const messageData: IMessageData = {
            content: `${process.env.NEXT_PUBLIC_DAK_API_SLUTE}/slute.png`,
            replyTo: null,
            conversationId: idConversation,
            type: MESSAGE_TYPE.STICKER
        };
        sendMessage(
            EVENT_NAMES.CONVERSATION.SEEN_MESSAGE,
            socket,
            () => { },
            messageData
        );
    };
    const handleCall = () => {
        setIsCalling(true);
    };
    const handleImageError = (event: SyntheticEvent<HTMLImageElement, Event>) => {

        const imgElement = event.target as HTMLImageElement;
        imgElement.src = userAvatar.src;
    };
    return (
        <>
            <div className={`w-screen overflow-x-hidden flex`}>
                <div
                    className={`h-[92vh] flex flex-col relative flex-shrink-0 w-screen`}
                >
                    <div className="chat_header py-2 px-4 dark:bg-thNewtral1 bg-light-thNewtral1 flex justify-between items-center">
                        <div className="flex gap-x-2 items-center">
                            <div className="relative">
                                <Image
                                    src={
                                        conversation?.otherAvatar === "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg"
                                            ? "/default_avatar.jpg"
                                            : conversation?.otherAvatar
                                    }
                                    sizes="(max-width: 44px) 44px, 44px"
                                    alt={"avt"}
                                    onError={handleImageError}
                                    loading="eager"
                                    priority
                                    width={44}
                                    height={44}
                                    className="rounded-full object-cover h-11"
                                />
                                <div className=" dark:bg-thPrimary bg-light-thPrimary w-3 h-3 rounded-full absolute bottom-0 right-0 border-2 border-thNewtral1"></div>
                            </div>
                            <div className="flex flex-col dark:text-thWhite text-light-thWhite">
                                <p className="text-base font-semibold">User name</p>
                                <span className="text-xs font-normal opacity-70">
                                    online now{" "}
                                </span>
                            </div>
                        </div>
                        <div className="flex gap-x-3 items-center">
                            <Image
                                src={phone}
                                sizes="(max-width: 24px) 24px, 24px"
                                alt={"avt"}
                                onError={handleImageError}
                                loading="eager"
                                priority
                                width={24}
                                height={24}
                                className="rounded-full h-10 cursor-pointer"
                                onClick={handleCall}
                            />
                            <Image
                                src={videoCall}
                                sizes="(max-width: 24px) 24px, 24px"
                                alt={"avt"}
                                onError={handleImageError}
                                loading="eager"
                                priority
                                width={24}
                                height={24}
                                className="rounded-full h-10 cursor-pointer"
                                onClick={handleCall}
                            />
                            <Image
                                src={info}
                                sizes="(max-width: 24px) 24px, 24px"
                                alt={"avt"}
                                onError={handleImageError}
                                loading="eager"
                                priority
                                width={24}
                                height={24}
                                className="rounded-full h-10 cursor-pointer"
                                onClick={() => setIsOpenInfo(true)}
                            />
                        </div>
                    </div>
                    {/* nếu đang mở socket gọi thì nhớ sửa lại điều kiện ở đoạn dưới đây */}
                    {isCalling && (
                        <p className="w-full  dark:bg-thPrimary bg-light-thPrimary py-1 text-thDark-background text-center">
                            Cuộc gọi đang diễn ra quay lại cuộc gọi...
                        </p>
                    )}
                    <div
                        className={`h-full flex flex-col overflow-y-auto snap-end py-3 ${styles.inboxList}`}
                        ref={divRef}
                        onClick={() => setIsOpenEmoji(false)}
                    >
                        <div className="mt-auto">
                            <div className="socket_conversation flex flex-col gap-y-2 px-4">
                                {messages?.map((message: IMessage, index: number) => {
                                    const curMsgDate = moment(message.createdAt).format(
                                        "DD-MM-YYYY"
                                    );
                                    const nextMsgDate =
                                        messages[index + 1] &&
                                        moment(messages[index + 1].createdAt).format("DD-MM-YYYY");
                                    return (
                                        <div key={message._id}>
                                            {index === 0 && (
                                                <div className="my-6">
                                                    <p className="text-sm w-fit mx-auto dark: bg-thNewtral2 bg-light-thNewtral2 px-1.5 py-1 rounded opacity-60">
                                                        {nextMsgDate}
                                                    </p>
                                                </div>
                                            )}
                                            {(messages[index]?.createdBy as ISeenUser)._id !==
                                                user.id &&
                                                (messages[index]?.createdBy as string) !== user.id ? (
                                                    <div
                                                        key={(messages[index]?.createdBy as ISeenUser)._id}
                                                    >
                                                        <PartnerText
                                                            socket={socket}
                                                            reactionID={reactionID}
                                                            setReactionID={setReactionID}
                                                            message={messages[index]}
                                                            setForwardContent={setForwardContent}
                                                            setMessageReply={() => { }}
                                                            setMessageScroll={() => { }}
                                                            setIdMediaModal={setIdMediaModal}
                                                            setUrlMediaModal={setUrlMediaModal}
                                                            setIsOpenMediaLibrary={setIsOpenMediaLibrary}
                                                        />
                                                    </div>
                                                ) : (
                                                    <div
                                                        key={(messages[index]?.createdBy as ISeenUser)._id}
                                                    >
                                                        <OwnerText
                                                            socket={socket}
                                                            reactionID={reactionID}
                                                            setReactionID={setReactionID}
                                                            message={messages[index]}
                                                            isLastMsg={
                                                                messages[messages.length - 1]._id ===
                                                            messages[index]?._id
                                                            }
                                                            setForwardContent={setForwardContent}
                                                            setMessageReply={() => { }}
                                                            setMessageScroll={() => { }}
                                                            setIdMediaModal={setIdMediaModal}
                                                            setUrlMediaModal={setUrlMediaModal}
                                                            setIsOpenMediaLibrary={setIsOpenMediaLibrary}
                                                        />
                                                    </div>
                                                )}
                                            {index >= 1 &&
                                                curMsgDate !== nextMsgDate &&
                                                nextMsgDate && (
                                                <div className="my-6">
                                                    <p className="text-sm w-fit mx-auto dark: bg-thNewtral2 bg-light-thNewtral2 px-1.5 py-1 rounded opacity-60">
                                                        {nextMsgDate}
                                                    </p>
                                                </div>
                                            )}
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                    <div className="absolute bottom-16 right-6">
                        {isOpenEmoji && (
                            <Picker
                                onEmojiClick={onEmojiClick}
                                height={337}
                                width={340}
                                searchDisabled={true}
                                theme={Theme.DARK}
                            />
                        )}
                    </div>
                    {Object.keys(forwardContent).length > 0 && (
                        <>
                            <div
                                className={`overlay absolute h-full w-full z-10 ${styles.glass} cursor-pointer`}
                                onClick={() => setForwardContent({})}
                            ></div>
                            <div className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 z-50">
                                <Forward
                                    message={forwardContent}
                                    socket={socket}
                                    close={() => setForwardContent({})}
                                />
                            </div>
                        </>
                    )}
                    {isAccept ? (
                        <div className="chat_area mt-auto py-4 px-4 flex gap-x-4 items-center">
                            <div className=" flex items-center justify-between w-full">
                                <div className="dark:bg-thNewtral1 bg-light-thPrimary border-black dark:border-none w-full py-3 px-2 flex gap-x-4 justify-between items-center rounded-2xl">
                                    <input
                                        type="text"
                                        ref={inputRef}
                                        className="px-4 outline-none border-none w-full bg-transparent"
                                        placeholder="nhập gì đó..."
                                        onFocus={() => setIsFocused(true)}
                                        onBlur={() => setIsFocused(false)}
                                        onKeyDown={handleKeyDown}
                                    />
                                    <ul
                                        className={`flex items-center gap-x-3 opacity-60 ${styles.input_feature}`}
                                    >
                                        <li onClick={() => setIsOpenEmoji(!isOpenEmoji)}>
                                            <AiOutlineSmile size={20} />
                                        </li>
                                        <li>
                                            <BsImage size={20} />
                                        </li>
                                        <li>
                                            <BsFileEarmarkText size={20} />
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div>
                                {isFocused ? (
                                    <div onClick={handleSubmit}>
                                        <Image
                                            src={Send}
                                            sizes="(max-width: 40px) 40px, 40px"
                                            alt={"avt"}
                                            onError={handleImageError}
                                            loading="eager"
                                            priority
                                            width={40}
                                            height={40}
                                            className="rounded-full h-10 cursor-pointer"
                                        />
                                    </div>
                                ) : (
                                    <div onClick={handleSlute}>
                                        <Image
                                            src={Slute}
                                            sizes="(max-width: 40px) 40px, 40px"
                                            alt={"avt"}
                                            onError={handleImageError}
                                            loading="eager"
                                            priority
                                            width={40}
                                            height={40}
                                            className="rounded-full h-10 cursor-pointer"
                                        />
                                    </div>
                                )}
                            </div>
                        </div>
                    ) : (
                        <div className="request_area mt-auto py-4 px-4 dark:bg-thNewtral1 bg-light-thNewtral1 flex flex-col justify-between items-center gap-3">
                            <div className="flex justify-around items-center w-full">
                                <button
                                    className="text-lg font-semibold text-thPrimary"
                                    onClick={() => setIsAccept(true)}
                                >
                                    Accept
                                </button>
                                <button className="text-lg font-semibold ">Decline</button>
                                <button className="text-lg font-semibold text-thRed">
                                    Block
                                </button>
                            </div>
                            <div className="opacity-60 text-sm">
                                <p className="text-center">
                                    Accept the request to chat or decline this user if they harass
                                    you
                                </p>
                            </div>
                        </div>
                    )}
                </div>
                <InfoChatMbl
                    idConversation={idConversation}
                    isOpenInfo={isOpenInfo}
                    setIsOpenInfo={setIsOpenInfo}
                    setIdMediaModal={setIdMediaModal}
                    setUrlMediaModal={setUrlMediaModal}
                    setIsOpenMediaLibrary={setIsOpenMediaLibrary}
                    heightVirtualList={heightVirtualList}
                />
            </div>
            {/* call component */}
            {/* {isCalling && (<CallMbl setIsCalling={setIsCalling}/> )} */}
        </>
    );
}
