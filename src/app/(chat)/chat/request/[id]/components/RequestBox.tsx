/* eslint-disable react-hooks/exhaustive-deps */
"use client";
import React, {
    ReactElement,
    useEffect,
    useState,
    useRef,
    ChangeEvent,
    SyntheticEvent
} from "react";
import phone from "@/public/call.svg";
import videoCall from "@/public/video-call.svg";
import info from "@/public/info-circle.svg";
import Slute from "@/public/slute.png";
import Send from "@/public/send-2.svg";
import Image from "next/image";
import styles from "../request.module.css";
import { AiOutlineSmile } from "react-icons/ai";
import {
    BsImage,
    BsFileEarmarkText,
    BsLink45Deg,
    BsReplyAllFill
} from "react-icons/bs";
import { IoMdClose, IoIosCloseCircleOutline } from "react-icons/io";
import dynamic from "next/dynamic";
import { Theme } from "emoji-picker-react";
import PartnerText from "../../../conversation/[id]/components/PartnerText";
import OwnerText from "../../../conversation/[id]/components/OwnerText";
import InfoChat from "../../../conversation/[id]/components/InfoChat";
import Forward from "../../../conversation/[id]/components/Forward";
// import Call from '../../../conversation/[id]/components/Call';
import { IMessage, IMessageData } from "@/src/types/Message";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import moment from "moment";
import axios from "axios";
import useCreateSocket from "@/src/hook/useCreateSocket";
import { Socket } from "socket.io-client";
import { EVENT_NAMES, MESSAGE_TYPE } from "@/src/constants/chat";
import { setLastMessage } from "@/src/redux/slices/conversationSlice";

import { sendMessage } from "@/src/utils/messages/sendMessage";
import { decompressMessage } from "@/src/utils/messages/decompressMessage";
import { ISeenUser } from "@/src/types/User";
import { getFileIcon, getFileName } from "@/src/utils/messages/handleUrl";
import userAvatar from "@/public/user.png";
const Picker = dynamic(
    () => {
        return import("emoji-picker-react");
    },
    { ssr: false }
);

interface ChatBoxProps {
    listMessage: IMessage[];
    idConversation: string;
}

export default function RequestBox({
    listMessage,
    idConversation
}: ChatBoxProps): ReactElement {
    const { user, conversation } = useAppSelector((state) => state.user);
    const dispatch = useAppDispatch();
    const [page, setPage] = useState<number>(2);
    const [messages, setMessages] = useState<IMessage[]>(listMessage);
    const [isBottom, setIsBottom] = useState<boolean>(false);
    const [isOpenInfo, setIsOpenInfo] = useState<boolean>(false);
    const [isFocused, setIsFocused] = useState<boolean>(false);
    const [isOpenEmoji, setIsOpenEmoji] = useState<boolean>(false);
    const [forwardContent, setForwardContent] = useState<IMessage | any>({});
    const divRef = useRef<HTMLDivElement>(null);
    const inputRef = useRef<HTMLInputElement>(null);
    const socket: Socket<any, any> = useCreateSocket(idConversation);
    const [isCalling, setIsCalling] = useState<boolean>(false);
    const [selectedImage, setSelectedImage] = useState<File[]>([]);
    const [reactionID, setReactionID] = useState<string>("");
    const [isAccept, setIsAccept] = useState<boolean>(false);
    const [isSubmitting, setIsSubmitting] = useState<boolean>(false);
    const [messageReply, setMessageReply] = useState<IMessage | any>({});
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [isOpenMediaLibrary, setIsOpenMediaLibrary] = useState<boolean>(false);
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [idMediaModal, setIdMediaModal] = useState<string>("");
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [urlMediaModal, setUrlMediaModal] = useState<string>("");
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [heightVirtualList, setHeightVirtualList] = useState<number>(0);
    /**
   * Khi chat box được rendered hoặc nhận được tin nhắn mới, khung chat sẽ được cuộn xuống dưới
   */
    useEffect(() => {
        ("test scroll");
        if (divRef.current && !isBottom) {
            divRef.current.scrollTop = divRef.current.scrollHeight;
        }
    }, [messages]);

    useEffect(() => {
        (async () => {
            try {
                if (!isBottom) return;
                const response = await axios.get(
                    `/api/conversations/messages?id=${idConversation}&page=${page}`
                );
                if (response.data) {
                    setMessages([...response.data, ...messages]);
                    setPage(page + 1);
                }
            } catch (error) {
                console.log(error);
            }
        })();
    }, [isBottom]);

    useEffect(() => {
        const handleAddMessages = () => {
            if (!divRef.current) return;
            const divEl = divRef.current;
            const isBottomScroll = divEl.scrollTop === 0;
            setIsBottom(isBottomScroll);
        };
        divRef.current?.addEventListener("scroll", handleAddMessages);
        return () => {
            divRef.current?.removeEventListener("scroll", handleAddMessages);
        };
    }, []);

    //add emoji to input
    const onEmojiClick = (emojiObject: any) => {
        if (inputRef.current !== null) {
            const value = inputRef.current.value;
            const selectionStart = inputRef.current.selectionStart;
            // Thêm emoji vào vị trí con trỏ bất kỳ
            if (selectionStart !== null) {
                const updatedValue =
                    value.substring(0, selectionStart) +
                    emojiObject.emoji +
                    value.substring(selectionStart);
                // Cập nhật giá trị và vị trí con trỏ mới
                inputRef.current.value = updatedValue;
                inputRef.current.setSelectionRange(
                    selectionStart + emojiObject.emoji.length,
                    selectionStart + emojiObject.emoji.length
                );
                inputRef.current.focus();
            }
        }
    };
    const handleSubmit = () => {
        if (isSubmitting) {
            alert("You are messaging too fast, take a break! ");
            return;
        }
        setIsSubmitting(true);
        const inputValue = inputRef.current?.value;
        if (!inputValue) return;

        const messageData: IMessageData = {
            content: inputValue || "",
            replyTo: null,
            conversationId: idConversation,
            type: MESSAGE_TYPE.TEXT
        };
        sendMessage(
            EVENT_NAMES.CONVERSATION.SEND_MESSAGE,
            socket,
            () => {
                if (inputRef.current !== null) inputRef.current.value = "";
            },
            messageData
        );
        setTimeout(() => {
            setIsSubmitting(false);
        }, 300);
    };

    //event for conversation socket
    useEffect(() => {
        if (!socket) return;
        socket.on(EVENT_NAMES.CONVERSATION.SENDED, (msg: IMessage) => {
            if (msg.isCompressed) {
                decompressMessage(msg);
            }
            setMessages((prevState) => [...prevState, msg]);
            dispatch(setLastMessage(msg));
        });

        return () => {
            socket.off(EVENT_NAMES.CONVERSATION.SENDED);
        };
    }, [socket]);

    //event for chat socket
    useEffect(() => {
        if (!user.socket) return;
        user.socket.on(
            EVENT_NAMES.CONVERSATION.RECEIVE_MESSAGE,
            (msg: IMessage) => {
                if (msg.isCompressed) {
                    decompressMessage(msg);
                }
                setMessages((prevState) => [...prevState, msg]);
                dispatch(setLastMessage(msg));
            }
        );
        return () => {
            user.socket.off(EVENT_NAMES.CONVERSATION.RECEIVE_MESSAGE);
        };
    }, [user.socket]);

    const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === "Enter") {
            handleSubmit();
        }
    };

    const handleSlute = () => {
        const messageData: IMessageData = {
            content: `${process.env.NEXT_PUBLIC_DAK_API_SLUTE}/slute.png`,
            replyTo: null,
            conversationId: idConversation,
            type: MESSAGE_TYPE.STICKER
        };
        sendMessage(
            EVENT_NAMES.CONVERSATION.SEND_MESSAGE,
            socket,
            () => { },
            messageData
        );
    };

    const handleCall = () => {
        setIsCalling(true);
    };
    const handleImageChange = (event: ChangeEvent<HTMLInputElement>) => {
        const files = event.target.files;
        if (files && files.length > 0) {
            const newImages = Array.from(files);
            setSelectedImage([...selectedImage, ...newImages]);
        }
    };
    const handleDeleteimg = (index: number) => {
        const newArray = selectedImage.filter((_, i) => i !== index);
        setSelectedImage(newArray);
    };
    const handleImageError = (event: SyntheticEvent<HTMLImageElement, Event>) => {

        const imgElement = event.target as HTMLImageElement;
        imgElement.src = userAvatar.src;
    };
    return (
        <div className="h-full w-full relative">
            {/* <div className='h-full w-full absolute flex'>
                {
                    isCalling && (
                        <>
                            <Call isCalling={isCalling} setIsCalling={setIsCalling} avatar={conversation?.otherAvatar} />
                            <div className='w-[32%] flex-shrink-0'></div>
                        </>)
                }
            </div> */}
            <div className="flex h-full w-full absolute z-10 ">
                {isCalling && (
                    <div className="h-full w-full absolute flex">
                        {/* <Call isCalling={isCalling} setIsCalling={setIsCalling} avatar={conversation?.otherAvatar} /> */}
                        <div className="lg:w-[32%] md:w-0 flex-shrink-0"></div>
                    </div>
                )}
                <div
                    className={`chat_box dark:bg-thNewtral bg-light-thNewtral w-full flex flex-col ml-auto relative z-20 ${isOpenInfo
                        ? "md:!w-0 md:overflow-hidden flex-grow"
                        : "flex-shrink-0"
                    } ${isCalling
                        ? "lg:!w-1/3 md:!w-0 flex-shrink-0 rounded-2xl overflow-hidden transform transition-all duration-500"
                        : ""
                    }`}
                >
                    <div className="chat_header py-2 px-4 dark:bg-thNewtral1 bg-light-thNewtral1 flex justify-between items-center">
                        <div className="flex gap-x-2 items-center">
                            <div className="relative">
                                {/* nếu có thẻ video khi call thì check điều kiện bật video rồi cho nó thay thế thẻ Image ở dưới  */}
                                <Image
                                    src={
                                        conversation?.otherAvatar === "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg"
                                            ? "/default_avatar.jpg"
                                            : conversation?.otherAvatar
                                    }
                                    sizes="(max-width: 44px) 44px, 44px"
                                    alt={"avt"}
                                    loading="eager"
                                    onError={handleImageError}
                                    priority
                                    width={44}
                                    height={44}
                                    className="rounded-full object-cover h-11"
                                />
                                <div className=" dark:bg-thPrimary bg-light-thPrimary w-3 h-3 rounded-full absolute bottom-0 right-0 border-2 border-thNewtral1"></div>
                            </div>
                            <div className="flex flex-col dark:text-thWhite text-light-thWhite">
                                <p className="text-base font-semibold">
                                    {conversation?.otherUsername || ""}
                                </p>
                                <span className="text-xs font-normal opacity-70">
                                    online now{" "}
                                </span>
                            </div>
                        </div>
                        <div className="flex gap-x-3 items-center">
                            <Image
                                src={phone}
                                sizes="(max-width: 24px) 24px, 24px"
                                alt={"avt"}
                                loading="eager"
                                onError={handleImageError}
                                priority
                                width={24}
                                height={24}
                                className="rounded-full h-10 cursor-pointer"
                                onClick={handleCall}
                            />
                            <Image
                                src={videoCall}
                                sizes="(max-width: 24px) 24px, 24px"
                                alt={"avt"}
                                loading="eager"
                                onError={handleImageError}
                                priority
                                width={24}
                                height={24}
                                className="rounded-full h-10 cursor-pointer"
                                onClick={handleCall}
                            />
                            <Image
                                src={info}
                                sizes="(max-width: 24px) 24px, 24px"
                                alt={"avt"}
                                loading="eager"
                                onError={handleImageError}
                                priority
                                width={24}
                                height={24}
                                className="rounded-full h-10 cursor-pointer"
                                onClick={() => setIsOpenInfo(true)}
                            />
                        </div>
                    </div>
                    <div
                        className={`h-full flex flex-col overflow-y-auto snap-end py-3 ${styles.inboxList}`}
                        ref={divRef}
                        onClick={() => setIsOpenEmoji(false)}
                    >
                        <div className="mt-auto">
                            <div className="socket_conversation flex flex-col gap-y-2 px-4">
                                {messages?.map((message: IMessage, index: number) => {
                                    const curMsgDate = moment(message.createdAt).format(
                                        "DD-MM-YYYY"
                                    );

                                    const nextMsgDate =
                                        messages[index + 1] &&
                                        moment(messages[index + 1].createdAt).format("DD-MM-YYYY"); 
                                    return (
                                        <div key={message._id}>
                                            {index === 0 && (
                                                <div className="my-6">
                                                    <p className="text-sm w-fit mx-auto dark: bg-thNewtral2 bg-light-thNewtral2 px-1.5 py-1 rounded opacity-60">
                                                        {nextMsgDate}
                                                    </p>
                                                </div>
                                            )}
                                            {(messages[index]?.createdBy as ISeenUser)._id !==
                                                user.id &&
                                                (messages[index]?.createdBy as string) !== user.id ? (
                                                    <div
                                                        key={(messages[index]?.createdBy as ISeenUser)._id}
                                                    >
                                                        <PartnerText
                                                            socket={socket}
                                                            reactionID={reactionID}
                                                            setReactionID={setReactionID}
                                                            message={messages[index]}
                                                            setForwardContent={setForwardContent}
                                                            setMessageReply={setMessageReply}
                                                            setMessageScroll={() => { }}
                                                            setIdMediaModal={setIdMediaModal}
                                                            setUrlMediaModal={setUrlMediaModal}
                                                            setIsOpenMediaLibrary={setIsOpenMediaLibrary}
                                                        />
                                                    </div>
                                                ) : (
                                                    <div
                                                        key={(messages[index]?.createdBy as ISeenUser)._id}
                                                    >
                                                        <OwnerText
                                                            socket={socket}
                                                            reactionID={reactionID}
                                                            setReactionID={setReactionID}
                                                            message={messages[index]}
                                                            isLastMsg={
                                                                messages[messages.length - 1]._id ===
                                                            messages[index]?._id
                                                            }
                                                            setForwardContent={setForwardContent}
                                                            setMessageReply={setMessageReply}
                                                            setMessageScroll={() => { }}
                                                            setIdMediaModal={setIdMediaModal}
                                                            setUrlMediaModal={setUrlMediaModal}
                                                            setIsOpenMediaLibrary={setIsOpenMediaLibrary}
                                                        />
                                                    </div>
                                                )}
                                            {index >= 1 &&
                                                curMsgDate !== nextMsgDate &&
                                                nextMsgDate && (
                                                <div className="my-6">
                                                    <p className="text-sm w-fit mx-auto dark: bg-thNewtral2 bg-light-thNewtral2 px-1.5 py-1 rounded opacity-60">
                                                        {nextMsgDate}
                                                    </p>
                                                </div>
                                            )}
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                    </div>
                    <div className="absolute bottom-16 right-6">
                        {isOpenEmoji && (
                            <Picker
                                onEmojiClick={onEmojiClick}
                                height={337}
                                width={340}
                                searchDisabled={true}
                                theme={Theme.DARK}
                            />
                        )}
                    </div>
                    {Object.keys(forwardContent).length > 0 && (
                        <>
                            <div
                                className={`overlay absolute h-full w-full z-10 ${styles.glass} cursor-pointer`}
                                onClick={() => setForwardContent({})}
                            ></div>
                            <div className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 z-50">
                                <Forward
                                    message={forwardContent}
                                    socket={socket}
                                    close={() => setForwardContent({})}
                                />
                            </div>
                        </>
                    )}
                    {isAccept ? (
                        <div
                            className={`chat_area mt-auto py-4 px-4 rounded-lg ${selectedImage.length > 0 &&
                                "bg-thNewtral1 border-t-4 border-t-thNewtral !pt-0"
                            }`}
                        >
                            {selectedImage.length > 0 && (
                                <div className="py-2">
                                    <div className="flex items-center justify-between">
                                        <p className="mb-2 text-sm">
                                            {" "}
                                            <span className="text-thPrimary">
                                                {selectedImage.length}
                                            </span>{" "}
                                            Ảnh được chọn 👇
                                        </p>
                                        <div
                                            className="cursor-pointer"
                                            onClick={() => setSelectedImage([])}
                                        >
                                            <IoIosCloseCircleOutline size={24} />
                                        </div>
                                    </div>

                                    <div className="flex gap-2 w-full overflow-x-auto">
                                        {selectedImage?.map((image, index) => (
                                            <div
                                                className="w-24 h-24 rounded-md overflow-hidden relative"
                                                key={image?.name}
                                            >
                                                <Image
                                                    src={URL.createObjectURL(image)}
                                                    sizes="300px"
                                                    alt={"img"}
                                                    loading="eager"
                                                    priority
                                                    fill
                                                    className="object-cover dark:bg-thNewtral bg-light-thNewtral"
                                                />
                                                <div
                                                    className={`${styles.call_btn} absolute top-1 right-1 rounded-full !p-1`}
                                                    onClick={() => handleDeleteimg(index)}
                                                >
                                                    <IoMdClose
                                                        size={24}
                                                        className="opacity-40 hover:opacity-100"
                                                    />
                                                </div>
                                            </div>
                                        ))}
                                    </div>
                                </div>
                            )}
                            <div className="flex gap-x-4 items-center">
                                <div className=" flex-col items-center justify-between w-full">
                                    {messageReply && Object.keys(messageReply).length > 0 && (
                                        <div className="py-2 bg-gray-600 text-white rounded-md mb-2 flex justify-between">
                                            <div className="flex-col gap-2 w-full overflow-x-auto ml-2 border-l-blue-200 border-l-4 pl-2">
                                                <div className="flex">
                                                    <BsReplyAllFill size={20} />{" "}
                                                    <span>
                                                        Trả lời{" "}
                                                        <b>{`${messageReply?.createdBy.username || ""}`}</b>
                                                    </span>
                                                </div>
                                                {messageReply.type === MESSAGE_TYPE.STICKER ? (
                                                    <Image
                                                        alt={messageReply.content}
                                                        src={messageReply.content}
                                                        className="w-[40px] h-[40px]"
                                                        width={40}
                                                        height={40}
                                                    />
                                                ) : messageReply.type === MESSAGE_TYPE.IMAGE ? (
                                                    <div className={`flex gap-1 justify-start`}>
                                                        {JSON.parse(messageReply.content).map(
                                                            (item: string) => {
                                                                return (
                                                                    <Image
                                                                        key={item}
                                                                        alt={item}
                                                                        src={item}
                                                                        className={`object-fill ${JSON.parse(messageReply.content).length >=
                                                                            2
                                                                            ? "w-[50px] h-[50px]"
                                                                            : "w-[50px] h-[50px]"
                                                                        }`}
                                                                        width={
                                                                            JSON.parse(messageReply.content).length >=
                                                                                2
                                                                                ? 70
                                                                                : 50
                                                                        }
                                                                        height={
                                                                            JSON.parse(messageReply.content).length >=
                                                                                2
                                                                                ? 70
                                                                                : 50
                                                                        }
                                                                    />
                                                                );
                                                            }
                                                        )}
                                                    </div>
                                                ) : messageReply.type === MESSAGE_TYPE.TEXT ? (
                                                    <span className="text-[.9375rem]">
                                                        {messageReply.content}
                                                    </span>
                                                ) : messageReply.type === MESSAGE_TYPE.MEDIA ? (
                                                    <video
                                                        src={messageReply.content}
                                                        width={100}
                                                        height={100}
                                                        className="w-[100px] h-auto"
                                                    ></video>
                                                ) : messageReply.type === MESSAGE_TYPE.LINK ? (
                                                    <span className="flex items-center text-blue-600 underline">
                                                        <BsLink45Deg size={30} /> {messageReply.content}
                                                    </span>
                                                ) : (
                                                    <span className="flex items-center">
                                                        {/* eslint-disable-next-line @next/next/no-img-element */}
                                                        <img
                                                            src={getFileIcon(
                                                                messageReply.content.split(".").pop() as string
                                                            )}
                                                            alt="icon"
                                                            width={50}
                                                            height={50}
                                                        />
                                                        <p className="ml-2">
                                                            {getFileName(messageReply.content)}
                                                        </p>
                                                    </span>
                                                )}
                                            </div>
                                            <div className="flex items-center ">
                                                <p className="mb-2 text-sm"> </p>
                                                <div
                                                    className="cursor-pointer px-2"
                                                    onClick={() => setMessageReply({})}
                                                >
                                                    <IoIosCloseCircleOutline size={24} />
                                                </div>
                                            </div>
                                        </div>
                                    )}
                                    <div
                                        className={`${selectedImage.length > 0
                                            ? "bg-thNewtral2"
                                            : "bg-thNewtral1"
                                        } w-full py-3 px-2 flex gap-x-4 justify-between items-center rounded-2xl`}
                                    >
                                        <input
                                            type="text"
                                            ref={inputRef}
                                            className="px-4 outline-none border-none w-full bg-transparent"
                                            placeholder="nhập gì đó..."
                                            onFocus={() => setIsFocused(true)}
                                            onBlur={() => setIsFocused(false)}
                                            onKeyDown={handleKeyDown}
                                        />
                                        <ul
                                            className={`flex items-center gap-x-3 opacity-60 ${styles.input_feature}`}
                                        >
                                            <li onClick={() => setIsOpenEmoji(!isOpenEmoji)}>
                                                <AiOutlineSmile size={20} />
                                            </li>
                                            <li className="cursor-pointer">
                                                <label htmlFor="image-input" className="cursor-pointer">
                                                    <input
                                                        id="image-input"
                                                        type="file"
                                                        accept="image/*"
                                                        className="hidden"
                                                        onChange={handleImageChange}
                                                    />
                                                    <BsImage size={20} className="cursor-pointer" />
                                                </label>
                                            </li>
                                            <li>
                                                <BsFileEarmarkText size={20} />
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div>
                                    {isFocused ? (
                                        <div onClick={handleSubmit}>
                                            <Image
                                                src={Send}
                                                sizes="(max-width: 40px) 40px, 40px"
                                                alt={"avt"}
                                                onError={handleImageError}
                                                loading="eager"
                                                priority
                                                width={40}
                                                height={40}
                                                className="rounded-full h-10 cursor-pointer"
                                            />
                                        </div>
                                    ) : (
                                        <div onClick={handleSlute}>
                                            <Image
                                                src={Slute}
                                                sizes="(max-width: 40px) 40px, 40px"
                                                alt={"avt"}
                                                onError={handleImageError}
                                                loading="eager"
                                                priority
                                                width={40}
                                                height={40}
                                                className="rounded-full h-10 cursor-pointer"
                                            />
                                        </div>
                                    )}
                                </div>
                            </div>
                        </div>
                    ) : (
                        <div className="request_area mt-auto py-4 dark:bg-thNewtral1 bg-light-thNewtral1 flex flex-col justify-between items-center gap-3">
                            <div className="flex justify-around items-center w-[60%]">
                                <button
                                    className="text-lg font-semibold text-thPrimary hover:brightness-150"
                                    onClick={() => setIsAccept(true)}
                                >
                                    Accept
                                </button>
                                <button className="text-lg font-semibold hover:brightness-150">
                                    Decline
                                </button>
                                <button className="text-lg font-semibold text-thRed hover:brightness-150">
                                    Block
                                </button>
                            </div>
                            <div className="opacity-60 text-sm">
                                <p>
                                    Accept the request to chat or decline this user if they harass
                                    you
                                </p>
                            </div>
                        </div>
                    )}
                </div>
                {isOpenInfo && (
                    <InfoChat
                        idConversation={idConversation}
                        isOpenInfo={isOpenInfo}
                        setIsOpenInfo={setIsOpenInfo}
                        setIdMediaModal={setIdMediaModal}
                        setUrlMediaModal={setUrlMediaModal}
                        setIsOpenMediaLibrary={setIsOpenMediaLibrary}
                        heightVirtualList={heightVirtualList}
                    />
                )}
            </div>
        </div>
    );
}
