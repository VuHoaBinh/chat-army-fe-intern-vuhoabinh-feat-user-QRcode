"use client";
import React, { ReactElement, useState, useEffect, cache, use } from "react";
import LayoutBox from "../../../../(chat)/chat/components/LayoutBox";
import ProfileCard from "./components/ProfileCard";
import PersonalInfo from "./components/PersonalInfo";
// import PersonalBio from "./components/PersonalBio";
import { IUserProfile } from "@/src/types/User";
import axiosClient from "@/src/utils/axios/axiosClient";
import { useRouter } from "next/navigation";
import { FiChevronLeft } from "react-icons/fi";

const fetchProfile = cache(() =>
    axiosClient().get(`${process.env.NEXT_PUBLIC_DAK_API}/user`)
);

const Profile = ({ params }: { params: { id: string } }): ReactElement => {
    // Sample Data
    const sampleData: IUserProfile = {
        id: params.id,
        mode: "dark",
        language: "vi",
        background_img: "",
        last_login: 1697510596176,
        name: "Name",
        active: 1,
        bio: "Sample bio text",
        email: "email@gmail.com",
        birthday: "2001-06-16",
        gender: 1,
        avatar: "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg",
        username: "User name"
    };

    const router = useRouter();
    const data: any = use(fetchProfile());
    // const [profileUser, setProfileUser] = useState(data.data);
    const profileUser = data.data.data;
    const [userInfo, setUserInfo] = useState(sampleData); // giá trị cố định
    const [infoValue, setInfoValue] = useState(sampleData);

    // Phần này để cập nhật những thông tin từ fetchProfile vào sample data
    useEffect(() => {
        if (profileUser) {
            const updatedData: IUserProfile = {
                ...sampleData,
                username: profileUser.username,
                name: profileUser.name,
                background_img: profileUser.background_img,
                avatar: profileUser.avatar,
                email: profileUser.email
            };
            // setProfileUser(sampleData)
            setUserInfo(updatedData);
            setInfoValue(updatedData);
        }
    }, [profileUser]);

    useEffect(() => {
        setInfoValue(userInfo);
    }, [userInfo]);

    const handleDataChange = () => {
        interface IDataToUpdate {
            [key: string]: any;
        }
        const data: IDataToUpdate = {};

        if (infoValue.name !== profileUser.name) {
            data.name = infoValue.name;
        }

        if (infoValue.email !== profileUser.email) {
            data.email = infoValue.email;
        }

        if (infoValue.username !== profileUser.username) {
            data.username = infoValue.username;
        }

        if (infoValue.avatar !== profileUser.avatar) {
            data.avatar = infoValue.avatar;
        }

        return data;
    };

    const handleSaveProfile = async () => {
        const dataToUpdate = handleDataChange();
        if (Object.keys(dataToUpdate).length == 0) {
            alert("Nothing changed");
            return;
        }
        try {
            const result = await axiosClient().put(
                `${process.env.NEXT_PUBLIC_DAK_API}/user`,
                dataToUpdate
            );
            if (result) {
                alert("Update successful");
                setUserInfo((prevInfo) => ({
                    ...prevInfo,
                    ...dataToUpdate
                }));
                setInfoValue((prevValue) => ({
                    ...prevValue,
                    ...dataToUpdate
                }));
            }
        } catch (err: any) {
            alert(err.response.data.error[0].message);
        }
    };

    return (
        <>
            <div className="pl-4 py-2 flex items-center lg:hidden">
                <div
                    className="relative cursor-pointer"
                    onClick={() => router.push("/chat")}
                >
                    <FiChevronLeft size={40} />
                </div>
                <h1 className="font-bold text-2xl">Profile</h1>
            </div>

            <div className="grid grid-cols-1  text-white rounded-2xl p-4 gap-8 text-sm lg:text-base">
                <div className="flex flex-1 flex-col gap-8">
                    <LayoutBox>
                        <ProfileCard
                            userInfo={userInfo}
                            setUserInfo={setUserInfo}
                            handleSave={handleSaveProfile}
                        />
                    </LayoutBox>
                    <LayoutBox>
                        <PersonalInfo userInfo={infoValue} setUserInfo={setInfoValue} />
                    </LayoutBox>
                </div>
                {/* Giao diện của Tiểu sử */}
                {/* <div className="flex flex-1 flex-col gap-8">
                <LayoutBox>
                    <PersonalBio userInfo={infoValue} setUserInfo={setInfoValue} />
                </LayoutBox>
            </div> */}
            </div>
        </>
    );
};

export default Profile;
