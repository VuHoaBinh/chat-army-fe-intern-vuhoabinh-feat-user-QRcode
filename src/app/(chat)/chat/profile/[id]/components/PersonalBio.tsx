"use client";
import { IUserProfile } from '@/src/types/User';
import React, { ReactElement, ChangeEvent } from 'react';

interface ProfileCardProps {
    userInfo: IUserProfile;
    setUserInfo: (userInfo: IUserProfile) => void;
}

const PersonalBio = ({ userInfo, setUserInfo }: ProfileCardProps): ReactElement => {
    const handleChangeBio = (e: ChangeEvent<HTMLTextAreaElement>) => {
        setUserInfo({
            ...userInfo,
            bio: e.target.value
        });
    };

    return (
        <div className='px-[16px] py-[24px] flex flex-col gap-2'>
            <span>Tiểu sử</span>
            <textarea
                onChange={(e) => handleChangeBio(e)}
                value={userInfo.bio}
                maxLength={250}
                className='appearance-none outline-none dark:bg-thNewtral bg-light-thNewtral p-4 rounded-lg h-44 resize-none'
            />
        </div>
    );
};

export default PersonalBio;