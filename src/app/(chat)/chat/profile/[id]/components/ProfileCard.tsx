"use client";
import React, { ReactElement, SyntheticEvent, useEffect, useState } from "react";
import Image from "next/image";
import EditIcon from "@/public/edit.svg";
import LoadingIcon from "@/public/loader.svg";
import ProfileButton from "./ProfileButton";
import { IUserProfile } from "@/src/types/User";
import axiosClient from "@/src/utils/axios/axiosClient";
import { uploadFile } from "@/src/utils/upload/getUploadToken";
import { useAppDispatch } from "@/src/redux/hook";
import { updateAvatar } from "@/src/redux/slices/userSlice";
import ModalResizeAvatar from "./Modal/ModalResizeAvatar";
import ModalResizeBanner from "./Modal/ModalResizeBanner";
import userAvatar from "@/public/user.png";
interface ProfileCardProps {
    userInfo: IUserProfile;
    setUserInfo: (userInfo: IUserProfile) => void;
    handleSave: () => void;
}

// const getUserFollower = cache((id: string) => axios.get(`/api/follower?id=${id}`));
// const getUserFollowing = cache((id: string) => axios.get(`/api/following?id=${id}`));
// const updateBackground = cache((file: any) => axiosClient.post(`/api/upload`, file, {
//     headers: { "Content-Type": "multipart/form-data" }
// }));

// const updateBackground = cache((file: any) =>
//   axiosClient.post(`${process.env.NEXT_PUBLIC_DAK_API}/upload`,file)
// );
// const updateProfile = cache((body: IUserProfile) => axios.put(`/api/profile/`, body));

const ProfileCard = ({
    userInfo,
    setUserInfo,
    handleSave
}: ProfileCardProps): ReactElement => {
    // const ProfileCard = ({ userInfo, handleSave }: ProfileCardProps): ReactElement => {
    const dispatch = useAppDispatch();

    const [loadingBackground, setLoadingBackground] = useState(false);
    const [isResizeAvatar, setIsResizeAvatar] = useState(false);
    const [selectedAvatar, setSelectedAvatar] = useState<File | null>(null);
    const [isResizeBanner, setIsResizeBanner] = useState(false);
    const [selectedBanner, setSelectedBanner] = useState<File | null>(null);

    // const follower_res = use(getUserFollower(userInfo.id));
    // const following_res = use(getUserFollowing(userInfo.id));
    // const follower_res = userInfo.id;
    // const following_res = userInfo.id;

    // const followers = follower_res.data.data;
    // const followings = following_res.data.data;
    // const followers = userInfo.id;
    // const followings = userInfo.id;

    const handleDeleteAccount = (): void => {
        ("delete account");
    };
    const handleBannerResize = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (!event.target.files) return;
        const file: File = event.target.files[0];
        setSelectedBanner(file);
        setIsResizeBanner(true);
        event.target.value = "";
    };
    useEffect(() => {
        if (isResizeBanner === false && selectedBanner) {
            handleBannerChange(selectedBanner);
        }
    }, [isResizeBanner]);
    const handleBannerChange = async (file: File) => {
        const fileData = new FormData();
        fileData.append("files", file);

        if (file) {
            try {
                setLoadingBackground(true);
                const data = await uploadFile(fileData);
                const linkImageBackground = data[0].link;
                if (linkImageBackground) {
                    try {
                        const result = await axiosClient().put(
                            `${process.env.NEXT_PUBLIC_DAK_API}/user`,
                            { background_img: linkImageBackground }
                        );
                        if (result) {
                            alert("Update background successful");
                            setUserInfo({
                                ...userInfo,
                                background_img: linkImageBackground
                            });
                        }
                    } catch (err: any) {
                        alert(err.response.data.error[0].message);
                    }
                }
            } catch (error) {
                console.log(error);
            } finally {
                setLoadingBackground(false);
            }
        }
    };
    const handleAvatarResize = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (!event.target.files) return;
        const file: File = event.target.files[0];
        setSelectedAvatar(file);
        setIsResizeAvatar(true);
        event.target.value = "";
    };
    useEffect(() => {
        if (isResizeAvatar === false && selectedAvatar) {
            handleAvatarChange(selectedAvatar);
        }
    }, [isResizeAvatar]);
    const handleAvatarChange = async (file: File) => {
        const fileData = new FormData();
        fileData.append("files", file);

        if (file) {
            try {
                const data = await uploadFile(fileData);
                const linkImageAvatar = data[0].link;
                if (linkImageAvatar) {
                    try {
                        const result = await axiosClient().put(
                            `${process.env.NEXT_PUBLIC_DAK_API}/user`,
                            { avatar: linkImageAvatar }
                        );
                        if (result) {
                            alert("Update avatar successful");
                            setUserInfo({
                                ...userInfo,
                                avatar: linkImageAvatar
                            });
                            dispatch(updateAvatar(linkImageAvatar));
                        }
                    } catch (err: any) {
                        alert(err.response.data.error[0].message);
                    }
                }
            } catch (error) {
                console.log(error);
            }
        }
    };
    const handleImageError = (event: SyntheticEvent<HTMLImageElement, Event>) => {

        const imgElement = event.target as HTMLImageElement;
        imgElement.src = userAvatar.src;
    };
    return (
        <div className="flex flex-col">
            {/* Background container*/}
            <div className="relative h-48  overflow-hidden rounded-t-2xl">
                <Image
                    src={userInfo.background_img}
                    alt="wallpaper"
                    fill
                    style={{ objectFit: "cover" }}
                />
                <label
                    className="absolute top-3 right-3 rounded-full p-2 bg-[#22242699] cursor-pointer"
                    htmlFor="backgroundInput"
                >
                    <Image
                        src={loadingBackground ? LoadingIcon : EditIcon}
                        alt="edit background image"
                        width={24}
                    />
                    <input
                        id="backgroundInput"
                        type="file"
                        accept="image/*"
                        style={{ display: "none" }}
                        onChange={handleBannerResize}
                    />
                </label>
            </div>
            {/* Avatar and information container */}
            <div className="px-4 pb-7 relative">
                {/* Avatar and follow */}
                <div
                    className="
                flex 
                justify-center
                mt-20
                mb-5
                flex-col
                xl:mt-8
                xl:justify-end     
            "
                >
                    <div
                        className="
                    absolute 
                    top-0
                    xl:left-4 
                    border-8 
                    border-thNewtral1
                    w-36 
                    h-36 
                    overflow-hidden 
                    rounded-full 
                    -translate-y-1/2
                "
                    >
                        <label htmlFor="avatarInput">
                            <Image
                                src={userInfo.avatar}
                                alt="avatar"
                                onError={handleImageError}
                                fill
                                style={{ objectFit: "cover" }}
                            />
                            <input
                                id="avatarInput"
                                type="file"
                                accept="image/*"
                                style={{ display: "none" }}
                                onChange={handleAvatarResize}
                            />
                        </label>
                    </div>
                    {/* Giao diện follower/ing */}
                    <div className="flex flex-col gap-2 xl:flex-row xl:pt-9">
                        {/* <div className="flex gap-2">
                            <span className="font-semibold">{followers.length}</span>
                            <span>Người theo dõi</span>
                        </div>
                        <div className="flex gap-2">
                            <span className="font-semibold">{followings.length}</span>
                            <span>Đang theo dõi</span>
                        </div> */}
                    </div>
                </div>
                {isResizeAvatar && (
                    <ModalResizeAvatar
                        selectedAvatar={selectedAvatar}
                        setSelectedAvatar={setSelectedAvatar}
                        setIsResizeAvatar={setIsResizeAvatar}
                    />
                )}
                {isResizeBanner && (
                    <ModalResizeBanner
                        selectedBanner={selectedBanner}
                        setSelectedBanner={setSelectedBanner}
                        setIsResizeBanner={setIsResizeBanner}
                    />
                )}

                {/* User info and button */}
                <div>
                    <div className="mb-4">
                        <h1 className="font-semibold">{userInfo.name}</h1>
                        {/* Giao diện Bio */}
                        {/* <span>{userInfo.bio}</span> */}
                    </div>
                    <div className="flex flex-col justify-start items-start gap-4 md:flex-row md:items-center">
                        <ProfileButton type="danger" onClick={() => handleDeleteAccount()}>
                            <span>Xóa tài khoản</span>
                        </ProfileButton>
                        <ProfileButton type="primary" onClick={handleSave}>
                            <span>Lưu</span>
                        </ProfileButton>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ProfileCard;
