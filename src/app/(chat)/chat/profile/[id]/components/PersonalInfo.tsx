"use client";
import React, { ReactElement, ChangeEvent } from 'react';
import ProfileInput from "./ProfileInput";
// import ProfileRadioGroup from "./ProfileRadioGroup";
import { IUserProfile } from '@/src/types/User';

interface ProfileCardProps {
    userInfo: IUserProfile;
    setUserInfo: (userInfo: IUserProfile) => void;
}

const PersonalInfo = ({ userInfo, setUserInfo }: ProfileCardProps): ReactElement => {
    const handleChangeFullname = (e: ChangeEvent<HTMLInputElement>) => {
        setUserInfo({
            ...userInfo,
            name: e.target.value
        });
    };

    const handleChangeEmail = (e: ChangeEvent<HTMLInputElement>) => {
        setUserInfo({
            ...userInfo,
            email: e.target.value
        });
    };

    // const handleChangeBirthday = (e: ChangeEvent<HTMLInputElement>) => {    
    //     setUserInfo({
    //         ...userInfo,
    //         birthday: e.target.value
    //     });
    // };

    // const handleChangeGender = (selectedIndex: number) => {
    //     setUserInfo({
    //         ...userInfo,
    //         gender: selectedIndex
    //     });
    // };

    return (
        <div className='px-[16px] py-[24px]'>
            <h2 className='font-semibold mb-3'>Thông tin cá nhân</h2>
            <div className="flex flex-col gap-3">
                <ProfileInput title="Họ tên" type="text" maxLength={50} value={userInfo.name} onChange={(e) => handleChangeFullname(e)} />
                <ProfileInput title="Email" type="email" maxLength={50} value={userInfo.email} disabled={true} onChange={(e) => handleChangeEmail(e)} />
                {/* <ProfileInput title="Ngày sinh" type="date" value={userInfo.birthday} onChange={(e) => handleChangeBirthday(e)} />
                <ProfileRadioGroup 
                    labelText="Giới tính"
                    options={[
                        {
                            label: 'Nam',
                            value: 1
                        },
                        {
                            label: 'Nữ',
                            value: 2
                        }
                    ]}
                    value={userInfo.gender}
                    onChange={handleChangeGender}
                /> */}
            </div>
        </div>
    );
};

export default PersonalInfo;