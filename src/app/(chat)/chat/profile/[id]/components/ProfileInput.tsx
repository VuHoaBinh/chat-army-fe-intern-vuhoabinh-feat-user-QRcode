import React, { ChangeEvent, ReactElement } from 'react';
interface InputProps {
    title: string;
    type: string;
    maxLength?: number;
    value?: any;
    disabled?: boolean;
    onChange?: (e: ChangeEvent<HTMLInputElement>) => void;
}

const ProfileInput = ({ title, type, maxLength, value, disabled, onChange }: InputProps): ReactElement => {
    return (
        <div className='flex flex-col gap-2'>
            <span>{title}</span>
            <input
                value={value}
                onChange={onChange}
                type={type}
                maxLength={maxLength}
                disabled={disabled}
                className={`${type === 'date' && 'calendar-white'} appearance-none outline-none dark:bg-thNewtral bg-light-thNewtral p-3 rounded-lg`}
            />
        </div>
    );
};

export default ProfileInput;