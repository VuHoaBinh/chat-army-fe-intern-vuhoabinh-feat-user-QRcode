import React, { ReactElement } from 'react';

interface ButtonProps {
    children: ReactElement;
    type: 'danger' | 'primary';
    onClick?: () => void;
}

const ProfileButton = ({ children, type, onClick }: ButtonProps): ReactElement => {
    const renderStyleByType = (type: 'danger' | 'primary') => {
        switch (type) {
        case 'danger':
            return 'bg-[transparent] border-thRed text-thRed hover:bg-thRed hover:text-white transition ease-in';
        case 'primary':
            return ' dark:bg-thPrimary bg-light-thPrimary border-thPrimary text-thNewtral1';

        default:
            return ' dark:bg-thPrimary bg-light-thPrimary border-thPrimary text-thNewtral1';
        }
    };

    return (
        <div
            className={`px-3 py-2 border-2 rounded-md font-semibold cursor-pointer ${renderStyleByType(type)}`}
            onClick={onClick}
        >
            {children}
        </div>
    );
};

export default ProfileButton;