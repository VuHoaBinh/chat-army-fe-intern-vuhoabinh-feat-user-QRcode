import {
    ReactElement,
    useCallback,
    useEffect,
    useMemo,
    useState
} from "react";
import Cropper from "react-easy-crop";
import getCroppedImg from "../cropImage";

interface IModalResizeAvatar {
    selectedAvatar: any;
    setSelectedAvatar: (selectedAvatar: File | null) => void;
    setIsResizeAvatar: (isResizeAvatar: boolean) => void;
}

const ModalResizeAvatar = ({
    selectedAvatar,
    setSelectedAvatar,
    setIsResizeAvatar
}: IModalResizeAvatar): ReactElement => {
    const [crop, setCrop] = useState({ x: 0, y: 0 });
    const [zoom, setZoom] = useState(1);
    const [croppedAreaPixels, setCroppedAreaPixels] = useState(null);
    const [fileName, setFileName] = useState("");


    const onCropComplete = useCallback(
        (croppedArea: any, croppedAreaPixels: any) => {
            setCroppedAreaPixels(croppedAreaPixels);
        },
        []
    );

    useEffect(() => {
        setFileName(selectedAvatar.name);
    }, []);

    const avaImage = useMemo(() => {
        const blob = selectedAvatar as Blob;
        return URL.createObjectURL(blob);
    }, [selectedAvatar]);

    // const avaImage =
    // 'https://img.huffingtonpost.com/asset/5ab4d4ac2000007d06eb2c56.jpeg?cache=sih0jwle4e&ops=1910_1000'



    const updateCroppedImage = useCallback(async () => {
        try {
            const croppedImage = (await getCroppedImg(
                avaImage,
                croppedAreaPixels
            )) as Blob;
            // setCroppedImage(URL.createObjectURL(croppedImage));

            setSelectedAvatar(
                new File([croppedImage], fileName, { type: "image/jpeg" })
            );
            setIsResizeAvatar(false);
        } catch (e) {
            console.error(e);
        }
    }, [croppedAreaPixels]);

    return (
        <div className="w-screen h-screen bg-thWhite bg-opacity-30 lg:bg-opacity-20 fixed top-0 left-0 ">
            <div
                className="relative left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2 p-3 dark:bg-thNewtral bg-light-thNewtral
          rounded-lg w-[90vw] md:w-[500px] h-[50vh] md:h-[80vh] min-h-[550px] lg:min-h-[600px] overflow-y-auto"
            >
                <div className="mb-2 text-2xl">Cập nhật ảnh đại diện</div>
                <hr className="bg-black dark:bg-white h-[1px] w-full" />
                <div className="absolute top-[120px] left-0 right-0 w-full h-[300px]">
                    <Cropper
                        image={avaImage}
                        crop={crop}
                        zoom={zoom}
                        aspect={3 / 3}
                        onCropChange={setCrop}
                        onCropComplete={onCropComplete}
                        onZoomChange={setZoom}
                        cropShape="round"
                    />
                </div>
                <div className="absolute top-[450px] left-1/2 -translate-x-1/2 h-[30px] w-4/5">
                    <input
                        type="range"
                        value={zoom}
                        min={1}
                        max={3}
                        step={0.1}
                        aria-labelledby="Zoom"
                        onChange={(e) => {
                            setZoom(parseFloat(e.target.value));
                        }}
                        className="w-full h-3 dark: bg-thNewtral2 bg-light-thNewtral2  md:bg-thPrimaryHover   md:active: dark:bg-thPrimary bg-light-thPrimary rounded-lg appearance-none cursor-pointer accent-thPrimary md:accent-thNewtral2"
                    />
                </div>
                <div className="absolute bottom-[0px] right-0 mr-3 mb-3">
                    <button
                        className="bg-gray-400 p-3 rounded-md mr-3"
                        onClick={() => {
                            setSelectedAvatar(null);
                            setIsResizeAvatar(false);
                        }}
                    >
                        Hủy
                    </button>
                    <button
                        className=" dark:bg-thPrimary bg-light-thPrimary p-3 rounded-md hover:opacity-75 text-thNewtral1"
                        onClick={updateCroppedImage}
                    >
                        Cập nhật
                    </button>
                </div>
            </div>
        </div>
    );
};

export default ModalResizeAvatar;
