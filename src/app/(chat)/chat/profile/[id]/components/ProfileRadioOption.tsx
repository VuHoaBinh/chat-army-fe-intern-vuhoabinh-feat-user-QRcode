import React, { ReactNode, ReactElement } from 'react';

interface RadioOptionProps {
    index: any,
    selectedIndex?: number,
    onSelect: (index: number) => void;
    children: ReactNode
}

const ProfileRadioOption = ({ index, selectedIndex, onSelect, children }: RadioOptionProps): ReactElement => {
    const isSelected = index === selectedIndex;
    return (
        <div
            className='flex items-center cursor-pointer'
            onClick={() => onSelect(index)}>
            <div className={`flex items-center justify-center rounded-full w-6 h-6 border border-thNewtral2 transition mr-2 ${isSelected && 'border-thPrimary'}`}>
                {isSelected &&
                    <div className='rounded-full w-3 h-3  dark:bg-thPrimary bg-light-thPrimary'></div>
                }
            </div>
            {children}
        </div>
    );
};

export default ProfileRadioOption;