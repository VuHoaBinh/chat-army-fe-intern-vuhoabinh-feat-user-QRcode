"use client";
import { useAppSelector } from "@/src/redux/hook";
import { getLoginCookies } from "@/src/utils/auth/handleCookies";
import dynamic from "next/dynamic";
import { redirect } from "next/navigation";
import React, { ReactElement, Suspense, useEffect, useState } from "react";
import { useCookies } from "react-cookie";
import { IoIosArrowBack } from "react-icons/io";
import useWindowDimensions from "../../../hook/useWindowDimension";
import "../../globals.css";
import Header from "./components/Header";
import Call from "./conversation/[id]/components/Call";
import CallMbl from "./conversation/[id]/components/CallMbl";
const LeftBar = dynamic(() => import("./components/LeftBar"));
import { addListIdToListIdConversationDecoded } from "@/src/redux/slices/endToEndEncryptionSlice";
import { useAppDispatch } from "@/src/redux/hook";

export default function RootLayout({
    children
}: {
    children: React.ReactNode;
}): ReactElement {
    const { width } = useWindowDimensions();
    const cookies = getLoginCookies();
    const [typeOfCalling, setTypeOfCalling] = useState<number>();
    const [cookiesCalling] = useCookies(["data"]);
    const dispatch = useAppDispatch();
    useEffect(() => {
        if (cookiesCalling && cookiesCalling.data) {

            setTypeOfCalling(cookiesCalling.data.type);
        }
    }, [cookiesCalling]);
    //Kiếm tra nếu cookies được trả về
    if (!cookies && width > 0) {
        localStorage.removeItem("listEndToEndEncryptionConversation");
        dispatch(addListIdToListIdConversationDecoded([]));
        redirect("/login");
    }
    // State để bật tắt UI Calling của Mobile
    const [isCalling, setIsCalling] = useState<boolean>(true);
    const [isDragVideoHidden, setIsDragVideoHidden] = useState<boolean>(false);
    const user = useAppSelector((state) => state.user.user);
    const call = useAppSelector((state) => state.user.call);

    return (
        <main className="relative">

            <Header />

            {width > 768 ? (
                <Suspense fallback={<div>Loading...</div>}>
                    <div className="flex items-center">
                        <LeftBar />
                        <div className="bg-light-thNewtral dark:bg-thNewtral mt-4 mr-4 w-full h-[90dvh] lg:h-[85dvh] rounded-2xl relative overflow-hidden overflow-y-auto">
                            {user.is_calling && call.userCreatCall == user.id ? (
                                <div className="h-full w-full absolute flex">
                                    <Call />
                                    <div className="lg:w-[32%] md:w-0 flex-shrink-0"></div>
                                </div>
                            ) : user.is_calling && call.userAcceptCall == user.id && call.deviceAcceptCall == "desktop" ? (<div className="h-full w-full absolute flex">
                                <Call />
                                <div className="lg:w-[32%] md:w-0 flex-shrink-0"></div>
                            </div>) : null}
                            {children}
                        </div>
                    </div>
                </Suspense>
            ) : (
                <Suspense fallback={<div>Loading...</div>}>

                    <div className="h-full">

                        {isDragVideoHidden && (
                            <div
                                className="absolute top-1/3 right-0 bg-light-thNewtral dark:bg-thNewtral h-24 w-6 rounded-l-xl flex	z-50"
                                onClick={() => {
                                    setIsDragVideoHidden(false);
                                }}
                            >
                                <IoIosArrowBack size={40} className="m-auto" />
                            </div>
                        )}

                        {user.is_calling && (call.deviceCall == "smartphone" || call.deviceAcceptCall == "smartphone") && (
                            <p
                                className={`w-full  dark:bg-thPrimary bg-light-thPrimary py-1 text-thDark-background text-center first-letterfirst-line
                            ${typeOfCalling === 2 ? "hidden" : "block"}`}
                                onClick={() => setIsCalling(true)}
                            >
                                Cuộc gọi đang diễn ra quay lại cuộc gọi...
                            </p>
                        )}
                        {/* {call?.userAcceptCall && call.deviceAcceptCall == "smartphone" && (
                            <CallMbl
                                isCalling={isCalling}
                                setIsCalling={setIsCalling}
                                isDragVideoHidden={isDragVideoHidden}
                                setIsDragVideoHidden={setIsDragVideoHidden}
                            />
                        )} */}
                        {user.is_calling && user.id == call.userCreatCall ? (

                            <CallMbl
                                isCalling={isCalling}
                                setIsCalling={setIsCalling}
                                isDragVideoHidden={isDragVideoHidden}
                                setIsDragVideoHidden={setIsDragVideoHidden}
                            />
                        ) : call.userAcceptCall == user.id && call.deviceAcceptCall == "smartphone" ? (
                            <>

                                <CallMbl
                                    isCalling={isCalling}
                                    setIsCalling={setIsCalling}
                                    isDragVideoHidden={isDragVideoHidden}
                                    setIsDragVideoHidden={setIsDragVideoHidden}
                                />
                            </>

                        ) : null}


                        {children}
                    </div>
                </Suspense>
            )}
        </main>
    );
}
