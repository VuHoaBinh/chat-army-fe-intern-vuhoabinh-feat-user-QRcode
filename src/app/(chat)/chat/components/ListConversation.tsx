import { CONVERSATION, EVENT_NAMES, MESSAGE_TYPE } from "@/src/constants/chat";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { addListIdToListIdConversationTurnOffNotify } from "@/src/redux/slices/NotifyOfConversationSlice";
import {
    setLastMessage,
    setListConversations,
    setListHiddenConversation,
    setOtherUser,
    setTotalUnseenMessage
} from "@/src/redux/slices/conversationSlice";
import { IConversation } from "@/src/types/Conversation";
import { IMessage } from "@/src/types/Message";
import { ISeenUser } from "@/src/types/User";
import axiosClient from "@/src/utils/axios/axiosClient";
import {
    filterConversation,
    sortConversationByNewest
} from "@/src/utils/conversations/filterConversation";
import { Tooltip } from "antd";
import moment from "moment";
import Image from "next/image";
import Link from "next/link";
import { usePathname } from "next/navigation";
import {
    ReactElement,
    SyntheticEvent,
    cache,
    use,
    useCallback,
    useEffect,
    useLayoutEffect,
    useRef,
    useState
} from "react";
import { AiFillCheckCircle } from "react-icons/ai";
import { GoPin } from "react-icons/go";
import { MdOutlineGroupAdd, MdPersonAddAlt } from "react-icons/md";
import { Virtuoso } from "react-virtuoso";
import userAvatar from "@/public/user.png";
const fetchConversations = cache(() =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations?offset=0&status=[2]&limit=20&type=1`
    )
);
const fetchUnseenMessages = cache(() =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/unseen`
    )
);
interface Props {
    setUnSeenHiddenMessage: (unSeenHiddenMessage: number) => void;
    onOpenModal: (modalType: string) => void;
}
export default function ListConversation({
    setUnSeenHiddenMessage,
    onOpenModal
}: Props): ReactElement {
    const dispatch = useAppDispatch();
    const user = useAppSelector((state) => state.user.user);
    const notify = useAppSelector((state) => state.user.notify);
    const conversation = useAppSelector((state) => state.user.conversation);
    const pathname = usePathname();
    const fetchData = useCallback(async () => {
        const { data } = await fetchConversations();
        return filterConversation(data, user.id);
    }, [user.id]);
    useEffect(() => {
        fetchData().then((data) => {
            setListConversation(sortConversationByNewest(data));
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [user.id]);
    const [listConversation, setListConversation] = useState<IConversation[]>(
        sortConversationByNewest(
            filterConversation(conversation.listConversation, user.id)
        )
    );
    const localStorageData = useAppSelector((state: any) => state.user);
    const [dataStorage, setDataStorage] = useState<any>([{}] as any);
    useEffect(() => {
        const dataString = localStorage.getItem("Othername");

        if (dataString) {
            setDataStorage(JSON.parse(dataString));
        }
    }, [localStorageData]);
    // ("asdsadasda", dataStorage);
    const currentDateTime = moment();
    const { data: dataUnSeen }: { data: number } = use(fetchUnseenMessages());
    useEffect(() => {
        dispatch(setTotalUnseenMessage(dataUnSeen));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [dataUnSeen]);
    const [listConversationNotHidden, setListConversationNotHidden] = useState<IConversation[]>([]);
    // ("listConversationNotHidden", listConversationNotHidden);
    useEffect(() => {
        dispatch(setListConversations(listConversation));
        setListConversationNotHidden(listConversation);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [listConversation]);
    const lastMessageConversationRef = useRef<IMessage>(conversation.lastMessage);
    const totalUnseenMessageRef = useRef<number>(
        conversation.totalUnseenMessage || 0
    );
    useEffect(() => {
        (async () => {
            try {
                const lastMessageConversation = lastMessageConversationRef.current;
                if (
                    totalUnseenMessageRef.current ===
                    (conversation.totalUnseenMessage as number) &&
                    (!conversation.lastMessage ||
                        lastMessageConversation?._id === conversation.lastMessage?._id)
                )
                    return;
                const { data } = await fetchConversations();
                const { data: dataUnread } = await fetchUnseenMessages();
                setListConversation(
                    sortConversationByNewest(filterConversation(data, user.id))
                );
                dispatch(
                    setListConversations(
                        sortConversationByNewest(filterConversation(data, user.id))
                    )
                );
                setListConversationNotHidden(sortConversationByNewest(filterConversation(data, user.id)));
                dispatch(setTotalUnseenMessage(dataUnread));
            } catch (error) {
                console.error("Error fetching conversations:", error);
            }
        })();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [conversation.lastMessage, user.id, conversation.totalUnseenMessage]);
    useEffect(() => {
        if (!user.socket || pathname.includes("/chat/conversation")) return;
        user.socket.on(
            EVENT_NAMES.CONVERSATION.RECEIVE_MESSAGE,
            (msg: IMessage) => {
                dispatch(setLastMessage(msg));
            }
        );
    }, [dispatch, pathname, user.socket]);

    useEffect(() => {
        if (!user.socket) return;
        user.socket.on(EVENT_NAMES.CONVERSATION.SEENED_MESSAGE, (data: any) => {
            // ("seened", data);
            listConversation.map((conversation: any) => {
                if (
                    conversation._id === data.conversationId &&
                    data.userSeenId !== user.id
                ) {
                    const newUserSeen: ISeenUser = {
                        _id: data?.userSeenId,
                        username: data?.userSeenUsername,
                        avatar: data?.userSeenAvatar,
                        id: data?.userSeenId,
                        time: ""
                    };
                    const Conver =
                        conversation.latestMessage[conversation?.latestMessage?.length - 1];
                    const newSeen = Conver.seen.find(
                        (item: ISeenUser) => item._id === newUserSeen._id
                    )
                        ? Conver.seen
                        : [...Conver.seen, newUserSeen];
                    const newLastMessage = {
                        ...Conver,
                        seen: [...newSeen]
                    };

                    setListConversation((prev: any) => {
                        return prev.map((item: any) => {
                            if (item._id === conversation._id) {
                                return {
                                    ...item,
                                    latestMessage: [
                                        ...item.latestMessage.slice(
                                            0,
                                            item?.latestMessage?.length - 1
                                        ),
                                        newLastMessage
                                    ]
                                };
                            }
                            return item;
                        });
                    });
                }
            });
        });
        // return () => {
        //   // user.socket.off(EVENT_NAMES.CONVERSATION.SEENED_MESSAGE);
        // return () => {
        //   // user.socket.off(EVENT_NAMES.CONVERSATION.SEENED_MESSAGE);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [user.socket, conversation, user.id]);

    const divVirtualListRef = useRef<HTMLDivElement>(null);
    const [heightVirtualList, setHeightVirtualList] = useState<number>(0);
    // Tính height cho Virtuallist dựa vào div contain
    useLayoutEffect(() => {
        const handleWindowResize = () => {
            if (divVirtualListRef.current) {
                setHeightVirtualList(divVirtualListRef.current.clientHeight - 25);
            }
        };
        handleWindowResize();
        window.addEventListener("resize", handleWindowResize);
        return () => {
            window.removeEventListener("resize", handleWindowResize);
        };
    }, []);

    //   -----------Handle Hidden Conversation
    // Set listHidden
    useEffect(() => {
        if (conversation.listConversation) {
            // Filter list conversationHidden
            const conversationsWithHiddenFlag = conversation.listConversation.filter(
                (item: any) => item.isConversationHidden
            );
            dispatch(setListHiddenConversation(conversationsWithHiddenFlag));


        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [conversation.listConversation]);

    // Lấy tổng số lượng Tin nhắn Unseen của conversationHidden
    useEffect(() => {
        if (conversation.listHiddenConversation) {
            // Loại bỏ các conversationhidden
            if (conversation.listConversation) {
                const arrIdConversationHidden: string[] = conversation.listHiddenConversation.map(
                    (conversation) => conversation._id
                );
                const conversationsNotHidden = conversation.listConversation.filter(
                    (item: any) => item.type === 1 && !arrIdConversationHidden.includes(item._id)
                );
                setListConversationNotHidden(conversationsNotHidden);
            }

            const listHiddenConversation = conversation.listHiddenConversation.filter(
                (item) => item.type === 1
            );
            const totalUnseenMessages = listHiddenConversation.reduce(
                (unseenHiddenMessage, conversation) =>
                    unseenHiddenMessage + conversation.unSeenMessageTotal,
                0
            );
            setUnSeenHiddenMessage(totalUnseenMessages);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [conversation.listHiddenConversation]);

    const { isOpenModalTurnOffNotification, listIdConversationTurnOffNotify } = notify;
    const [listIdConversationTurnedOffNotifications, setListIdConversationTurnedOffNotifications] = useState<string[] | any>(listIdConversationTurnOffNotify);
    const [totalUnseenMessageInConversationTurnedOffNotifications, settotalUnseenMessageInConversationTurnedOffNotifications] = useState<number>(0);
    const [totalUnseenMessage2, setTotalUnseenMessage2] = useState<number>(conversation.totalUnseenMessage || 0);

    useEffect(() => {
        let total: number = 0;
        listConversation.forEach((c: any) => {
            total += c.unSeenMessageTotal as number;
        });
        setTotalUnseenMessage2(total);
    }, [conversation, listConversation, isOpenModalTurnOffNotification, listIdConversationTurnOffNotify]);
    useEffect(() => {
        let newTotalUnseen: number = 0;
        conversation.listConversation?.forEach((c: any) => {
            if (listIdConversationTurnedOffNotifications.includes(c.id)) {
                newTotalUnseen += c.unSeenMessageTotal;
            }
        });
        settotalUnseenMessageInConversationTurnedOffNotifications(newTotalUnseen);
    }, [conversation.listConversation, conversation.totalUnseenMessage, listIdConversationTurnedOffNotifications]);
    useEffect(() => {
        setListIdConversationTurnedOffNotifications(listIdConversationTurnOffNotify);
    }, [isOpenModalTurnOffNotification, listIdConversationTurnOffNotify, conversation.ListIdPinnedConversation]);

    useEffect(() => {
        const newListId: string[] | any = [];
        conversation.listConversation?.forEach((c: any) => {
            const menuSetting = c?.conversationSetting && c?.conversationSetting.find((st: any) => {
                return st.type === 1;
            });
            const turnOffTime = menuSetting && menuSetting.value;

            if (turnOffTime || turnOffTime === null) {
                newListId.push(c.id);
            }
        });
        if (newListId.length > 0) {
            dispatch(addListIdToListIdConversationTurnOffNotify(newListId));
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [conversation.listConversation, conversation.ListIdPinnedConversation]);

    useEffect(() => {
        // sau khi pin thì lấy conversation mới
        const getNewConversation = async () => {
            const { data } = await fetchConversations();
            setListConversation(filterConversation(data, user.id));
        };
        getNewConversation();
    }, [conversation.ListIdPinnedConversation, user.id]);
    const handleImageError = (event: SyntheticEvent<HTMLImageElement, Event>) => {

        const imgElement = event.target as HTMLImageElement;
        imgElement.src = userAvatar.src;
    };
    return (
        <div className="p-4 flex flex-col gap-y-6 flex-shrink-0 rounded-2xl border-2 bg-light-thNewtral dark:bg-thDark-background border-thNewtral1 h-[calc(90dvh-1.5rem)] lg:h-[calc(85dvh-1.5rem)] mx-4 overflow-y-auto">
            <div className="flex gap-x-1 items-center justify-between">
                <div className="flex gap-x-2 items-center">

                    <h1 className="text-xl font-semibold">Message</h1>
                    <p className="bg-thRed font-medium text-sm px-2 h-fit rounded grid place-items-center">
                        {(totalUnseenMessage2 && totalUnseenMessage2 - totalUnseenMessageInConversationTurnedOffNotifications) || 0}
                    </p>
                </div>
                <div className="flex gap-x-2 items-center">

                    <div
                        onClick={() => onOpenModal("addFriend")}
                        className="cursor-pointer"
                    >
                        <Tooltip title={"Thêm bạn"}  >
                            <MdPersonAddAlt size={24} />



                        </Tooltip>
                    </div>
                    <div
                        className="cursor-pointer"
                        onClick={() => onOpenModal("createGroup")}
                    >
                        <Tooltip title={"Tạo nhóm"}  >
                            <MdOutlineGroupAdd size={24} />
                        </Tooltip>
                    </div>
                </div>
            </div>
            <div className="flex-1" ref={divVirtualListRef}>
                <Virtuoso
                    data={listConversationNotHidden}
                    style={{ height: heightVirtualList, width: "330px" }}
                    itemContent={(index, c: IConversation | any) => {
                        const checkNewMsg =
                            conversation.lastMessage?.conversationId === c._id;
                        const userNewMessage =
                            conversation.lastMessage?.idOther === user.id
                                ? "Bạn: "
                                : `${conversation.lastMessage?.createdBy?.username}: `;
                        const userMessage =
                            c.latestMessage?.[c?.latestMessage?.length - 1]?.idOther
                                ?._id === user.id
                                ? "Bạn: "
                                : `${c?.latestMessage?.[c?.latestMessage?.length - 1]?.idOther
                                    ?.username
                                }`;

                        const targetDate = moment(
                            c?.latestMessage?.[c?.latestMessage?.length - 1]?.createdAt
                        );
                        // Tính khoảng thời gian giữa hiện tại và thời điểm cần kiểm tra
                        const duration = moment.duration(currentDateTime.diff(targetDate));
                        //   const checkDate = targetDate.isBefore(currentDateTime, "day");
                        // if(c.isConversationHidden)
                        //     {(c.isConversationHidden.pin)}
                        return (
                            c?.type === CONVERSATION.TYPE.INDIVIDUAL && (
                                <Link href={`/chat/conversation/${c._id}`} key={c._id}>
                                    <div
                                        onClick={() =>
                                            dispatch(
                                                setOtherUser({
                                                    otherUsername: c?.name,
                                                    otherAvatar: c?.avatar,
                                                    lastMessage: conversation.lastMessage || {}
                                                })
                                            )
                                        }
                                        className={`flex gap-x-2 items-center py-2 px-2 ${pathname.split("/").pop() === c.id ? "dark:bg-gray-600 bg-light-thPrimary" : ""
                                        } dark:hover:bg-thNewtral1 hover:bg-light-thNewtral1 rounded-lg`}
                                    >
                                        {
                                            c?.isPinned && (<div className="absolute top-0 -left-[1px]">
                                                <GoPin />
                                            </div>)
                                        }
                                        <Image
                                            // src={conversation?.avatar}
                                            src={
                                                c?.avatar === "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg"
                                                    ? "/default_avatar.jpg"
                                                    : c?.avatar
                                            }
                                            sizes="(max-width: 56px) 56px, 56px"
                                            alt={"Logo"}
                                            onError={handleImageError}
                                            loading="eager"
                                            priority
                                            width={56}
                                            height={56}
                                            className="rounded-full object-cover h-[56px]"
                                        />
                                        <div className="flex justify-between h-full items-center flex-grow">
                                            <div>
                                                <p className="text-[.9375rem] font-semibold lg:max-w-[185px] text-ellipsis line-clamp-1">
                                                    {dataStorage[c.idOther] ? dataStorage[c.idOther] : c?.name}
                                                </p>
                                                <span
                                                    className={`${checkNewMsg && "font-bold"
                                                    } text-[.8125rem] opacity-80 max-w-[180px] line-clamp-1 `}
                                                >
                                                    {checkNewMsg ? (
                                                        conversation.lastMessage.type ===
                                                            MESSAGE_TYPE.IMAGE ||
                                                            conversation.lastMessage.type ===
                                                            MESSAGE_TYPE.STICKER ? (
                                                                <span>[Hình ảnh]</span>
                                                            ) : conversation.lastMessage.type ===
                                                            MESSAGE_TYPE.MEDIA ? (
                                                                    <span>[Video]</span>
                                                                ) : (
                                                                    <span >{`${userNewMessage === "Bạn: " ? userNewMessage : ""}${conversation.lastMessage.content}`}</span>
                                                                )
                                                    ) : c?.latestMessage?.type === MESSAGE_TYPE.IMAGE ||
                                                        c?.latestMessage?.type === MESSAGE_TYPE.STICKER ? (
                                                            <span>[Hình ảnh]</span>
                                                        ) : c?.latestMessage?.type === MESSAGE_TYPE.MEDIA ? (
                                                            <span>[Video]</span>
                                                        ) : (
                                                            <span className="line-clamp-1">{`${userMessage === "Bạn: " ? userMessage : ""} ${c?.latestMessage?.[c?.latestMessage?.length - 1]
                                                                ?.content ?? ""
                                                            }`}</span>
                                                        )}
                                                </span>
                                            </div>
                                            <div className="flex flex-col justify-between gap-y-1 items-end lg:ml-3">
                                                <span className="text-xs opacity-60">
                                                    {/* {!checkDate
                                ? moment(
                                    conversation?.latestMessage?.[
                                      conversation?.latestMessage?.length - 1
                                    ]?.createdAt
                                  ).format("hh:mm")
                                : moment(
                                    conversation?.latestMessage?.[
                                      conversation?.latestMessage?.length - 1
                                    ]?.createdAt
                                  ).format("DD/MM")} */}
                                                    {duration.asHours() < 24
                                                        ? moment(targetDate).format("HH:mm")
                                                        : duration.asDays() < 365
                                                            ? moment(targetDate).format("DD/MM")
                                                            : moment(targetDate).format("DD/MM/YYYY")}
                                                </span>
                                                {c?.unSeenMessageTotal !== 0 ? (
                                                    <p className="bg-thRed font-medium text-xs px-2 h-fit rounded-full grid place-items-center">
                                                        {c?.unSeenMessageTotal > 5
                                                            ? "5+"
                                                            : c?.unSeenMessageTotal}
                                                    </p>
                                                ) : (
                                                    <>
                                                        {c?.latestMessage?.[
                                                            c?.latestMessage?.length - 1
                                                        ]?.seen?.find(
                                                            (index: any) => index._id !== user.id
                                                        ) ? (
                                                                <AiFillCheckCircle
                                                                    size={16}
                                                                    className="text-thPrimary"
                                                                />
                                                            ) : null}
                                                    </>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                </Link>
                            )
                        );
                    }}
                />
            </div>
        </div >
    );
}
