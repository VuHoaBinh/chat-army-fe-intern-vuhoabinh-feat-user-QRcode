import React, { ReactElement, SyntheticEvent } from 'react';
import Image from 'next/image';
import user from "@/public/user.png";
interface Props {
    url: string
}
export const Avatar = ({ url }: Props): ReactElement => {
    const handleImageError = (event: SyntheticEvent<HTMLImageElement, Event>) => {

        const imgElement = event.target as HTMLImageElement;
        imgElement.src = user.src;
    };
    return (
        <Image 
            sizes="500px"
            src={url}
            alt='avatar'
            loading="eager"
            onError={handleImageError}
            fill
            className='object-cover absolute'
        />
    );
};
