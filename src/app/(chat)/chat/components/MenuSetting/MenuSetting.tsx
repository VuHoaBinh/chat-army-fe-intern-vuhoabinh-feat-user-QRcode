'use client';
import LanguageIcon from "@/public/language.svg";
import { deleteCookie } from "@/src/services/cookie";
import { deleteLoginCookies } from "@/src/utils/auth/handleCookies";
import axiosClient from "@/src/utils/axios/axiosClient";
import { clearDataInIndexDB } from "@/src/utils/search/useIndexDB";
import Image from "next/image";
import { useRouter } from "next/navigation";
import { ReactElement, RefObject, useEffect, useRef, useState } from "react";
import { useClearCache } from 'react-clear-cache';
import { useCookies } from 'react-cookie';
import { IoMdSettings } from "react-icons/io";
import { IoLogOutOutline } from "react-icons/io5";
import { TiContacts } from "react-icons/ti";
import SettingSwitch from "../../setting/components/SettingSwitch";
import MenuItem from "./MenuItem";
import { useAppDispatch } from "@/src/redux/hook";
import { addListIdToListIdConversationDecoded } from "@/src/redux/slices/endToEndEncryptionSlice";

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
interface Props {
    socket: any;
    setIsSetting: (isSetting: boolean) => void;
    refBtn: RefObject<HTMLDivElement>;
}

const MenuSetting = ({ socket, setIsSetting, refBtn }: Props): ReactElement => {
    const menuSettingRef = useRef<HTMLDivElement | null>(null);
    const [cookies] = useCookies();
    const { emptyCacheStorage } = useClearCache();
    const dispatch = useAppDispatch();
    const [isChecked, setIsChecked] = useState(() => localStorage.getItem("theme") === 'dark');
    const router = useRouter();
    const menu = [
        {
            title: "Giao diện",
            subTitle: "Tối",
            children: <SettingSwitch checked={isChecked ? true : false} />,
            onClick: () => {

                handleToggleTheme();
            }
        },
        {
            title: "Ngôn ngữ",
            children: <Image src={LanguageIcon} width={24} alt="language" />,
            onClick: () => { setIsSetting(false); }
        },
        {
            title: "Danh bạ",
            children: <TiContacts size={24} />,
            onClick: () => {
                router.push("/chat/contact");
                setIsSetting(false);
            }
        },
        {
            title: "Thiết lập",
            children: <IoMdSettings size={24} />,
            onClick: () => {
                router.push("/chat/setting");
                setIsSetting(false);
            }
        },
        {
            title: "Đăng xuất",
            children: <IoLogOutOutline size={24} />,
            onClick: () => handleLogout()
        }
    ];
    const handleToggleTheme = () => {

        setIsChecked(!isChecked);
        localStorage.setItem("theme", isChecked ? "light" : "dark");
        if (!isChecked) {
            document.documentElement.classList.add("dark");
        } else { document.documentElement.classList.remove("dark"); };
    };
    const handleLogout = async () => {
        // Mã hóa đầu cuối => logic xóa khi đăng xuất
        localStorage.removeItem("listEndToEndEncryptionConversation");
        // xóa toàn bộ dữ liệu về list cuộc trò chuyện mã hóa trong redux
        dispatch(addListIdToListIdConversationDecoded([]));

        await deleteLoginCookies();

        const cookieNames = Object.keys(cookies);
        cookieNames.forEach(async (cookieName: any) => {
            await deleteCookie(cookieName);
        });

        clearDataInIndexDB();
        emptyCacheStorage();

        if (socket) socket.disconnect();
        // useDispatch(removeUser());
        router.push("/login");

        try {
            const result = await axiosClient().post(
                `${process.env.NEXT_PUBLIC_DAK_API}/auth/logout`
            );
            if (result) {
                deleteLoginCookies();
                if (socket) socket.disconnect();
                router.push("/login");
            }
        } catch (err) {
            console.log(err);
        }
    };
    const closeMenuSetting = () => {
        setIsSetting(false);
    };

    useEffect(() => {
        const handleOutsideClick = (e: MouseEvent) => {
            if (
                menuSettingRef.current &&
                !menuSettingRef.current.contains(e.target as Node)
            ) {
                if (refBtn.current && !refBtn.current.contains(e.target as Node)) {
                    closeMenuSetting();
                }
            }
        };

        document.addEventListener("mousedown", handleOutsideClick);

        return () => {
            document.removeEventListener("mousedown", handleOutsideClick);
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div
            className="
            absolute 
            right-12 
            top-12 
           dark:bg-thNewtral1 bg-light-thNewtral2
            rounded-lg 
            overflow-hidden
            shadow-2xl
            transition-all
            duration-200
            ease-in-out
            w-max
            p-3
            z-50
            animate-c-show-popup
            origin-top-right
            "
            ref={menuSettingRef}
        >
            {menu.map((item) => (
                <MenuItem
                    key={`menu-${item.title}`}
                    title={item.title}
                    subtitle={item.subTitle}
                    onClick={item.onClick}
                >
                    {item.children}
                </MenuItem>
            ))}
        </div>
    );
};

export default MenuSetting;
