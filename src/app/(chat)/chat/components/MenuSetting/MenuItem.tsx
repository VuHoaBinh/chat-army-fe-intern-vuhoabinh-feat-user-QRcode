import React, { ReactElement } from 'react';

interface MenuItemProps {
    title: string;
    subtitle?: string;
    children: ReactElement;
    onClick?: () => void;
}

const MenuItem = ({ title, subtitle, children, onClick }: MenuItemProps): ReactElement => {
    return (
        <div className='flex justify-between px-2 py-3 cursor-pointer
        dark:hover:bg-thNewtral2
        hover:bg-light-thNewtral1
        dark:hover:text-thPrimary
        hover:text-light-thPrimary
        rounded-md
        select-none
        w-56'
        onClick={onClick}>
            <div>
                <span>{title}</span>
                {subtitle && <span>: {subtitle}</span>}
            </div>
            {children}
        </div>
    );
};

export default MenuItem;