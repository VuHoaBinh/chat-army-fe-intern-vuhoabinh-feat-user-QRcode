"use client";
// import useWindowDimensions from "../../../../hook/useWindowDimension";
import { ReactElement } from "react";

const Home = (): ReactElement => {
    
    // const { width } = useWindowDimensions();

    return (
        <div className="absolute w-fit top-1/2 -translate-x-1/2 left-1/2 -translate-y-1/2">
            <h1 className="w-fit text-lg font-semibold opacity-80">Hãy chọn một đoạn chat bất kỳ để bắt đầu cuộc trò chuyện</h1>
        </div>
    );
};

export default Home;