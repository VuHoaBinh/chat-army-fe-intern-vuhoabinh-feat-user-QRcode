"use client";
import { CONVERSATION, EVENT_NAMES, MESSAGE_TYPE } from "@/src/constants/chat";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import {
    setLastMessage,
    setListConversations,
    setListHiddenConversation,
    setOtherUser,
    setTotalUnseenMessage
} from "@/src/redux/slices/conversationSlice";
import { IConversation } from "@/src/types/Conversation";
import { IMessage } from "@/src/types/Message";
import axiosClient from "@/src/utils/axios/axiosClient";
import {
    filterConversation,
    sortConversationByNewest
} from "@/src/utils/conversations/filterConversation";
import moment from "moment";
import dynamic from "next/dynamic";
import Link from "next/link";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { GoPin } from "react-icons/go";
import {
    ReactElement,
    cache,
    use,
    useCallback,
    useEffect,
    useLayoutEffect,
    useRef,
    useState
} from "react";
import { AiFillCheckCircle } from "react-icons/ai";
import { HiUserGroup } from "react-icons/hi2";
import { IoIosChatbubbles } from "react-icons/io";
import { IoMailUnread } from "react-icons/io5";
import { MdOutlineGroupAdd, MdPersonAddAlt } from "react-icons/md";
import { Avatar } from "./Avatar";
import { Virtuoso } from "react-virtuoso";
import ModalAddContact from "../contact/components/Modal/ModalAddContact";
import ModalCreateGroup from "../contact/components/Modal/ModalCreateGroup";
import { ISeenUser } from "@/src/types/User";
import { BiSolidBellOff } from "react-icons/bi";
import { addListIdToListIdConversationTurnOffNotify } from "@/src/redux/slices/NotifyOfConversationSlice";
import { updateStatusSeen } from "@/src/redux/slices/statusSeenedSlice";
import { fetchStatusSeened } from "@/src/utils/api/seenedMessage";
const ListUserRequest = dynamic(() => import("./ListUserRequest"));
const fetchConversations = cache(() =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations?offset=0&status=[2]&limit=10&type=1`
    )
);
const fetchUnseenMessages = cache(() =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/unseen`
    )
);
const fetchConversationsGroup = cache(() =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations?offset=0&status=[2]&limit=10&type=2`
    )
);

const fetchRequestFriends = cache(() =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations??offset=0&status=[1]&limit=10`
    )
);
const fetchListRequestGroups = cache(() =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/group/request?offset=0&limit=100`
    )
);
export default function LeftBarMbl(): ReactElement {
    const dispatch = useAppDispatch();
    const { user, conversation, notify } = useAppSelector((state) => state.user);
    const { data: dataUnSeen }: { data: number } = use(fetchUnseenMessages());
    const [unSeenHiddenMessage, setUnSeenHiddenMessage] = useState<number>(0);
    const [listConversation, setListConversation] = useState<
        IConversation[] | any
    >([]);
    const [modalOpen, setModalOpen] = useState<string>("");

    const [countRequest, setCountRequest] = useState<number>(0);
    const [groupsList, setGroupsList] = useState<IConversation[] | any>([]);
    const pathname = usePathname();
    const router = useRouter();
    console.log("countRequest", modalOpen);
    const params = useSearchParams();
    const fetchData = useCallback(async () => {
        const { data } = await fetchRequestFriends();
        return filterConversation(data, user.id);
    }, [user.id]);
    useEffect(() => {
        fetchData().then((data) => {
            const listUSerWaiting = data.filter(
                (data: any) => data.createdBy != user.id
            );
            setCountRequest(listUSerWaiting?.length);
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [user.id, conversation]);
    const [option, setOption] = useState<string>(() => {
        if (params.get("option") === "group") {
            return "group";
        }
        return "chat";
    });
    const [showModalAddContact, setShowModalAddContact] =
        useState<boolean>(false);

    const [showModalCreateGroup, setShowModalCreateGroup] =
        useState<boolean>(false);

    const modalCreateGroupRef = useRef<any>();
    const modalAddContactRef = useRef<any>();
    const localStorageData = useAppSelector((state: any) => state.user);
    const [dataStorage, setDataStorage] = useState<any>([{}] as any);
    useEffect(() => {
        const dataString = localStorage.getItem("Othername");

        if (dataString) {
            setDataStorage(JSON.parse(dataString));
        }
    }, [localStorageData]);
    useEffect(() => {
        document.addEventListener("mousedown", handleClickOutside);

        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, []);
    const handleClickOutside = (event: { target: any }) => {
        if (
            modalAddContactRef.current &&
            !modalAddContactRef.current.contains(event.target)
        ) {
            setShowModalAddContact(false);
            setModalOpen("");
        }
        if (
            modalCreateGroupRef.current &&
            !modalCreateGroupRef.current.contains(event.target)
        ) {
            setShowModalCreateGroup(false);
            setModalOpen("");
        }
    };
    const [isActiveSeended, setIsActiveSeended] = useState<boolean>();
    useEffect(() => {
        (async () => {
            const data = await fetchStatusSeened();

            setIsActiveSeended(data?.data[0]?.value);
        })();
    }, []);
    useEffect(() => {
        dispatch(updateStatusSeen(fetchStatusSeened()));
    }, [user.id]);
    useEffect(() => {
        (async () => {
            if (option === "group") {
                const { data }: { data: IConversation[] | any } =
                    await fetchConversationsGroup();
                dispatch(
                    setListConversations(
                        sortConversationByNewest(filterConversation(data, user.id))
                    )
                );
                setGroupsList(
                    sortConversationByNewest(filterConversation(data, user.id))
                );
            } else {
                const { data }: { data: IConversation[] | any } =
                    await fetchConversations();
                dispatch(
                    setListConversations(
                        sortConversationByNewest(filterConversation(data, user.id))
                    )
                );
            }
        })();
    }, [dispatch, option, user.id]);

    useEffect(() => {
        setListConversation(
            sortConversationByNewest(
                filterConversation(conversation.listConversation, user.id)
            )
        );
        if (conversation.listConversation) {

            // Filter list conversationHidden
            const conversationsWithHiddenFlag = conversation.listConversation.filter(
                (item: any) => item.isConversationHidden
            );
            dispatch(setListHiddenConversation(conversationsWithHiddenFlag));

            // Lấy tổng số lượng Tin nhắn Unseen của conversationHidden
            const totalUnseenMessages = conversationsWithHiddenFlag.reduce(
                (unseenHiddenMessage, conversation) => unseenHiddenMessage + conversation.unSeenMessageTotal,
                0
            );
            setUnSeenHiddenMessage(totalUnseenMessages);
            // Loại bỏ các conversationhidden
            const arrIdConversationHidden: string[] = conversationsWithHiddenFlag.map(
                (conversation) => conversation._id
            );
            if (option === "group") {
                const conversationsNotHidden = conversation.listConversation.filter(
                    (item: any) => item.type === 2 && !arrIdConversationHidden.includes(item._id)
                );
                setGroupsList(conversationsNotHidden);
            } else {
                const conversationsNotHidden = conversation.listConversation.filter(
                    (item: any) => item.type === 1 && !arrIdConversationHidden.includes(item._id)
                );
                setListConversation(conversationsNotHidden);

            }


        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [conversation.listConversation, user.id]);
    useEffect(() => {
        dispatch(setTotalUnseenMessage(dataUnSeen));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [dataUnSeen]);
    const lastMessageConversationRef = useRef<IMessage>(conversation.lastMessage);
    const totalUnseenMessageRef = useRef<number>(
        conversation?.totalUnseenMessage || 0
    );
    useEffect(() => {
        (async () => {
            try {
                const lastMessageConversation = lastMessageConversationRef.current;
                if (
                    totalUnseenMessageRef.current ===
                    (conversation.totalUnseenMessage as number) &&
                    (!conversation.lastMessage ||
                        lastMessageConversation?._id === conversation.lastMessage?._id)
                )
                    return;
                const { data } = await fetchConversations();
                const { data: dataUnread } = await fetchUnseenMessages();
                const { data: dataGroup } = await fetchConversationsGroup();
                setListConversation(
                    sortConversationByNewest(filterConversation(data, user.id))
                );
                setGroupsList(
                    sortConversationByNewest(filterConversation(dataGroup, user.id))
                );
                if (option === "group") {
                    dispatch(
                        setListConversations(
                            sortConversationByNewest(filterConversation(dataGroup, user.id))
                        )
                    );
                } else {
                    dispatch(
                        setListConversations(
                            sortConversationByNewest(filterConversation(data, user.id))
                        )
                    );
                }

                dispatch(setTotalUnseenMessage(dataUnread));
            } catch (error) {
                console.error("Error fetching conversations:", error);
            }
        })();
        return () => {
            lastMessageConversationRef.current = conversation.lastMessage;
            totalUnseenMessageRef.current = conversation?.totalUnseenMessage || 0;
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [
        conversation.lastMessage,
        user.id,
        conversation.totalUnseenMessage,
        dispatch
    ]);
    useEffect(() => {
        if (!user.socket || pathname.includes("/chat/conversation")) return;
        user.socket.on(
            EVENT_NAMES.CONVERSATION.RECEIVE_MESSAGE,
            (msg: IMessage) => {
                dispatch(setLastMessage(msg));
                window.postMessage(
                    `${msg.conversationId}-${msg.createdBy.username}-receiMessage`,
                    "*"
                );
            }
        );
    }, [dispatch, pathname, user.socket]);
    useEffect(() => {
        if (!user.socket) return;
        user.socket.on(EVENT_NAMES.CONVERSATION.SEENED_MESSAGE, (data: any) => {
            let dataArr;
            if (option === "group") {
                dataArr = groupsList;
            } else {
                dataArr = listConversation;
            }
            dataArr.map((conversation: any) => {
                if (
                    conversation._id === data.conversationId &&
                    data.userSeenId !== user.id
                ) {
                    const newUserSeen: ISeenUser = {
                        _id: data?.userSeenId,
                        username: data?.userSeenUsername,
                        avatar: data?.userSeenAvatar,
                        id: data?.userSeenId,
                        time: ""
                    };
                    const Conver =
                        conversation.latestMessage[conversation.latestMessage.length - 1];
                    const newSeen = Conver.seen.find(
                        (item: ISeenUser) => item._id === newUserSeen._id
                    )
                        ? Conver.seen
                        : [...Conver.seen, newUserSeen];
                    const newLastMessage = {
                        ...Conver,
                        seen: [...newSeen]
                    };
                    if (option === "group") {
                        setGroupsList((prev: any) => {
                            return prev.map((item: any) => {
                                if (item._id === conversation._id) {
                                    return {
                                        ...item,
                                        latestMessage: [
                                            ...item.latestMessage.slice(
                                                0,
                                                item.latestMessage.length - 1
                                            ),
                                            newLastMessage
                                        ]
                                    };
                                }
                                return item;
                            });
                        });
                    } else {
                        setListConversation((prev: any) => {
                            return prev.map((item: any) => {
                                if (item._id === conversation._id) {
                                    return {
                                        ...item,
                                        latestMessage: [
                                            ...item.latestMessage.slice(
                                                0,
                                                item.latestMessage.length - 1
                                            ),
                                            newLastMessage
                                        ]
                                    };
                                }
                                return item;
                            });
                        });
                    }
                }
            });
        });
        return () => {
            user.socket.off(EVENT_NAMES.CONVERSATION.SEENED_MESSAGE);
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [user.socket, conversation, user.id]);

    useEffect(() => {
        if (!user.socket || window.location.pathname !== "/chat") return;
        user.socket.on(
            EVENT_NAMES.CONVERSATION.RECEIVE_MESSAGE,
            (msg: IMessage) => {
                dispatch(setLastMessage(msg));
                // window.postMessage(`${msg.conversationId}-${msg.createdBy.username}-receiMessage`, '*');
            }
        );
    }, [dispatch, user.socket]);

    const [countRequestGroup, setCountRequestGroup] = useState<number>(0);
    useEffect(() => {
        (async () => {
            const { data } = await fetchListRequestGroups();
            setCountRequestGroup(data?.length);
        })();
    }, []);

    const renderLastMessageContent = (conversation: any, userNewMessage: any) => {
        if (
            conversation.lastMessage.type === MESSAGE_TYPE.IMAGE ||
            conversation.lastMessage.type === MESSAGE_TYPE.STICKER
        ) {
            return <span>[Hình ảnh]</span>;
        }
        return (
            <span>{`${userNewMessage}${conversation.lastMessage.content}`}</span>
        );
    };
    const renderLatestMessageContent = (item: any, userMessage: any) => {
        if (
            item?.latestMessage?.type === MESSAGE_TYPE.IMAGE ||
            item?.latestMessage?.type === MESSAGE_TYPE.STICKER
        ) {
            return <span>[Hình ảnh]</span>;
        } else if (item?.latestMessage?.type === MESSAGE_TYPE.MEDIA) {
            return <span>[Video]</span>;
        }
        return (
            <span>{`${userMessage}${item?.latestMessage?.[item?.latestMessage?.length - 1]?.content
            }`}</span>
        );
    };
    const [heightVirtualList, setHeightVirtualList] = useState<number>(0);
    const divVirtualListRef = useRef<HTMLDivElement>(null);

    // Tính height cho Virtuallist dựa vào div contain
    useLayoutEffect(() => {
        const handleWindowResize = () => {
            if (divVirtualListRef.current) {
                // Div parent có pading-y là 12 nên phải trừ đi
                setHeightVirtualList(divVirtualListRef.current.clientHeight - 76);
            }
        };
        handleWindowResize();
        window.addEventListener("resize", handleWindowResize);
        return () => {
            window.removeEventListener("resize", handleWindowResize);
        };
    }, []);
    function renderRequestList() {
        return (
            // <div className="flex flex-col gap-y-6 flex-shrink-0 h-[85vh] mx-4 mt-4 overflow-y-auto">
            //   <div className="flex gap-x-1 items-center">
            //     <h1 className="text-xl font-semibold">Request</h1>
            //     <p className="bg-thRed font-medium text-sm px-2 h-fit rounded grid place-items-center">
            //       20
            //     </p>
            //   </div>
            //   <div className="flex flex-col gap-y-2">
            //     {listConversation.map((item: IConversation | any) => (
            //       <Link href={`/chat/conversation/${item._id}`} key={item._id}>
            //         <div
            //           className="flex gap-x-2 items-center py-2 hover:bg-thNewtral1 rounded-lg"
            //           onClick={() =>
            //             dispatch(
            //               setOtherUser({
            //                 otherUsername: item?.name,
            //                 otherAvatar: item?.avatar,
            //               })
            //             )
            //           }
            //         >
            //           <Image
            //             src={item?.avatar}
            //             sizes="(max-width: 56px) 56px, 56px"
            //             alt={"Logo"}
            //             loading="eager"
            //             priority
            //             width={56}
            //             height={56}
            //             className="rounded-full object-cover h-[56px]"
            //           />
            //           <div className="flex gap-x-16 justify-between h-full items-center w-full">
            //             <div>
            //               <p className="text-[.9375rem] font-semibold">
            //                 {item?.name}
            //               </p>
            //               <span className="text-[.8125rem] opacity-80 max-w-[195px] line-clamp-1">
            //                 {item?.latestMessage?._id === user.id ? "you: " : ""}
            //                 {item?.latestMessage?.content}
            //               </span>
            //             </div>
            //             <span className="text-xs opacity-60">
            //               {moment(item?.updatedAt).format("HH:mm")}
            //             </span>
            //           </div>
            //         </div>
            //       </Link>
            //     ))}
            //   </div>
            // </div>
            <ListUserRequest setOption={() => { }} onOpenModal={(modalType: string) => {
                setModalOpen(modalType + "");
                setShowModalAddContact(modalType === "addFriend");
                setShowModalCreateGroup(modalType === "createGroup");
            }} backGroup={() => { }} setCountRequestGroup={setCountRequestGroup} />
        );
    }

    const { isOpenModalTurnOffNotification, listIdConversationTurnOffNotify } = notify;
    const [listIdConversationTurnedOffNotifications, setListIdConversationTurnedOffNotifications] = useState<string[] | any>(listIdConversationTurnOffNotify);
    const [totalUnseenMessageInConversationTurnedOffNotifications, settotalUnseenMessageInConversationTurnedOffNotifications] = useState<number>(0);
    const [totalUnseenMessage2, setTotalUnseenMessage2] = useState<number>(conversation.totalUnseenMessage ? conversation.totalUnseenMessage : 0);

    useEffect(() => {
        let total: number = 0;
        listConversation.forEach((c: any) => {
            total += c.unSeenMessageTotal as number;
        });
        setTotalUnseenMessage2(total);
    }, [conversation, listConversation, isOpenModalTurnOffNotification, listIdConversationTurnOffNotify]);

    useEffect(() => {
        const newListId: string[] | any = [];
        conversation.listConversation?.forEach((c: any) => {
            const menuSetting = c?.conversationSetting && c?.conversationSetting.find((st: any) => {
                return st.type === 1;
            });
            const turnOffTime = menuSetting && menuSetting.value;

            if (turnOffTime || turnOffTime === null) {
                newListId.push(c.id);
            }
        });
        if (newListId.length > 0) {
            dispatch(addListIdToListIdConversationTurnOffNotify(newListId));
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [conversation.listConversation]);
    useEffect(() => {
        let newTotalUnseen: number = 0;
        conversation.listConversation?.forEach((c: any) => {
            if (listIdConversationTurnedOffNotifications.includes(c.id)) {
                newTotalUnseen += c.unSeenMessageTotal;
            }
        });
        settotalUnseenMessageInConversationTurnedOffNotifications(newTotalUnseen);
    }, [conversation.listConversation, conversation.totalUnseenMessage, listIdConversationTurnedOffNotifications]);
    useEffect(() => {
        setListIdConversationTurnedOffNotifications(listIdConversationTurnOffNotify);
    }, [isOpenModalTurnOffNotification, listIdConversationTurnOffNotify, conversation.ListIdPinnedConversation]);

    useEffect(() => {
        // sau khi pin thì lấy conversation mới
        const getNewConversation = async () => {
            const { data } = await fetchConversations();
            setListConversation(filterConversation(data, user.id));
        };
        getNewConversation();
    }, [conversation.ListIdPinnedConversation, user.id]);

    function renderConversationList() {
        return (
            <div className="flex flex-col gap-y-4 flex-shrink-0 h-[calc(100dvh-4.25rem)] mx-4 mt-4 overflow-y-auto">
                <div className="flex gap-x-1 items-center justify-between">
                    <div className="flex gap-x-2 items-center">

                        <h1 className="text-xl font-semibold">Message</h1>
                        <p className="bg-thRed font-medium text-sm px-2 h-fit rounded grid place-items-center">
                            {(totalUnseenMessage2 && totalUnseenMessage2 - totalUnseenMessageInConversationTurnedOffNotifications) || 0}
                        </p>
                    </div>
                    <div className="flex gap-x-2 items-center">

                        <div
                            onClick={() => {

                                setModalOpen("addFriend");
                                setShowModalAddContact(true);
                            }}
                            className="cursor-pointer"
                        >

                            <MdPersonAddAlt size={24} />




                        </div>
                        <div
                            className="cursor-pointer"
                            onClick={() => {

                                setModalOpen("createGroup");
                                setShowModalCreateGroup(true);
                            }}
                        >

                            <MdOutlineGroupAdd size={24} />

                        </div>
                    </div>
                </div>
                <div className="flex-1" ref={divVirtualListRef}>
                    <Virtuoso
                        data={listConversation}
                        style={{ height: heightVirtualList }}
                        itemContent={(index, c: IConversation | any) => {
                            const checkNewMsg =
                                conversation.lastMessage?.conversationId === c._id;

                            const userNewMessage =
                                conversation.lastMessage?.createdBy?._id === user.id
                                    ? "Bạn: "
                                    : `${dataStorage[conversation.lastMessage?.createdBy?._id] ? dataStorage[conversation.lastMessage?.createdBy?._id] : conversation.lastMessage?.createdBy?.username}`;
                            const userMessage =
                                c.latestMessage?.[c?.latestMessage?.length - 1]?.createdBy
                                    ?._id === user.id
                                    ? "Bạn: "
                                    : `${dataStorage[c.latestMessage?.[c?.latestMessage?.length - 1]?.createdBy
                                        ?._id] ? dataStorage[c.latestMessage?.[c?.latestMessage?.length - 1]?.createdBy
                                            ?._id] : c?.latestMessage?.[c?.latestMessage?.length - 1]?.createdBy
                                            ?.username
                                    }`;

                            // const userNewMessage =
                            //     conversation.lastMessage?.createdBy?._id === user.id
                            //         ? "Bạn: "
                            //         : `${conversation.lastMessage?.createdBy?.username}: `;
                            // const userMessage =
                            //     c.latestMessage?.[c?.latestMessage?.length - 1]?.createdBy
                            //         ?._id === user.id
                            //         ? "Bạn: "
                            //         : `${c?.latestMessage?.[c?.latestMessage?.length - 1]
                            //             ?.createdBy?.username
                            //         }`;
                            const targetDate = moment(
                                c?.latestMessage?.[c?.latestMessage?.length - 1]?.createdAt
                            );
                            const currentDateTime = moment();
                            const duration = moment.duration(
                                currentDateTime.diff(targetDate)
                            );

                            //   const checkDate = targetDate.isBefore(currentDateTime, "day");

                            return (
                                c.type === CONVERSATION.TYPE.INDIVIDUAL && (
                                    <Link href={`/chat/conversation/${c._id}`} key={c._id}>
                                        <div
                                            className="flex gap-x-2 items-center py-2 hover:bg-light-thNewtral1 dark:hover:bg-thNewtral1 rounded-lg"
                                            onClick={() =>
                                                dispatch(
                                                    setOtherUser({
                                                        otherUsername: listConversation[index]?.name,
                                                        otherAvatar: listConversation[index]?.avatar
                                                    })
                                                )
                                            }
                                        >
                                            {
                                                c?.isPinned && (<div className="absolute top-0 -left-[1px]">
                                                    <GoPin />
                                                </div>)
                                            }
                                            <div className="relative rounded-full h-14 w-14 overflow-hidden shrink-0">
                                                <Avatar url={listConversation[index]?.avatar} />
                                            </div>

                                            <div className="flex flex-1 justify-between h-full items-center w-full pr-2">
                                                <div className="flex-1 flex flex-col max-w-[40vw]">
                                                    <p className="text-[.9375rem] font-semibold text-ellipsis line-clamp-1">
                                                        {dataStorage[c.createdBy] ? dataStorage[c.idOther] :
                                                            listConversation[index]?.name}
                                                    </p>
                                                    <span
                                                        className={`text-[.8125rem] opacity-80 line-clamp-1 ${checkNewMsg && "font-bold"
                                                        }`}
                                                    >

                                                        {checkNewMsg
                                                            ? renderLastMessageContent(
                                                                conversation,
                                                                userNewMessage ===
                                                                    "Bạn: " ? userMessage : ""
                                                            )
                                                            : renderLatestMessageContent(c, userMessage ===
                                                                "Bạn: " ? userMessage : "")}
                                                    </span>
                                                </div>
                                                <div className="flex flex-col justify-between gap-y-1 items-center">
                                                    <span className="text-xs opacity-60">
                                                        {duration.asHours() < 24
                                                            ? moment(targetDate).format("HH:mm")
                                                            : duration.asDays() < 365
                                                                ? moment(targetDate).format("DD/MM")
                                                                : moment(targetDate).format("DD/MM/YYYY")}
                                                    </span>
                                                    {listConversation[index]?.unSeenMessageTotal !== 0 ? (
                                                        <p className="bg-thRed font-medium text-xs px-2 h-fit rounded-full grid place-items-center">
                                                            {listConversation[index]?.unSeenMessageTotal > 5
                                                                ? "5+"
                                                                : listConversation[index]?.unSeenMessageTotal}
                                                        </p>
                                                    ) : (
                                                        <>
                                                            {c?.latestMessage?.[
                                                                c?.latestMessage?.length - 1
                                                            ]?.seen?.find(
                                                                (index: any) => index._id !== user.id
                                                            ) ? (
                                                                // <AiFillCheckCircle
                                                                //     size={16}
                                                                //     className="text-thPrimary"
                                                                // />
                                                                    <div className="flex">
                                                                        {
                                                                            listIdConversationTurnedOffNotifications?.includes(c.id) ? <BiSolidBellOff size={16} /> : (
                                                                                <AiFillCheckCircle
                                                                                    size={16}
                                                                                    className="text-thPrimary ml-1"
                                                                                />
                                                                            )
                                                                        }
                                                                    </div>
                                                                ) : null}
                                                        </>
                                                    )}
                                                </div>
                                            </div>
                                        </div>
                                    </Link>
                                )
                            );
                        }}
                    />
                </div>
            </div>
        );
    }
    function renderGroupList() {
        return (
            <div className="flex flex-col gap-y-4 flex-shrink-0 h-[calc(100dvh-4.25rem)] mx-4 mt-4 overflow-y-auto">
                <div className="flex gap-x-1 items-center justify-between">
                    <div className="flex gap-x-1 items-center">
                        <h1 className="text-xl font-semibold">Message</h1>
                        <p className="bg-thRed font-medium text-sm px-2 h-fit rounded grid place-items-center">
                            {conversation.totalUnseenMessage}
                        </p>
                    </div>

                    <div className="flex gap-x-2 items-center">

                        <div
                            onClick={() => {

                                setModalOpen("addFriend");
                                setShowModalAddContact(true);
                            }}
                            className="cursor-pointer"
                        >

                            <MdPersonAddAlt size={24} />




                        </div>
                        <div
                            className="cursor-pointer"
                            onClick={() => {

                                setModalOpen("createGroup");
                                setShowModalCreateGroup(true);
                            }}
                        >

                            <MdOutlineGroupAdd size={24} />

                        </div>
                    </div>
                </div>

                <div className="flex-1" ref={divVirtualListRef}>
                    <Virtuoso
                        data={groupsList}
                        style={{ height: heightVirtualList }}
                        itemContent={(index) => {
                            const lastMessage =
                                groupsList[index]?.latestMessage?.[
                                    groupsList[index]?.latestMessage.length - 1
                                ];
                            const checkNewMsg = !lastMessage?.seen?.find(
                                (item: any) => item?.userId === user.id
                            );
                            const userMessage =
                                lastMessage?.createdBy?._id === user.id
                                    ? "Bạn: "
                                    : `${dataStorage[lastMessage?.createdBy
                                        ?._id] ? dataStorage[lastMessage?.createdBy
                                            ?._id] : lastMessage?.createdBy?.username}`;
                            const targetDate = moment(
                                groupsList[index]?.latestMessage?.[
                                    groupsList[index]?.latestMessage?.length - 1
                                ]?.createdAt
                            );

                            const currentDateTime = moment();
                            const duration = moment.duration(
                                currentDateTime.diff(targetDate)
                            );

                            //   const checkDate = targetDate.isBefore(currentDateTime, "day"); 
                            return (
                                <Link
                                    href={`/chat/conversation/${groupsList[index]._id}`}
                                    key={groupsList[index]._id}
                                >
                                    <div
                                        className="flex gap-x-2 items-center hover:bg-light-thNewtral1 py-2 dark:hover:bg-thNewtral1 rounded-lg"
                                        onClick={() =>
                                            dispatch(
                                                setOtherUser({
                                                    otherUsername: groupsList[index]?.name,
                                                    otherAvatar: groupsList[index]?.avatar
                                                })
                                            )
                                        }
                                    >
                                        <div className="relative rounded-full h-14 w-14 overflow-hidden shrink-0">
                                            <Avatar url={groupsList[index]?.avatar} />
                                        </div>

                                        <div className="flex flex-1 justify-between h-full items-center w-full pr-2">
                                            <div className="flex-1 flex flex-col max-w-[40vw]">
                                                <p className="text-[.9375rem] font-semibold text-ellipsis line-clamp-1">
                                                    {groupsList[index]?.name}
                                                </p>
                                                <span
                                                    className={`text-[.8125rem] opacity-80 line-clamp-1 ${checkNewMsg && "font-bold"
                                                    }`}
                                                >
                                                    {
                                                        // checkNewMsg
                                                        //     ? renderLastMessageContent(
                                                        //         conversation,
                                                        //         userNewMessage
                                                        //     )
                                                        renderLatestMessageContent(
                                                            groupsList[index],
                                                            userMessage === "Bạn: " ? userMessage : userMessage + ": "
                                                        )
                                                    }
                                                </span>
                                            </div>
                                            <div className="flex flex-col justify-between gap-y-1 items-center">
                                                <span className="text-xs opacity-60">
                                                    {duration.asHours() < 24
                                                        ? moment(targetDate).format("HH:mm")
                                                        : duration.asDays() < 365
                                                            ? moment(targetDate).format("DD/MM")
                                                            : moment(targetDate).format("DD/MM/YYYY")}
                                                </span>
                                                {groupsList[index]?.unSeenMessageTotal !== 0 ? (
                                                    <p className="bg-thRed font-medium text-xs px-2 h-fit rounded-full grid place-items-center">
                                                        {groupsList[index]?.unSeenMessageTotal > 5
                                                            ? "5+"
                                                            : groupsList[index]?.unSeenMessageTotal}
                                                    </p>
                                                ) : (
                                                    <>
                                                        {isActiveSeended == false ? (<>
                                                            {groupsList[index]?.latestMessage?.[groupsList[index]?.latestMessage?.length - 1]?.createdBy?._id !== user.id && (<AiFillCheckCircle
                                                                size={16}
                                                                className="text-thPrimary"
                                                            />)}
                                                        </>) : (<>
                                                            {groupsList[index]?.latestMessage?.[
                                                                groupsList[index]?.latestMessage?.length - 1
                                                            ]?.seen?.find(
                                                                (index: any) => index._id !== user.id && groupsList[index]?.userOffStatusMsg.includes(index._id) == false || groupsList[index]?.latestMessage?.[groupsList[index]?.latestMessage?.length - 1]?.createdBy?._id !== user.id
                                                            ) ? (
                                                                    <AiFillCheckCircle
                                                                        size={16}
                                                                        className="text-thPrimary"
                                                                    />
                                                                ) : null}</>)}

                                                    </>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                </Link>
                            );
                        }}
                    />
                </div>
            </div>
        );
    }

    return (
        <div className="relative flex-grow-0 flex-shrink-0">
            {option === "chat"
                ? renderConversationList()
                : option === "request"
                    ? renderRequestList()
                    : renderGroupList()}
            <div className="absolute bottom-5 left-1/2 -translate-x-1/2 flex items-center px-4 py-2 rounded-2xl gap-x-8 bg-light-thNewtral1 dark:bg-thNewtral1">
                <div
                    className={`${option === "chat"
                        ? unSeenHiddenMessage > 0
                            ? "text-thRed"
                            : "text-thPrimary"
                        : "text-thNewtral2"
                    } `}
                    onClick={() => {
                        setOption("chat");
                        router.push("/chat");
                    }}
                >
                    <IoIosChatbubbles size={40} />
                </div>
                <div
                    className={`${option === "group"
                        ? unSeenHiddenMessage > 0
                            ? "text-thRed"
                            : "text-thPrimary"
                        : "text-thNewtral2"
                    } `}
                    onClick={() => {
                        router.push("/chat?option=group");
                        setOption("group");
                    }}
                >
                    {/* <Image
                        src={GroupIcon}
                        sizes="(max-width: 40px) 40px, 40px"
                        alt={"Logo"}
                        loading="eager"
                        priority
                        width={40}
                        height={40}
                        className="rounded-full object-cover"
                    /> */}
                    <HiUserGroup size={40} />
                </div>
                <div
                    className={`${option === "request"
                        ? "text-thPrimary"
                        : "text-thNewtral2 "
                    }`}
                    onClick={() => setOption("request")}
                >
                    {/* <Image
                        src={RequestIcon}
                        sizes="(max-width: 40px) 40px, 40px"
                        alt={"Logo"}
                        loading="eager"
                        priority
                        width={40}
                        height={40}
                        className="rounded-full object-cover"
                    /> */}
                    <IoMailUnread size={40} />

                    {countRequest > 0 && (
                        <div className="absolute right-[-2px] w-5 h-5 text-center text-white bg-red-500 rounded-full top-[-2px]">
                            {countRequest + countRequestGroup}
                        </div>
                    )}
                </div>
                {/* <div
                    className={`${modalOpen === "addFriend" ? "text-thPrimary" : "text-thNewtral2"
                        }  cursor-pointer`}
                    onClick={() => {

                        setModalOpen("addFriend");
                        setShowModalAddContact(true);
                    }}
                >
                    <FaRegSquarePlus size={35} />
                </div>
                <div
                    className={`${modalOpen === "createGroup" ? "text-thPrimary" : "text-thNewtral2"
                        }  cursor-pointer`}
                    onClick={() => {

                        setModalOpen("createGroup");
                        setShowModalCreateGroup(true);
                    }}
                >
                    <MdOutlineGroupAdd size={35} />
                </div> */}

            </div>
            {showModalAddContact && (<div className="fixed w-screen flex items-center justify-center top-0 h-full bg-[#00000080] z-50">

                <div ref={modalAddContactRef}>
                    <ModalAddContact
                        onClose={() => {
                            setShowModalAddContact(false);
                            setModalOpen("");
                        }}
                    />
                </div>


            </div>)}
            {showModalCreateGroup && (<div className="fixed w-screen flex h-full items-center justify-center top-0 left-0 bg-[#00000080] z-50">


                <div ref={modalCreateGroupRef} >
                    <ModalCreateGroup
                        onClose={() => {
                            setShowModalCreateGroup(false);
                            setModalOpen("");
                        }}
                    />
                </div>



            </div>)}

        </div>
    );
}
