import axiosClient from "@/src/utils/axios/axiosClient";
import Image from "next/image";
import Link from "next/link";
import {
    ReactElement,
    SyntheticEvent,
    cache,
    useCallback,
    useEffect,
    useLayoutEffect,
    useRef,
    useState
} from "react";

import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import {
    setListConversations,
    setOtherUser
} from "@/src/redux/slices/conversationSlice";
import { UserRequestFriend } from "@/src/types/RequestFriends";
import { filterConversation } from "@/src/utils/conversations/filterConversation";
import { useRouter } from "next/navigation";
import { Virtuoso } from "react-virtuoso";
import { Tooltip } from "antd";
import { MdOutlineGroupAdd, MdPersonAddAlt } from "react-icons/md";
import { IConversation } from "@/src/types/Conversation";
import { toast } from "react-toastify";
import { setDetailChat } from "@/src/redux/slices/detailChatSlice";
import { RiGroupLine } from "react-icons/ri";
import { CONVERSATION } from "@/src/constants/chat";
import userAvatar from "@/public/user.png";
const fetchRequestFriends = cache(() =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations??offset=0&status=[1]&limit=10`
    )
);
const disPathConversation = (user: any, dispatch: any) => {
    axiosClient()
        .get(
            `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations??offset=0&status=[2]&limit=10`
        )
        .then((response: any) => {
            const { data } = response;
            const listConversation = filterConversation(data, user.id);
            dispatch(setListConversations(listConversation));
        });
};
interface ListUserRequestProps {
    setOption: (option: string) => void;
    onOpenModal: (typeModal: string) => void;
    backGroup: () => void
    setCountRequestGroup: (count: number) => void
}
const fetchListRequestGroups = cache(() =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/group/request?offset=0&limit=100`
    )
);
export default function ListUserRequest({
    setOption,
    onOpenModal,
    backGroup,
    setCountRequestGroup
}: ListUserRequestProps): ReactElement {
    const { push } = useRouter();
    const dispatch = useAppDispatch();
    const [deledted, setDeledted] = useState(false);

    const listAction = [
        {
            name: "Từ Chối",

            color: "bg-thNewtral2",
            onClick: (e: any, userRequestFriend: UserRequestFriend) => {
                axiosClient()
                    .delete(
                        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${userRequestFriend._id}`
                    )
                    .then((response: any) => {
                        if (response.status === 200) {
                            disPathConversation(user, dispatch);
                            setDeledted(!deledted);
                        }
                    })
                    .catch((error: any) => {
                        console.log(error);
                    });
            }
        },
        {
            name: "Đồng ý",

            color: " dark:bg-thPrimary bg-light-thPrimary",
            onClick: (e: any, userRequestFriend: UserRequestFriend) => {
                // ("userRequestFriend", userRequestFriend);
                axiosClient()
                    .put(
                        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${userRequestFriend._id}/decide/2`
                    )
                    .then(() => {
                        disPathConversation(user, dispatch);

                        dispatch(
                            setOtherUser({
                                otherUsername: userRequestFriend?.name,
                                otherAvatar: userRequestFriend?.avatar,
                                lastMessage: userRequestFriend.latestMessage || {}
                            })
                        );
                        push(`/chat/conversation/${userRequestFriend._id}`);
                        setOption("chat");
                    })
                    .catch((error: any) => {
                        console.log(error);
                    });
            }
        }
    ];
    const { user } = useAppSelector((state) => state.user);

    // const { data } = use(fetchRequestFriends());
    const [listRequestFriends, setListRequestFriends] = useState<
        UserRequestFriend[]
    >([]);
    const listUSerWaiting = listRequestFriends.filter(
        (userRequest) => userRequest.createdBy != user.id
    );
    const fetchData = useCallback(async () => {
        const { data } = await fetchRequestFriends();
        return filterConversation(data, user.id);
    }, [user.id]);
    useEffect(() => {
        fetchData().then((data) => {
            setListRequestFriends(data);
        });
    }, [user.id, deledted]);

    const divVirtualListRef = useRef<HTMLDivElement>(null);
    const [heightVirtualList, setHeightVirtualList] = useState<number>(0);
    // Tính height cho Virtuallist dựa vào div contain
    useLayoutEffect(() => {
        const handleWindowResize = () => {
            if (divVirtualListRef.current) {
                setHeightVirtualList(divVirtualListRef.current.clientHeight - 25);
            }
        };
        handleWindowResize();
        window.addEventListener("resize", handleWindowResize);
        return () => {
            window.removeEventListener("resize", handleWindowResize);
        };
    }, []);
    const [listRequestGroups, setListRequestGroups] = useState<IConversation[] | any>([]);
    useEffect(() => {
        (async () => {
            const { data } = await (fetchListRequestGroups());
            setListRequestGroups(filterConversation(data, user.id));
        })();
    }, [user.id]);
    const handleAcceptAndDeny = async (idConversation: string, action: number) => {
        try {
            const response = await axiosClient().put(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/group/decide/member/${idConversation}/${action}`, {});
            if (response.status === 200) {
                toast.success("Thành công");
                if (action === 1) {
                    push("/chat/conversation/" + idConversation);
                    const { data: newDetailChat } = await axiosClient().get(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${idConversation}`);
                    dispatch(setDetailChat(newDetailChat));
                    const conversation = listRequestGroups.find((item: any) => item._id === idConversation);
                    dispatch(
                        setOtherUser({
                            otherUsername: conversation?.name,
                            otherAvatar: conversation?.avatar,
                            lastMessage: conversation?.latestMessage || {}
                        })
                    );
                    backGroup();
                }
                if (action === 2) {
                    push("/chat");
                    const newListRequestGroups = listRequestGroups.filter((item: any) => item._id !== idConversation);
                    setListRequestGroups(
                        filterConversation(newListRequestGroups, user.id)
                    );
                }
                setCountRequestGroup(listRequestGroups.length - 1);
            }
        } catch (err: any) {
            toast.error(err.response.data.message);
            console.log(err);
        }
    };
    const handleImageError = (event: SyntheticEvent<HTMLImageElement, Event>) => {

        const imgElement = event.target as HTMLImageElement;
        imgElement.src = userAvatar.src;
    };
    const mergeListRequest: IConversation[] | any[] = [...listUSerWaiting, ...listRequestGroups];
    return (
        <div className="p-4 flex flex-col gap-y-6 flex-shrink-0 rounded-2xl border-2 border-thNewtral1 bg-light-thNewtral dark:bg-thDark-background h-[calc(90dvh-1.5rem)] lg:h-[calc(85dvh-1.5rem)] mx-4 overflow-y-auto min-w-[350px]">
            <div className="flex gap-x-1 items-center justify-between">
                <div className="flex gap-x-2 items-center">
                    <h1 className="text-xl font-semibold">Request</h1>
                    <p className="bg-thRed font-medium text-sm px-2 h-fit rounded grid place-items-center">
                        {listUSerWaiting?.length}
                    </p>
                </div>

                <div className="flex gap-x-2 items-center">

                    <div
                        onClick={() => onOpenModal("addFriend")}
                        className="cursor-pointer"
                    >
                        <Tooltip title={"Thêm bạn"}  >
                            <MdPersonAddAlt size={24} />



                        </Tooltip>
                    </div>
                    <div
                        className="cursor-pointer"
                        onClick={() => onOpenModal("createGroup")}
                    >
                        <Tooltip title={"Tạo nhóm"}  >
                            <MdOutlineGroupAdd size={24} />
                        </Tooltip>
                    </div>
                </div>
            </div>
            <div className="flex-1 w-[330px]" ref={divVirtualListRef}>
                {
                    mergeListRequest.length > 0 &&
                    <Virtuoso
                        data={mergeListRequest}
                        style={{
                            height: heightVirtualList,
                            width: '330px'
                        }}
                        itemContent={(index, c: UserRequestFriend | any) => {
                            return (
                                <Link href={`#`} key={c._id}>
                                    <div className="flex gap-x-2 items-center py-2 px-2 dark:hover:bg-thNewtral1 hover:bg-light-thNewtral1  rounded-lg">
                                        <Image
                                            src={
                                                c?.avatar === "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg"
                                                    ? "/default_avatar.jpg"
                                                    : c?.avatar
                                            }
                                            sizes="(max-width: 56px) 56px, 56px"
                                            alt={"Logo"}
                                            loading="eager"
                                            onError={handleImageError}
                                            priority
                                            width={56}
                                            height={56}
                                            className="rounded-full object-cover h-[56px]"
                                        />
                                        <div className="flex gap-x-8  gap-y-2 justify-between h-full items-center flex-col">
                                            <div className="flex gap-x-2 w-full">
                                                {
                                                    c?.type === 2 && <RiGroupLine size={20} />
                                                }
                                                <p className="text-[.9375rem] font-semibold md:max-w-[72px] lg:max-w-[185px] text-ellipsis line-clamp-1">
                                                    {c?.name}
                                                </p>
                                            </div>
                                            {
                                                c?.type === 2 &&
                                                <div className="flex gap-x-4 text-black font-semibold">
                                                    {
                                                        c?.member?.find((item: any) => item.id === user.id)?.type === CONVERSATION.ROLE_TYPE.MEMBER_WAITING_ACCEPT &&
                                                        <div className="flex gap-x-2">
                                                            <button className="bg-thPrimary text-thDark-background rounded-md font-bold px-2 opacity-70 hover:opacity-100"
                                                                onClick={() => handleAcceptAndDeny(c.id, 1)}
                                                            >
                                                                Đồng ý
                                                            </button>
                                                            <button className="bg-thRed text-white rounded-md font-bold px-2 opacity-70 hover:opacity-100"
                                                                onClick={() => handleAcceptAndDeny(c.id, 2)}
                                                            >
                                                                Từ chối
                                                            </button>
                                                        </div>
                                                    }
                                                </div>
                                            }
                                            {
                                                c?.type === 1 &&
                                                <div className="flex gap-x-4 text-black font-semibold">
                                                    {listAction.map((action) => (
                                                        <div
                                                            key={`index-${action.name} w-auto`}
                                                            className={`${action.color} px-4 py-2 rounded-md cursor-pointer `}
                                                        >
                                                            <button
                                                                className={``}
                                                                onClick={(e) => action.onClick(e, c)}
                                                            >
                                                                {action.name}
                                                            </button>
                                                        </div>
                                                    ))}
                                                </div>
                                            }
                                        </div>
                                    </div>
                                </Link>
                            );
                        }}
                    />
                }
            </div>
        </div>
    );
}
