'use client';
import { IConversation } from "@/src/types/Conversation";
import axiosClient from "@/src/utils/axios/axiosClient";
import Image from "next/image";
import Link from "next/link";
import { ReactElement, SyntheticEvent, useEffect, useState } from "react";
import { IoMdAdd } from "react-icons/io";
import { MdOutlineBookmarkAdded } from "react-icons/md";
import SearchNoResult from "../contact/components/SearchNoResult";
import { useAppSelector } from "@/src/redux/hook";
import userAvatar from "@/public/user.png";
interface ListUserProps {
    dataSearch: any;
}

export default function ListUser({ dataSearch }: ListUserProps): ReactElement {
    const [addedContact, setAddedContact] = useState();
    const localStorageData = useAppSelector((state: any) => state.user);
    const [dataStorage, setDataStorage] = useState<any>([{}] as any);
    useEffect(() => {
        const dataString = localStorage.getItem("Othername");

        if (dataString) {
            setDataStorage(JSON.parse(dataString));
        }
    }, [localStorageData]);
    const handleAddContact = (e: any, c: any) => {
        e.stopPropagation();
        const dataBody = { type: 1, memberIds: [c._id] };
        axiosClient()
            .post(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations`, dataBody)
            .then(() => {
                // (response);
                setAddedContact(c._id);
            })
            .catch((error: any) => {
                console.log(error);
            });
    };
    const handleImageError = (event: SyntheticEvent<HTMLImageElement, Event>) => {

        const imgElement = event.target as HTMLImageElement;
        imgElement.src = userAvatar.src;
    };
    return (
        <div className="p-4 flex flex-col gap-y-6 flex-shrink-0 rounded-2xl  h-[90vh] lg:h-[85vh] mx-4 mt-4 ">
            <div className="flex flex-col gap-y-2 lg:min-w-[300px] md:min-w-[300px]">
                {dataSearch.length === 0 ? (
                    <SearchNoResult />
                ) : (
                    dataSearch.map((c: IConversation | any) => {
                        return (
                            <Link href={`#`} key={c._id}>
                                <div className="flex gap-x-2 items-center py-2 px-2 rounded-lg">
                                    <Image
                                        src={
                                            c?.avatar === "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg"
                                                ? "/default_avatar.jpg"
                                                : c?.avatar
                                        }
                                        sizes="(max-width: 56px) 56px, 56px"
                                        alt={"Logo"}
                                        loading="eager"
                                        onError={handleImageError}
                                        priority
                                        width={56}
                                        height={56}
                                        className="rounded-full object-cover h-[56px]"
                                    />
                                    <div className="flex gap-x-8 justify-between h-full items-center flex-grow">
                                        <div>
                                            <p className="text-[.9375rem] font-semibold md:max-w-[72px] lg:max-w-[185px] text-ellipsis line-clamp-1">
                                                {dataStorage[c.idOrder] ? dataStorage[c.idOrder] : c?.username}
                                            </p>
                                        </div>
                                    </div>
                                    {addedContact === c._id ? (
                                        <MdOutlineBookmarkAdded />
                                    ) : (
                                        // <Image
                                        //     src={AddIcon}
                                        //     width={28}
                                        //     alt="Add contact"
                                        //     className="hover:opacity-50"
                                        //     onClick={(e) => {
                                        //         handleAddContact(e, c);
                                        //     }}
                                        // />
                                        <IoMdAdd size={28} className="hover:opacity-50 text-light-thPrimary dark:text-thPrimary"
                                            onClick={(e) => {
                                                handleAddContact(e, c);
                                            }} />
                                    )}
                                </div>
                            </Link>
                        );
                    })
                )}
            </div>
        </div>
    );
}
