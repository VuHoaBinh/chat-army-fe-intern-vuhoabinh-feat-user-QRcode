import React, { ReactElement } from 'react';

interface Props {
    children: ReactElement;
}

const LayoutBox = ({ children }: Props): ReactElement => {

    return (
        <div className='flex w-full h-full flex-col justify-center dark:bg-thNewtral1 bg-light-thNewtral1 rounded-2xl dark:text-white text-black'>
            {children}
        </div>
    );
};

export default LayoutBox;