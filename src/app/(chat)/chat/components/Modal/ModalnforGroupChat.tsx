'use client';
import FormAddGroup from "@/src/app/(chat)/chat/conversation/[id]/components/group/FormCreateGroup";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { updateUserCreateGroup } from "@/src/redux/slices/listUserCreateGroupSlice";
import getAxiosClient from "@/src/utils/axios/axiosClient";
import { Alert, Avatar, Snackbar, Tooltip } from "@mui/material";
import AvatarGroup from '@mui/material/AvatarGroup';
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Typography from "@mui/material/Typography";
import Image from "next/image";
import React, { ReactElement, SyntheticEvent, cache, useCallback, useEffect, useMemo, useRef, useState } from "react";
import { AiFillCloseSquare, AiOutlineCamera, AiOutlineEdit, AiOutlineLink } from "react-icons/ai";
import { IoIosLogOut } from "react-icons/io";
import { IoSettingsOutline } from "react-icons/io5";
import { LiaTimesSolid } from "react-icons/lia";
import { PiShareFatBold } from "react-icons/pi";
import { RiFileCopyLine } from "react-icons/ri";
import Members from "../../conversation/[id]/components/group/Members";
import ModalShareGroupChat from "./ModalShareGroupChat";

import { setListConversations, setOtherUser } from "@/src/redux/slices/conversationSlice";
import { setDetailChat } from "@/src/redux/slices/detailChatSlice";
import { filterConversation, sortConversationByNewest } from "@/src/utils/conversations/filterConversation";
import { uploadFile } from "@/src/utils/upload/getUploadToken";
import { useRouter } from "next/navigation";
import Cropper from "react-easy-crop";
import { BiImageAlt } from "react-icons/bi";
import { toast } from "react-toastify";
import ModalAddMembers from "../../conversation/[id]/components/Modal/ModalAddMembers";
import ModalChangeName from "../../conversation/[id]/components/Modal/ModalChangeName";
import ModalConfirm from "../../conversation/[id]/components/Modal/ModalConfirm";
import ModalSettingGroup from "../../conversation/[id]/components/Modal/ModalSettingGroup";
import getCroppedImg from "../../profile/[id]/components/cropImage";
import userAvatar from "@/public/user.png";
const style = {
    position: "absolute" as const,
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,

    border: "2px solid #000",
    boxShadow: 24,
    "@media (max-width: 600px)": {
        width: "90%" // Thay đổi kích thước cho màn hình nhỏ
    }

};
const featGroup = [
    {
        id: 1,
        name: "Quản lý nhóm",
        icon: <IoSettingsOutline size={24} />,
        color: ""
    },
    {
        id: 2,
        name: "Rời nhóm",
        icon: <IoIosLogOut size={24} />,
        color: "text-red-500"
    }
];
const fetchDetailChat = cache((id: string) =>
    getAxiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${id}`
    )
);

export default function ModalInforGroupChat({
    isOpen,
    onClose,
    dataGroup,
    socket,
    setIsOpenInfo


}: {
    isOpen: boolean;
    onClose: () => void;
    dataGroup?: any;
    socket: any;
    reminiscentName?: string;
    setIsOpenInfo: any;


}): ReactElement {
    const { detailChat } = useAppSelector((state) => state.user);
    const { conversation } = useAppSelector((state) => state.user);
    const { user } = useAppSelector((state) => state.user);
    const spanRef = useRef<HTMLSpanElement>(null);
    const [open, setOpen] = React.useState(false);
    const [share, setShare] = React.useState(false);
    const [urlGroup, setUrlGroup] = React.useState("");
    const dispatch = useAppDispatch();
    const [isShowMembers, setIsShowMembers] = useState<boolean>(false);
    const [isShowModal, setIsShowModal] = useState<string>("");
    // const [detailChat, setDetailChat] = useState<any>({});
    const [commonGroup, setCommonGroup] = useState<any>([]);

    const [isEditUserName, setIsEditUserName] = useState(false);

    const [, setOtherUsername] = useState<any>("");

    const modalEditUserRef = useRef<HTMLDivElement>(null);
    const [dataStorage, setDataStorage] = useState<any>([]);
    const localStorageData = useAppSelector((state: any) => state.user);

    useEffect(() => {
        const dataString = localStorage.getItem("Othername");
        if (dataString) {
            setDataStorage(JSON.parse(dataString));
        }
    }, [localStorageData]);
    const handleEditUsername = (e: any, contact: any) => {
        e.stopPropagation();
        setOtherUsername(
            dataStorage[contact?.idOther]
                ? dataStorage[contact?.idOther]
                : contact?.name
        );
        setIsEditUserName(!isEditUserName);

    };
    const handleClickOutside = (event: { target: any }) => {
        if (
            modalEditUserRef.current &&
            !modalEditUserRef.current.contains(event.target)
        ) {
            setIsEditUserName(false);
            onClose();
        }
    };
    useEffect(() => {
        document.addEventListener("mousedown", handleClickOutside);

        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, []);
    const handleClose = (
        event?: React.SyntheticEvent | Event,
        reason?: string
    ) => {

        if (reason === "clickaway") {
            return;
        }

        setOpen(false);
    };
    // useEffect(() => {
    //     (async () => {
    //         try {
    //             const { data: detail }: any = await (fetchDetailChat(dataGroup?._id));
    //             setDetailChat(detail);
    //         } catch (err) {
    //             (err);
    //         }
    //     })();
    // }, [dataGroup?._id]);
    const handleCopyLink = () => {
        if (!spanRef.current) return;
        const data = spanRef.current?.textContent;
        setOpen(true);
        navigator.clipboard.writeText(data || "");
    };
    const handleShare = (e: any) => {
        e.stopPropagation();
        setShare(true);
        setUrlGroup(spanRef.current?.textContent || "");
    };
    const handleShowListMember = () => {
        setIsShowMembers(true);
    };

    const [showChangName, setShowChangName] = useState(false);
    const [selectedAvatar, setSelectedAvatar] = useState<File | null>();
    const [isShowModalChangeAvatar, setIsShowModalChangeAvatar] = useState<boolean>(false);
    const [isShowModalConfirm, setIsShowModalConfirm] = useState<boolean>(false);
    const [isShowModalAddMembers, setIsShowModalAddMembers] = useState<boolean>(false);
    const [isShowSetting, setIsShowSetting] = useState<boolean>(false);
    const router = useRouter();
    const handleOutGroup = async () => {
        try {
            const response = await getAxiosClient().delete(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/group/${detailChat?._id}/out`);
            if (response.status === 200) {
                toast.success("Rời khỏi nhóm thành công");
                router.push("/chat");
                const newListConversations = conversation?.listConversation?.filter((item: any) => item._id !== detailChat?._id);
                dispatch(
                    setListConversations(
                        sortConversationByNewest(filterConversation(newListConversations, user.id))
                    )
                );
            }
        } catch (err) {
            console.log(err);
        }
    };
    const handleChangeAvatar = async (file: File) => {
        if (file) {
            const fileData = new FormData();
            fileData.append("files", file);
            try {
                const data = await uploadFile(fileData);
                const linkImageAvatar = data[0].link;
                if (linkImageAvatar) {
                    try {
                        const response = await getAxiosClient().patch(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${detailChat?.id}`,
                            {
                                avatar: linkImageAvatar
                            }
                        );
                        if (response.status === 200) {
                            toast.success("Đổi ảnh đại diện thành công");
                            setIsShowModalChangeAvatar(false);
                            dispatch(setOtherUser({
                                otherUsername: response.data.name,
                                otherAvatar: linkImageAvatar
                            }));
                            const newListConversations = conversation?.listConversation?.map((item: any) => {
                                if (item?.id === detailChat?.id) {
                                    return {
                                        ...item,
                                        avatar: linkImageAvatar
                                    };
                                }
                                return item;
                            });
                            dispatch(
                                setListConversations(
                                    sortConversationByNewest(filterConversation(newListConversations, user.id))
                                )
                            );
                            dispatch(setDetailChat({
                                ...detailChat,
                                avatar: linkImageAvatar
                            }));
                        }
                    } catch (err: any) {
                        toast.error(err.response.data.message);
                    }
                }
            } catch (error: any) {
                const errorMessage = error instanceof Error ? error.message : String(error);
                toast.error(errorMessage);
            }
        }
    };
    const [crop, setCrop] = useState({ x: 0, y: 0 });
    const [zoom, setZoom] = useState(1);
    const [croppedAreaPixels, setCroppedAreaPixels] = useState(null);
    const [fileName, setFileName] = useState("");


    const onCropComplete = useCallback(
        (croppedArea: any, croppedAreaPixels: any) => {
            setCroppedAreaPixels(croppedAreaPixels);
        },
        []
    );

    const avaImage = useMemo(() => {
        if (!selectedAvatar) return "";
        const blob = selectedAvatar as Blob;
        return URL.createObjectURL(blob);
    }, [selectedAvatar]);


    const updateCroppedImage = useCallback(async () => {
        try {
            const croppedImage = (await getCroppedImg(
                avaImage,
                croppedAreaPixels
            )) as Blob;
            // setCroppedImage(URL.createObjectURL(croppedImage));

            const newFileCrop = new File([croppedImage], fileName, { type: "image/jpeg" });
            handleChangeAvatar(newFileCrop);
            // setIsResizeAvatar(false);
        } catch (e) {
            console.error(e);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [croppedAreaPixels]);
    const handleImageError = (event: SyntheticEvent<HTMLImageElement, Event>) => {

        const imgElement = event.target as HTMLImageElement;
        imgElement.src = userAvatar.src;
    };
    return (
        <div>


            {
                !isShowSetting &&
                <Modal
                    open={isOpen}
                    onClose={onClose}
                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                    className={``}
                    onBackdropClick={() => { dispatch(updateUserCreateGroup([])); }}
                >

                    {
                        !showChangName && !isShowModalChangeAvatar ?
                            <Box sx={style} className={`${isEditUserName ? `h-0 relative` : ``}w-[90%] md:w-full ${isShowMembers ? "h-[90%]" : ""}dark:bg-thNewtral1 bg-light-thNewtral z-50 border-none`}>
                                {/* {isEditUserName &&
                                    <div ref={modalEditUserRef} >


                                        <EditUserName
                                            contact={{ user: dataGroup }}
                                            onClose={() => {
                                                setIsEditUserName(false);
                                                onClose();
                                            }}
                                            btnBack={() => { setIsEditUserName(false); }}
                                            onSubmit={(otherUsername) => {
                                                setOtherUsername(otherUsername);
                                                setIsEditUserName(false);
                                            }}
                                            otherUsername={reminiscentName ? reminiscentName : otherUsername}

                                        />
                                    </div>



                                } */}

                                {
                                    isShowMembers && <Members close={() => setIsShowMembers(false)} setIsShowModalAddMembers={setIsShowModalAddMembers} fetchDetailChat={fetchDetailChat} setIsShowModalConfirm={setIsShowModalConfirm} />
                                }
                                {
                                    isShowModal && <FormAddGroup detailChat={detailChat} setIsOpen={setIsShowModal} idConversation={detailChat?._id} action={isShowModal} setCommonGroup={setCommonGroup} commonGroup={commonGroup} fetchDetailChat={fetchDetailChat} />
                                }
                                {
                                    isShowModalConfirm && <ModalConfirm close={() => setIsShowModalConfirm(false)} handleConfirm={handleOutGroup} message="Bạn chắc chắn muốn out group này?" />
                                }
                                {
                                    isShowModalAddMembers && <ModalAddMembers close={() => setIsShowModalAddMembers(false)} members={detailChat?.members} idConversation={detailChat?._id} fetchDetailChat={fetchDetailChat} />
                                }
                                <div className="relative w-full">
                                    {share && (
                                        <ModalShareGroupChat
                                            onClose={() => setShare(false)}
                                            urlGroup={urlGroup}
                                            socket={socket}

                                        />
                                    )}


                                </div>

                                <div className={`flex justify-between items-center px-4 py-2 ${isEditUserName || isShowMembers ? "hidden" : ""}`}>
                                    <Typography id="modal-modal-title" variant="h6" component="h2">
                                        Thông tin nhóm
                                    </Typography>
                                    <LiaTimesSolid
                                        size={28}
                                        className="hover:cursor-pointer hover:opacity-70"
                                        onClick={onClose}
                                    />
                                </div>

                                {!isEditUserName && (<hr className="bg-black dark:bg-white h-[1px] w-full" />)}
                                <div className={`w-full flex items-center justify-center mt-5 flex-col gap-y-3 ${isEditUserName || isShowMembers ? "hidden" : ""}`}>
                                    <div className="relative w-20 h-20  rounded-full">
                                        <Image
                                            fill
                                            alt="avt"
                                            src={dataGroup?.avatar}
                                            onError={handleImageError}
                                            className="object-cover rounded-full overflow-hidden"

                                        />
                                        <AiOutlineCamera
                                            size={30}
                                            className=" text-[#b5b5b5] font-bold rounded-full border-[1px] p-1 cursor-pointer z-20 absolute bottom-0 right-[-4px] border-white"
                                            onClick={() => setIsShowModalChangeAvatar(true)}
                                        />
                                    </div>

                                    {/* <div className="text-xl font-bold flex gap-x-2 items-center">{reminiscentName ? reminiscentName : detailChat?.name} </div> */}
                                    <div className="flex items-center justify-center">
                                        <p className="text-lg font-semibold">
                                            {detailChat?.name}
                                        </p>
                                        <span className="ml-2 cursor-pointer" onClick={() => setShowChangName(true)}>
                                            <AiOutlineEdit size={24} />

                                        </span>

                                        <div
                                            className=" rounded-md md:pr-0 w-[1.5rem] cursor-pointer"
                                            onClick={(e) => handleEditUsername(e, dataGroup)}
                                        >
                                            {/* <Image
                                    src={EditIcon}
                                    width={20}
                                    alt="edit icon"
                                    className="m-2"
                                /> */}
                                            {/* <BiEditAlt size={20} className="m-2 text-black dark:text-white" /> */}
                                        </div>
                                    </div>
                                    <button className="px-10 py-1 dark:bg-[#FCD535] bg-light-thPrimary hover:bg-[#e0c34d] rounded-sm mb-2" onClick={() => {
                                        onClose();
                                        setIsOpenInfo(false);

                                    }

                                    }>Nhắn tin</button>
                                    <hr className="bg-black dark:bg-white h-[1px] w-full" />
                                    <div className="flex flex-col w-full px-4 gap-4">
                                        <div className="font-bold text-[16px]">{`Thành viên (${dataGroup?.member.length})`}</div>
                                        <div className="flex justify-start ">
                                            <AvatarGroup
                                                max={5}
                                                total={detailChat?.members.length}
                                                onClick={handleShowListMember}
                                                className="hover:cursor-pointer"
                                            >
                                                {dataGroup?.member.map((member: any, index: number) => (
                                                    <Tooltip title={`${member.username}`} key={index}>
                                                        <Avatar
                                                            alt={`${member.username}`}
                                                            src={`${member.avatar}`}
                                                            className={`MuiAvatar-root hover:cursor-pointer`}
                                                            onClick={(e: any) => { e.stopPropagation(); }
                                                            }
                                                        />
                                                    </Tooltip>

                                                ))}
                                            </AvatarGroup>
                                        </div>
                                    </div>
                                    <hr className="w-full  h-1" />
                                    <div className="w-full">
                                        <div
                                            className="flex flex-row justify-between  rounded-sm dark:hover:bg-thNewtral2 hover:bg-light-thNewtral2 w-full h-14 relative items-center hover:cursor-pointer  p-3"
                                            onClick={handleCopyLink}
                                        >
                                            <div className="  flex gap-3 items-center">
                                                <AiOutlineLink size={24} />
                                                <div className="md:w-[240px] w-[190px] truncate">
                                                    Link tham gia nhóm
                                                    <br />
                                                    <span ref={spanRef} className="text-blue-500 ">

                                                        {`${process.env.NEXT_PUBLIC_DAK_API_SHARE_GROUP}/g/${detailChat?.inviteId}`}
                                                    </span>
                                                </div>
                                            </div>
                                            <div className="flex gap-3">
                                                <RiFileCopyLine
                                                    size={30}
                                                    className="rounded-full p-1 bg-white border-[1px] text-neutral-700 hover:bg-thNewtral2"
                                                />
                                                <PiShareFatBold
                                                    size={30}
                                                    className="rounded-full p-1 bg-white border-[1px]   text-neutral-700 hover:bg-thNewtral2"
                                                    onClick={(e: any) => handleShare(e)}
                                                />
                                            </div>
                                        </div>

                                        {featGroup.map((feat) => (
                                            <>

                                                <hr className="w-[90%] absolute right-0" />
                                                <div
                                                    className={`flex flex-row justify-between  rounded-sm dark:hover:bg-thNewtral2 hover:bg-light-thNewtral2 w-full h-14 relative items-center hover:cursor-pointer  p-3 ` + feat.color}
                                                    onClick={handleCopyLink}
                                                >

                                                    <div className="  flex gap-3 items-center">

                                                        {feat.icon}
                                                        <div className="w-[240px] truncate">

                                                            {feat.name}
                                                            <br />

                                                        </div>
                                                    </div>
                                                </div>

                                            </>
                                        ))}
                                    </div>

                                </div>

                                <Snackbar
                                    sx={{ height: "100%" }}
                                    open={open}
                                    autoHideDuration={3000}
                                    onClose={handleClose}
                                    anchorOrigin={{ vertical: "top", horizontal: "center" }}
                                >
                                    <Alert
                                        onClose={handleClose}
                                        sx={{
                                            color: "white",
                                            backgroundColor: "#1f211d",
                                            "& .MuiAlert-icon": { display: "none" },
                                            "& .MuiAlert-action": { display: "none" }
                                        }}
                                    >
                                        Đã sao chép
                                    </Alert>
                                </Snackbar>
                            </Box> :
                            <Box>
                                {
                                    // Hiển thị Modal đổi tên
                                    showChangName &&
                                    <ModalChangeName setIsShowModal={setShowChangName} name={detailChat?.name} type={detailChat?.type} idConversation={detailChat?.id} />
                                }
                                {
                                    // hiển thị modal Đổi avatar group
                                    isShowModalChangeAvatar &&
                                    <div className="relative z-50 w-full max-w-md  max-[480px]:w-[90%] max-h-full p-4 m-auto text-thWhite bg-thDark-background border border-thPrimary rounded-md mt-5">
                                        <div className="flex gap-x-4 items-center justify-between">
                                            <p className="text-lg font-semibold">Cập nhật ảnh đại diện</p>
                                            <div className="cursor-pointer" onClick={() => setIsShowModalChangeAvatar(false)}>
                                                <AiFillCloseSquare size={40} />
                                            </div>
                                        </div>
                                        <div className="w-full text-center mt-2 flex justify-center items-center bg-thNewtral2 hover:bg-thPrimaryHover rounded-md">
                                            <BiImageAlt size={24} />
                                            <label htmlFor="avatar" className="py-2 text-center ">
                                                Tải ảnh lên từ máy tính
                                            </label>
                                        </div>
                                        {
                                            selectedAvatar &&
                                            <div>
                                                <div className="relative h-52 mt-2">
                                                    <Cropper
                                                        image={avaImage}
                                                        crop={crop}
                                                        zoom={zoom}
                                                        aspect={3 / 3}
                                                        onCropChange={setCrop}
                                                        onCropComplete={onCropComplete}
                                                        onZoomChange={setZoom}
                                                        cropShape="round"
                                                    />
                                                </div>
                                                <input
                                                    type="range"
                                                    value={zoom}
                                                    min={1}
                                                    max={3}
                                                    step={0.1}
                                                    aria-labelledby="Zoom"
                                                    onChange={(e) => {
                                                        setZoom(parseFloat(e.target.value));
                                                    }}
                                                    className="w-full h-3 bg-thNewtral2  md:bg-thPrimaryHover   md:active:bg-thPrimary rounded-lg appearance-none cursor-pointer accent-thPrimary md:accent-thNewtral2 mt-2"
                                                />
                                            </div>
                                        }
                                        <input type="file" id="avatar"
                                            className="hidden"
                                            onChange={(e) => {
                                                setSelectedAvatar(e.target.files?.[0]);
                                                setFileName(e.target.files?.[0]?.name || "");
                                            }}
                                            accept="image/*" />
                                        <div className="w-full flex justify-end mt-2 items-center">
                                            <button className="w-fit rounded-md py-2 bg-gray-500 text-black font-bold px-4"
                                                onClick={() => {
                                                    setSelectedAvatar(null);
                                                    setFileName("");
                                                    setIsShowModalChangeAvatar(false);
                                                }}
                                            >Hủy</button>
                                            <button className="w-fit rounded-md py-2 bg-blue-700 text-white font-bold px-4 ml-2"
                                                onClick={updateCroppedImage}
                                            >Xác nhận</button>
                                        </div>
                                    </div>
                                }
                            </Box>
                    }
                </Modal>
            }
            {
                isShowSetting &&
                <ModalSettingGroup close={() => setIsShowSetting(false)} />
            }
        </div >
    );
}
