import { useAppSelector } from "@/src/redux/hook";
import { updateLocalStorageData } from "@/src/redux/slices/localStorageSlice ";
import Image from "next/image";
import { ReactElement, useEffect, useRef, useState } from "react";
import { IoIosArrowBack } from "react-icons/io";
import { LiaTimesSolid } from "react-icons/lia";
import { useDispatch } from "react-redux";
interface EditUserNameProps {
    contact: any;
    onClose: () => void;
    onSubmit: (otherUsername: string) => void;
    otherUsername: string;
    btnBack?: () => void;


}
const EditUserName = ({
    contact,
    onClose,
    onSubmit,
    otherUsername,
    btnBack

}: EditUserNameProps): ReactElement => {
    console.log("name", contact?.user?.name);
    const { user } = useAppSelector((state) => state.user);
    const [inputValue, setInputValue] = useState(
        otherUsername !== "" ? otherUsername : contact?.user?.name
    );
    const dispatch = useDispatch();
    const inputRef = useRef<HTMLInputElement>(null);
    useEffect(() => {
        if (inputRef.current) {
            inputRef.current.focus();
            inputRef.current.select();
        }
    }, []);
    const handleSubmitEditUserName = (inputValue: string, contact: any) => {
        const data = { [contact.user.idOther]: inputValue };
        console.log("value input", data, contact, user.id);
        dispatch(updateLocalStorageData(data[contact?.user?.idOther] || ""));
        const existingData = JSON.parse(localStorage.getItem("Othername") || "{}");

        // Thêm dữ liệu mới vào dữ liệu hiện có
        const newData = { ...existingData, ...data };

        // Lưu dữ liệu đã cập nhật vào localStorage
        localStorage.setItem("Othername", JSON.stringify(newData));

        onSubmit(inputValue);
    };
    console.log("contact", contact);

    return (
        <div
            className="
        fixed 
        top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2
       dark:bg-thNewtral1 bg-light-thNewtral1 
        rounded-lg 
        overflow-hidden 
  
        transition-all 
        duration-200 
        ease-in-out
        min-w-[340px] 
        min-h-[300px] 
        max-w-[95vw] 
        p-3 
        z-50
        flex 
        gap-x-4 gap-y-2
        flex-col
   
       
    "
        >
            <div className="flex  items-center justify-between"><div className="flex gap-x-2 items-center">
                {btnBack ? (<div className="dark:hover:bg-neutral-600 hover:bg-light-thNewtral2 cursor-pointer rounded-full p-1"><IoIosArrowBack size={24} onClick={btnBack} /></div>) : (<></>)} Đặt tên gợi nhớ  </div>
            <LiaTimesSolid
                size={28}
                // color={"black"}
                className="font-bold  min-w-[10px] dark:hover:bg-neutral-600 hover:bg-light-thNewtral2 cursor-pointer rounded-full p-1"
                onClick={(e) => {
                    e.stopPropagation();
                    onClose();
                }}
            />

            </div>
            <hr className="bg-black dark:bg-white h-[1px] w-full" />
            <div className=" flex items-center justify-center w-full">
                <Image
                    src={contact?.user?.avatar === "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg" ? "/default_avatar.jpg" : contact?.user?.avatar}
                    alt="avartar"
                    width={50}
                    height={50}
                    className="rounded-full w-[50px] h-[50px] object-cover"
                />
            </div>
            <div className="text-sm flex flex-col ">
                <span>
                    Hãy đặt tên cho <strong className="text-[16px]">{otherUsername}</strong>{" "}
                    một cái tên dễ nhớ.
                </span>
                <span>Lưu ý: Tên gợi nhớ sẽ chỉ hiển thị riêng với bạn.</span>
            </div>
            <div className="flex items-center border rounded-md relative w-full mb-4 dark:border-thPrimary border-light-thPrimary">
                <input
                    type="text"
                    ref={inputRef}
                    value={inputValue}
                    placeholder=""
                    className="bg-transparent outline-none p-2 w-full pr-12 pl-3 "
                    onClick={(e) => {
                        e.stopPropagation();
                    }}
                    onChange={(e) => {
                        e.stopPropagation();
                        setInputValue(e.target.value);
                    }}
                />
            </div>
            <div className="flex justify-end gap-x-4">
                <button
                    className="bg-gray-400 p-3 rounded-md"
                    onClick={(e) => {
                        e.stopPropagation();
                        onClose();
                    }}
                >
                    Hủy
                </button>
                <button
                    className=" dark:bg-thPrimary bg-light-thPrimary p-3 rounded-md hover:opacity-75"
                    onClick={(e) => {
                        e.stopPropagation();
                        handleSubmitEditUserName(inputValue, contact);
                    }}
                >
                    Xác Nhận
                </button>
            </div>
        </div>
    );
};

export default EditUserName;
