'use client';
import { useAppDispatch } from "@/src/redux/hook";
import { updateUserCreateGroup } from "@/src/redux/slices/listUserCreateGroupSlice";
import getAxiosClient from "@/src/utils/axios/axiosClient";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Typography from "@mui/material/Typography";
import Image from "next/image";
import { ReactElement, SyntheticEvent, cache, useEffect, useRef, useState } from "react";

import { BiEditAlt } from "react-icons/bi";
import { FiTrash } from "react-icons/fi";
import { HiOutlineUserGroup } from "react-icons/hi";
import { IoBanSharp } from "react-icons/io5";
import { LiaTimesSolid } from "react-icons/lia";
import { TiWarningOutline } from "react-icons/ti";
import CommonGroup from "../../conversation/[id]/components/group/CommonGroup";
import FormAddGroup from "../../conversation/[id]/components/group/FormCreateGroup";
import EditUserName from "./EditUserName";
import userAvatar from "@/public/user.png";
const style = {
    position: "absolute" as const,
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,


    boxShadow: 24,
    "@media (max-width: 600px)": {
        width: "90%" // Thay đổi kích thước cho màn hình nhỏ
    }

};

const fetchDetailChat = cache((id: string) =>
    getAxiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${id}`
    )
);

export default function ModalInforConversation({
    isOpen,
    onClose,
    conversation,

    reminiscentName,
    commonGroup,
    setIsOpenInfo,
    openCall
}: {
    isOpen: boolean;
    onClose: () => void;
    conversation?: any;

    reminiscentName?: string;
    commonGroup?: any;
    setIsOpenInfo: any;
    openCall?: () => void;
}): ReactElement {
    const actions = [
        {
            id: 1,
            name: "Nhóm chung",
            icon: <HiOutlineUserGroup size={24} />,
            onclick: () => {
                handleShowCommonGroup();
            }

        },

        {
            id: 2,
            name: "Chặn tin nhắn và cuộc gọi",
            icon: <IoBanSharp size={24} />,
            onclick: () => { }
        }
        , {
            id: 3,
            name: "Báo xấu",
            icon: <TiWarningOutline size={24} />,
            onclick: () => { }
        }
        , {
            id: 4,
            name: "Xóa khỏi danh sách bạn bè",
            icon: <FiTrash size={24} />,
            onclick: () => { }
        }
    ];

    const [commonGroupInfor, setCommonGroupInfor] = useState<any>([]);

    const dispatch = useAppDispatch();

    const [isShowModal, setIsShowModal] = useState<string>("");
    const [detailChat, setDetailChat] = useState<any>({});

    const [otherUsername, setOtherUsername] = useState<any>("");

    const [isEditUserName, setIsEditUserName] = useState(false);
    const modalEditUserRef = useRef<HTMLDivElement>(null);
    const [isShowCommonGroup, setIsShowCommonGroup] = useState<boolean>(false);
    const handleEditUsername = (e: any, contact: any) => {
        e.stopPropagation();

        // setOtherUsername(
        //     dataStorage[contact.id]
        //         ? dataStorage[contact.id]
        //         : contact.name
        // );
        setOtherUsername(

            contact.name
        );
        setIsEditUserName(!isEditUserName);
        isOpen = false;
    };
    const handleShowCommonGroup = () => {
        setIsShowCommonGroup(true);
    };
    const handleClickOutside = (event: { target: any }) => {
        if (
            modalEditUserRef.current &&
            !modalEditUserRef.current.contains(event.target)
        ) {
            setIsEditUserName(false);
            onClose();
        }
    };
    useEffect(() => {
        document.addEventListener("mousedown", handleClickOutside);

        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, []);

    useEffect(() => {
        (async () => {
            try {
                const { data: detail }: any = await (fetchDetailChat(conversation?._id));
                setDetailChat(detail);
            } catch (err) {
                console.log(err);
            }
        })();
    }, [conversation?._id]);

    if (isShowModal) {
        return (<>
            {
                <FormAddGroup setIsOpen={setIsShowModal} idConversation={conversation?.id} action={isShowModal} detailChat={detailChat} setCommonGroup={setCommonGroupInfor} fetchDetailChat={fetchDetailChat} commonGroup={commonGroup || commonGroupInfor} />
            }</>);
    }
    const handleImageError = (event: SyntheticEvent<HTMLImageElement, Event>) => {

        const imgElement = event.target as HTMLImageElement;
        imgElement.src = userAvatar.src;
    };
    return (

        <div>



            <Modal
                open={isOpen}
                onClose={onClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
                className={``}
                onBackdropClick={() => { dispatch(updateUserCreateGroup([])); }}
            >

                <Box sx={style} className={` ${isEditUserName ? "relative" : "h-[90%] overflow-y-scroll"} w-[90%] md:w-full  ${isShowModal && 'hidden'} dark:bg-thNewtral1 bg-light-thNewtral z-50 border-none `}>
                    <>

                        {
                            isShowCommonGroup && <CommonGroup setIsShow={setIsShowCommonGroup} setIsOpenForm={setIsShowModal} data={commonGroup} />
                        }
                        {isEditUserName &&
                            <div ref={modalEditUserRef} >


                                <EditUserName
                                    contact={{ user: conversation }}
                                    onClose={() => {
                                        setIsEditUserName(false);
                                        onClose();
                                    }}
                                    btnBack={() => { setIsEditUserName(false); }}
                                    onSubmit={(otherUsername) => {
                                        setOtherUsername(otherUsername);
                                        setIsEditUserName(false);
                                    }}
                                    otherUsername={reminiscentName ? reminiscentName : otherUsername}

                                />
                            </div>



                        }</>

                    <div className={`flex justify-between items-center px-4 py-2 ${isEditUserName || isShowCommonGroup ? "hidden" : ""}`}>
                        <Typography id="modal-modal-title" variant="h6" component="h2">
                            Thông tin tài khoản
                        </Typography>
                        <LiaTimesSolid
                            size={28}
                            className="hover:cursor-pointer hover:opacity-70"
                            onClick={onClose}
                        />
                    </div>


                    <div className={`w-full flex items-center justify-center  flex-col  ${isEditUserName || isShowCommonGroup ? "hidden" : ""}`}>
                        <div className="w-full h-40 relative">
                            <Image
                                alt="background infor partner"
                                src={conversation?.directUser.background_img}
                                className="object-cover relative "
                                fill
                            />


                        </div>
                        <div className="relative w-20 h-12 "> <div className="absolute top-[-40px]  w-20 h-20  rounded-full border-[2px] border-white">

                            <Image
                                fill
                                alt="avt"
                                src={conversation?.avatar === "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg" ? '/default_avatar.jpg' : conversation?.avatar}
                                onError={handleImageError}
                                className="object-cover rounded-full overflow-hidden "

                            />

                        </div></div>
                        <div className="text-xl font-bold flex gap-x-2 items-center mb-4">{reminiscentName ? reminiscentName : conversation?.directUser?.username}
                            <div
                                className=" rounded-md md:pr-0 w-[1.5rem] cursor-pointer"
                                onClick={(e) => handleEditUsername(e, conversation)}
                            >
                                {/* <Image
                                    src={EditIcon}
                                    width={20}
                                    alt="edit icon"
                                    className="m-2"
                                /> */}
                                <BiEditAlt size={20} className="m-2 text-black dark:text-white" />
                            </div></div>
                        <div className="flex justify-evenly items-center  w-full gap-x-4 font-medium">
                            <button className="px-12 py-1 rounded-sm mb-2 dark:bg-thNewtral2 bg-light-thNewtral2 dark:hover:bg-neutral-600 hover:opacity-75" onClick={() => {
                                onClose();
                                // dispatch(setUserIsCalling(true));
                                // dispatch(updateCalling({ isCalling: true, conversationId: conversation?.id || "", userAcceptCall: user?.id, deviceAcceptCall: isMobile() ? "smartphone" : "desktop" })); 
                                if (openCall) openCall();
                                setIsOpenInfo(false);
                            }}>Gọi điện</button>
                            <button className="px-12 py-1  rounded-sm mb-2 dark:bg-thNewtral2 bg-light-thNewtral2 dark:hover:bg-neutral-600 hover:opacity-75" onClick={() => {
                                onClose();
                                setIsOpenInfo(false);
                            }}>Nhắn tin</button>
                        </div>

                        <div className="w-full  h-[5px] dark:bg-thNewtral2 bg-light-thNewtral2 mt-4" ></div>
                        <div className="flex flex-col w-full px-6 gap-4 mt-4">
                            <div className="font-semibold">Thông tin cá nhân</div>
                            <div className="flex items-center gap-x-10">
                                <span className="w-36 text-gray-400"> Username</span>

                                <div className=" text-start truncate w-full">{conversation?.directUser?.username}</div></div>
                            <div className="flex items-center gap-x-10">
                                <span className="w-36 text-gray-400 ">Email</span>
                                <div className="truncate text-start w-full">{conversation?.directUser?.email}</div></div>


                        </div>
                        <div className="w-full  h-[5px] dark:bg-thNewtral2 bg-light-thNewtral2 mt-4 " ></div>
                        {actions.map((feat, index) => (
                            <>


                                <div
                                    className={`flex flex-row justify-between  rounded-sm dark:hover:bg-thNewtral2 hover:bg-light-thNewtral1 w-full h-12 relative items-center hover:cursor-pointer  px-6 `}
                                    onClick={feat.onclick}
                                    key={feat.id}
                                >

                                    <div className="  flex gap-3 items-center">

                                        {feat.icon}
                                        <div className="w-[240px] truncate">

                                            {feat.id == 1 ? feat.name + ` (${commonGroup?.length})` : feat.name}
                                            <br />

                                        </div>
                                    </div>
                                    {index !== actions.length - 1 && <hr className="w-[85%] absolute bottom-0 right-0 dark:bg-light-thDark-background bg-light-thNewtral2 h-[1px] " />}
                                </div>

                            </>
                        ))}
                    </div>


                </Box>
            </Modal>

        </div >
    );
}
