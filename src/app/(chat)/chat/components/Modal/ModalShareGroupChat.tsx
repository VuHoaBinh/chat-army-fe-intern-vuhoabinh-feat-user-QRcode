"use client";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { updateUserCreateGroup } from "@/src/redux/slices/listUserCreateGroupSlice";
import { ReactElement, useState } from "react";
import { BiSearch } from "react-icons/bi";
import { LiaTimesSolid } from "react-icons/lia";
import ListContact from "../../contact/components/ListContact";
import ListUserShareLinkGroup from "../../contact/components/ListUserShareLinkGroup";
import { IForwardMessageData } from "@/src/types/Message";
import { EVENT_NAMES, MESSAGE_TYPE } from "@/src/constants/chat";
import { setLastMessage, setMessageForward } from "@/src/redux/slices/conversationSlice";
import { toast } from "react-toastify";

const ModalShareGroupChat = ({
    onClose,
    urlGroup,
    socket

}: {
    onClose: () => void;
    urlGroup: string;
    socket: any;

}): ReactElement => {
    const [inputValue, setInputValue] = useState("");
    const dispatch = useAppDispatch();
    const { listUserCreateGroup } = useAppSelector((state) => state.user) as any;

    const handleCloseCreateGroup = () => {
        onClose();
        dispatch(updateUserCreateGroup([]));
    };



    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setInputValue(e.target.value);
    };


    const handleShareLink = (urlGroup: string, ListUserShareLinkGroup: any) => {
        const listId = [] as any;
        ListUserShareLinkGroup.map((user: any) => {
            listId.push(
                user?.user.id
            );
        });
        for (const id of listId) {
            const forwardMessage: IForwardMessageData = {
                content: urlGroup,
                type: MESSAGE_TYPE.LINK,
                forward_conversation_id: id
            };
            socket.emit(EVENT_NAMES.CONVERSATION.FORWARD_MESSAGE, forwardMessage, (data: any) => {
                if (data && data.success) {
                    dispatch(setMessageForward(data.data));
                    if (id === listId[listId.length - 1]) {
                        dispatch(setLastMessage(data.data));
                    }
                } else {
                    toast.error("Không thể chia sẻ tin nhắn");
                }
            });

        }
        toast.success("Chia sẻ thành công");
        handleCloseCreateGroup();
    };

    return (
        <div
            className="
            absolute 
            top-[-30px]
          
           dark:bg-thNewtral1 bg-light-thNewtral1
            rounded-lg 
            overflow-hidden
            shadow-2xl
            transition-all
            duration-200
            ease-in-out
            w-full
            md:min-w-[600px]
            p-3
            z-50
            animate-c-show-popup
            origin-top-right
            md:min-h-[500px]
            sm:h-[90vh]
            flex gap-4
            flex-col
            "
        >
            <div className=" flex justify-between items-center">
                <span>Chia sẻ link nhóm chat</span>
                <LiaTimesSolid
                    size={28}
                    className="hover:cursor-pointer hover:opacity-70"
                    onClick={handleCloseCreateGroup}
                />
            </div>
            <hr className="bg-black dark:bg-white h-[1px] w-full" />

            <div className="text-sm flex flex-col max-h-[300px] md:mb-5 ">
                <div className="flex items-center rounded-md relative w-full h-full md:mb-3 border-[1px]">
                    <BiSearch className="ml-3 text-[#b5b5b5]" />
                    <input
                        type="text"
                        placeholder="Nhập kiếm hội thoại cần chia sẻ"
                        className="bg-transparent outline-none p-2 w-full pr-12 pl-3"
                        value={inputValue}
                        onChange={handleInputChange}
                    // onKeyDown={handleKeyDown}
                    />
                    {/* <div className="absolute w-full top-9">
                        {dataFilter.length > 0 && (
                            <UserSearchCreateGroup
                                listSearch={dataFilter}
                                onChecked={handleCheckedSearch}
                            />
                        )}
                    </div> */}
                </div>
            </div>
            <hr className="bg-black dark:bg-white h-[1px] w-full" />
            <div className="md:min-h-[340px] h-[520px] flex flex-col md:flex-row">
                <div
                    className={`overflow-y-auto ${listUserCreateGroup.length > 0 ? "md:w-[70%] w-full" : "w-full"
                    }  h-full`}
                >
                    <ListContact nameList="listFriend" serchUserGroup={inputValue} />
                </div>

                <div
                    className={` ${listUserCreateGroup.length > 0 ? "w-[30%] md:block hidden" : "w-0 hidden"
                    }  ml-4 `}
                >
                    <ListUserShareLinkGroup />
                </div>
                <div className={`md:hidden ${listUserCreateGroup.length > 0 ?
                    "block" : "hidden"} w-full h-40 `}>
                    <ListUserShareLinkGroup url={urlGroup} handleShareLink={() => handleShareLink(urlGroup, listUserCreateGroup)} />
                </div>
            </div>
            <div className=" justify-end gap-x-4 hidden md:flex">
                <button
                    className="bg-gray-400 p-3 rounded-md hidden md:block"
                    onClick={() => {
                        handleCloseCreateGroup();
                    }}
                >
                    Hủy
                </button>
                <button
                    type="button"
                    className=" dark:bg-thPrimary bg-light-thPrimary p-3 rounded-md hover:opacity-90 disabled:opacity-50 hidden md:block"
                    disabled={listUserCreateGroup.length < 1}
                    onClick={(e) => {
                        e.stopPropagation();
                        handleShareLink(urlGroup, listUserCreateGroup);
                    }}
                >
                    Chia Sẻ
                </button>
            </div>
        </div>
    );
};

export default ModalShareGroupChat;
