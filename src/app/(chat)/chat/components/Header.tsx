/* eslint-disable react-hooks/exhaustive-deps */
"use client";
import Logo from "@/public/DP 1.png";
import { EVENT_NAMES } from "@/src/constants/chat";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { updateCalling } from "@/src/redux/slices/callSlice";
import { setOtherUser } from "@/src/redux/slices/conversationSlice";
import {
    setArrayValueToSearch,
    toggleOpenHeaderSearch
} from "@/src/redux/slices/searchSlice";
import { setUserIsCalling, setUserLogin, setWaitCalling } from "@/src/redux/slices/userSlice";
import { IUserCall } from "@/src/types/Call";
import {
    deleteLoginCookies,
    getLoginCookies
} from "@/src/utils/auth/handleCookies";
import axiosClient from "@/src/utils/axios/axiosClient";
import { isMobile } from "@/src/utils/device/detechDevice";
import {
    clearDataInIndexDB,
    getAllDataFromIndexDB,
    storeDataInIndexDBById
} from "@/src/utils/search/useIndexDB";
import Image from "next/image";
import { useRouter } from "next/navigation";
import { ReactElement, cache, useEffect, useRef, useState } from "react";
import { BiSearch } from "react-icons/bi";
import { IoMdSettings } from "react-icons/io";
import { Socket, io } from "socket.io-client";
import { useDebounce } from "use-debounce";
import HeaderSearchBox from "../conversation/[id]/components/HeaderSearchBox";
import PopupCallInComing from "../conversation/[id]/components/PopupCall";
import { Avatar } from "./Avatar";
import MenuSetting from "./MenuSetting/MenuSetting";
import styles from "./layout.module.css";
import { useCookies } from "react-cookie";
import { addIdToListIdConversationDecoded, addListIdToListIdConversationDecoded } from "@/src/redux/slices/endToEndEncryptionSlice";

const fetchAllConversation = cache(() =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations?offset=0&status=[2]`
    )
);

export default function Header(): ReactElement {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [cookies, setCookie] = useCookies(["data"]);
    const [isSetting, setIsSetting] = useState<boolean>(false);
    const { user, conversation, search } = useAppSelector((state) => state.user);
    const dispatch = useAppDispatch();
    const cookieData = getLoginCookies();
    const { push } = useRouter();
    const [isLogin, setIsLogin] = useState<boolean>(true);
    const router = useRouter();
    const [converCalling, setConverCalling] = useState<IUserCall>();
    const [isPopup, setIsPopup] = useState<boolean>(false);
    const [isCalling, setIsCalling] = useState<boolean>(false);
    const [socketCall, setSocketCall] = useState<Socket<any, any> | any>(null);
    const [inputSearchValue, setInputSearchValue] = useState<string>("");

    const [value] = useDebounce(inputSearchValue, 200);

    useEffect(() => {
        if (value.trim() === "" && search.isOpenHeaderSearch) {
            dispatch(toggleOpenHeaderSearch());
        } else if (value.trim() !== "" && !search.isOpenHeaderSearch) {
            dispatch(toggleOpenHeaderSearch());
        }
    }, [value]);
    useEffect(() => {
        if (!user.is_calling) {
            handleCallOut();
        }
    }, [user.is_calling]);
    // tắt menu settings khi bấm ra ngoài
    const menuSettingRef = useRef<any>();
    const settingsIconRef = useRef<any>();
    // tắt headerSearchBox khi bấm ra ngoài
    const searchBoxRef = useRef<any>();
    const inputRef = useRef<any>();
    const handleCallAccept = () => {
        try {
            socketCall.emit(
                EVENT_NAMES.INDIVIDUAL_CALL.CALL_ACCEPT_DEMO,
                {
                    userId: user?.id,
                    conversationId: converCalling?.conversationId,
                    exceptSocketId: socketCall.id
                },
                () => {
                    dispatch(setUserIsCalling(true));
                    dispatch(updateCalling({ isCalling: true, conversationId: converCalling?.conversationId || "", userAcceptCall: user?.id, deviceAcceptCall: isMobile() ? "smartphone" : "desktop" }));
                    if (!isMobile()) {
                        push(`/chat/conversation/${converCalling?.conversationId}`);
                        dispatch(
                            setOtherUser({
                                otherUsername: converCalling?.username || "",
                                otherAvatar: converCalling?.avatar || ""
                            })
                        );

                    }


                }
            );
        } catch (error) {
            console.log(error);
        }
    };
    const handleCallOut = () => {
        try {
            socketCall.emit(
                EVENT_NAMES.INDIVIDUAL_CALL.CALL_OUT_DEMO,
                {
                    userId: user?.id,
                    conversationId: converCalling?.conversationId,
                    exceptSocketId: socketCall.id
                },
                () => {
                    // dispatch(setUserIsCalling(false));
                }
            );
        } catch (error) {
            console.log(error);
        }
    };
    // useEffect(() => {
    //     if (!call.isCalling) {
    //         handleCallOut();
    //     }

    // }, [call.isCalling]);
    const handleCallDeny = () => {

        try {
            socketCall.emit(
                EVENT_NAMES.INDIVIDUAL_CALL.CALL_DENY_DEMO,
                {
                    userId: user?.id,
                    conversationId: converCalling?.conversationId,
                    exceptSocketId: socketCall.id
                },
                () => {
                }

            );

        } catch (error) {
            console.log(error);
        }
    };
    const handleClickOutside = (event: { target: any }) => {
        if (
            searchBoxRef.current &&
            !searchBoxRef.current.contains(event.target) &&
            inputRef.current &&
            !inputRef.current.contains(event.target)
        ) {
            if (search.isOpenHeaderSearch) {
                dispatch(toggleOpenHeaderSearch());
            }
        }
        if (
            menuSettingRef.current &&
            !menuSettingRef.current.contains(event.target) &&
            settingsIconRef.current &&
            !settingsIconRef.current.contains(event.target)
        ) {
            setIsSetting(false);
        }
    };

    useEffect(() => {
        document.addEventListener("mousedown", handleClickOutside);

        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [search.isOpenHeaderSearch]);

    useEffect(() => {
        const indexDB = async () => {
            const { data } = await fetchAllConversation();

            // (data);

            data.forEach((c: any) => {
                const Id: string = c.id || "";
                storeDataInIndexDBById(Id, c);
            });

            getAllDataFromIndexDB()
                .then((allData: any) => {
                    if (allData) {
                        dispatch(setArrayValueToSearch(allData));
                    }
                })
                .catch((error) => {
                    console.error("Error fetching and dispatching data:", error);
                });
        };
        indexDB();
    }, [conversation]);

    useEffect(() => {

        if (cookieData && !user.socket) {
            // if (!data) return;
            const socket = io(`${process.env.NEXT_PUBLIC_DAK_CHAT_SOCKET}`, {
                auth: { token: `Bearer ${cookieData.token_chat}` },
                autoConnect: true,
                transports: ["websocket"],
                withCredentials: true
            });
            dispatch(setUserLogin({ ...cookieData, socket }));
        }

        return () => {
            if (user.socket) {
                user.socket.disconnect();
                dispatch(setUserLogin({ ...user, socket: null }));
            }
        };
    }, []);
    useEffect(() => {
        if (cookieData) {
            const socketRealtime = io(`${process.env.NEXT_PUBLIC_DAK_CHAT_SOCKET}`, {
                transports: ["websocket"],
                auth: { token: `${cookieData.token_chat}` },
                withCredentials: true
            });

            socketRealtime.on("connect", () => {
                ("connect realtime service");
            });

            socketRealtime.on("LOGOUT", () => {
                const getUserLogin = async () => {
                    try {
                        await axiosClient().get(`${process.env.NEXT_PUBLIC_DAK_API}/auth`);
                    } catch (err: any) {
                        if (err.response.data.error_code === "LOGIN_REQUIRE") {
                            setIsLogin(false);
                        }
                    }
                };
                getUserLogin();
            });

            socketRealtime.on("UNAUTHORIZATION", () => {
            });
        }

        if (!isLogin) {
            // Mã hóa đầu cuối
            localStorage.removeItem("listEndToEndEncryptionConversation");
            dispatch(addListIdToListIdConversationDecoded([]));

            deleteLoginCookies();
            clearDataInIndexDB();
            alert("Bạn đã bị đăng xuất từ nơi khác");
            router.refresh();
        }
    }, [isLogin]);

    useEffect(() => {
        // Mã hóa đầu cuối
        const encryptConversation = async () => {
            const { data } = await fetchAllConversation();

            data.forEach((c: any) => {
                const hashedConversation: any = c.latestMessage.find((m: any) => m.isHashed === true);
                if (hashedConversation) {
                    dispatch(addIdToListIdConversationDecoded({
                        idConversation: c.id,
                        hashEncryptionPassword: "",
                        encryptionPassword: "",
                        isDecoded: false
                    }));
                }
            });
        };
        encryptConversation();
    }, []);

    // EVENT CALL COMING
    const isTabActive = useRef(true);
    const handleVisibilityChange = () => {
        if (document.visibilityState === "hidden" || document.title !== "DAC") {
            isTabActive.current = false;
        } else {
            isTabActive.current = true;
            document.title = ("DAC");
        }
    };

    useEffect(() => {
        document.addEventListener("visibilitychange", handleVisibilityChange);
        (() => {
            try {
                const socketConversation = io(
                    `${process.env.NEXT_PUBLIC_DAK_CHAT_SOCKET}`,
                    {
                        auth: { token: `${cookieData?.token_chat}` },
                        autoConnect: true,
                        transports: ["websocket"],
                        withCredentials: true
                    }
                );

                socketConversation.on("connect", () => {
                    setSocketCall(socketConversation);
                    socketConversation.on(
                        EVENT_NAMES.INDIVIDUAL_CALL.CALL_INCOMING_DEMO,
                        (data: any) => {
                            setCookie(
                                "data",
                                JSON.stringify({
                                    conversationId: data?.conversationId,
                                    username: user?.username,
                                    type: data?.type,
                                    usercall: data?.username
                                }),
                                { sameSite: "strict", secure: true, maxAge: 30 * 24 * 60 * 60 }
                            );
                            if (data) {
                                if (!isTabActive.current) {
                                    document.title = `${data?.username} Đang gọi!`;
                                } else {
                                    document.title = ("DAC");
                                }
                            }

                            if (isCalling) {

                                socketConversation.emit(
                                    EVENT_NAMES.INDIVIDUAL_CALL.CALL_BUSY_DEMO,
                                    { sendToUserId: data.userId }
                                );
                                return null;
                            }
                            setIsPopup(data);
                            setConverCalling(data);
                            dispatch(updateCalling({ isCalling: true, conversationId: data?.conversationId || "", deviceCall: data?.device, userCreatCall: data?.userId }));
                            window.postMessage(`${data?.username}-${data?.type}-IncomingCall-${data?.conversationId}-${user.id}`, "*");
                        }
                    );
                    socketConversation.on(
                        EVENT_NAMES.INDIVIDUAL_CALL.CALL_ACCEPT_DEMO,
                        (data: any) => {
                            dispatch(setWaitCalling(false));
                            dispatch(updateCalling({ isCalling: true, conversationId: data?.conversationId || "", deviceAcceptCall: data?.device, userAcceptCall: data?.userId }));
                            setIsPopup(false);

                            socketCall?.emit(
                                EVENT_NAMES.INDIVIDUAL_CALL.CALL_MUTE_DEMO,
                                {
                                    userId: user.id,
                                    conversationId: converCalling?.conversationId
                                },
                                () => {
                                }
                            );
                        }
                    );
                    socketConversation.on(
                        EVENT_NAMES.INDIVIDUAL_CALL.CALL_DENY_DEMO,
                        () => {

                            dispatch(updateCalling({
                                isCalling: false,
                                conversationId: converCalling?.conversationId || "",
                                userAcceptCall: "",
                                userCreatCall: "",
                                deviceCall: "",
                                deviceAcceptCall: ""
                            }));

                            socketConversation.emit(
                                EVENT_NAMES.INDIVIDUAL_CALL.CALL_MUTE_DEMO,
                                {
                                    userId: user?.id,
                                    conversationId: converCalling?.conversationId
                                },
                                () => {
                                }
                            );
                            setIsPopup(false);
                            // let callSlice: ICallState = {
                            //   isCalling: false,
                            //   conversationId: converCalling?.conversationId || "",
                            // };

                        }
                    );
                    socketConversation.on(
                        EVENT_NAMES.INDIVIDUAL_CALL.CALL_BUSY_DEMO,
                        () => {
                            setIsCalling(false);
                            dispatch(setUserIsCalling(false));
                        }
                    );
                    socketConversation.on(

                        EVENT_NAMES.INDIVIDUAL_CALL.CALL_OUT_DEMO,
                        () => {
                            window.postMessage(`dsadsa-dsaddas-CALL_OUT`, "*");
                            dispatch(updateCalling({ isCalling: false, conversationId: converCalling?.conversationId || "", userAcceptCall: "", userCreatCall: "", deviceCall: "", deviceAcceptCall: "" }));
                            setIsPopup(false);
                            setIsPopup(false);

                        }
                    );
                    // window.addEventListener('message', (event) => {
                    //     const data1: string = event.data;
                    //     ("typeofData", typeof data1, data1);
                    //     if (data1.includes("ACCEPT")) {
                    //         socketConversation.emit(
                    //             EVENT_NAMES.INDIVIDUAL_CALL.CALL_ACCEPT_DEMO,
                    //             {
                    //                 userId: data1.split("&")[2],
                    //                 conversationId: data1.split("&")[1],
                    //                 exceptSocketId: socketConversation.id
                    //             },
                    //             (data: any) => {
                    //                 ("emit accept call", data, window.navigator.userAgent);
                    //                 dispatch(updateCalling({ isCalling: true, conversationId: data1.split("&")[1] || "", userAcceptCall: data1.split("&")[2], deviceAcceptCall: isMobile() ? "smartphone" : "desktop" }));

                    //                 push(`/chat/conversation/${data1.split("&")[1]}`);
                    //                 dispatch(
                    //                     setOtherUser({
                    //                         otherUsername: converCalling?.username || "",
                    //                         otherAvatar: converCalling?.avatar || ""
                    //                     })
                    //                 );
                    //             }
                    //         );
                    //         setIsPopup(false);
                    //         return;
                    //     }
                    //     if (data1.includes("DENY")) {
                    //         socketConversation.emit(
                    //             EVENT_NAMES.INDIVIDUAL_CALL.CALL_DENY_DEMO,
                    //             {
                    //                 userId: data1.split("&")[2],
                    //                 conversationId: data1.split("&")[1],
                    //                 exceptSocketId: socketConversation.id
                    //             },
                    //             (data: any) => {
                    //                 ("emit deny call", data);

                    //             });
                    //         setIsPopup(false);
                    //         return;
                    //     }


                    // });
                    return () => {
                        document.removeEventListener("visibilitychange", handleVisibilityChange);
                    };
                });

            } catch (error: any) {
                throw new Error(error);
            }
        })();
    }, [setIsCalling, isCalling]);
    useEffect(() => {
        if (!socketCall) return;

        return () => {
            user.socket.off(EVENT_NAMES.INDIVIDUAL_CALL.CALL_INCOMING_DEMO);
        };
    }, [socketCall]);
    return (
        <div className="flex justify-between items-center my-3 mx-6 relative ">
            <Image
                src={Logo.src}
                alt={"Logo"}
                loading="eager"
                priority
                width={43.702}
                height={43.702}
                className="bg-thNewtral1 rounded-full object-cover hidden md:block lg:block"
                onClick={() => router.push("/chat")}
            />
            {isPopup && (
                <PopupCallInComing
                    callInComing={isPopup}
                    setCallInComing={setIsPopup}
                    handCallAccept={handleCallAccept}
                    handleCallDeny={handleCallDeny}
                    userLogin={user}
                />
            )}
            <div className="feature_area flex gap-x-2 items-center justify-between lg:justify-normal md:justify-normal w-full md:w-fit lg:w-fit">
                <div
                    className={`relative h-[40px] flex items-center gap-2 bg-light-thNewtral1 dark:bg-thNewtral1 p-2 w-[70%] md:w-[300px] lg:w-[300px] rounded-lg ${styles.search}`}
                    ref={inputRef}
                >
                    <div className="opacity-50">
                        <BiSearch />
                    </div>
                    <input
                        type="text"
                        className="bg-transparent border-none outline-none text-sm font-normal"
                        placeholder="Search"
                        value={inputSearchValue}
                        onChange={(e) => setInputSearchValue(e.target.value)}
                    />
                    {search.isOpenHeaderSearch && (
                        <div ref={searchBoxRef}>
                            <HeaderSearchBox
                                value={value}
                                setInputSearchValue={setInputSearchValue}
                            />
                        </div>
                    )}
                </div>
                <div className="flex gap-x-2 items-center">
                    <div
                        ref={settingsIconRef}
                        className="grid place-items-center p-2 rounded bg-light-thNewtral1
                        hover:bg-light-thNewtral
                        dark:bg-thNewtral1 h-[40px] w-[40px] dark:hover:bg-thNewtral cursor-pointer"
                        onClick={() => setIsSetting(!isSetting)}
                    >
                        <IoMdSettings size={24} />
                    </div>
                    <div
                        className="relative rounded-full w-10 h-10 object-cover border-2 border-thPrimary cursor-pointer overflow-hidden"
                        onClick={() => push(`/chat/profile/${user.id}`)}
                    >
                        <Avatar
                            url={
                                user?.avatar
                            }
                        />
                    </div>
                </div>
            </div>
            {isSetting && (
                <MenuSetting
                    socket={user.socket}
                    setIsSetting={setIsSetting}
                    refBtn={inputRef}
                />
            )}
        </div>
    );
}
