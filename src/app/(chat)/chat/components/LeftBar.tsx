"use client";
import { EVENT_NAMES } from "@/src/constants/chat";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import {
    // setListConversations,
    setTotalUnseenMessage
} from "@/src/redux/slices/conversationSlice";
import {
    setIdConversation,
    toggleOpenSearch
} from "@/src/redux/slices/searchSlice";
// import { IConversation } from "@/src/types/Conversation";
import { IMessage } from "@/src/types/Message";
import axiosClient from "@/src/utils/axios/axiosClient";
import { filterConversation } from "@/src/utils/conversations/filterConversation";

import dynamic from "next/dynamic";
import {
    ReactElement,
    cache,
    // use,
    useCallback,
    useEffect,
    useRef,
    useState
} from "react";
import { HiUserGroup } from "react-icons/hi2";
import { IoIosChatbubbles } from "react-icons/io";
import { IoMailUnread } from "react-icons/io5";
import ModalAddContact from "../contact/components/Modal/ModalAddContact";
import ModalCreateGroup from "../contact/components/Modal/ModalCreateGroup";
import SearchBox from "../conversation/[id]/components/SearchBox";
const ListConversation = dynamic(() => import("./ListConversation"));
const ListUserRequest = dynamic(() => import("./ListUserRequest"));
const ListConversationGroups = dynamic(() => import("./ListConverationGroups"));

const fetchRequestFriends = cache(() =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations??offset=0&status=[1]&limit=10`
    )
);
// const fetchConversations = cache(() =>
//     axiosClient().get(
//         `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations?offset=0&status=[2]&limit=10&type=1`
//     )
// );
const fetchUnseenMessages = cache(() =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/unseen`
    )
);
const fetchListRequestGroups = cache(() =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/group/request?offset=0&limit=100`
    )
);
export default function LeftBar(): ReactElement {
    const [option, setOption] = useState<string>("chat");

    const { user, conversation, search } = useAppSelector((state) => state.user);

    const [countRequest, setCountRequest] = useState<number>(0);
    const dispatch = useAppDispatch();
    const [prevTitle, setPrevTitle] = useState<string>(document.title);
    // const { data } = use(fetchConversations()) as { data: any };
    // const [listConversation, setListConversation] = useState<
    //     IConversation[] | any
    // >(filterConversation(data, user.id));
    const [unSeenHiddenMessage, setUnSeenHiddenMessage] = useState<number>(0);
    const [unSeenHiddenGroupMessage, setUnSeenHiddenGroupMessage] = useState<number>(0);
    const [showModalAddContact, setShowModalAddContact] =
        useState<boolean>(false);

    const [showModalCreateGroup, setShowModalCreateGroup] =
        useState<boolean>(false);

    const modalCreateGroupRef = useRef<any>();
    const modalAddContactRef = useRef<any>();

    useEffect(() => {
        document.addEventListener("mousedown", handleClickOutside);

        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, []);
    const handleClickOutside = (event: { target: any }) => {
        if (
            modalAddContactRef.current &&
            !modalAddContactRef.current.contains(event.target)
        ) {
            setShowModalAddContact(false);

        }
        if (
            modalCreateGroupRef.current &&
            !modalCreateGroupRef.current.contains(event.target)
        ) {
            setShowModalCreateGroup(false);

        }
    };
    const fetchData = useCallback(async () => {
        const { data } = await fetchRequestFriends();
        return filterConversation(data, user.id);
    }, [user.id]);
    const fetchDataUnseen = useCallback(async () => {
        const { data } = await fetchUnseenMessages();
        return data;
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [user.id]);
    // const { data: dataUnSeen }: { data: number } = use(fetchUnseenMessages());

    // useEffect(() => {
    //   dispatch(setTotalUnseenMessage(dataUnSeen));
    //   // eslint-disable-next-line react-hooks/exhaustive-deps
    // }, [dataUnSeen, conversation]);
    // useEffect(() => {
    //     dispatch(setListConversations(listConversation));
    //     // eslint-disable-next-line react-hooks/exhaustive-deps
    // }, [listConversation]);

    // useEffect(() => {
    //     if (!user.socket) return;
    //     user.socket.on(EVENT_NAMES.CONVERSATION.SEENED_MESSAGE, (data: any) => {
    //         console.log("seened", data);
    //         listConversation.map((conversation: any) => {
    //             if (
    //                 conversation._id === data.conversationId &&
    //                 data.userSeenId !== user.id
    //             ) {
    //                 const newUserSeen: ISeenUser = {
    //                     _id: data?.userSeenId,
    //                     username: data?.userSeenUsername,
    //                     avatar: data?.userSeenAvatar,
    //                     id: data?.userSeenId,
    //                     time: ""
    //                 };
    //                 const Conver =
    //                     conversation.latestMessage[conversation.latestMessage.length - 1];
    //                 const newSeen = Conver.seen.find(
    //                     (item: ISeenUser) => item._id === newUserSeen._id
    //                 )
    //                     ? Conver.seen
    //                     : [...Conver.seen, newUserSeen];
    //                 const newLastMessage = {
    //                     ...Conver,
    //                     seen: [...newSeen]
    //                 };

    //                 setListConversation((prev: any) => {
    //                     return prev.map((item: any) => {
    //                         if (item._id === conversation._id) {
    //                             return {
    //                                 ...item,
    //                                 latestMessage: [
    //                                     ...item.latestMessage.slice(
    //                                         0,
    //                                         item.latestMessage.length - 1
    //                                     ),
    //                                     newLastMessage
    //                                 ]
    //                             };
    //                         }
    //                         return item;
    //                     });
    //                 });
    //             }
    //         });
    //     });
    //     return () => {
    //         // user.socket.off(EVENT_NAMES.CONVERSATION.SEENED_MESSAGE);
    //     };
    // }, [user.socket, conversation, user.id]);
    useEffect(() => {
        fetchData().then((data) => {
            const listUSerWaiting = data.filter(
                (data: any) => data.createdBy != user.id
            );
            setCountRequest(listUSerWaiting.length);
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [user.id, conversation]);
    useEffect(() => {
        fetchDataUnseen().then((data) => {
            dispatch(setTotalUnseenMessage(data));
        });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [user.id, conversation]);
    // useEffect(() => {
    //     console.log("test");
    //     // if(Object.keys(conversation.lastMessage) || !conversation.lastMessage) return;
    //     (async () => {
    //         // const { data } = await axios.get("/api/conversations");
    //         const { data } = await fetchConversations();
    //         setListConversation(filterConversation(data, user.id));
    //     })();
    //     // eslint-disable-next-line react-hooks/exhaustive-deps
    // }, [conversation.lastMessage, conversation.totalUnseenMessage]);
    // RECEIVE MESSAGE
    const isTabActive = useRef(true);
    const handleVisibilityChange = () => {
        if (document.visibilityState === "hidden") {
            isTabActive.current = false;
        } else {
            isTabActive.current = true;
            setPrevTitle(
                document.title !== "Tin nhắn mới!" ? document.title : prevTitle
            );
            document.title = prevTitle;
        }
    };
    useEffect(() => {
        document.addEventListener("visibilitychange", handleVisibilityChange);

        user.socket.on(
            EVENT_NAMES.CONVERSATION.RECEIVE_MESSAGE,
            (msg: IMessage) => {
                if (msg) {
                    if (!isTabActive.current) {
                        document.title = "Tin nhắn mới!";
                    } else {
                        document.title = prevTitle;
                    }
                }
            }
        );
        return () => {
            document.removeEventListener("visibilitychange", handleVisibilityChange);
        };
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [user.socket, conversation]);

    const closeSearchBox = () => {
        if (search.isOpenSearch) {
            dispatch(toggleOpenSearch());
            dispatch(setIdConversation(""));
        }
    };
    const [countRequestGroup, setCountRequestGroup] = useState<number>(0);
    console.log(countRequestGroup);
    useEffect(() => {
        (async () => {
            const { data } = await (fetchListRequestGroups());
            setCountRequestGroup(data.length);
        })();
    }, []);

    return (
        <div className="relative lg:flex-grow-0 lg:flex-shrink-0 md:flex-shrink-0 h-[90dvh] lg:h-[85dvh] mt-4">

            {search.isOpenSearch ? (
                <SearchBox />
            ) : option === "chat" ? (
                <ListConversation setUnSeenHiddenMessage={setUnSeenHiddenMessage} onOpenModal={(modalType: string) => {

                    setShowModalAddContact(modalType === "addFriend");
                    setShowModalCreateGroup(modalType === "createGroup");
                }} />
            ) : option === "request" ? (
                <ListUserRequest
                    setOption={() => {
                        closeSearchBox();
                        setOption("chat");
                    }}
                    onOpenModal={(modalType: string) => {

                        setShowModalAddContact(modalType === "addFriend");
                        setShowModalCreateGroup(modalType === "createGroup");
                    }}
                    backGroup={() => setOption("group")}
                    setCountRequestGroup={setCountRequestGroup}
                />
            ) : (
                <ListConversationGroups setUnSeenHiddenGroupMessage={setUnSeenHiddenGroupMessage} onOpenModal={(modalType: string) => {

                    setShowModalAddContact(modalType === "addFriend");
                    setShowModalCreateGroup(modalType === "createGroup");
                }} />
            )}

            <div className="absolute bottom-0 left-1/2 -translate-x-1/2 flex items-center px-4 py-2 rounded-2xl gap-x-8 bg-thPrimary dark:bg-thNewtral1 ">
                <div

                    className={`${option === "chat"
                        ? unSeenHiddenMessage > 0
                            ? "text-thRed"
                            : "dark:text-thPrimary text-light-thPrimary"
                        : "text-thNewtral2"
                    } cursor-pointer`}
                    onClick={() => {
                        closeSearchBox();
                        setOption("chat");
                    }}
                >
                    <IoIosChatbubbles size={35} />
                </div>

                <div
                    className={`${option === "group" ? unSeenHiddenGroupMessage > 0
                        ? "text-thRed"
                        : "dark:text-thPrimary text-light-thPrimary"
                        : "text-thNewtral2"
                    } cursor-pointer`}
                    onClick={() => {
                        closeSearchBox();
                        setOption("group");
                    }}
                >
                    <HiUserGroup size={35} />
                </div>
                <div
                    className={`${option === "request" ? "dark:text-thPrimary text-light-thPrimary" : "text-thNewtral2 "
                    }  cursor-pointer relative`}
                    onClick={() => {
                        closeSearchBox();
                        setOption("request");
                    }}
                >
                    <IoMailUnread size={35} />

                    {countRequest > 0 && (
                        <div className="absolute right-[-2px] w-5 h-5 text-center color-white bg-red-500 rounded-full top-[-2px] text-white">
                            {countRequest}
                        </div>
                    )}

                </div>



            </div>
            {showModalAddContact && (<div className="fixed w-screen flex items-center justify-center top-0 h-full  bg-[#00000080] z-50">

                <div ref={modalAddContactRef}>
                    <ModalAddContact
                        onClose={() => {
                            setShowModalAddContact(false);

                        }}
                    />
                </div>


            </div>)}
            {showModalCreateGroup && (<div className="fixed w-screen flex items-center justify-center top-0 h-full bg-[#00000080] z-50">


                <div ref={modalCreateGroupRef} >
                    <ModalCreateGroup
                        onClose={() => {
                            setShowModalCreateGroup(false);

                        }}
                    />
                </div>



            </div>)}

        </div>
    );
}
