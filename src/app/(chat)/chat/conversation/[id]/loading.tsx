'use client';
import { useCallback, useState } from "react";
import styles from "./conversation.module.css";
export default function Loading(): JSX.Element {
    // You can add any UI inside Loading, including a Skeleton.
    const [theme, setTheme] = useState(localStorage.getItem("theme"));
    useCallback(() => {

        setTheme(localStorage.getItem("theme"));
    }, []);
    return (
        <div className="w-full h-full px-4 flex flex-col justify-between py-5">
            <div className="flex flex-col justify-center items-center gap-y-2">
                <div className={`w-20 h-20 rounded-full ${theme === 'dark' ? `${styles.gradient}` : `${styles.gradient_light}`} `}></div>
                <div className={`w-32 h-2 rounded ${theme === 'dark' ? `${styles.gradient}` : `${styles.gradient_light}`}`}></div>
                <div className={`w-20 h-1.5 rounded  ${theme === 'dark' ? `${styles.gradient}` : `${styles.gradient_light}`}`}></div>
            </div>
            <div className="flex flex-col gap-y-2">
                <div className="flex gap-x-1 items-end">
                    <div className={`w-4 h-4 rounded-full`}></div>
                    <div className={`w-56 h-12  ${theme === 'dark' ? `${styles.gradient}` : `${styles.gradient_light}`} ${styles.partner_box}`}></div>
                </div>
                <div className="flex gap-x-1 items-end">
                    <div className={`w-4 h-4 rounded-full`}></div>
                    <div className={`w-32 h-12  ${theme === 'dark' ? `${styles.gradient}` : `${styles.gradient_light}`} ${styles.partner_box}`}></div>
                </div>
                <div className="flex gap-x-1 items-end">
                    <div className={`w-4 h-4 rounded-full  ${theme === 'dark' ? `${styles.gradient}` : `${styles.gradient_light}`}`}></div>
                    <div className={`w-72 h-24  ${theme === 'dark' ? `${styles.gradient}` : `${styles.gradient_light}`} ${styles.partner_box}`}></div>
                </div>
                <div className="flex justify-end w-full">
                    <div className={`w-56 h-16  ${theme === 'dark' ? `${styles.gradient}` : `${styles.gradient_light}`} ${styles.owner_box}`}></div>
                </div>
            </div>
        </div>
    );
}