import { ReactElement, useState } from "react";
import { AiFillCheckCircle } from "react-icons/ai";
import { Avatar } from "../../../../components/Avatar";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import getAxiosClient from "@/src/utils/axios/axiosClient";
import { setDetailChat } from "@/src/redux/slices/detailChatSlice";
import { CONVERSATION } from "@/src/constants/chat";
import { message } from "antd";
import ModalConfirm from "./ModalConfirm";
interface IModalManagerGroupProps {
    close: () => void;
    members: any;
    action?: string;
}
const ModalManagerGroup = ({ close, members, action }: IModalManagerGroupProps): ReactElement => {
    const { user, detailChat } = useAppSelector(state => state.user);
    const [keySearch, setKeySearch] = useState<string>("");
    const [users, setUsers] = useState<string[]>([]);
    const handleChooseUser = (user: any) => {
        if (action === 'CHANGE_OWNER') {
            // Nếu action là CHANGE_OWNER, chỉ chọn một user
            setUsers([user.id]);
        } else {
            // Ngược lại, xử lý như trước đó
            if (users.includes(user.id)) {
                setUsers(users.filter((item) => item !== user.id));
            } else {
                setUsers([...users, user.id]);
            }
        }
    };

    const dispatch = useAppDispatch();
    const [isShowModalConfirm, setIsShowModalConfirm] = useState<boolean>(false);
    const handleAction = async (user: string, role?: number, block?: boolean) => {
        const url = block
            ? `/conversations/setting/group/prevent-join/${detailChat.id}`
            : `/conversations/group/grant/${detailChat.id}`;
        const data = block ? { userId: user } : { userId: user, role };

        try {
            if (block) {
                const response = await getAxiosClient().delete(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/group/owner/${detailChat?.id}/kick`,
                    {
                        data: {
                            ids: [user]
                        }
                    }
                );
                if (response.status === 200) {
                    const res = await getAxiosClient().put(
                        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}${url}`,
                        data
                    );
                    if (res.status === 200) {
                        setUsers((prevUsers) => prevUsers.filter((item) => item !== user));
                    }
                }
            } else {
                const res = await getAxiosClient().put(
                    `${process.env.NEXT_PUBLIC_DAK_CHAT_API}${url}`,
                    data
                );
                if (res.status === 200) {
                    setUsers((prevUsers) => prevUsers.filter((item) => item !== user));
                }
            }
        } catch (err) {
            console.error(err);
            // Xử lý lỗi nếu cần thiết
        }
    };

    const handleSubmit = async () => {
        try {
            if (action === 'ADD_ADMIN') {
                await Promise.all(users.map((user) => handleAction(user, CONVERSATION.ROLE_TYPE.ADMIN)));
            }

            if (action === 'CHANGE_OWNER') {
                setIsShowModalConfirm(true);
            }

            if (action === 'BLOCK_USER') {
                await Promise.all(users.map((user) => handleAction(user, undefined, true)));
            }

            const { data: newDetailChat } = await getAxiosClient().get(
                `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${detailChat.id}`
            );
            dispatch(setDetailChat(newDetailChat));
            // message.success("Thành công");
        } catch (err) {
            console.error(err);
            // Xử lý lỗi nếu cần thiết
        }
    };

    const handleChangeOwner = async () => {
        try {
            await handleAction(users[0], CONVERSATION.ROLE_TYPE.OWNER);
            setIsShowModalConfirm(false);
            const { data: newDetailChat } = await getAxiosClient().get(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${detailChat.id}`);
            dispatch(setDetailChat(newDetailChat));
            message.success("Thành công");
            close();
        }
        catch (err) {
            console.log(err);
            message.error("Thất bại");
        }
    };
    return (
        <div className="fixed top-0 left-0 right-0 z-50 w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] max-h-full flex items-center justify-center">
            <div className="fixed inset-0 w-full h-screen bg-transparent overlay" onClick={() => close()}></div>
            <div className="relative w-full max-w-2xl max-h-full z-50">
                <div className="relative  rounded-lg shadow dark:bg-thDark-background bg-light-thNewtral2 border border-thPrimary">
                    <div className="flex items-start justify-between p-3 border-b rounded-t border-gray-600">
                        <h3 className="text-xl font-semibold text-white">
                            {
                                action === "BLOCK_USER" && "Chặn khỏi nhóm"
                            }
                            {
                                action === "ADD_ADMIN" && "Thêm phó nhóm"
                            }
                            {
                                action === "CHANGE_OWNER" && "Chuyển quyền nhóm trưởng"
                            }
                        </h3>
                        <button
                            type="button"
                            className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ml-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white"
                            data-modal-hide="defaultModal"
                            onClick={() => close()}
                        >
                            <svg
                                className="w-3 h-3 text-thPrimary"
                                aria-hidden="true"
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 14 14"
                            >
                                <path
                                    stroke="currentColor"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    strokeWidth={2}
                                    d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"
                                />
                            </svg>
                            <span className="sr-only">Close modal</span>
                        </button>
                    </div>
                    {/* Modal body */}
                    <div className="px-6 py-2  flex flex-col">
                        <div className="flex items-center justify-center">
                            <label
                                htmlFor="default-search"
                                className="mb-2 text-sm font-medium sr-only text-white"
                            >
                                Search
                            </label>
                            <div className="relative w-full">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <svg
                                        className="w-4 h-4 text-gray-400"
                                        aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="none"
                                        viewBox="0 0 20 20"
                                    >
                                        <path
                                            stroke="currentColor"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            strokeWidth={2}
                                            d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
                                        />
                                    </svg>
                                </div>
                                <input
                                    type="search"
                                    id="default-search"
                                    className="block w-full p-2 pl-10 text-sm border border-gray-300 rounded-full bg-transparent text-white focus:ring-blue-500 focus:border-blue-500 "
                                    placeholder="Nhập tên..."
                                    value={keySearch}
                                    onChange={(e) => setKeySearch(e.target.value)}
                                />
                            </div>
                        </div>
                        <div className="flex w-full border-t border-gray-200 pt-2 mt-2 gap-2">
                            <div className={`${users.length === 0 ? "w-full" : "w-2/3"} max-[480px]:w-full h-[50vh] overflow-y-auto`}>
                                {
                                    members?.filter((item: any) => {
                                        if (action === "BLOCK_USER") {
                                            return item?.id !== user.id && item.type !== CONVERSATION.ROLE_TYPE.ADMIN && item.type !== CONVERSATION.ROLE_TYPE.OWNER;
                                        }
                                        if (action === "ADD_ADMIN") {
                                            return item?.id !== user.id && item.type !== CONVERSATION.ROLE_TYPE.OWNER && item.type !== CONVERSATION.ROLE_TYPE.ADMIN;
                                        }
                                        if (action === "CHANGE_OWNER") {
                                            return item?.id !== user.id && item.type !== CONVERSATION.ROLE_TYPE.OWNER;
                                        }
                                        return true;
                                    })
                                        ?.filter((item: any) =>
                                            item?.username
                                                ?.toLowerCase()
                                                .includes(keySearch.toLowerCase())
                                        )
                                        ?.map((member: any) => {
                                            return (
                                                member?.id !== user.id &&
                                                <div
                                                    key={member._id}
                                                    className={`flex items-center justify-start gap-2 item  mt-2 hover:bg-gray-500 
                                                }`}
                                                    onClick={() => handleChooseUser(member)}
                                                >
                                                    {
                                                        <>
                                                            {
                                                                users.includes(member?.id)
                                                                    ? (
                                                                        <AiFillCheckCircle
                                                                            size={16}
                                                                            className="cursor-pointer"
                                                                        />
                                                                    ) :
                                                                    <div className="h-3 w-3 border-[1.5px] border-thNewtral2 bg-transparent rounded-full cursor-pointer"></div>
                                                            }

                                                            <div className="relative rounded-full w-10 h-10 object-cover border-2 border-thPrimary cursor-pointer overflow-hidden">
                                                                <Avatar
                                                                    url={
                                                                        member?.avatar === "http://avatar.com"
                                                                            ? "/default_avatar.jpg"
                                                                            : member?.avatar
                                                                    }
                                                                />
                                                            </div>
                                                            <span>{member?.username}</span>
                                                            <span className="text-sm text-white">
                                                                {/* {checkCommonGroup && "(Đã tham gia)"} */}
                                                            </span>
                                                        </>}
                                                </div>
                                            );
                                        })}
                            </div>
                            <div className={`${users.length === 0 ? "hidden" : "w-1/3"} p-2 rounded-md max-[480px]:hidden h-[50vh] border border-thPrimary overflow-y-auto`}>
                                <h3>Đã chọn {users.length}</h3>
                                {
                                    members
                                        ?.map((member: any) => {
                                            return (
                                                users.includes(member?.id) &&
                                                <div
                                                    key={member._id}
                                                    className={`flex items-center justify-start gap-2 item  mt-2 hover:bg-gray-500 
                                                }`}
                                                    onClick={() => handleChooseUser(member)}
                                                >
                                                    {
                                                        <>
                                                            <div className="relative rounded-full w-10 h-10 object-cover border-2 border-thPrimary cursor-pointer overflow-hidden">
                                                                <Avatar
                                                                    url={
                                                                        member?.avatar === "http://avatar.com"
                                                                            ? "/default_avatar.jpg"
                                                                            : member?.avatar
                                                                    }
                                                                />
                                                            </div>
                                                            <span>{member?.username}</span>
                                                        </>}
                                                </div>
                                            );
                                        })}
                            </div>
                        </div>
                    </div>
                    {/* Modal footer */}
                    <div className="flex items-center justify-end p-3 space-x-2 border-t border-gray-200 rounded-b dark:border-gray-600">
                        <button
                            className="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600"
                            onClick={() => {
                                close();
                            }}
                        >
                            Hủy
                        </button>
                        <button
                            className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                            onClick={handleSubmit}
                        >
                            {
                                action === "BLOCK_USER" && "Chặn"
                            }
                            {
                                (action === "ADD_ADMIN" || action === "CHANGE_OWNER") && "Xác nhận"
                            }
                        </button>
                    </div>
                </div>
            </div>
            {
                isShowModalConfirm && <ModalConfirm close={() => setIsShowModalConfirm(false)} handleConfirm={handleChangeOwner} message="
                Người được chọn sẽ trở thành trưởng nhóm và có mọi quyền quản lý nhóm. Bạn sẽ mất quyền quản lý nhóm nhưng vẫn là phó nhóm. Hành động này không thể phục hồi.
                "
                titleSubmit="Xác nhận"
                titleCancel="Hủy"
                />
            }
        </div>
    );
};
export default ModalManagerGroup;