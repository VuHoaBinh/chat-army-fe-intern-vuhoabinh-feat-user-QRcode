"use client";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import {
    setListConversations,
    setListHiddenConversation
} from "@/src/redux/slices/conversationSlice";
import axiosClient from "@/src/utils/axios/axiosClient";
import {
    filterConversation,
    sortConversationByNewest
} from "@/src/utils/conversations/filterConversation";
import * as bcrypt from "bcryptjs";
import React, {
    ChangeEvent,
    ReactElement,
    cache,
    useCallback,
    useState
} from "react";
import { AiFillEye, AiFillEyeInvisible } from "react-icons/ai";
import { toast } from "react-toastify";

interface Props {
    conversationId: string;
    setIsOpenSetPinHidden: (isOpenSetPinHidden: boolean) => void;
}
const fetchConversations = cache(() =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations?offset=0&status=[2]`
    )
);
// const fetchConversations = cache((offset: number) =>
//     axiosClient().get(
//         `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations?offset=${offset}&status=[2]&limit=20&type=2`
//     )
// );

const ModalSetPinHidden = ({
    conversationId,
    setIsOpenSetPinHidden
}: Props): ReactElement => {
    const [inputValue, setInputValue] = useState<string>("");
    const [type, setType] = useState<string>("password");
    const user = useAppSelector((state) => state.user.user);
    const dispatch = useAppDispatch();

    const fetchData = useCallback(async () => {
        const { data } = await fetchConversations();
        return filterConversation(data, user.id);
    }, [user.id]);

    const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
        setInputValue(event.target.value);
    };
    const handleButtonConfirm = async () => {
        // Kiểm tra input rỗng
        if (inputValue === "") {
            alert("Vui lòng nhập mã PIN!");
            return;
        }
        const id = toast.loading("Đang ẩn trò chuyện...");
        try {
            const salt = 10;
            const hashedPassword = await bcrypt.hash(inputValue, salt);
            const body: {
                conversationId: string;
                pin: string;
            } = {
                conversationId: conversationId,
                pin: hashedPassword
            };
            axiosClient()
                .post(
                    `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/hidden`,
                    body
                )
                .then(() => {
                    fetchData().then((data) => {
                        const conversationsWithHiddenFlag = sortConversationByNewest(
                            data
                        ).filter((item: any) => item.isConversationHidden);
                        dispatch(setListHiddenConversation(conversationsWithHiddenFlag));
                        dispatch(setListConversations(sortConversationByNewest(data)));
                    });

                    toast.update(id, {
                        render: "Đã ẩn thành công",
                        type: "success",
                        isLoading: false,
                        autoClose: 3000,
                        closeButton: true
                    });
                    setIsOpenSetPinHidden(false);
                })
                .catch(() => {
                    toast.update(id, {
                        render: "Ẩn trò chuyện thất bại",
                        type: "error",
                        isLoading: false,
                        autoClose: 3000,
                        closeButton: true
                    });
                });
        } catch (e) {
            console.error(e);
        }
    };
    const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === "Enter") {
            handleButtonConfirm();
        }
    };

    return (
        <div className="flex flex-col h-[220px] lg:h-[200px] w-[270px] lg:w-[300px] fixed dark:bg-thNewtral1 bg-light-thNewtral1 rounded-2xl overflow-hidden top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 z-50 px-3 pb-3">
            <div className=" h-12 w-full flex justify-between items-center">
                <h1 className="h-10 w-max flex justify-center items-center">
                    Đặt mã PIN
                </h1>
                <span
                    className="h-10 aspect-square flex justify-center items-center dark:hover:bg-thNewtral hover:bg-light-thNewtral rounded-full  cursor-pointer"
                    onClick={() => {
                        setIsOpenSetPinHidden(false);
                    }}
                >
                    X
                </span>
            </div>
            <div className=" flex-1 flex justify-center items-center">
                <input
                    type={type}
                    onChange={handleInputChange}
                    maxLength={5}
                    placeholder="Nhập mã PIN"
                    className={"outline-none dark:bg-thNewtral bg-light-thNewtral p-2 rounded-lg w-[90%] "}
                    onKeyDown={handleKeyDown}
                />
                {type === "password" ? (
                    <AiFillEye
                        size={20}
                        className="text-thPrimary ml-2"
                        onClick={() => setType("text")}
                    />
                ) : (
                    <AiFillEyeInvisible
                        size={20}
                        className="text-thPrimary ml-2"
                        onClick={() => setType("password")}
                    />
                )}
            </div>
            <div className=" h-12 w-full flex justify-end items-center">
                <button
                    className="h-10 w-[64px] bg-gray-400 rounded-md mr-2"
                    onClick={() => {
                        setIsOpenSetPinHidden(false);
                    }}
                >
                    Hủy
                </button>
                <button
                    className="h-10 w-[116px]  dark:bg-thPrimary bg-light-thPrimary text-thNewtral1 hover:opacity-75 rounded-md"
                    onClick={handleButtonConfirm}
                >
                    Xác nhận
                </button>
            </div>
        </div>
    );
};

export default ModalSetPinHidden;
