/* eslint-disable react-hooks/exhaustive-deps */

import { EMOJI, MESSAGE_TYPE } from "@/src/constants/chat";
import { IMessage } from "@/src/types/Message";
import axiosClient from "@/src/utils/axios/axiosClient";
import moment from "moment";
import Image from "next/image";

import { ReactElement, SyntheticEvent, cache, useEffect, useRef, useState } from "react";
import { AiFillPlayCircle, AiOutlineSmile } from "react-icons/ai";

import { MdDownload } from "react-icons/md";
import { RiShareForwardFill } from "react-icons/ri";
import ReactionBox from "../ReactionBox";
import { Socket } from "socket.io-client";
import { GroupedVirtuoso } from "react-virtuoso";
import { FiChevronLeft } from "react-icons/fi";
import { useAppSelector } from "@/src/redux/hook";
import CryptoJS from "crypto-js";
import userAvatar from "@/public/user.png";
interface Props {
    socket?: Socket<any, any>;
    idConversation: string;
    idMediaModal: string;
    urlMediaModal: string;
    otherUsername: string;
    setIsOpenMediaLibrary: (isOpenMediaLibrary: boolean) => void;
    setForwardContent: (forwardContent: IMessage) => void;
    reactionID: string;
    setReactionID: (reactionID: string) => void;
    message: IMessage[];
}

const fetchMedia = cache((id: string) =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${id}/medias?sort=desc`
    )
);

const ModalMediaLibrary = ({
    socket,
    idConversation,
    idMediaModal,
    urlMediaModal,
    otherUsername,
    setIsOpenMediaLibrary,
    setForwardContent,
    reactionID,
    setReactionID,
    message
}: Props): ReactElement => {
    const { endToEndEncryption } = useAppSelector((state) => state.user);
    const [dataMedia, setDataMedia] = useState<IMessage[]>([]);
    const [targetMedia, setTargetMedia] = useState<IMessage>();
    const [isTarget, setIsTarget] = useState<string>();
    const handleKeyIcon = new Set();
    const virtuoso = useRef(null);
    const [indexInitialScroll, setIndexInitialScroll] = useState<number>();
    const [groupMediaImageCounts, setGroupMediaImageCounts] = useState<number[]>(
        []
    );
    const [groupMediaImage, setGroupMediaImage] = useState<string[]>([]);

    const handleResponseData = (data: any) => {
        // Mã hóa đầu cuối => logic
        const encryptionConversation = endToEndEncryption.listIdConversationDecoded.find((c: any) => c.idConversation === idConversation);
        let newData: any = [];
        if (encryptionConversation) {
            if (encryptionConversation?.isDecoded) {
                newData = data.map((d: any) => {
                    return {
                        ...d,
                        content: decryptMessage(encryptionConversation.encryptionPassword, d.content)
                    };
                });
            } else {
                return;
            }
        } else {
            return;
        }
        if (!newData) {
            return;
        }

        // Filter data MEDIA only
        const mediaResponse = newData.filter(
            (message: IMessage) =>
                message.type === MESSAGE_TYPE.IMAGE ||
                message.type === MESSAGE_TYPE.MEDIA
        );
        // Filter MEDIA messages
        const dataMedia = mediaResponse.filter(
            (message: IMessage) => message.type === MESSAGE_TYPE.MEDIA
        );
        // Filter and parse IMAGE messages
        const dataImage = mediaResponse?.filter(
            (message: IMessage) => message.type === MESSAGE_TYPE.IMAGE
        );
        const parseDataImage = dataImage.flatMap((message: IMessage) => {
            const contentArray = JSON.parse(message.content);
            return contentArray.map((content: any) => ({
                ...message,
                content: JSON.stringify([content]) // Convert the content link to a single-element array
            }));
        });

        // Combine the IMAGE and MEDIA messages into a single array
        const newDataMedia = [...parseDataImage, ...dataMedia];
        const sortedData = newDataMedia.sort((a, b) => {
            const dateA = new Date(a.createdAt).getTime();
            const dateB = new Date(b.createdAt).getTime();

            return dateB - dateA;
        });
        // Set the state with the combined data
        setDataMedia(sortedData);

        // Group data by createdAt date
        const groupDataMediaByDay = sortedData.reduce(
            (acc: any, item: IMessage) => {
                const createdAt = new Date(item.createdAt).toLocaleDateString();
                acc[createdAt] = acc[createdAt] || [];
                acc[createdAt].push(item);
                return acc;
            },
            {}
        );
        const groupCounts: number[] = Object.values(groupDataMediaByDay).map(
            (arr: any) => arr.length
        );
        const groups = Object.keys(groupDataMediaByDay);
        setGroupMediaImageCounts(groupCounts);
        setGroupMediaImage(groups);

        const foundMessage = sortedData.find((item: IMessage) => {
            return item.type === MESSAGE_TYPE.IMAGE
                ? item._id === targetMedia?._id &&
                JSON.parse(item.content).includes(targetMedia.content)
                : item._id === targetMedia?._id;
        });

        if (foundMessage) {
            if (foundMessage.type === MESSAGE_TYPE.IMAGE) {
                const matchingContent = JSON.parse(foundMessage.content).find(
                    (url: string) => url === targetMedia?.content
                );
                const updatedTargetMedia = {
                    ...foundMessage,
                    content: matchingContent
                };
                setTargetMedia(updatedTargetMedia);
                setIsTarget(matchingContent);
            }
            if (foundMessage.type === MESSAGE_TYPE.MEDIA) {
                setTargetMedia(foundMessage);
                setIsTarget(foundMessage.content);
            }
        }
    };
    useEffect(() => {
        const fetchDataMedia = async (conversationId: string) => {
            try {
                const response = await fetchMedia(conversationId);
                if (response) {
                    handleResponseData(response.data);
                }
            } catch (error) {
                console.error("Error fetching media data:", error);
            }
        };
        fetchDataMedia(idConversation);
    }, [message]);

    // Mã hóa đầu cuối => logic
    const decryptMessage = (password: string, message: string) => {
        try {
            const bytes = CryptoJS.AES.decrypt(message, password);
            const originalText = bytes.toString(CryptoJS.enc.Utf8);
            return originalText;
        } catch (e) {
            console.log(e);
        }
    };
    const handleInitDataMedia = (data: any) => {
        // Mã hóa đầu cuối => logic
        const encryptionConversation = endToEndEncryption.listIdConversationDecoded.find((c: any) => c.idConversation === idConversation);
        let newData: any = [];
        if (encryptionConversation) {
            if (encryptionConversation?.isDecoded) {
                newData = data.map((d: any) => {
                    return {
                        ...d,
                        content: decryptMessage(encryptionConversation.encryptionPassword, d.content)
                    };
                });
            } else {
                newData = data.filter((d: any) => {
                    if (d.content.includes("https")) {
                        return d;
                    }
                });
            }
        } else {
            newData = data;
        }
        if (!newData) {
            return;
        }

        // Filter data MEDIA only
        const mediaResponse = newData.filter(
            (message: IMessage) =>
                message.type === MESSAGE_TYPE.IMAGE ||
                message.type === MESSAGE_TYPE.MEDIA
        );
        // Filter MEDIA messages
        const dataMedia = mediaResponse.filter(
            (message: IMessage) => message.type === MESSAGE_TYPE.MEDIA
        );
        // Filter and parse IMAGE messages
        const dataImage = mediaResponse?.filter(
            (message: IMessage) => message.type === MESSAGE_TYPE.IMAGE
        );
        const parseDataImage = dataImage.flatMap((message: IMessage) => {
            const contentArray = JSON.parse(message.content);
            return contentArray.map((content: any) => ({
                ...message,
                content: JSON.stringify([content]) // Convert the content link to a single-element array
            }));
        });

        // Combine the IMAGE and MEDIA messages into a single array
        const newDataMedia = [...parseDataImage, ...dataMedia];
        const sortedData = newDataMedia.sort((a, b) => {
            const dateA = new Date(a.createdAt).getTime();
            const dateB = new Date(b.createdAt).getTime();

            return dateB - dateA;
        });
        const foundMessage = sortedData.find((item: IMessage) => {
            return item.type === MESSAGE_TYPE.IMAGE
                ? item._id === idMediaModal &&
                JSON.parse(item.content).includes(urlMediaModal)
                : item._id === idMediaModal;
        });

        if (foundMessage) {
            if (foundMessage.type === MESSAGE_TYPE.IMAGE) {
                const matchingContent = JSON.parse(foundMessage.content).find(
                    (url: string) => url === urlMediaModal
                );
                const updatedTargetMedia = {
                    ...foundMessage,
                    content: matchingContent
                };
                setTargetMedia(updatedTargetMedia);
                setIsTarget(matchingContent);
            }
            if (foundMessage.type === MESSAGE_TYPE.MEDIA) {
                setTargetMedia(foundMessage);
                setIsTarget(foundMessage.content);
            }
        }
        const indexMessage = sortedData.findIndex((item: IMessage) => {
            if (item.type === MESSAGE_TYPE.IMAGE) {
                const testSrc = JSON.parse(item.content);
                const url = testSrc[0];
                return item._id === idMediaModal && url === urlMediaModal;
            }
            if (item.type === MESSAGE_TYPE.MEDIA) {
                return item._id === idMediaModal;
            }
        });
        setIndexInitialScroll(indexMessage);
    };
    useEffect(() => {
        const fetchDataMedia = async (conversationId: string) => {
            try {
                const response = await fetchMedia(conversationId);
                if (response) {
                    handleInitDataMedia(response.data);
                }
            } catch (error) {
                console.error("Error fetching media data:", error);
            }
        };
        fetchDataMedia(idConversation);
    }, []);

    const handleDownloadUrl = async (url: string) => {
        try {
            const response = await fetch(url);
            const blob = await response.blob();
            const objectUrl = URL.createObjectURL(blob);
            return objectUrl;
        } catch (error) {
            console.error("Error fetching the file:", error);
            return null;
        }
    };
    const downloadFile = async (media: IMessage) => {
        if (media.type === MESSAGE_TYPE.IMAGE) {
            const parts = media.content.split("/");
            const filename = parts[parts.length - 1];

            const objectUrl = await handleDownloadUrl(media.content);

            if (objectUrl) {
                const a = document.createElement("a");
                a.href = objectUrl;
                a.download = filename;
                a.click();
            }
        }
        if (media.type === MESSAGE_TYPE.MEDIA) {
            const parts = media.content.split("/");
            const filename = parts[parts.length - 1];

            const objectUrl = await handleDownloadUrl(media.content);

            if (objectUrl) {
                const a = document.createElement("a");
                a.href = objectUrl;
                a.download = filename;
                a.click();
            }
        }
    };
    const [isReact, setIsReact] = useState<boolean>(false);

    const handleReaction = () => {
        if (targetMedia) {
            if (isReact === true && reactionID === targetMedia._id) {
                setIsReact(false);
                setReactionID("");
                return;
            }
            if (isReact === true && reactionID !== targetMedia._id) {
                setReactionID(targetMedia._id);
                return;
            }
            setIsReact(true);
            setReactionID(targetMedia._id);
        }
    };
    const handleImageError = (event: SyntheticEvent<HTMLImageElement, Event>) => {

        const imgElement = event.target as HTMLImageElement;
        imgElement.src = userAvatar.src;
    };
    return (
        <div className="flex flex-col w-full h-full fixed bg-thDark-background top-0 left-0 z-10 text-thWhite">
            <div className="w-full h-16 dark:bg-thNewtral1 bg-light-thNewtral1 flex items-center justify-between">
                <div className="flex items-center flex-1">
                    <span
                        onClick={() => {
                            setIsOpenMediaLibrary(false);
                        }}
                        className="h-full aspect-square text-center flex justify-center items-center"
                    >
                        <FiChevronLeft size={40} />
                    </span>
                    <p className="truncate text-base font-semibold">{otherUsername}</p>
                </div>
                <div className="w-max h-full  flex justify-center gap-2 p-1">
                    <div className="h-full aspect-square flex ">
                        <RiShareForwardFill
                            onClick={() => {
                                if (targetMedia) {
                                    if (targetMedia.type === MESSAGE_TYPE.IMAGE) {
                                        const formattedArray = JSON.stringify([
                                            targetMedia.content
                                        ]);
                                        const updatedTargetMedia = {
                                            ...targetMedia,
                                            content: formattedArray
                                        };
                                        setForwardContent(updatedTargetMedia);
                                        setIsOpenMediaLibrary(false);
                                    }
                                    if (targetMedia.type === MESSAGE_TYPE.MEDIA) {
                                        setForwardContent(targetMedia);
                                        setIsOpenMediaLibrary(false);
                                    }
                                }
                            }}
                            className="h-3/5 w-3/5 m-auto"
                        />
                    </div>
                    <div className="h-full aspect-square flex ">
                        <MdDownload
                            className="h-3/5 w-3/5 m-auto"
                            onClick={() => {
                                if (targetMedia) {
                                    downloadFile(targetMedia);
                                }
                            }}
                        />
                    </div>
                </div>
            </div>
            <div className="relative h-full w-full mb-20 mt-2">
                {/* <div className="absolute m-auto  left-0 top-0 bottom-0 w-auto h-3/4  max-w-[75vw]"> */}
                <div className="absolute m-auto left-0 top-0 bottom-0 w-[75vw] h-auto  max-h-[70vh]">
                    {targetMedia?.content ? (
                        targetMedia.type === MESSAGE_TYPE.IMAGE ? (
                            <Image
                                key={targetMedia?._id}
                                alt={"avt"}
                                loading="eager"
                                priority
                                fill
                                src={targetMedia.content}
                                onError={handleImageError}
                                className="w-full h-full object-contain"
                            />
                        ) : (
                            <video
                                src={targetMedia.content}
                                width={96}
                                height={96}
                                className="w-full h-full"
                                controls
                            />
                        )
                    ) : (
                        <p>No Image Available</p> // You can customize this message
                    )}
                </div>
                {/* <div className="absolute top-0 right-0 w-24 h-full pt-3 overflow-y-auto max-w-[20vw]"> */}
                <div className="absolute top-0 px-2 right-0 w-[25vw] h-full pt-3 overflow-y-hidden ">
                    {indexInitialScroll != null && (
                        <GroupedVirtuoso
                            groupCounts={groupMediaImageCounts}
                            style={{ height: "100%" }}
                            ref={virtuoso}
                            initialTopMostItemIndex={indexInitialScroll}
                            groupContent={(index) => {
                                return (
                                    <div className="w-full bg-thDark-background text-xs font-semibold text-center">
                                        {groupMediaImage[index]}
                                    </div>
                                );
                            }}
                            itemContent={(index) => {
                                let url = "";
                                if (dataMedia[index].type === MESSAGE_TYPE.IMAGE) {
                                    const parseContent = JSON.parse(dataMedia[index].content);
                                    url = parseContent[0];
                                }

                                return dataMedia[index].type === MESSAGE_TYPE.IMAGE ? (
                                    <div
                                        className={`h-full w-full my-2  ${isTarget === url ? "border-4 border-thPrimary" : ""
                                        }`}
                                        onClick={() => {
                                            const updatedFilteredMsg = {
                                                ...dataMedia[index],
                                                content: url
                                            };
                                            setTargetMedia(updatedFilteredMsg);
                                            setIsTarget(url);
                                            setIndexInitialScroll(index);
                                        }}
                                    >
                                        <Image
                                            key={url}
                                            sizes="(max-width: 96px) 96px, 96px"
                                            alt={"avt"}
                                            loading="eager"
                                            priority
                                            width={96}
                                            height={96}
                                            onError={handleImageError}
                                            src={url}
                                            className="w-full aspect-square object-cover rounded-xl"
                                        />
                                    </div>
                                ) : (
                                    <div
                                        className={`h-full w-full my-2 relative   ${isTarget === dataMedia[index].content
                                            ? "border-4 border-thPrimary"
                                            : ""
                                        }`}
                                        onClick={() => {
                                            setTargetMedia(dataMedia[index]);
                                            setIsTarget(dataMedia[index].content);
                                            setIndexInitialScroll(index);
                                        }}
                                    >
                                        <AiFillPlayCircle
                                            size={40}
                                            className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2"
                                        />
                                        <video
                                            src={dataMedia[index].content}
                                            width={96}
                                            height={96}
                                            className="w-full aspect-square object-cover rounded-xl"
                                            style={{ pointerEvents: "none" }}
                                        />
                                    </div>
                                );
                            }}
                        />
                    )}
                </div>
            </div>
            <div className="absolute bottom-0 left-0 w-full h-16 dark:bg-thNewtral1 bg-light-thNewtral1 flex justify-between items-center px-4">
                <div className="w-1/5 h-11  flex items-center">
                    <div className="h-full aspect-square">
                        <Image
                            key={targetMedia?.createdBy._id}
                            sizes="(max-width: 44px) 44px, 44px"
                            alt={"avt"}
                            loading="eager"
                            priority
                            width={44}
                            height={44}
                            onError={handleImageError}
                            src={targetMedia?.createdBy.avatar}
                            className="w-full aspect-square object-cover rounded-full"
                        />
                    </div>
                    <div className=" h-full flex-1 pl-2 flex flex-col justify-between">
                        <p className="truncate h-full text-base font-semibold">
                            {targetMedia?.createdBy.username}
                        </p>
                        <p className="truncate h-full">
                            {moment(targetMedia?.createdAt).format("DD-MM-YYYY")}
                        </p>
                    </div>
                </div>

                <div className="w-1/5 h-full  flex justify-end items-center">
                    {targetMedia && targetMedia?.reactions?.length > 0 && (
                        <div className="flex justify-center items-center gap-2 bg-green-600 rounded-lg px-1.5 py-1">
                            {targetMedia?.reactions?.map((react: any, index: number) => {
                                if (index <= 1) {
                                    if (!handleKeyIcon.has(react.icon)) {
                                        handleKeyIcon.add(react.icon);
                                        return (
                                            <span key={react.icon}>
                                                {EMOJI.find((e) => e.id === react.icon)?.src || ""}
                                            </span>
                                        );
                                    }
                                }
                            })}

                            {targetMedia.reactions.length > 1 && (
                                <span className="text-[12px] opacity-60">
                                    {targetMedia.reactions.length}
                                </span>
                            )}
                        </div>
                    )}
                    <div className="h-full w-max flex p-3">
                        {isReact && reactionID === targetMedia?._id && (
                            <ReactionBox
                                messageId={targetMedia._id}
                                conversationSocket={socket}
                                position="right-5"
                            />
                        )}
                        <AiOutlineSmile
                            className="h-4/5 w-auto  m-auto bg-white rounded-full text-thNewtral1"
                            onClick={handleReaction}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ModalMediaLibrary;
