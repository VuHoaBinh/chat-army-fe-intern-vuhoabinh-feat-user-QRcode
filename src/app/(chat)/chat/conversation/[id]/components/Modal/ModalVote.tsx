import React, { ReactElement, cache, useEffect, useState } from "react";
import { FaPlus } from "react-icons/fa";
import { FiChevronLeft } from "react-icons/fi";
// import { DatePicker } from 'antd';
import { IoSettingsOutline } from "react-icons/io5";
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import dayjs from "dayjs";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import 'dayjs/locale/vi';
import getAxiosClient from "@/src/utils/axios/axiosClient";
import * as yup from 'yup';
import { toast } from "react-toastify";
import { message } from "antd";
import { setVotes } from "@/src/redux/slices/newsletterSlice";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { CONVERSATION } from "@/src/constants/chat";
import { IoMdClose } from "react-icons/io";

interface CheckboxItem {
    id: string;
    label: string;
    checked: boolean;
}
interface IModalVoteProps {
    setIsShowModal: React.Dispatch<React.SetStateAction<boolean>>;
    idConversation: string;
}
const fetchVotes = cache((id: string) =>
    getAxiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/vote/${id}?limit=20&offset=0`
    )
);
export default function ModalVote({
    setIsShowModal,
    idConversation
}: IModalVoteProps): ReactElement {
    const [value, setValue] = useState("");
    const [choices, setChoices] = useState<string[]>(["", ""]); // Initialize with two empty strings
    dayjs.locale('vi');
    const [duration, setDuration] = useState<dayjs.Dayjs | null>(null);
    const [errorMessageShown, setErrorMessageShown] = useState(false);
    const { user, detailChat } = useAppSelector((state) => state.user);
    // Event handler for date and time picker value change
    const handleDateChange = (newDate: dayjs.Dayjs | null) => {
        setDuration(newDate);
    };

    const handleAddChoice = () => {
        setChoices([...choices, ""]);
    };
    const handleRemoveChoice = (index: number) => {
        const updatedChoices = [...choices];
        updatedChoices.splice(index, 1);
        setChoices(updatedChoices);
    };

    const handleChoiceChange = (index: number, choice: string) => {
        const updatedChoices = [...choices];
        updatedChoices[index] = choice;
        setChoices(updatedChoices);
    };
    const [isShowSetting, setIsShowSetting] = useState(false);
    const [advancedSettings, setAdvancedSettings] = useState<CheckboxItem[]>([
        { id: 'isPinned', label: 'Ghim lên đầu trò chuyện', checked: false },
        { id: 'allowMultipleAnswers', label: 'Chọn nhiều phương án', checked: true },
        { id: 'allowAddOption', label: 'Có thể thêm phương án', checked: true }
    ]);

    const [anonymousVoting, setAnonymousVoting] = useState<CheckboxItem[]>([
        { id: 'hideResultBeforeAnswers', label: 'Ẩn kết quả khi chưa bình chọn', checked: false },
        { id: 'hideMemberAnswers', label: 'Ẩn người bình chọn', checked: false }
    ]);
    const isTypeWaitting = detailChat?.members.find((item: any) => item.id === user?.id)?.type === CONVERSATION.ROLE_TYPE.MEMBER_WAITING_ACCEPT;
    const isOwner = detailChat?.members.find((item: any) => item.id === user?.id)?.type === CONVERSATION.ROLE_TYPE.OWNER;
    const isAdmin = detailChat?.members.find((item: any) => item.id === user?.id)?.type === CONVERSATION.ROLE_TYPE.ADMIN;
    const handleCheckboxChange = (section: 'advancedSettings' | 'anonymousVoting', id: string) => {
        if (section === 'advancedSettings') {
            if (id === "isPinned") {
                if ((detailChat?.conversationSetting?.find((item) => item.type === 3)?.value === true && !isTypeWaitting) ||
                    ((isAdmin || isOwner))) {
                    setAdvancedSettings((prevSettings) =>
                        prevSettings.map((item) =>
                            item.id === id ? { ...item, checked: !item.checked } : item
                        )
                    );
                } else {
                    message.error("Tính năng ghim đã khóa");
                }
            } else {
                setAdvancedSettings((prevSettings) =>
                    prevSettings.map((item) =>
                        item.id === id ? { ...item, checked: !item.checked } : item
                    )
                );
            }
        } else if (section === 'anonymousVoting') {
            setAnonymousVoting((prevSettings) => {
                return prevSettings.map((item) =>
                    item.id === id ? { ...item, checked: !item.checked } : item
                );
            });
        }
    };
    useEffect(() => {
        if (anonymousVoting.find(item => item.id === "hideResultBeforeAnswers")?.checked === true) {
            setAdvancedSettings((prevSettings) =>
                prevSettings.map((item) =>
                    item.id === "allowAddOption" ? { ...item, checked: false } : item
                )
            );
        } else {
            setAdvancedSettings((prevSettings) =>
                prevSettings.map((item) =>
                    item.id === "allowAddOption" ? { ...item, checked: true } : item
                )
            );
        }
    }, [anonymousVoting]);
    const voteSchema = yup.object({
        question: yup.string().required("Vui lòng nhập vào câu hỏi").max(200),
        duration: yup.number().nullable()
            .test('future-date', 'Thời gian không được nhỏ hơn thời gian hiện tại', function (value) {
                // Skip validation if duration is null (not required)
                if (!value) return true;

                // Validate that the value is a future date
                const currentDate = dayjs();
                const selectedDate = dayjs(value);

                return selectedDate.isAfter(currentDate);
            }),
        option: yup
            .array()
            .required("Không để trống phương án")
            .min(2, "Ít nhất phải có 2 phương án")
            .max(28, "Tối đa chỉ được nhập 30 phương án")
            .of(yup.string().max(200).required("Không để trống phương án"))
            .test('unique-options', 'Các phương án không được trùng nhau', function (options) {
                const nonEmptyOptions = options.filter(opt => opt !== ""); // Lọc bỏ các phương án rỗng

                for (let i = 0; i < nonEmptyOptions.length; i++) {
                    for (let j = i + 1; j < nonEmptyOptions.length; j++) {
                        if (nonEmptyOptions[i] === nonEmptyOptions[j]) {
                            throw this.createError({
                                message: 'Các phương án không được trùng nhau',
                                path: this.path
                            });
                        }
                    }
                }

                return true;
            })

    });
    const dispatch = useAppDispatch();

    const handleSubmit = async () => {
        const convertToMilliseconds = (date: dayjs.Dayjs | null): number => {
            return date ? date.valueOf() : 0;
        };
        // Convert duration to milliseconds when needed
        const durationInMilliseconds = convertToMilliseconds(duration);
        const isPinned = advancedSettings.find(item => item.id === 'isPinned')?.checked;
        const allowMultipleAnswers = advancedSettings.find(item => item.id === 'allowMultipleAnswers')?.checked;
        const allowAddOption = advancedSettings.find(item => item.id === 'allowAddOption')?.checked;
        const hideResultBeforeAnswers = anonymousVoting.find(item => item.id === 'hideResultBeforeAnswers')?.checked;
        const hideMemberAnswers = anonymousVoting.find(item => item.id === 'hideMemberAnswers')?.checked;
        const dataToValidate: {
            isPinned: boolean | undefined;
            question: string;
            option: string[];
            allowMultipleAnswers: boolean | undefined;
            allowAddOption: boolean | undefined;
            hideResultBeforeAnswers: boolean | undefined;
            hideMemberAnswers: boolean | undefined;
            duration?: string;
        } = {
            isPinned,
            question: value,
            option: choices,
            allowMultipleAnswers,
            allowAddOption,
            hideResultBeforeAnswers,
            hideMemberAnswers
        };
        if (durationInMilliseconds !== 0) {
            dataToValidate.duration = JSON.stringify(durationInMilliseconds);
        }
        // Add your logic to handle the form submission
        try {
            const validatedData = voteSchema.validateSync(dataToValidate, { abortEarly: false });
            if (!validatedData) {
                return;
            }
            const res = await getAxiosClient().post(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/vote/${idConversation}`, dataToValidate);
            if (res.status === 200) {
                message.success("Đã tạo bình chọn thành công");
                const { data } = await fetchVotes(idConversation);
                dispatch(setVotes(data));
                setIsShowModal(false);
            }
        } catch (err: any) {
            if (err instanceof yup.ValidationError) {
                err.errors.forEach((error) => {
                    toast.error(error);
                });
            } else {
                console.error('Unexpected Error:', err.message);
            }
        }
    };
    const handleInputChange = (e: any) => {
        if (e.target.value.length <= 200) {
            setValue(e.target.value);
            // Reset the error message state when the length is within the limit
            setErrorMessageShown(false);
        } else if (!errorMessageShown) {
            // Show the error message only once when the limit is exceeded
            message.error('Bạn đã nhập quá số ký tự cho phép');
            setErrorMessageShown(true);
        }
    };

    return (
        <div
            id="default-modal"
            tabIndex={-1}
            className=" overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0  justify-center items-center w-full md:inset-0 h-auto z-40"
        >
            <div
                className="fixed inset-0 w-full h-screen opacity-40"
                onClick={() => setIsShowModal(false)}
            ></div>
            <div className="relative z-50 max-w-[90%] md:max-w-[70%] lg:max-w-[50%] max-h-[90vh] overflow-y-auto  max-[480px]:w-[90%]  p-4 m-auto text-thWhite bg-thDark-background border border-thPrimary rounded-md mt-5">
                <div className="flex gap-x-4 items-center">
                    <div className="cursor-pointer" onClick={() => setIsShowModal(false)}>
                        <FiChevronLeft size={40} />
                    </div>
                    <p className="text-lg font-semibold">Tạo bình chọn</p>
                </div>
                <div className="mt-2">
                    <div className="grid grid-cols-1 md:grid-cols-5 lg:grid-cols-5 gap-2">
                        <div className={isShowSetting ? "col-span-3" : "col-span-5"}>
                            <label htmlFor="note" className="text-sm">
                                Chủ đề bình chọn
                            </label>
                            <div className="relative">
                                <textarea
                                    className="w-full h-40 border border-gray-300 rounded-md p-2 mt-2 bg-thNewtral2 text-white"
                                    name="note"
                                    id="note"
                                    value={value}
                                    onChange={handleInputChange}
                                ></textarea>
                                <span className={`absolute bottom-2 right-2 text-xs`}>{value.length} / 200</span>
                            </div>
                            <div className="flex flex-col mb-4">
                                <p className="text-sm">Các lựa chọn</p>
                                {choices.map((choice, index) => (
                                    <div key={index} className="relative mt-2">
                                        <input
                                            type="text"
                                            className="w-full border border-gray-300 rounded-md p-2  bg-thNewtral2 text-white"
                                            placeholder={`Lựa chọn ${index + 1}`}
                                            value={choice}
                                            onChange={(e) => handleChoiceChange(index, e.target.value)}
                                            required
                                        />
                                        {
                                            index > 1 &&
                                            <span className="absolute right-2 text-xs top-1/2 -translate-y-1/2" onClick={() => handleRemoveChoice(index)}><IoMdClose color="red" size={20} /></span>
                                        }
                                    </div>
                                ))}
                                <button
                                    className="flex items-center gap-2 mt-2"
                                    onClick={handleAddChoice}
                                >
                                    <FaPlus /> Thêm lựa chọn
                                </button>
                            </div>
                        </div>
                        <div className={isShowSetting ? "col-span-2 pt-2" : "hidden"}>
                            <div>
                                <h5>Thời hạn bình chọn</h5>
                                <div className="w-full">
                                    <LocalizationProvider dateAdapter={AdapterDayjs}>
                                        <DateTimePicker
                                            value={duration}
                                            onChange={handleDateChange}
                                            ampm={false}
                                            className="bg-thNewtral2 text-white"
                                        />
                                    </LocalizationProvider>
                                </div>
                            </div>
                            <div className="mt-2 border-t-2 border-solid border-gray-500 flex flex-col gap-x-2">
                                <h5 className="font-bold">Thiết lập nâng cao</h5>
                                {advancedSettings.map((item) => (
                                    <div key={item.id} className="flex justify-between items-center">
                                        <span className="flex items-center gap-1">{item.label}</span>
                                        <label className="relative inline-flex items-center cursor-pointer">
                                            <input
                                                type="checkbox"
                                                value={item.id}
                                                id={item.id}
                                                className="sr-only peer"
                                                checked={item.checked}
                                                onChange={() => handleCheckboxChange('advancedSettings', item.id)}
                                            />
                                            {/* ... (phần tử UI khác) */}
                                            <div className="w-9 h-5 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-4 after:w-4 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600" />
                                            <span className=""></span>
                                        </label>
                                    </div>
                                ))}
                            </div>

                            <div className="mt-2 border-t-2 border-solid border-gray-500 flex flex-col gap-x-2">
                                <h5 className="font-bold">Bình chọn ẩn danh</h5>
                                {anonymousVoting.map((item) => (
                                    <div key={item.id} className="flex justify-between items-center mt-2">
                                        <span className="flex items-center gap-1">{item.label}</span>
                                        <label className="relative inline-flex items-center cursor-pointer">
                                            <input
                                                type="checkbox"
                                                value={item.id}
                                                id={item.id}
                                                className="sr-only peer"
                                                checked={item.checked}
                                                onChange={() => handleCheckboxChange('anonymousVoting', item.id)}
                                            />
                                            {/* ... (phần tử UI khác) */}
                                            <div className="w-9 h-5 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-4 after:w-4 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600" />
                                            <span className=""></span>
                                        </label>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                    <div className="mt-2 border-t-2 border-gray-500 flex justify-between items-center gap-3 py-2">
                        <button className="w-fit rounded-md py-2 bg-gray-500 text-black font-bold px-4"
                            onClick={() => setIsShowSetting(!isShowSetting)}
                        ><IoSettingsOutline size={24} /></button>
                        <div>
                            <button
                                className="w-fit rounded-md py-2 bg-gray-500 text-black font-bold px-4"
                                onClick={() => setIsShowModal(false)}
                            >
                                Hủy
                            </button>
                            <button
                                className="w-fit rounded-md py-2 bg-blue-700 text-white font-bold px-4 ml-2"
                                onClick={handleSubmit}
                            >
                                Xác nhận
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
