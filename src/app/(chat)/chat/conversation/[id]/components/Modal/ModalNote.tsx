
import { CONVERSATION } from "@/src/constants/chat";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { setNotes, setStartEditNote } from "@/src/redux/slices/newsletterSlice";
import getAxiosClient from "@/src/utils/axios/axiosClient";
import { sortNotes } from "@/src/utils/newsletter/sortHepper";
import { message } from "antd";
import moment from "moment";
import { ReactElement, cache, useState } from "react";
import { FiChevronLeft } from "react-icons/fi";
import { toast } from "react-toastify";
interface IModalNoteProps {
    setIsShowModal: React.Dispatch<React.SetStateAction<boolean>>,
    idConversation: string,
}
const fetchNotes = cache((id: string) =>
    getAxiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/note/${id}?limit=20&offset=0`
    )
);
export default function ModalNote({ setIsShowModal, idConversation }: IModalNoteProps): ReactElement {
    const { user, newsletter, detailChat } = useAppSelector((state) => state.user);
    const [value, setValue] = useState(newsletter.actionForNote.noteItem?.content || "");
    const [isPin, setIsPin] = useState(newsletter.actionForNote.noteItem?.isPinned || false);
    const action = newsletter.actionForNote.type;
    const dispatch = useAppDispatch();
    const handleSubmit = async () => {
        try {
            const endpoint = action === "EDIT"
                ? `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/note/${newsletter.actionForNote.noteItem?.id}`
                : `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/note/${idConversation}`;
            const response = await getAxiosClient().request({
                method: action === "EDIT" ? "PUT" : "POST",
                url: endpoint,
                data: {
                    isPinned: isPin,
                    content: value
                }
            });
            if (response.status === 200) {
                if (action === "EDIT") {
                    const findNote = [...newsletter.noteList as any]?.find((item) => item.id === newsletter.actionForNote.noteItem?.id);
                    const newNote = {
                        ...findNote,
                        isPinned: isPin,
                        content: value
                    };
                    const newListNote = newsletter.noteList?.map((item) => {
                        if (item.id === newsletter.actionForNote.noteItem?.id) {
                            return newNote;
                        }
                        return item;
                    });
                    dispatch(setNotes(sortNotes(newListNote as any)));
                } else {
                    const fetchNote = await fetchNotes(idConversation);
                    dispatch(setNotes(sortNotes(fetchNote.data as any)));
                }
                const successMessage = action === "EDIT" ? "Thay đổi ghi chú thành công" : "Tạo ghi chú thành công";
                toast.success(successMessage);
                setIsShowModal(false);
            }
        } catch (err: any) {
            toast.error(err.response?.data?.message || "An error occurred");
            console.error(err);
        }
    };
    const isTypeWaitting = detailChat?.members.find((item: any) => item.id === user?.id)?.type === CONVERSATION.ROLE_TYPE.MEMBER_WAITING_ACCEPT;
    const isOwner = detailChat?.members.find((item: any) => item.id === user?.id)?.type === CONVERSATION.ROLE_TYPE.OWNER;
    const isAdmin = detailChat?.members.find((item: any) => item.id === user?.id)?.type === CONVERSATION.ROLE_TYPE.ADMIN;
    return (
        <div
            id="default-modal"
            tabIndex={-1}
            className=" overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0  justify-center items-center w-full md:inset-0 h-[calc(100%-1rem)] max-h-full z-40"
        >
            <div className="fixed inset-0 w-full h-screen opacity-40" onClick={() => setIsShowModal(false)}></div>
            <div className="relative z-50 w-full max-w-md max-h-full  max-[480px]:w-[90%] p-4 m-auto dark:text-thWhite bg-light-thNewtral2 dark:bg-thDark-background border border-thPrimary rounded-md mt-5">
                <div className="flex gap-x-4 items-center">
                    <div className="cursor-pointer" onClick={() => setIsShowModal(false)}>
                        <FiChevronLeft size={40} />
                    </div>
                    <p className="text-lg font-semibold">{action === "EDIT" ? "Thay đổi ghi chú" : "Tạo ghi chú"}</p>
                </div>
                <div className="mt-2">
                    {
                        action === "VIEW" &&
                        <>
                            <p className="w-full text-sm text-center font-['Roboto'] p-2">Tạo bởi {newsletter.actionForNote.noteItem?.createdBy?.username} - {moment(newsletter.actionForNote.noteItem?.createdAt).locale('vi').fromNow()}</p>
                            <p className="text-base font-medium mt-2 break-words"> {newsletter.actionForNote.noteItem?.content}</p>
                        </>
                    }
                    {
                        (action === "EDIT" || action === "CREATE") &&
                        <>
                            <label htmlFor="note" className="text-sm">Nội dung</label>
                            <textarea
                                className="w-full h-40 border border-gray-300 rounded-md p-2 mt-2 bg-thNewtral2 text-white"
                                name="note"
                                id="note"
                                value={value}
                                onChange={(e) => setValue(e.target.value)}
                            ></textarea>
                            <div className="flex items-center mb-4">
                                <input id="checkbox-2" type="checkbox" value="" className="w-4 h-4  rounded focus:ring-blue-600 ring-offset-gray-800 focus:ring-offset-gray-800 focus:ring-2 bg-gray-700 border-gray-600"
                                    checked={isPin}
                                    onChange={() => {
                                        if ((detailChat?.conversationSetting?.find((item) => item.type === 3)?.value === true && !isTypeWaitting) ||
                                            ((isAdmin || isOwner))) {
                                            setIsPin(!isPin);
                                        } else {
                                            message.error("Tính năng ghim đã khóa");
                                            setIsPin(false);
                                        }
                                    }}
                                />
                                <label htmlFor="checkbox-2" className="ms-2 text-sm font-medium text-thWhite">Ghim lên đầu trò chuyện</label>
                            </div>
                        </>
                    }
                    <div className="mt-2 border-t-2 border-gray-500 flex justify-end items-center gap-3 py-2">
                        {
                            action === "VIEW" &&
                            <>
                                <button className="w-fit rounded-md py-2 bg-gray-500 text-black font-bold px-4"
                                    onClick={() => setIsShowModal(false)}
                                >Đóng</button>
                                <button className="w-fit rounded-md py-2 bg-blue-700 text-white font-bold px-4"
                                    onClick={() => dispatch(setStartEditNote({ typeAction: "EDIT", note: newsletter.actionForNote.noteItem as any }))}
                                >Chỉnh sửa</button>
                            </>
                        }
                        {
                            (action === "EDIT" || action === "CREATE") &&
                            <>
                                <button className="w-fit rounded-md py-2 bg-gray-500 text-black font-bold px-4"
                                    onClick={() => setIsShowModal(false)}
                                >Hủy</button>
                                <button className="w-fit rounded-md py-2 bg-blue-700 text-white font-bold px-4"
                                    onClick={handleSubmit}
                                >{action === "EDIT" ? "Lưu" : "Xác nhận"}</button>
                            </>
                        }
                    </div>
                </div>
            </div>
        </div>
    );
}