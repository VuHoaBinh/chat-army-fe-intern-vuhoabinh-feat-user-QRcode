"use client";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import {
    setListConversations,
    setListHiddenConversation
} from "@/src/redux/slices/conversationSlice";
import axiosClient from "@/src/utils/axios/axiosClient";
import {
    filterConversation,
    sortConversationByNewest
} from "@/src/utils/conversations/filterConversation";
import * as bcrypt from "bcryptjs";
import React, {
    ChangeEvent,
    ReactElement,
    cache,
    useCallback,
    useEffect,
    useState
} from "react";
import { AiFillEye, AiFillEyeInvisible } from "react-icons/ai";
import { toast } from "react-toastify";

interface Props {
    idConversation: string;
    setIsOpenUpdatePinHidden: (isOpenUpdatePinHidden: boolean) => void;
}
const fetchConversations = cache(() =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations?offset=0&status=[2]`
    )
);

const ModalUpdatePinHidden = ({
    idConversation,
    setIsOpenUpdatePinHidden
}: Props): ReactElement => {
    const [inputOldPinValue, setInputOldPinValue] = useState<string>("");
    const [inputNewPinValue, setInputNewPinValue] = useState<string>("");
    const [pinHidden, setPinHidden] = useState<string>("");
    const [typeOldPin, setTypeOldPin] = useState<string>("password");
    const [typeNewPin, setTypeNewPin] = useState<string>("password");
    const user = useAppSelector((state) => state.user.user);
    const conversation = useAppSelector((state) => state.user.conversation);

    const dispatch = useAppDispatch();
    useEffect(() => {
        if (conversation.listHiddenConversation) {
            const hiddenConversation = conversation.listHiddenConversation.find(
                (item) => item._id === idConversation
            );
            if (hiddenConversation) {
                setPinHidden(hiddenConversation?.isConversationHidden.pin);
            }
        }
    }, []);

    const fetchData = useCallback(async () => {
        const { data } = await fetchConversations();
        return filterConversation(data, user.id);
    }, [user.id]);


    const handleInputOldPinChange = (event: ChangeEvent<HTMLInputElement>) => {
        setInputOldPinValue(event.target.value);
    };
    const handleInputNewPinChange = (event: ChangeEvent<HTMLInputElement>) => {
        setInputNewPinValue(event.target.value);
    };

    const handleButtonConfirm = () => {
        // Kiểm tra input rỗng
        if (inputNewPinValue === "" || inputOldPinValue === "") {
            alert("Vui lòng điền đầy đủ thông tin!");
            return;
        }
        // Kiểm tra mã PIN cũ
        try {
            bcrypt.compare(inputOldPinValue, pinHidden, async function (err, result) {
                if (err) {
                    console.error(err);
                    return;
                }
                if (result) {
                    const id = toast.loading("Đang đổi mã PIN...");

                    try {
                        const salt = 10;
                        const hashedPassword = await bcrypt.hash(inputNewPinValue, salt);
                        const bodyUnhidden: {
                            conversationId: string;
                        } = {
                            conversationId: idConversation
                        };
                        const bodyUpdate: {
                            conversationId: string;
                            pin: string;
                        } = {
                            conversationId: idConversation,
                            pin: hashedPassword
                        };
                        axiosClient()
                            .post(
                                `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/unhidden`,
                                bodyUnhidden
                            )
                            .then(() => {
                                axiosClient()
                                    .post(
                                        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/hidden`,
                                        bodyUpdate
                                    )
                                    .then(() => {
                                        fetchData().then((data) => {
                                            const conversationsWithHiddenFlag =
                                                sortConversationByNewest(data).filter(
                                                    (item: any) => item.isConversationHidden
                                                );
                                            dispatch(
                                                setListHiddenConversation(conversationsWithHiddenFlag)
                                            );
                                            dispatch(
                                                setListConversations(sortConversationByNewest(data))
                                            );
                                        });

                                        toast.update(id, {
                                            render: "Đổi mã PIN thành công",
                                            type: "success",
                                            isLoading: false,
                                            autoClose: 3000,
                                            closeButton: true
                                        });
                                        setIsOpenUpdatePinHidden(false);
                                    })
                                    .catch((error: any) => {
                                        console.log(error);
                                    });
                            })
                            .catch((error: any) => {
                                console.log(error);
                            });
                    } catch (e) {
                        console.error(e);
                    }
                } else {
                    alert("Mã PIN không đúng!");
                }
            });
        } catch (e) {
            console.error(e);
        }

    };
    const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === "Enter") {
            handleButtonConfirm();
        }
    };

    return (
        <div className="flex flex-col h-[220px] lg:h-[250px] w-[270px] lg:w-[300px] fixed dark:bg-thNewtral1 bg-light-thNewtral1 shadow-2xl rounded-2xl overflow-hidden top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 z-50 px-3 pb-3">
            <div className=" h-12 w-full flex justify-between items-center">
                <h1 className="h-10 w-max flex justify-center items-center">
                    Đổi mã PIN
                </h1>
                <span
                    className="h-10 aspect-square flex justify-center items-center hover:bg-thNewtral  cursor-pointer"
                    onClick={() => {
                        setIsOpenUpdatePinHidden(false);
                    }}
                >
                    X
                </span>
            </div>
            <div className=" flex-1 flex flex-col justify-evenly ">
                <div className="flex justify-center items-center">
                    <input
                        type={typeOldPin}
                        onChange={handleInputOldPinChange}
                        maxLength={5}
                        placeholder="Nhập mã PIN cũ"
                        className={
                            "outline-none dark:bg-thNewtral bg-light-thNewtral p-2 rounded-lg w-[90%] text-base"
                        }
                    />
                    {typeOldPin === "password" ? (
                        <AiFillEye
                            size={20}
                            className="text-thPrimary ml-2"
                            onClick={() => setTypeOldPin("text")}
                        />
                    ) : (
                        <AiFillEyeInvisible
                            size={20}
                            className="text-thPrimary ml-2"
                            onClick={() => setTypeOldPin("password")}
                        />
                    )}
                </div>
                <div className="flex justify-center items-center">
                    <input
                        type={typeNewPin}
                        onChange={handleInputNewPinChange}
                        maxLength={5}
                        placeholder="Nhập mã PIN mới"
                        className={
                            "outline-none dark:bg-thNewtral bg-light-thNewtral p-2 rounded-lg w-[90%] text-base"
                        }
                        onKeyDown={handleKeyDown}
                    />
                    {typeNewPin === "password" ? (
                        <AiFillEye
                            size={20}
                            className="text-thPrimary ml-2"
                            onClick={() => setTypeNewPin("text")}
                        />
                    ) : (
                        <AiFillEyeInvisible
                            size={20}
                            className="text-thPrimary ml-2"
                            onClick={() => setTypeNewPin("password")}
                        />
                    )}
                </div>
            </div>
            <div className=" h-12 w-full flex justify-end items-center">
                <button
                    className="h-10 w-[64px] bg-gray-400 rounded-md mr-2"
                    onClick={() => {
                        setIsOpenUpdatePinHidden(false);
                    }}
                >
                    Hủy
                </button>
                <button
                    className="h-10 w-[116px]  dark:bg-thPrimary bg-light-thPrimary text-thNewtral1 hover:opacity-75 rounded-md"
                    onClick={handleButtonConfirm}
                >
                    Xác nhận
                </button>
            </div>
        </div>
    );
};

export default ModalUpdatePinHidden;
