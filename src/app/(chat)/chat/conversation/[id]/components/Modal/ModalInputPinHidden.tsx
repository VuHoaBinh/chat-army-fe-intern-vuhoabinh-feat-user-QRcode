"use client";
import * as bcrypt from "bcryptjs";
import React, {
    ChangeEvent,
    ReactElement,
    cache,
    use,
    useState
} from "react";
import { AiFillEye, AiFillEyeInvisible } from "react-icons/ai";
import { useRouter } from "next/navigation";
import { CONVERSATION } from "@/src/constants/chat";
import axiosClient from "@/src/utils/axios/axiosClient";
import ModalResetPinHidden from "./ModalResetPinHidden";

interface Props {
    pinHidden: string;
    setIsHidden: (isHidden: boolean) => void;
    idConversation: string;
}

const fetchDetailChat = cache((id: string) =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${id}`
    )
);

const ModalInputPinHidden = ({
    pinHidden,
    setIsHidden,
    idConversation
}: Props): ReactElement => {
    const [inputValue, setInputValue] = useState<string>("");
    const [type, setType] = useState<string>("password");
    const [isOpenResetPinHidden, setIsOpenResetPinHidden] =
        useState<boolean>(false);
    const router = useRouter();

    const handleInputChange = (event: ChangeEvent<HTMLInputElement>) => {
        setInputValue(event.target.value);
    };
    const handleButtonConfirm = () => {
        // Kiểm tra input rỗng
        if (inputValue === "") {
            alert("Vui lòng nhập mã PIN!");
            return;
        }
        try {
            bcrypt.compare(inputValue, pinHidden, function (err, result) {
                if (err) {
                    console.error(err);
                    return;
                }
                if (result) {
                    setIsHidden(false);
                } else {
                    alert("Mật khẩu không đúng!");
                }
            });
        } catch (e) {
            console.error(e);
        }
    };
    const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === "Enter") {
            handleButtonConfirm();
        }
    };
    const isGroupRef =
        (use(fetchDetailChat(idConversation)) as any)?.data?.type ===
            CONVERSATION.TYPE.GROUP
            ? true
            : false;

    return (
        <div className="flex flex-col h-[240px] w-max lg:w-[300px] fixed dark:bg-thNewtral1 bg-light-thNewtral1 shadow-2xl rounded-2xl overflow-hidden top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 z-50 px-3 pb-3">
            <div className=" h-12 w-full flex justify-between items-center">
                <h1 className="h-10 w-max flex justify-center items-center">
                    Nhập mã PIN để mở trò chuyện
                </h1>
                <span
                    className="h-10 aspect-square flex justify-center items-center  cursor-pointer"
                    onClick={() => {
                        if (isGroupRef) {
                            router.push(`/chat?option=group`);
                        } else {
                            router.push(`/chat`);
                        }
                    }}
                >
                    X
                </span>
            </div>
            <div className=" flex-1 flex flex-col justify-evenly items-center">
                <div className="flex items-center">
                    <input
                        type={type}
                        onChange={handleInputChange}
                        maxLength={5}
                        placeholder="Nhập mã PIN"
                        className={"outline-none dark:bg-thNewtral bg-light-thNewtral p-2 rounded-lg w-[90%] "}
                        onKeyDown={handleKeyDown}
                    />
                    {type === "password" ? (
                        <AiFillEye
                            size={20}
                            className="text-thPrimary ml-2"
                            onClick={() => setType("text")}
                        />
                    ) : (
                        <AiFillEyeInvisible
                            size={20}
                            className="text-thPrimary ml-2"
                            onClick={() => setType("password")}
                        />
                    )}
                </div>
                <p className="text-thWhite text-[.8125rem] text-center font-light">
                    Quên mã PIN? Bạn phải{" "}
                    <span
                        className="text-thPrimary hover:brightness-125 cursor-pointer"
                        onClick={() => setIsOpenResetPinHidden(true)}
                    >
                        Đặt lại mã PIN
                    </span>
                </p>
            </div>
            <div className=" h-12 w-full flex justify-end items-center">
                <button
                    className="h-10 w-[64px] bg-gray-400 rounded-md mr-2"
                    onClick={() => {
                        if (isGroupRef) {
                            router.push(`/chat?option=group`);
                        } else {
                            router.push(`/chat`);
                        }
                    }}
                >
                    Hủy
                </button>
                <button
                    className="h-10 w-[116px]  dark:bg-thPrimary bg-light-thPrimary text-thNewtral1 hover:opacity-75 rounded-md"
                    onClick={handleButtonConfirm}
                >
                    Xác nhận
                </button>
            </div>
            {isOpenResetPinHidden && (
                <ModalResetPinHidden
                    isGroupRef={isGroupRef}
                    setIsOpenResetPinHidden={setIsOpenResetPinHidden}
                />
            )}
        </div>
    );
};

export default ModalInputPinHidden;
