"use client";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { setListConversations, setListHiddenConversation } from "@/src/redux/slices/conversationSlice";

import axiosClient from "@/src/utils/axios/axiosClient";
import {
    filterConversation,
    sortConversationByNewest
} from "@/src/utils/conversations/filterConversation";
import { useRouter } from "next/navigation";
import React, {
    ReactElement,
    cache,
    useCallback,
    useEffect,
    useState
} from "react";
import { toast } from "react-toastify";

const fetchConversations = cache(() =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations?offset=0&status=[2]`
    )
);

interface Props {
    setIsOpenResetPinHidden: (isOpenResetPinHidden: boolean) => void;
    isGroupRef?: boolean;
}
const ModalResetPinHidden = ({
    setIsOpenResetPinHidden,
    isGroupRef
}: Props): ReactElement => {
    const [listIdHiddenConversation, setListIdHiddenConversation] =
        useState<string[]>();
    const router = useRouter();
    const dispatch = useAppDispatch();
    const user = useAppSelector((state) => state.user.user);

    const fetchData = useCallback(async () => {
        const { data } = await fetchConversations();
        return filterConversation(data, user.id);
    }, [user.id]);
    useEffect(() => {
        fetchData().then((data) => {
            const conversationsWithHiddenFlag = sortConversationByNewest(data).filter(
                (item: any) => item.isConversationHidden
            );
            if (conversationsWithHiddenFlag) {
                const arrIdConversationHidden: string[] = conversationsWithHiddenFlag.map(
                    (conversation) => conversation._id
                );
                setListIdHiddenConversation(arrIdConversationHidden);
            }
        });
    }, []);

    const handleUnhidden = (idConversation: string) => {
        const bodyUnhidden: {
            conversationId: string;
        } = {
            conversationId: idConversation
        };

        axiosClient()
            .post(
                `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/unhidden`,
                bodyUnhidden
            )
            .then(() => {
                fetchData().then((data) => {
                    dispatch(setListHiddenConversation([]));
                    dispatch(setListConversations(sortConversationByNewest(data)));
                });
            })
            .catch((error: any) => {
                console.log(error);
            });
    };

    const handleButtonConfirm = () => {
        const id = toast.loading("Đang đặt lại mã PIN...");
        try {
            if (listIdHiddenConversation) {
                listIdHiddenConversation.forEach((id) => {
                    handleUnhidden(id);
                });
            }


            toast.update(id, {
                render: "Hoàn tất đặt lại mã PIN",
                type: "success",
                isLoading: false,
                autoClose: 3000,
                closeButton: true
            });

            // Nếu có isGroupRef là đặt lại từ Pages, nếu không là đặt lại từ Setting
            if (isGroupRef !== undefined) {
                if (isGroupRef) {
                    router.push(`/chat?option=group`);
                } else {
                    router.push(`/chat`);
                }
            } else {
                setIsOpenResetPinHidden(false);
            }
        } catch (e) {
            toast.update(id, {
                render: "Có lỗi khi đặt lại mã PIN, vui lòng quay lại sau",
                type: "error",
                isLoading: false,
                autoClose: 3000,
                closeButton: true
            });
        }
    };

    return (
        <div
            className={`flex flex-col h-[240px] w-[270px] lg:w-[300px] fixed ${isGroupRef === undefined ? "dark:bg-thNewtral bg-light-thNewtral" : "dark:bg-thNewtral1 bg-light-thNewtral"
            } shadow-2xl rounded-2xl overflow-hidden top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 z-50 px-3 pb-3`}
        >
            <div className=" h-12 w-full flex justify-between items-center">
                <h1 className="h-10 w-max flex justify-center items-center">
                    Đặt lại mã PIN
                </h1>
                <span
                    className="h-10 aspect-square flex justify-center items-center dark:hover:bg-thNewtral hover:bg-light-thNewtral  cursor-pointer"
                    onClick={() => {
                        setIsOpenResetPinHidden(false);
                    }}
                >
                    X
                </span>
            </div>
            <div className=" flex-1 flex flex-col justify-evenly ">
                Khi đặt lại mã PIN, tất cả cuộc trò chuyện sẽ bị bỏ ẩn. Bạn có muốn tiếp
                tục?
            </div>
            <div className=" h-12 w-full flex justify-end items-center">
                <button
                    className="h-10 w-[64px] bg-gray-400 rounded-md mr-2 hover:opacity-75"
                    onClick={() => {
                        setIsOpenResetPinHidden(false);
                    }}
                >
                    Hủy
                </button>
                <button
                    className="h-10 w-max px-2 bg-thRed hover:opacity-75 rounded-md"
                    onClick={handleButtonConfirm}
                >
                    Đặt lại mã PIN
                </button>
            </div>
        </div>
    );
};

export default ModalResetPinHidden;
