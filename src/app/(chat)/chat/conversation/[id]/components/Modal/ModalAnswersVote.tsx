import { CONVERSATION } from "@/src/constants/chat";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { setEndAnswersVote, setVotes } from "@/src/redux/slices/newsletterSlice";
import getAxiosClient from "@/src/utils/axios/axiosClient";
import { Dropdown, message } from "antd";
import moment from "moment";
import Image from "next/image";
import React, { ReactElement, useEffect, useState } from "react";
import { AiFillCheckCircle } from "react-icons/ai";
import { FaPlus } from "react-icons/fa";
import { IoSettingsOutline } from "react-icons/io5";
import { toast } from "react-toastify";

export default function ModalAnswersVote(): ReactElement {
    const dispatch = useAppDispatch();
    const { answersVoteItem } = useAppSelector(
        (state) => state.user.newsletter
    );
    const { user, detailChat, newsletter } = useAppSelector((state) => state.user);
    const closeModal = () => {
        dispatch(setEndAnswersVote());
    };
    const [voteItem, setVoteItem] = useState({ ...answersVoteItem });

    const [selectedAnswers, setSelectedAnswers] = useState<(string | number)[]>([]);
    const [newChoice, setNewChoice] = useState<string>("");
    const [choices, setChoices] = useState<string[]>([]);
    const isClosed = voteItem?.status === 2;

    const handleAnswerSelection = (optionId: string | number) => {
        if (detailChat?.members?.find(member => member.id === user?.id)?.type === CONVERSATION.ROLE_TYPE.MEMBER_WAITING_ACCEPT || isClosed) return;
        setVoteItem((prevVoteItem) => {
            if (!prevVoteItem) return prevVoteItem;
            if (prevVoteItem.allowMultipleAnswers) {
            // Toggle the selected state
                const isSelected = selectedAnswers.includes(optionId);
                const updatedAnswers = isSelected
                    ? selectedAnswers.filter((id) => id !== optionId)
                    : [...selectedAnswers, optionId];
                // Update the selected answers in the vote item
                const updatedOptions = prevVoteItem.conversationVoteOptions?.map((option) => {
                    if (option.id === optionId) {
                        return {
                            ...option,
                            conversationVoteAnswers: isSelected
                                ? option.conversationVoteAnswers?.filter((answer) => answer.user.id !== user?.id)
                                : [
                                    ...(option.conversationVoteAnswers || []),
                                    { user: user, createdAt: moment().toISOString(), updatedAt: moment().toISOString() }
                                ]
                        };
                    }
                    return option;
                });
                setSelectedAnswers(updatedAnswers);
                return {
                    ...prevVoteItem,
                    conversationVoteOptions: updatedOptions
                };}
            // Toggle the selected state
            const isSelected = selectedAnswers.includes(optionId);

            // If the option is already selected, do nothing
            if (isSelected) return prevVoteItem;

            // Set the selected option
            const updatedOptions = prevVoteItem.conversationVoteOptions?.map((option) => {
                return {
                    ...option,
                    conversationVoteAnswers: option.id === optionId
                        ? [
                            { user: user, createdAt: moment().toISOString(), updatedAt: moment().toISOString() }
                        ]
                        : []
                };
            });

            setSelectedAnswers([optionId]);

            return {
                ...prevVoteItem,
                conversationVoteOptions: updatedOptions
            };
        });
    };

    const handleAddChoice = () => {
        if(isClosed) return;
        setChoices([...choices, newChoice]);
        setNewChoice("");
    };

    const handleDeleteChoice = (index: number) => {
        const updatedChoices = [...choices];
        updatedChoices.splice(index, 1);
        setChoices(updatedChoices);
    };

    const handleVoteConfirmation = async () => {
        if(detailChat?.members?.find(member => member.id === user?.id)?.type === CONVERSATION.ROLE_TYPE.MEMBER_WAITING_ACCEPT) return message.error("Bạn không có quyền vote");
        if(isClosed) return;
        if(selectedAnswers.length === 0){
            message.error("Vui lý chọn phương án");
            return;
        }
        try {
            const arrayAnswers = Array.from(new Set([...selectedAnswers.filter(answer => typeof answer === 'string')]));
            // check 2 choice không được giống nhau
            const selectNewChoice = [...selectedAnswers.filter(answer => typeof answer === 'number')];
            if(choices.length > 0){
                const addOption = await getAxiosClient().put(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/vote/${voteItem.id}/4`, {
                    option: choices
                });
                if (addOption.status === 200) {
                    if(selectNewChoice.length > 0){
                        selectNewChoice.forEach((item)=>{
                            arrayAnswers.push(addOption.data[item].id);
                        });
                    }
                }
            }
            const res = await getAxiosClient().post(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/answers-vote`, {
                voteOptionId: arrayAnswers
            });

            if (res.status === 200) {
                const getVote = await getAxiosClient().get(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/vote/${detailChat?.id}?limit=20&sort=asc&offset=0`);
                if (getVote.status === 200) {
                    // Dispatch the updated votes to the Redux store
                    dispatch(setVotes(getVote.data));
                }

                closeModal();
                message.success("Bạn đã bình chọn thành công");
            }
        } catch (err: any) {
            console.error(err);

            if (err.response?.data?.data) {
                message.error(err.response.data.data === "Vote closed." ? "Đã đóng bình chọn" : "Error");
            } else {
                message.error("An error occurred while processing your vote.");
            }
        }
    };

    useEffect(() => {
        if (voteItem) {
            voteItem.conversationVoteOptions?.forEach((option) => {
                const hasUserAnswered = option.conversationVoteAnswers.some(
                    (answer) => answer?.user.id === user.id
                );

                if (hasUserAnswered) {
                    setSelectedAnswers((prevSelectedAnswers) => [
                        ...prevSelectedAnswers,
                        option.id
                    ]);
                }
            });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [voteItem, user.id]);

    // Calculate totalMembersVoted
    const isTypeWaitting = detailChat?.members.find((item: any) => item.id === user?.id)?.type === CONVERSATION.ROLE_TYPE.MEMBER_WAITING_ACCEPT;
    const isOwner  = detailChat?.members.find((item: any) => item.id === user?.id)?.type === CONVERSATION.ROLE_TYPE.OWNER;
    const isAdmin = detailChat?.members.find((item: any) => item.id === user?.id)?.type === CONVERSATION.ROLE_TYPE.ADMIN;
    const handlePinVote = async(action: number)=>{
        try{
            if((detailChat?.conversationSetting?.find((item) => item.type === 3)?.value === true && !isTypeWaitting) ||
                                        ((isAdmin || isOwner) )){
                const res = await getAxiosClient().put(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/vote/${voteItem.id}/${action}`);
                if(res.status === 200){
                    const newVote = {
                        ...voteItem,
                        isPinned: action === 1 ? true : false 
                    };
                    toast.success(action === 1 ? "Đã ghim" : "Đã bỏ ghim");
                    const newListVote = [...newsletter.voteList as any].map((item) => {
                        if(item.id === voteItem.id){
                            return newVote;
                        }
                        return item;
                    });
                    dispatch(setVotes(newListVote as any));
                }
            }else{
                message.error("Tính năng ghim đã khóa");
            }
        }catch(err){
            console.log(err);
        }
    };
    const [isVoted, setIsVoted] = useState(false);

    useEffect(() => {
        if (voteItem) {
            setIsVoted(newsletter.voteList.find((item) => item.id === voteItem.id)?.conversationVoteOptions?.some((option) => option.conversationVoteAnswers.some((answer) => answer?.user.id === user.id)) || false);
        }
    }, [voteItem, user.id, newsletter.voteList]);
    const totalMembersVoted = (() => {
        const userIDs = new Set(
            voteItem?.conversationVoteOptions?.flatMap(option =>
                option.conversationVoteAnswers.map(answer => answer?.user.id)
            )
        );
        return (voteItem.hideResultBeforeAnswers && !isVoted) ? Math.max(userIDs.size, 1) : userIDs.size || 1;
    })();
    const handleCloseVote = async() => {
        try{
            const res = await getAxiosClient().put(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/vote/${voteItem.id}/3`);
            if(res.status === 200){
                closeModal();
                toast.success("Đã đóng bình chọn");
                const newVote = {
                    ...voteItem,
                    status: 2
                };
                const newListVote = [...newsletter.voteList as any].map((item) => {
                    if(item.id === newVote.id){
                        return newVote;
                    }
                    return item;
                });
                dispatch(setVotes(newListVote as any));
            }
        }catch(e){
            toast.error("Đóng bình chọn thất bại");
        }
    };
    return (
        <div
            id="default-modal"
            tabIndex={-1}
            className="overflow-x-hidden fixed top-0 right-0 left-0 justify-center items-center w-full md:inset-0 h-[calc(100%-1rem)] overflow-y-auto max-h-full z-40"
        >
            <div
                className="fixed inset-0 w-full h-screen opacity-40"
                onClick={closeModal}
            ></div>
            <div className="relative z-50 max-w-[50%] max-h-full max-[480px]:max-w-[90%] p-4 m-auto text-thWhite bg-thDark-background border border-thPrimary rounded-md mt-5">
                <div className="flex items-start justify-between p-3 border-b rounded-t border-gray-600">
                    <h3 className="text-xl font-semibold text-white">
                        Bình chọn
                    </h3>
                    <button
                        type="button"
                        className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ml-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white"
                        data-modal-hide="defaultModal"
                        onClick={closeModal}
                    >
                        <svg
                            className="w-3 h-3 text-thPrimary"
                            aria-hidden="true"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 14 14"
                        >
                            <path
                                stroke="currentColor"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth={2}
                                d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"
                            />
                        </svg>
                        <span className="sr-only">Close modal</span>
                    </button>
                </div>
                <div className="p-4 ">
                    <h2 className="text-lg font-bold py-2 break-words">{voteItem?.question}</h2>
                    <p className="text-thNewtral2 text-sm">Tạo bởi {voteItem?.createdBy?.username} - {moment(voteItem?.createdAt).fromNow()}</p>
                    {
                        voteItem?.allowMultipleAnswers &&
                        <p className="text-sm py-2">Chọn nhiều phương án</p>
                    }
                    {
                        isClosed &&
                        <p className="text-sm pb-2">Kết thúc lúc {moment(voteItem.updatedAt).fromNow()}</p>
                    }
                    <div className="flex gap-2 flex-col max-h-[50vh] overflow-y-auto">
                        {
                            voteItem?.conversationVoteOptions?.map((option) => {
                                const isSelected = selectedAnswers.includes(option?.id);
                                // Tính toán số lượng phiếu bình chọn cho lựa chọn
                                const totalVotesForOption = (voteItem.hideResultBeforeAnswers && !isVoted) 
                                    ? option.conversationVoteAnswers.some((answer) => answer?.user.id === user.id) ? 1 : 0 : option?.conversationVoteAnswers?.length;

                                const optionPercentage = totalMembersVoted !== 0
                                    ? (totalVotesForOption / totalMembersVoted) * 100
                                    : 0;

                                return (
                                    <div key={option?.id} className={`flex gap-2 items-center `} onClick={() => handleAnswerSelection(option.id)}>
                                        {isSelected ? (
                                            <AiFillCheckCircle size={16} className="cursor-pointer" />
                                        ) : (
                                            <div className={`h-3 w-3 border-[1.5px] border-thNewtral2 bg-transparent rounded-full cursor-pointer`} />
                                        )}
                                        <div className={`flex-1 cursor-pointer flex justify-between items-center p-2 border border-thPrimary rounded-md relative`}>
                                            <div className="absolute inset-0 -z-10 bg-thPrimary bg-opacity-60 transition-all rounded-md" style={{ width: `${optionPercentage}%` }}></div>
                                            <span>{option?.option}</span>
                                            {
                                                (voteItem.hideResultBeforeAnswers && !isVoted && !voteItem.hideMemberAnswers) || (!voteItem.hideMemberAnswers && !voteItem.hideResultBeforeAnswers) && 
                                                <div className="flex -space-x-4 rtl:space-x-reverse h-full items-center">
                                                    {
                                                        option.conversationVoteAnswers?.map((item: any, index: number) => (
                                                            index <= 4 &&
                                                            <Image width={32} height={32} key={item?.user?.id} className="w-8 h-8 border-2 border-white rounded-full dark:border-gray-800" src={item?.user?.avatar} alt="" />
                                                        ))
                                                    }
                                                    {option.conversationVoteAnswers?.length - 5 > 0 && <a className="flex items-center justify-center w-12 h-12 text-xs font-medium text-white bg-gray-700 border-2 border-white rounded-full hover:bg-gray-600 dark:border-gray-800" href="#">+{option.conversationVoteAnswers?.length - 5}</a>}
                                                </div> 
                                            }
                                        </div>
                                        <span>{totalVotesForOption}</span>
                                    </div>
                                );
                            })
                        }

                        {choices.map((choice, index) => (
                            <div key={index} className="flex gap-2 items-center">
                                {
                                    selectedAnswers.includes(index) ?
                                        <AiFillCheckCircle
                                            size={16}
                                            className="cursor-pointer"
                                            onClick={() => handleAnswerSelection(index)}
                                        /> :
                                        <div className={` h-3 w-3 border-[1.5px] border-thNewtral2 bg-transparent rounded-full cursor-pointer`}
                                            onClick={() => handleAnswerSelection(index)}
                                        ></div>
                                }
                                <div className="flex-1 cursor-pointer flex justify-between items-center p-2 border border-thPrimary rounded-md relative">
                                    <input
                                        type="text"
                                        value={choice}
                                        onChange={(e) => setChoices([...choices.slice(0, index), e.target.value, ...choices.slice(index + 1)])}
                                        placeholder="Nhập lựa chọn mới"
                                        className="w-full bg-transparent outline-none pr-3"
                                    />
                                    <button
                                        className="text-red-500 absolute right-2"
                                        onClick={() => handleDeleteChoice(index)}
                                    >
                                        X
                                    </button>
                                </div>
                                <span>{0}</span>
                            </div>
                        ))}

                        {
                            voteItem?.allowAddOption === true &&
                            <button
                                className="flex items-center gap-2"
                                onClick={handleAddChoice}
                            >
                                <FaPlus /> Thêm lựa chọn
                            </button>
                        }
                    </div>
                </div>
                <div className="mt-2 border-t-2 border-gray-500 flex justify-between items-center gap-3 py-2">
                    <Dropdown
                        menu={{
                            items: [
                                {
                                    key: "3",
                                    label: (
                                        <span className="flex gap-2 justify-start items-center border-b-2 border-black pb-2  "
                                        >
                                            {voteItem.isPinned ? "Bỏ Ghim " : "Ghim lên đầu trò chuyện"}
                                        </span>
                                    ),
                                    onClick : ()=>{
                                        if(voteItem.isPinned){
                                            handlePinVote(2);
                                        }else{
                                            handlePinVote(1);
                                        }
                                    }
                                },
                                {
                                    key: "4",
                                    label: (
                                        <span className="flex gap-2 justify-start items-center">
                                            Khóa bình chọn
                                        </span>
                                    ),
                                    onClick: () => {
                                        handleCloseVote();
                                    }
                                }
                            ]
                        }}
                        placement="bottomLeft"
                        arrow
                        trigger={["click"]}
                    >
                        <button className="w-fit rounded-md py-2 bg-gray-500 text-black font-bold px-4">
                            <IoSettingsOutline size={24}/>
                        </button>
                    </Dropdown>
                    <div>
                        <button
                            className="w-fit rounded-md py-2 bg-gray-500 text-black font-bold px-4"
                            onClick={closeModal}
                        >
                            Hủy
                        </button>
                        <button
                            className="w-fit rounded-md py-2 bg-blue-700 text-white font-bold px-4 ml-2"
                            onClick={handleVoteConfirmation}
                            disabled={detailChat?.members?.find(member => member.id === user?.id)?.type === CONVERSATION.ROLE_TYPE.MEMBER_WAITING_ACCEPT || isClosed}
                        >
                            Xác nhận
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
}
