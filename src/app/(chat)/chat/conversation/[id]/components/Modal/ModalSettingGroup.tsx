import { ReactElement, useState } from "react";
import ManagerGroup from "../ManagerGroup";
import ModalManagerGroup from "./ModalManagerGroup";
import { useAppSelector } from "@/src/redux/hook";

export default function ModalSettingGroup({
    close
}: {
    close: () => void
}): ReactElement {
    const [isShowModalBlockUser, setIsShowModalBlockUser] = useState<string>("");
    const { detailChat } = useAppSelector((state) => state.user);
    return <div
        id="default-modal"
        tabIndex={-1}
        className=" overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0  justify-center items-center w-full md:inset-0 h-[calc(100%-1rem)] max-h-full z-40"
    >
        <div className="fixed inset-0 w-full h-screen opacity-40" onClick={() => close()}>
        </div>
        <div className="relative z-50 w-full max-w-md max-h-full  max-[480px]:w-[90%] p-4 m-auto dark:text-thWhite bg-light-thNewtral2 dark:bg-thDark-background border border-thPrimary rounded-md mt-5">
            <ManagerGroup close={close} setIsShowModalBlockUser={setIsShowModalBlockUser} />
        </div>
        {
            isShowModalBlockUser !== "" && <ModalManagerGroup action={isShowModalBlockUser} close={() => setIsShowModalBlockUser("")} members={detailChat?.members} />
        }
    </div>
    ;
}