import { ReactElement, cache, use, useEffect, useRef, useState } from "react";
import { AiFillCheckCircle } from "react-icons/ai";
import { Avatar } from "../../../../components/Avatar";
import getAxiosClient from "@/src/utils/axios/axiosClient";
import { toast } from "react-toastify";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { IConversation } from "@/src/types/Conversation";
import { filterConversation, sortConversationByNewest } from "@/src/utils/conversations/filterConversation";
import { setDetailChat } from "@/src/redux/slices/detailChatSlice";
import { CONVERSATION } from "@/src/constants/chat";
import useWindowDimensions from "@/src/hook/useWindowDimension";
interface IModalAddMembersProps {
    close: () => void;
    members: any;
    idConversation: string,
    fetchDetailChat: (id: string) => any
}
const fetchListUser = cache((inputValue: string) =>
    getAxiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/users?page=1&limit=20&keyword=${inputValue}`
    ));
const fetchConversations = cache((offset: number) =>
    getAxiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations?offset=${offset}&status=[1,2]&limit=10&type=1`
    )
);
const ModalAddMembers = ({ close, members, idConversation, fetchDetailChat }: IModalAddMembersProps): ReactElement => {
    const { user: currentUser } = useAppSelector((state) => state.user);
    const dispatch = useAppDispatch();
    const [users, setUsers] = useState<any[]>([]);
    const [listUser, setListUser] = useState([]);
    const inputRef = useRef<HTMLInputElement>(null);
    const { data: conversationsData }: any = use(fetchConversations(0));
    const [listConversation, setListConversation] = useState<IConversation[] | any>(() => {
        return sortConversationByNewest(filterConversation(conversationsData, currentUser.id));
    });
    const handleChooseUser = (user: any) => {
        if (user?.idOther) {
            if (users.find((u: any) => u.id === user.idOther)) {
                setUsers(users.filter((item) => item.id !== user.idOther));
            } else {
                setUsers([...users, {
                    id: user.idOther,
                    username: user.name,
                    avatar: user.avatar,
                    _id: user.idOther
                }]);
            }
        } else
            if (users.find((u: any) => u.id === user.id)) {
                setUsers(users.filter((item) => item.id !== user.id));
            } else {
                setUsers([...users, user]);
            }
    };
    const handleSearch = async () => {
        if (!inputRef.current) return;
        // if(inputRef.current.value) {
        //     setListConversation(sortConversationByNewest(filterConversation(conversationsData, currentUser.id)));
        //     setListUser([]);
        //     return;
        // }
        const res = await fetchListUser(inputRef.current.value);
        const newListConversations = [...listConversation].filter((c: any) => c.name.toLowerCase().includes(inputRef.current?.value.toLowerCase()));
        setListUser(res.data);
        setListConversation(newListConversations);
    };
    const handleKeyDown = (event: any) => {
        if (event.key === "Enter") {
            handleSearch();
        }
    };
    const handleSubmit = async () => {
        if (users.length === 0) {
            return toast.warning("Vui lòng chọn thành viên");
        }
        if (members.find((item: any) => item.id === currentUser.id)?.type === CONVERSATION.ROLE_TYPE.MEMBER_WAITING_ACCEPT) {
            return toast.error("Bạn không thể mời thành viên mới khi chưa là thành viên trong nhóm");
        }
        try {
            const usersId = users.map((item: any) => item.id);
            const data = members.find((item: any) => item.id === currentUser.id)?.type === CONVERSATION.ROLE_TYPE.OWNER ? {
                ids: [...usersId],
                ownerAccepted: true
            } : {
                ids: [...usersId]
            };
            const response = await getAxiosClient().post(
                `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${idConversation}/add`,
                data
            );
            if (response.status === 200) {
                toast.success("Thêm thành công");
                const { data: newDetailChat } = await fetchDetailChat(idConversation);
                dispatch(setDetailChat(newDetailChat));
                close();
            } else {
                toast.error(response.message);
            }
        } catch (err: any) {
            toast.error("Không thể thêm vào nhóm do người này đang nằm trong danh sách chặn");
        }
    };
    useEffect(() => {
        if (listConversation.length === 0)
            handleSearch();

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    const windowDimensions = useWindowDimensions();
    return (
        <div className="fixed top-0 left-0 right-0 z-50 w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] max-h-full flex items-center justify-center">
            <div className="fixed inset-0 w-full h-screen bg-transparent overlay" onClick={() => close()}></div>
            <div className="relative w-full max-w-2xl max-h-full z-50">
                <div className="relative  rounded-lg shadow dark:bg-thDark-background bg-light-thNewtral2 border border-thPrimary">
                    <div className="flex items-start justify-between p-3 border-b rounded-t border-gray-600">
                        <h3 className="text-xl font-semibold text-white">
                            Thêm thành viên
                        </h3>
                        <button
                            type="button"
                            className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ml-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white"
                            data-modal-hide="defaultModal"
                            onClick={() => close()}
                        >
                            <svg
                                className="w-3 h-3 text-thPrimary"
                                aria-hidden="true"
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 14 14"
                            >
                                <path
                                    stroke="currentColor"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    strokeWidth={2}
                                    d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"
                                />
                            </svg>
                            <span className="sr-only">Close modal</span>
                        </button>
                    </div>
                    {/* Modal body */}
                    <div className="px-6 py-2  flex flex-col">
                        <div className="flex items-center justify-center">
                            <label
                                htmlFor="default-search"
                                className="mb-2 text-sm font-medium sr-only text-white"
                            >
                                Search
                            </label>
                            <div className="relative w-full">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <svg
                                        className="w-4 h-4 text-gray-400"
                                        aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="none"
                                        viewBox="0 0 20 20"
                                    >
                                        <path
                                            stroke="currentColor"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            strokeWidth={2}
                                            d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
                                        />
                                    </svg>
                                </div>
                                <input
                                    ref={inputRef}
                                    type="search"
                                    id="default-search"
                                    className="block w-full p-2 pl-10 text-sm border border-gray-300 rounded-full bg-transparent text-white focus:ring-blue-500 focus:border-blue-500 "
                                    placeholder="Nhập tên..."
                                    onKeyDown={handleKeyDown}
                                />
                            </div>
                        </div>
                        <div className="flex w-full border-t border-gray-200 pt-2 mt-2  gap-x-3">
                            <div className={`w-full  ${users.length === 0 ? "w-full" : "md:w-2/3"} min-h-[65vh] max-h-[65vh] overflow-y-auto`}>
                                {
                                    listConversation?.length > 0
                                    && listConversation.map((conversation: any) => {
                                        const checkInGroup = members?.find((member: any) => member.id === conversation.idOther);
                                        return (
                                            <div
                                                key={conversation._id}
                                                className={`flex items-center justify-start gap-2 item  mt-2 hover:bg-gray-500 ${checkInGroup && "bg-gray-500 cursor-not-allowed"}}`}
                                                onClick={() => {
                                                    if (checkInGroup) return;
                                                    handleChooseUser(conversation);
                                                }}
                                            >
                                                {
                                                    <>
                                                        {
                                                            (users.find((u: any) => u?.id === conversation?.idOther) || checkInGroup)
                                                                ? (
                                                                    <AiFillCheckCircle
                                                                        size={16}
                                                                        className="cursor-pointer"
                                                                    />
                                                                ) :
                                                                <div className="h-3 w-3 border-[1.5px] border-thNewtral2 bg-transparent rounded-full cursor-pointer"></div>
                                                        }
                                                        <div className="relative rounded-full w-10 h-10 object-cover border-2 border-thPrimary cursor-pointer overflow-hidden">
                                                            <Avatar
                                                                url={
                                                                    conversation?.avatar === "http://avatar.com"
                                                                        ? "/default_avatar.jpg"
                                                                        : conversation?.avatar
                                                                }
                                                            />
                                                        </div>
                                                        <span>{conversation?.name}</span>
                                                        <span className="text-sm text-white">
                                                            {checkInGroup && "(Đã tham gia)"}
                                                        </span>
                                                    </>}
                                            </div>
                                        );
                                    })
                                }
                                {listUser.length > 0 &&
                                    listUser
                                        ?.map((user: any) => {
                                            const checkInGroup = members?.find((member: any) => member.id === user._id);
                                            return (
                                                <div
                                                    key={user._id}
                                                    className={`flex items-center justify-start gap-2 item  mt-2 hover:bg-gray-500 ${checkInGroup && "bg-gray-500 cursor-not-allowed"}
                                                }`}
                                                    onClick={() => {
                                                        if (checkInGroup) return;
                                                        handleChooseUser(user);
                                                    }}
                                                >
                                                    {
                                                        <>
                                                            {
                                                                (users.find((u: any) => u?.id === user?.id) || checkInGroup)
                                                                    ? (
                                                                        <AiFillCheckCircle
                                                                            size={16}
                                                                            className="cursor-pointer"
                                                                        />
                                                                    ) :
                                                                    <div className="h-3 w-3 border-[1.5px] border-thNewtral2 bg-transparent rounded-full cursor-pointer"></div>
                                                            }
                                                            <div className="relative rounded-full w-10 h-10 object-cover border-2 border-thPrimary cursor-pointer overflow-hidden">
                                                                <Avatar
                                                                    url={
                                                                        user?.avatar === "http://avatar.com"
                                                                            ? "/default_avatar.jpg"
                                                                            : user?.avatar
                                                                    }
                                                                />
                                                            </div>
                                                            <span>{user?.username}</span>
                                                            <span className="text-sm text-white">
                                                                {checkInGroup && "(Đã tham gia)"}
                                                            </span>
                                                        </>}
                                                </div>
                                            );
                                        })}
                            </div>
                            {
                                windowDimensions.width > 480 &&
                                <div className={`w-full  ${users.length === 0 ? "hidden" : "md:w-1/3"} border divide-orange-50 p-3 rounded-md min-h-[65vh] max-h-[65vh] overflow-y-auto`}>
                                    <h3>Đã chọn {users.length}</h3>
                                    {
                                        users
                                            ?.map((member: any) => {
                                                return (
                                                    <div
                                                        key={member._id}
                                                        className={`flex items-center justify-start gap-2 item  mt-2 hover:bg-gray-500 
                                            }`}
                                                        onClick={() => handleChooseUser(member)}
                                                    >
                                                        {
                                                            <>
                                                                <div className="relative rounded-full w-10 h-10 object-cover border-2 border-thPrimary cursor-pointer overflow-hidden">
                                                                    <Avatar
                                                                        url={
                                                                            member?.avatar === "http://avatar.com"
                                                                                ? "/default_avatar.jpg"
                                                                                : member?.avatar
                                                                        }
                                                                    />
                                                                </div>
                                                                <span>{member?.username}</span>
                                                            </>}
                                                    </div>
                                                );
                                            })}
                                </div>
                            }
                        </div>
                    </div>
                    {/* Modal footer */}
                    <div className="flex items-center justify-end p-3 space-x-2 border-t border-gray-200 rounded-b dark:border-gray-600">
                        <button
                            className="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600"
                            onClick={() => {
                                close();
                            }}
                        >
                            Hủy
                        </button>
                        <button
                            className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                            onClick={handleSubmit}
                            disabled={users.length === 0}
                        >
                            Mời
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default ModalAddMembers;