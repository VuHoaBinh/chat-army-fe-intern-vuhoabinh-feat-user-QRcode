import { CONVERSATION } from "@/src/constants/chat";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { setListConversations, setOtherUser } from "@/src/redux/slices/conversationSlice";
import { setDetailChat } from "@/src/redux/slices/detailChatSlice";
import getAxiosClient from "@/src/utils/axios/axiosClient";
import { filterConversation, sortConversationByNewest } from "@/src/utils/conversations/filterConversation";
import { ReactElement, useState } from "react";
import { FiChevronLeft } from "react-icons/fi";
import { toast } from "react-toastify";
interface IModalChangeNameProps {
    setIsShowModal: React.Dispatch<React.SetStateAction<boolean>>,
    name: string
    type: any
    idConversation: string
}
export default function ModalChangeName({ setIsShowModal, name, type, idConversation }: IModalChangeNameProps): ReactElement {
    const [value, setValue] = useState(name);
    const dispatch = useAppDispatch();
    const { user, conversation } = useAppSelector(state => state.user);
    const { detailChat } = useAppSelector(state => state.user);
    const handleSubmit = async () => {
        try {
            const response = await getAxiosClient().patch(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${idConversation}`,
                {
                    name: value
                }
            );
            if (response.status === 200) {
                toast.success("Đổi tên thành công");
                setIsShowModal(false);
                dispatch(setOtherUser({
                    otherUsername: value,
                    otherAvatar: response.data.avatar
                }));
                const newListConversations = conversation?.listConversation?.map((item: any) => {
                    if (item?.id === idConversation) {
                        return {
                            ...item,
                            name: value
                        };
                    }
                    return item;
                });
                dispatch(
                    setListConversations(
                        sortConversationByNewest(filterConversation(newListConversations, user.id))
                    )
                );
                dispatch(setDetailChat({
                    ...detailChat,
                    name: value
                }));
            }
        } catch (err: any) {
            toast.error(err.response.data.message);
        }
    };
    return (
        <div
            id="default-modal"
            tabIndex={-1}
            className=" overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0  justify-center items-center w-full md:inset-0 h-[calc(100%-1rem)] max-h-full z-40"
        >
            <div className="fixed inset-0 w-full h-screen opacity-40" onClick={() => setIsShowModal(false)}></div>
            <div className="relative z-50 w-full max-w-md max-h-full  max-[480px]:w-[90%] p-4 m-auto dark:text-thWhite bg-light-thNewtral2 dark:bg-thDark-background border border-thPrimary rounded-md mt-5">
                <div className="flex gap-x-4 items-center">
                    <div className="cursor-pointer" onClick={() => setIsShowModal(false)}>
                        <FiChevronLeft size={40} />
                    </div>
                    <p className="text-lg font-semibold">{type === CONVERSATION.TYPE.GROUP ? "Thông tin nhóm" : "Đổi tên gợi nhớ"}</p>
                </div>
                <div className="mt-2">
                    {
                        type === CONVERSATION.TYPE.GROUP &&
                        <p>Bạn có chắc muốn đổi tên nhóm, khi xác nhận tin nhắn mới sẽ hiển thị với tất cả thành viên</p>
                    }
                    {
                        type === CONVERSATION.TYPE.INDIVIDUAL &&
                        <>
                            <p className="text-center">Hãy đặt tên cho <span className="font-bold">{name}</span> một cái tên dễ nhớ.</p>
                            <p className="text-center">Lưu ý: Tên gợi nhớ sẻ chỉ hiển thị riêng với bạn</p>
                        </>
                    }
                    <input type="text" className="w-full px-2 py-1 rounded-sm bg-thNewtral2 text-thWhite mt-2" value={value} onChange={(e) => setValue(e.target.value)} />
                    <div className="mt-2 border-t-2 border-gray-500 flex justify-end items-center gap-3 py-2">
                        <button className="w-fit rounded-md py-2 bg-gray-500 text-black font-bold px-4"
                            onClick={() => setIsShowModal(false)}
                        >Hủy</button>
                        <button className="w-fit rounded-md py-2 bg-blue-700 text-white font-bold px-4"
                            onClick={handleSubmit}
                        >Xác nhận</button>
                    </div>
                </div>
            </div>
        </div>
    );
}