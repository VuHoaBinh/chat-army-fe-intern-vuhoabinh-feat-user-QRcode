import { CONVERSATION, EMOJI, MESSAGE_TYPE, SOCIAL_REGEX_LIST } from "@/src/constants/chat";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { IMessage } from "@/src/types/Message";
import { getFileIcon, getFileName } from "@/src/utils/messages/handleUrl";
import moment from "moment";
import Image from "next/image";
import React, { ReactElement, useEffect, useRef, useState } from "react";
import {
    AiFillEyeInvisible,
    AiFillMessage,
    AiFillPlayCircle,
    AiFillSmile
} from "react-icons/ai";
import { BsLink45Deg, BsPinAngleFill } from "react-icons/bs";
import { RiShareForwardFill } from "react-icons/ri";
import { Socket } from "socket.io-client";
import styles from "../conversation.module.css";
import LinkPreview from "./LinkPrevew";
import MessageLink from "./MessageLink";
import ReactionBox from "./ReactionBox";
import { setPinMessages } from "@/src/redux/slices/newsletterSlice";
import getAxiosClient from "@/src/utils/axios/axiosClient";
import { Tooltip, message as notify } from "antd";

interface Text {
    message: IMessage;
    isLastMsg: boolean;
    setForwardContent: (forwardContent: IMessage) => void;
    reactionID: string;
    setReactionID: (reactionID: string) => void;
    socket?: Socket<any, any>;
    setMessageReply: React.Dispatch<any>;
    setMessageScroll: React.Dispatch<any>;
    isGroup?: boolean;
    setIdMediaModal: (idMediaModal: string) => void;
    setUrlMediaModal: (urlMediaModal: string) => void;
    setIsOpenMediaLibrary: (isOpenMediaLibrary: boolean) => void;
}
export default function OwnerText({
    isLastMsg,
    message,
    setForwardContent,
    reactionID,
    setReactionID,
    socket,
    setMessageReply,
    setMessageScroll,
    isGroup,
    setIdMediaModal,
    setUrlMediaModal,
    setIsOpenMediaLibrary
}: Text): ReactElement {
    const [isReact, setIsReact] = useState<boolean>(false);
    const { user } = useAppSelector((state) => state.user);
    const { newsletter } = useAppSelector((state) => state.user);

    const handleReaction = () => {
        if (isReact === true && reactionID === message?._id) {
            setIsReact(false);
            setReactionID("");
            return;
        }
        if (isReact === true && reactionID !== message?._id) {
            setReactionID(message._id);
            return;
        }
        setIsReact(true);
        setReactionID(message?._id);
    };
    // Mã hóa đầu cuối => logic
    let dataImages: string[] = [];
    if (message.type === MESSAGE_TYPE.IMAGE) {
        try {
            dataImages = JSON.parse(message.content);
        } catch (error) {
            console.error("Error parsing JSON:", error);
        }
    }
    const messageReply =
        message.replyTo &&
        Object.keys(message.replyTo).length > 0 &&
        message.replyTo;
    const [isShowLink, setIsShowLink] = useState<boolean>(false);
    const reactionBoxRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
        const handleClickOutside = (event: MouseEvent) => {
            if (
                reactionBoxRef.current &&
                !reactionBoxRef.current.contains(event.target as Node)
            ) {
                setIsReact(false);
            }
        };

        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, []);
    const dispatch = useAppDispatch();
    const handlePinMessage = async (message: IMessage)=>{
        try{
            const res = await getAxiosClient().post(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${message.conversationId}/message-pin/${message._id}`);
            if(!newsletter.messagePinned?.find((item) => item.message?.id === res.data.messageId)){
                dispatch(setPinMessages([...newsletter.messagePinned || [], {
                    _id: res.data._id,
                    conversationId: res.data.conversationId,
                    createdAt: res.data.createdAt,
                    updatedAt: res.data.updatedAt,
                    id: res.data.id,
                    message
                }]));
                notify.success("Đã ghim tin nhắn");
            }else{
                notify.warning("Tin nhắn đã được ghim trước đó");
            }
        }catch(e){
            notify.error("Ghim tin nhắn thất bại");
        }
    };
    const { detailChat } = useAppSelector((state) => state.user);
    const isOwner  = detailChat?.members.find((item: any) => item.id === user?.id)?.type === CONVERSATION.ROLE_TYPE.OWNER;
    const isAdmin = detailChat?.members.find((item: any) => item.id === user?.id)?.type === CONVERSATION.ROLE_TYPE.ADMIN;
    const isTypeWaitting = detailChat?.members.find((item: any) => item.id === user?.id)?.type === CONVERSATION.ROLE_TYPE.MEMBER_WAITING_ACCEPT;
    return (
        <>
            <div
                className={`flex w-full gap-6 justify-end items-center ${styles.text_box} max-[480px]:flex max-[480px]:flex-col max-[480px]:!items-end max-[480px]:gap-1`}
                onMouseLeave={() => setIsReact(false)}
            >
                <div className="relative max-[480px]:order-2" ref={reactionBoxRef}>
                    {isReact && reactionID === message?._id && (
                        <ReactionBox
                            messageId={message._id}
                            conversationSocket={socket}
                            position="right-1 translate-x-[0.88rem]"
                        />
                    )}
                    <div
                        className={`${styles.icon_area} flex items-center gap-x-2 opacity-25 `}
                    >
                        {
                            (detailChat?.conversationSetting?.find((item) => item.type === 3)?.value === true && !isTypeWaitting) ||
                                ((isAdmin || isOwner) || !isGroup) ? (
                                    <Tooltip placement="bottomRight" title={"Ghim tin nhắn"}>
                                        <BsPinAngleFill size={20} onClick={() => handlePinMessage(message)} />
                                    </Tooltip>
                                ) : null
                        }
                        <RiShareForwardFill
                            size={20}
                            onClick={() => setForwardContent(message)}
                        />
                        <AiFillMessage size={20} onClick={() => setMessageReply(message)} />
                        <AiFillSmile size={20} onClick={handleReaction} />
                    </div>
                </div>
                <div
                    className={` dark:bg-thPrimary bg-light-thPrimary text-thDark-background w-fit p-3 ${isShowLink && "max-[480px]:px-1"
                    } ${styles.owner_box
                    } max-w-[75%] max-[480px]:max-w-full  transition-all max-[480px]:order-1`}
                >
                    {messageReply && (
                        <div
                            className="py-2 bg-gray-600 text-white rounded-md mb-2 flex justify-between cursor-pointer"
                            onClick={() => setMessageScroll(messageReply)}
                        >
                            <div
                                className={`flex-col gap-2 overflow-x-hidden  ${messageReply.type !== MESSAGE_TYPE.IMAGE &&
                                    "ml-2 border-l-blue-200 border-l-4"
                                } px-2`}
                            >
                                {messageReply.type === MESSAGE_TYPE.STICKER ? (
                                    <Image
                                        alt={messageReply.content}
                                        src={messageReply.content}
                                        className="w-[40px] h-[40px]"
                                        width={40}
                                        height={40}
                                    />
                                ) : messageReply.type === MESSAGE_TYPE.IMAGE ? (
                                    <div
                                        className={`grid gap-1 ${JSON.parse(messageReply.content).length % 2 === 0
                                            ? "grid-cols-2"
                                            : JSON.parse(messageReply.content).length % 3 === 0 ||
                                                JSON.parse(messageReply.content).length >= 3
                                                ? "grid-cols-3"
                                                : "grid-cols-1"
                                        }`}
                                    >
                                        {JSON.parse(messageReply.content).map((item: string) => {
                                            return (
                                                <Image
                                                    key={item}
                                                    alt={item}
                                                    src={item}
                                                    className={`object-fill ${JSON.parse(messageReply.content).length >= 2
                                                        ? "w-[100px] h-[100px]"
                                                        : "w-[50px] h-[50px]"
                                                    }`}
                                                    width={
                                                        JSON.parse(messageReply.content).length >= 2
                                                            ? 70
                                                            : 200
                                                    }
                                                    height={
                                                        JSON.parse(messageReply.content).length >= 2
                                                            ? 70
                                                            : 200
                                                    }
                                                />
                                            );
                                        })}
                                    </div>
                                ) : messageReply.type === MESSAGE_TYPE.TEXT ? (
                                    <span className="text-[.9375rem] truncate">
                                        {messageReply.content}
                                    </span>
                                ) : messageReply.type === MESSAGE_TYPE.MEDIA ? (
                                    <video
                                        src={messageReply.content}
                                        width={100}
                                        height={100}
                                        className="w-[100px] h-auto"
                                    ></video>
                                ) : messageReply.type === MESSAGE_TYPE.LINK ? (
                                    <span className="flex items-center text-blue-600 underline truncate">
                                        <BsLink45Deg size={30} /> {messageReply.content}
                                    </span>
                                ) : (
                                    <span className="flex items-center truncate">
                                        {/* eslint-disable-next-line @next/next/no-img-element */}
                                        <img
                                            src={getFileIcon(
                                                messageReply.content.split(".").pop() as string
                                            )}
                                            alt="icon"
                                            width={50}
                                            height={50}
                                        />
                                        <p className="ml-2">{getFileName(messageReply.content)}</p>
                                    </span>
                                )}
                            </div>
                        </div>
                    )}
                    {message.type === MESSAGE_TYPE.STICKER ? (
                        <Image
                            alt={message.content}
                            src={message.content}
                            className="w-full h-auto"
                            width={40}
                            height={40}
                        />
                    ) : message.type === MESSAGE_TYPE.IMAGE ? (
                        <div
                            className={`grid gap-1 ${dataImages.length % 2 === 0
                                ? "grid-cols-2"
                                : dataImages.length % 3 === 0 || dataImages.length >= 3
                                    ? "grid-cols-3"
                                    : "grid-cols-1"
                            }`}
                        >
                            {dataImages.map((item: string) => {
                                return (
                                    <Image
                                        key={item}
                                        alt={item}
                                        src={item}
                                        className={`object-fill ${dataImages.length >= 2
                                            ? "w-[100px] h-[100px]"
                                            : "w-full h-full"
                                        }`}
                                        width={dataImages.length >= 2 ? 300 : 200}
                                        height={dataImages.length >= 2 ? 300 : 200}
                                        onClick={() => {
                                            setIdMediaModal(message?._id);
                                            setUrlMediaModal(item);
                                            setIsOpenMediaLibrary(true);
                                        }}
                                    />
                                );
                            })}
                        </div>
                    ) : message.type === MESSAGE_TYPE.TEXT ? (
                        <span className="text-[.9375rem] break-words">

                            {message.content}

                        </span>
                    ) : message.type === MESSAGE_TYPE.MEDIA ? (
                        <div
                            onClick={() => {
                                setIdMediaModal(message?._id);
                                setUrlMediaModal(message.content);
                                setIsOpenMediaLibrary(true);
                            }}
                            className="relative"
                        >
                            <AiFillPlayCircle
                                size={40}
                                className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 text-thWhite"
                            />

                            <video
                                src={message.content}
                                width={300}
                                height={300}
                                className="w-[300px] h-auto"
                                style={{ pointerEvents: "none" }}
                            />
                        </div>
                    ) : message.type === MESSAGE_TYPE.LINK ? (
                        <div className=" flex flex-col">
                            <a
                                target="_blank"
                                rel="noopener noreferrer"
                                href={message.content}
                                className="  break-words max-[480px]:truncate"
                            >
                                {<MessageLink url={message.content} />}
                            </a>
                            {SOCIAL_REGEX_LIST.some((regex) =>
                                regex.test(message.content)
                            ) && (
                                <>
                                    {!isShowLink && (
                                        <div className="flex justify-end">
                                            <button
                                                onClick={() => {
                                                    setIsShowLink(true);
                                                }}
                                                className="w-fit rounded-md dark: bg-thNewtral2 bg-light-thNewtral2 text-white px-2 py-1"
                                            >
                                                    Xem trước
                                            </button>
                                        </div>
                                    )}
                                    {isShowLink && <LinkPreview url={message.content} />}
                                    {isShowLink && (
                                        <div className="flex justify-end mt-2">
                                            <button
                                                onClick={() => setIsShowLink(false)}
                                                className="w-fit rounded-md dark: bg-thNewtral2 bg-light-thNewtral2 text-white px-2 py-1"
                                            >
                                                <AiFillEyeInvisible size={24} />
                                            </button>
                                        </div>
                                    )}
                                </>
                            )}
                        </div>
                    ) : (
                        <a
                            target="_blank"
                            rel="noopener noreferrer"
                            href={message.content}
                            className="flex items-center"
                        >
                            {/* eslint-disable-next-line @next/next/no-img-element */}
                            <img
                                src={getFileIcon(message.content.split(".").pop() as string)}
                                alt="icon"
                                width={50}
                                height={50}
                            />
                            <p className="ml-2 truncate">{getFileName(message.content)}</p>
                        </a>
                    )}
                    <div className="flex justify-between items-center gap-x-2 mt-2">
                        {message?.reactions?.length > 0 && (
                            <div className="flex justify-center items-center gap-2 bg-green-600 rounded-lg px-1.5 py-1">
                                {message?.reactions?.map((react: any, index: number) => {
                                    if (index <= 1) {
                                        return (
                                            <span key={react.icon}>
                                                {EMOJI.find((e) => e.id === react.icon)?.src || ""}
                                            </span>
                                        );
                                    }
                                })}
                                {message.reactions.length > 1 && (
                                    <span className="text-[12px] opacity-60">
                                        {message.reactions.length}
                                    </span>
                                )}
                            </div>
                        )}
                        <span className="text-[.775rem] opacity-80">
                            {moment(message?.createdAt).format("HH:mm")}
                        </span>
                    </div>
                </div>
            </div>
            {isLastMsg && isGroup ? (
                <div className="flex justify-end w-full py-1">
                    {message?.seen.find((u: any) => u._id !== user.id) ? (
                        <>
                            {message?.seen
                                ?.filter((u: any) => u._id !== user.id)
                                .map((u: any) => {
                                    return (
                                        <div key={u._id} className="group relative w-fit h-[16px]">
                                            <Image
                                                key={u._id}
                                                alt={u._id}
                                                src={u?.avatar}
                                                className="w-[15px] h-[15px] rounded-full"
                                                width={15}
                                                height={15}
                                            />
                                            <span className="pointer-events-none absolute -top-10 right-0 w-max opacity-0 transition-opacity group-hover:opacity-100 dark: bg-thNewtral2 bg-light-thNewtral2 p-2 rounded-sm">
                                                {u?.username}
                                            </span>
                                        </div>
                                    );
                                })}
                        </>
                    ) : (
                        <div className="text-right w-full opacity-40 text-xs py-1">
                            Đã gửi
                        </div>
                    )}
                </div>
            ) : (
                <div className="text-right w-full opacity-40 text-xs py-1">
                    {isLastMsg &&
                        (message?.seen.find((u: any) => u._id !== user.id)
                            ? "Đã xem"
                            : "Đã gửi")}
                </div>
            )}
        </>
    );
}
