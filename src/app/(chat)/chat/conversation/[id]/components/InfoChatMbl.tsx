"use client";
import Circle from "@/public//Ellipse 12.svg";
import AddIcon from "@/public/add-square.svg";
import ExportIcon from "@/public/export.svg";
import LockIcon from "@/public/lock.svg";
import GroupIcon from "@/public/people.svg";
import SecurityIcon from "@/public/security.svg";
import BlockIcon from "@/public/slash.svg";
import { CONVERSATION, MESSAGE_TYPE } from "@/src/constants/chat";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";

import { setDetailChat } from "@/src/redux/slices/detailChatSlice";
import { updateUserCreateGroup } from "@/src/redux/slices/listUserCreateGroupSlice";
import { updateStatusSeen } from "@/src/redux/slices/statusSeenedSlice";
import { IMessage } from "@/src/types/Message";
import { fetchTunrOffSeened } from "@/src/utils/api/seenedMessage";
import { default as axiosClient, default as getAxiosClient } from "@/src/utils/axios/axiosClient";

import { removeIdToListIdConversationTurnOffNotify, setOpenModalTurnOffNotifications } from "@/src/redux/slices/NotifyOfConversationSlice";
import { setListConversations, setListIdPinnedConversation } from "@/src/redux/slices/conversationSlice";
import { EncryptionConversationProp, findEncryptionConversationByIdConversation, setOpenModalGeneratesEncryptedPasswords, setTypeOfMofalHandlesPasswordsEncryption } from "@/src/redux/slices/endToEndEncryptionSlice";
import { filterConversation, sortConversationByNewest } from "@/src/utils/conversations/filterConversation";
import { checkFile, getFileName } from "@/src/utils/messages/handleUrl";
import useNotification from "@/src/utils/notify/useNotification";
import styled from "@emotion/styled";
import { Alert, Modal, Snackbar } from "@mui/material";
import Tippy from "@tippyjs/react";
import CryptoJS from "crypto-js";
import Image from "next/image";
import { useRouter } from "next/navigation";
import React, {
    ReactElement,
    SyntheticEvent,
    cache,
    use,
    useEffect,
    useRef,
    useState
} from "react";
import { AiFillEye, AiFillPlayCircle, AiFillPushpin, AiOutlineEdit, AiOutlineLink } from "react-icons/ai";
import { BiEditAlt, BiSolidBell, BiSolidBellOff } from "react-icons/bi";
import { BsFileEarmarkText, BsLink45Deg } from "react-icons/bs";
import { FaUnlockAlt } from "react-icons/fa";
import { FiChevronDown, FiChevronLeft, FiChevronRight, FiSearch } from "react-icons/fi";
import { HiUsers } from "react-icons/hi";
import { HiOutlineUserPlus, HiOutlineUsers, HiUserCircle } from "react-icons/hi2";
import { IoImage, IoSettingsOutline } from "react-icons/io5";
import { PiNoteLight, PiShareFatBold, PiSignOutLight } from "react-icons/pi";
import { RiFileCopyLine, RiUnpinFill } from "react-icons/ri";
import { toast } from "react-toastify";
import { GroupedVirtuoso, VirtuosoGrid } from "react-virtuoso";
import 'tippy.js/dist/tippy.css'; // Import CSS của Tippy.js
import EditUserName from "../../../components/Modal/EditUserName";
import ModalInforConversation from "../../../components/Modal/ModalInforConversation";
import ModalShareGroupChat from "../../../components/Modal/ModalShareGroupChat";
import ModalInforGroupChat from "../../../components/Modal/ModalnforGroupChat";
import InfoSwitch from "./InfoSwitch";
import ManagerGroup from "./ManagerGroup";
import ModalAddMembers from "./Modal/ModalAddMembers";
import ModalChangeName from "./Modal/ModalChangeName";
import ModalConfirm from "./Modal/ModalConfirm";
import ModalManagerGroup from "./Modal/ModalManagerGroup";

import { LuArrowDownSquare } from "react-icons/lu";
import { MdGroups } from "react-icons/md";
import ModalNote from "./Modal/ModalNote";
import ModalSetPinHidden from "./Modal/ModalSetPinHidden";
import ModalUpdatePinHidden from "./Modal/ModalUpdatePinHidden";
import ModalVote from "./Modal/ModalVote";
import NewsletterGroup from "./NewsletterGroup";
import CommonGroup from "./group/CommonGroup";
import FormAddGroup from "./group/FormCreateGroup";
import Members from "./group/Members";
import SearchBoxMbl from "./SearchBoxMbl";
import { setIdConversation, toggleOpenSearch } from "@/src/redux/slices/searchSlice";
import styles from "../conversation.module.css";
import userAvatar from "@/public/user.png";
interface Props {
    isOpenInfo: boolean;
    setIsOpenInfo: (isOpenInfo: boolean) => void;
    idConversation: string;
    setIdMediaModal: (idMediaModal: string) => void;
    setUrlMediaModal: (urlMediaModal: string) => void;
    setIsOpenMediaLibrary: (isOpenMediaLibrary: boolean) => void;
    heightVirtualList: number;
    dataGroup?: any;
    socket?: any;
    isOpenNewsletter?: boolean;
    setMessageScroll?: React.Dispatch<any>;
    setIsOpenModalExplainDeleteConversation?: any;
    openImageUploadModal?: () => void;
}

interface FeatureState {
    file: boolean;
    media: boolean;
    link: boolean;
}

// const fetchMedia = cache((id: string) => axios.get(`/api/conversations/media?id=${id}`));
const fetchMedia = cache((id: string) =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${id}/medias?sort=desc`
    )
);
const fetchDetailChat = cache((id: string) =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${id}`
    )
);
const fetchCommonGroup = cache((userId: string) =>
    getAxiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${userId}/same-group?sort=asc&limit=100`
    )
);

export default function InfoChatMbl({
    isOpenInfo,
    setIsOpenInfo,
    idConversation,
    setIdMediaModal,
    setUrlMediaModal,
    setIsOpenMediaLibrary,
    heightVirtualList,
    dataGroup,
    socket,
    isOpenNewsletter = false,
    setMessageScroll,
    openImageUploadModal
}: Props): ReactElement {
    const [options, setOptions] = useState<number[]>([]);
    const { conversation, notify, statusSeen } = useAppSelector((state) => state.user);
    const { detailChat } = useAppSelector((state) => state.user);
    const { user, endToEndEncryption } = useAppSelector((state) => state.user);
    const { search } = useAppSelector((state) => state.user);
    const [isFile, setIsFile] = useState<boolean>(false);
    const [feature, setFeature] = useState<FeatureState>({
        file: false,
        media: true,
        link: false
    });

    const handleShowMenu = (id: number) => {
        // Check if the clicked menu is already in the open options
        const isOpen = options.includes(id);

        if (isOpen) {
            // If it's already open, close it by removing it from the array
            setOptions(options.filter((optionId) => optionId !== id));
        } else {
            // If it's not open, open it by adding it to the array
            setOptions([...options, id]);
        }
    };
    const spanRef = useRef<HTMLSpanElement>(null);
    const [open, setOpen] = React.useState(false);
    // const [detailChat, setDetailChat] = useState<any>({});
    const [isShowModal, setIsShowModal] = useState<string>("");
    const [isShowCommonGroup, setIsShowCommonGroup] = useState<boolean>(false);
    const [isShowMembers, setIsShowMembers] = useState<boolean>(false);
    const [commonGroup, setCommonGroup] = useState<any>([]);
    const [dataMediaImage, setDataMediaImage] = useState<IMessage[]>([]);
    const [dataFiles, setDataFiles] = useState<IMessage[]>([]);
    const [dataLink, setDataLink] = useState<IMessage[]>([]);
    const [groupFilesCounts, setGroupFilesCounts] = useState<number[]>([]);
    const [groupLinkCounts, setGroupLinkCounts] = useState<number[]>([]);
    const [groupFiles, setGroupFiles] = useState<string[]>([]);
    const [groupLink, setGroupLink] = useState<string[]>([]);
    const { data } = use(fetchMedia(idConversation)) as { data: any };
    const [share, setShare] = React.useState(false);
    const [urlGroup, setUrlGroup] = React.useState("");
    const [isOpenInfoGroupChat, setIsOpenInfoGroupChat] =
        useState<boolean>(false);
    const [isEditUserName, setIsEditUserName] = useState(false);
    const [contact, setContact] = useState<any>([]);
    const [otherUsername, setOtherUsername] = useState<any>("");
    const modalEditUserRef = useRef<HTMLDivElement>(null);
    const [isShowManager, setIsShowManager] = useState<boolean>(false);
    const [isShowModalBlockUser, setIsShowModalBlockUser] = useState<string>("");
    const [isShowModalChangeName, setIsShowModalChangeName] = useState<boolean>(false);
    const [isShowModalAddMembers, setIsShowModalAddMembers] = useState<boolean>(false);
    const [isShowModalConfirm, setIsShowModalConfirm] = useState<boolean>(false);
    const router = useRouter();
    const [isShowNewsletter, setIsShowNewsletter] = useState<boolean>(isOpenNewsletter);
    const [isActiveSeended, setIsActiveSeended] = useState<boolean>(statusSeen?.value);
    const handleShowInfoGroupChat = (conversation: any) => {
        if (conversation.type == 2) {
            setIsOpenInfoGroupChat(true);
        }
        else {
            setIsOpenInforConversation(true);
        }
    };

    const dispatch = useAppDispatch();
    const handleShare = (e: any) => {
        e.stopPropagation();

        setShare(true);
        setUrlGroup(spanRef.current?.textContent || "");
    };
    const handleClose = (
        event?: React.SyntheticEvent | Event,
        reason?: string
    ) => {

        if (reason === "clickaway") {
            return;
        }

        setOpen(false);

    };
    const [dataStorage, setDataStorage] = useState<any>([]);
    const localStorageData = useAppSelector((state: any) => state.user);
    const [isOpenInforConversation, setIsOpenInforConversation] = useState<boolean>(false);
    useEffect(() => {
        const dataString = localStorage.getItem("Othername");
        if (dataString) {
            setDataStorage(JSON.parse(dataString));
        }
    }, [localStorageData]);
    const handleCopyLink = () => {
        if (!spanRef.current) return;
        const data = spanRef.current?.textContent;
        setOpen(true);
        navigator.clipboard.writeText(data || "");
    };
    const [isOpenSetPinHidden, setIsOpenSetPinHidden] = useState<boolean>(false);
    useEffect(() => {



        const elem = { user: dataGroup };



        setContact(elem);

    }, [dataGroup]);
    const toggleStatusSeenMsg = async (isActiveSeended: boolean) => {
        const data = await fetchTunrOffSeened(!isActiveSeended ? 'on' : 'off');
        if (data?.status === 200) {
            dispatch(updateStatusSeen({ value: !isActiveSeended, type: 1 }));
            setIsActiveSeended(!isActiveSeended);
        } else {
            toast.error('Something went wrong');
        }
    };
    const handleEditUsername = (e: any, contact: any) => {
        setOtherUsername(
            dataStorage[contact.idOther]
                ? dataStorage[contact.idOther]
                : contact.name
        );
        setIsEditUserName(!isEditUserName);

    };
    const handleClickOutside = (event: { target: any }) => {
        if (
            modalEditUserRef.current &&
            !modalEditUserRef.current.contains(event.target)
        ) {
            setIsEditUserName(false);
        }
    };
    useEffect(() => {
        document.addEventListener("mousedown", handleClickOutside);

        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, []);
    useEffect(() => {
        (async () => {
            try {
                const { data: detail }: any = await fetchDetailChat(idConversation);
                // setDetailChat(detail);
                dispatch(setDetailChat(detail));
            } catch (err) {
                console.log(err);
            }
        })();
    }, [dispatch, idConversation]);
    useEffect(() => {
        const conversationData: any = conversation.listConversation?.find(
            (item: any) => item._id === idConversation
        );
        if (detailChat?.type === CONVERSATION.TYPE.INDIVIDUAL) {
            (async () => {
                const { data } = await fetchCommonGroup(conversationData?.idOther);
                setCommonGroup(data);
            })();
        }
    }, [conversation.listConversation, detailChat?.type, idConversation]);

    const decryptMessage = (password: string, message: string) => {
        try {
            const bytes = CryptoJS.AES.decrypt(message, password);
            const originalText = bytes.toString(CryptoJS.enc.Utf8);
            return originalText;
        } catch (e) {
            console.log(e);
        }
    };

    useEffect(() => {
        // Mã hóa đầu cuối => kiểm tra nếu chưa mã hóa thì không cho xem thư viện ảnh
        const encryptionConversation = endToEndEncryption.listIdConversationDecoded.find((c: any) => c.idConversation === idConversation);
        let newData: any = [];
        if (encryptionConversation) {
            if (encryptionConversation?.isDecoded) {
                newData = data.map((d: any) => {
                    if (d.content.includes("https")) {
                        return d;
                    }
                    return {
                        ...d,
                        content: decryptMessage(encryptionConversation.encryptionPassword, d.content)
                    };
                });
            } else {
                newData = data.filter((d: any) => {
                    if (d.content.includes("https")) {
                        return d;
                    }
                });
            }
        } else {
            newData = data;
        }
        if (!newData) {
            return;
        }

        // Filter data MEDIA only
        const mediaResponse = newData.filter(
            (message: IMessage) =>
                message.type === MESSAGE_TYPE.IMAGE ||
                message.type === MESSAGE_TYPE.MEDIA
        );
        // Filter MEDIA messages
        const dataMedia = mediaResponse.filter(
            (message: IMessage) => message.type === MESSAGE_TYPE.MEDIA
        );
        // Filter and parse IMAGE messages
        const dataImage = mediaResponse?.filter(
            (message: IMessage) => message.type === MESSAGE_TYPE.IMAGE
        );
        const parseDataImage = dataImage.flatMap((message: IMessage) => {
            // Mã hóa đầu cuối => logic
            let contentArray: any = null;
            try {
                contentArray = JSON.parse(message.content);
            } catch (e) {
                console.log(e);
            }
            return (contentArray !== null && contentArray.map((content: any) => ({
                ...message,
                content: JSON.stringify([content]) // Convert the content link to a single-element array
            }))) || [];
        });

        // Combine the IMAGE and MEDIA messages into a single array
        const filterDataMediaImage = [...parseDataImage, ...dataMedia];
        const sortedData = filterDataMediaImage.sort((a, b) => {
            const dateA = new Date(a.createdAt).getTime();
            const dateB = new Date(b.createdAt).getTime();

            return dateB - dateA;
        });
        setDataMediaImage(sortedData);
        // Filter and set dataFile
        const filterDataFiles = data?.filter(
            (message: IMessage) =>
                message.type === MESSAGE_TYPE.FILE && checkFile(message.content)
        );
        setDataFiles(filterDataFiles);

        // Filter and set dataLink
        const filterDataLink = data?.filter(
            (message: IMessage) => message.type === MESSAGE_TYPE.LINK
        );
        setDataLink(filterDataLink);

        // ----------------GROUP FILES----------------
        const groupDataFilesByDay = filterDataFiles.reduce(
            (acc: any, item: IMessage) => {
                const createdAt = new Date(item.createdAt).toLocaleDateString();
                acc[createdAt] = acc[createdAt] || [];
                acc[createdAt].push(item);
                return acc;
            },
            {}
        );

        const groupDataFilesCounts: number[] = Object.values(
            groupDataFilesByDay
        ).map((arr: any) => arr.length);

        const groupDataFiles = Object.keys(groupDataFilesByDay);
        setGroupFilesCounts(groupDataFilesCounts);
        setGroupFiles(groupDataFiles);
        // ----------------GROUP FILES end----------------

        // ----------------GROUP LINK----------------
        const groupDataLinkByDay = filterDataLink.reduce(
            (acc: any, item: IMessage) => {
                const createdAt = new Date(item.createdAt).toLocaleDateString();
                acc[createdAt] = acc[createdAt] || [];
                acc[createdAt].push(item);
                return acc;
            },
            {}
        );
        const groupDataLinkCounts: number[] = Object.values(groupDataLinkByDay).map(
            (arr: any) => arr.length
        );
        const groupDataLink = Object.keys(groupDataLinkByDay);

        setGroupLinkCounts(groupDataLinkCounts);
        setGroupLink(groupDataLink);
        // ----------------GROUP LINK end----------------
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [data, endToEndEncryption]);

    const ItemContainer = styled.div`
    padding: 0.25rem;
    width: 33%;
    display: flex;
    flex: none;
    align-content: stretch;
  `;

    const ListContainer: any = styled.div`
    display: flex;
    flex-wrap: wrap;
  `;
    const virtuoso = useRef(null);
    const [isHidden, setIsHidden] = useState<boolean>(false);
    const [isOpenUpdatePinHidden, setIsOpenUpdatePinHidden] = useState<boolean>(false);
    const handleOutGroup = async () => {
        try {
            const response = await axiosClient().delete(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/group/${idConversation}/out`);
            if (response.status === 200) {
                toast.success("Rời khỏi nhóm thành công");
                router.push("/chat");
                const newListConversations = conversation?.listConversation?.filter((item: any) => item._id !== idConversation);
                dispatch(
                    setListConversations(
                        sortConversationByNewest(filterConversation(newListConversations, user.id))
                    )
                );
            }
        } catch (err) {
            console.log(err);
        }
    };
    const [isShowModalNote, setIsShowModalNote] = useState<boolean>(false);
    const [isShowModalVote, setIsShowModalVote] = useState<boolean>(false);

    // Mã hóa đầu cuối => logic
    // để khi hover vào button giải mã sẽ hiện ra
    const encodeContent = "Mã hóa toàn bộ tin nhắn bằng mật khẩu mã hóa, sau khi mã hóa bạn sẽ không thấy được tin nhắn đã bị mã hóa cho đến khi được giải mã";
    const decodeContent = "Giải mã toàn bộ tin nhắn bị mã hóa bằng mật khẩu mã hóa, sau khi giải mã bạn sẽ thấy được tin nhắn đã bị mã hóa";
    const decodedContent = "Cuộc trò chuyện của bạn đã được giải mã !";

    const [encryptionConversation, setEncryptionConversation] = useState<EncryptionConversationProp | null>(null);

    useEffect(() => {
        setEncryptionConversation(findEncryptionConversationByIdConversation(idConversation, endToEndEncryption?.listIdConversationDecoded));
    }, [endToEndEncryption, idConversation]);

    const handleOpenModalGeneratesEncryptedPasswords = () => {
        if (conversation?.listConversation?.find((c: any) => c.id === idConversation)?.type === 2) {
            const groupOwner: any = conversation?.listConversation?.find((c: any) => c.id === idConversation)?.createdByUser;
            if (!groupOwner || groupOwner?.id !== user?.id) {
                alert("Chỉ group owner mới có thể mã hóa cuộc trò chuyện !");
                return;
            }
        }
        dispatch(setOpenModalGeneratesEncryptedPasswords(true));
    };

    const handleOpenModalDecodeMessage = () => {
        dispatch(setTypeOfMofalHandlesPasswordsEncryption("enterPassword"));
    };


    // test pin
    const [isPinnedMessages, setIsPinnedMessages] = useState<boolean | any>(() => {
        return conversation.listConversation?.find((item: any) => item._id === idConversation) ? conversation.listConversation?.find((item: any) => item._id === idConversation)?.isPinned : false;
    });
    const { isOpenModalTurnOffNotification } = notify;
    const { timeTurnOffNotify, setTimeTurnOffNotify } = useNotification(idConversation, isOpenModalTurnOffNotification);

    const handlePinConversation = async () => {
        if (isPinnedMessages) {
            try {
                const result = await axiosClient().put(
                    `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/unpin/${idConversation}`
                );
                if (result) {
                    setIsPinnedMessages(false);
                    dispatch(setListIdPinnedConversation(idConversation));
                }
            } catch (err: any) {
                console.log(err);
            }
        } else {
            try {
                const result = await axiosClient().put(
                    `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/pin/${idConversation}`
                );
                if (result) {
                    setIsPinnedMessages(true);
                    dispatch(setListIdPinnedConversation(idConversation));
                }
            } catch (err: any) {
                console.log(err);
            }
        }
    };

    const handleTurnOnNotification = () => {
        dispatch(removeIdToListIdConversationTurnOffNotify(idConversation));
        setTimeTurnOffNotify(null);
    };
    const handleClickSearchButton = () => {
        if (!search.isOpenSearch) {
            dispatch(toggleOpenSearch());
            dispatch(setIdConversation(idConversation));
        };};
    const handleImageError = (event: SyntheticEvent<HTMLImageElement, Event>) => {

        const imgElement = event.target as HTMLImageElement;
        imgElement.src = userAvatar.src;
    };
    return (
        <>
            {
                isOpenInfoGroupChat && (
                    <ModalInforGroupChat
                        isOpen={isOpenInfoGroupChat}
                        onClose={() => {
                            setIsOpenInfoGroupChat(false);

                        }}
                        dataGroup={dataGroup}
                        socket={socket}
                        reminiscentName={dataStorage[dataGroup?.idOther] || conversation?.otherUsername}
                        setIsOpenInfo={setIsOpenInfo}


                    />
                )
            }
            {
                isOpenInforConversation && (
                    <ModalInforConversation
                        isOpen={isOpenInforConversation}
                        onClose={() => {
                            setIsOpenInforConversation(false);

                        }}
                        conversation={dataGroup}

                        reminiscentName={dataStorage[dataGroup?.idOther] || conversation?.otherUsername}
                        commonGroup={commonGroup}
                        setIsOpenInfo={setIsOpenInfo}
                    />
                )
            }
            <Snackbar
                sx={{ height: "100%" }}
                open={open}
                autoHideDuration={3000}
                onClose={handleClose}
                anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
            >
                <Alert
                    onClose={handleClose}
                    severity="success"
                    sx={{
                        color: 'white',
                        backgroundColor: '#1f211d',
                        '& .MuiAlert-icon': { display: 'none' },
                        '& .MuiAlert-action': { display: 'none' }
                    }}
                >
                    Đã sao chép
                </Alert>
            </Snackbar>
            <Modal
                open={share}
                onClose={() => handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"

                onBackdropClick={() => {
                    dispatch(updateUserCreateGroup([]));
                    setShare(false);
                }}
            >
                <div className="relative w-[90%] mx-auto h-full mt-[15%]">
                    {share && (
                        <ModalShareGroupChat
                            onClose={() => setShare(false)}
                            urlGroup={urlGroup}
                            socket={socket}

                        />
                    )}
                </div>
            </Modal>
            <div
                className={`h-[92dvh] relative dark:bg-thDark-background bg-light-thDark-background flex-shrink-0 overflow-y-auto transform transition duration-500 ${isOpenInfo ? "-translate-x-full" : "translate-x-0"
                } w-screen z-40`}
            >
                {!isShowCommonGroup && !isShowMembers && !isShowManager && !isShowNewsletter && (
                    <>
                        <div className={`${isFile ? "hidden" : "block"}`}>
                            <div
                                className="absolute left-4 cursor-pointer"
                                onClick={() => setIsOpenInfo(false)}
                            >
                                <FiChevronLeft size={40} />
                            </div>
                            <div className="w-full flex flex-col items-center gap-y-2 gap-x-2 ">
                                <Image
                                    src={conversation?.otherAvatar}
                                    sizes="(max-width: 96px) 96px, 96px"
                                    alt={"avt"}
                                    onError={handleImageError}
                                    loading="eager"
                                    priority
                                    width={96}
                                    height={96}
                                    className="rounded-full object-cover h-24"
                                    onClick={() => handleShowInfoGroupChat(dataGroup)}
                                />
                                <div className="flex items-center">
                                    <p className="text-lg font-semibold flex justify-center">
                                        {dataGroup?.type === CONVERSATION.TYPE.GROUP ? (conversation?.otherUsername) : dataStorage[dataGroup?.idOther] ? dataStorage[dataGroup?.idOther] : conversation?.otherUsername}
                                        {
                                            detailChat?.type === CONVERSATION.TYPE.GROUP &&
                                            <AiOutlineEdit size={24} onClick={() => { setIsShowModalChangeName(true); }} />
                                        }
                                    </p>
                                    {dataGroup?.type !== CONVERSATION.TYPE.GROUP && (<div
                                        className=" rounded-md md:pr-0 w-[1.5rem] cursor-pointer"
                                        onClick={(e) => handleEditUsername(e, dataGroup)}
                                    >
                                        {/* <Image
                                            src={EditIcon}
                                            width={20}
                                            alt="edit icon"
                                            className="m-2"
                                        /> */}
                                        <BiEditAlt size={20} className="m-2 text-black dark:text-white" />
                                    </div>)}

                                </div>

                            </div>
                            <div className="flex gap-x-4 justify-center mt-4">
                                <button
                                    className={`text-thDark-background flex px-2 py-2 rounded-md ${styles.button_profile}`}
                                >
                                    <HiUserCircle size={24} />{" "}
                                    <span className="font-medium">Trang cá nhân</span>
                                </button>
                                <button
                                    className={`text-thDark-background flex px-2 py-2 rounded-md bg-gray-300`}
                                >
                                    <FiSearch size={24} />{" "}
                                    <span className="font-medium" onClick={handleClickSearchButton}>Tìm kiếm</span>
                                </button>
                            </div>
                            <div className="flex justify-center gap-x-1 flex-nowrap mt-4 px-6">
                                {
                                    timeTurnOffNotify ? (
                                        <div className="flex flex-col gap-y-1 items-center w-[76px]" onClick={handleTurnOnNotification}>
                                            <div className="dark:bg-thNewtral1 dark:hover:bg-thNewtral2 
                                             hover:bg-light-thNewtral2 bg-light-thNewtral1 w-fit rounded-full px-2 py-2 cursor-pointer">
                                                <BiSolidBellOff size={24} />
                                            </div>
                                            <span className="opacity-60 text-center text-xs">
                                                Bật thông báo
                                            </span>
                                        </div>
                                    ) : (
                                        <div className="flex flex-col gap-y-1 items-center w-[76px]" onClick={() => {
                                            dispatch(setOpenModalTurnOffNotifications(true));
                                        }}>
                                            <div className="dark:bg-thNewtral1 bg-light-thNewtral1  
                                            hover:bg-light-thNewtral2 dark:hover:bg-thNewtral2 w-fit rounded-full px-2 py-2 cursor-pointer">
                                                <BiSolidBell size={24} />
                                            </div>
                                            <span className="opacity-60 text-center text-xs">
                                                Tắt thông báo
                                            </span>
                                        </div>
                                    )
                                }
                                <div className="flex flex-col items-center w-[76px] " onClick={() => toggleStatusSeenMsg(isActiveSeended)}>
                                    <div className={`${!isActiveSeended ? ' dark:bg-thPrimary bg-light-thPrimary' : 'dark:bg-thNewtral1 bg-light-thNewtral1'} w-fit rounded-full px-2 py-2`}>
                                        <AiFillEye size={24} />
                                    </div>
                                    <span className="opacity-60 text-center text-xs">Ẩn đã xem</span>
                                </div>
                                <div className="flex flex-col items-center w-[76px]" onClick={handlePinConversation}>
                                    {
                                        !isPinnedMessages ? (
                                            <>
                                                <div className="dark:bg-thNewtral1 dark:hover:bg-thNewtral2 
                                                hover:bg-light-thNewtral2  bg-light-thNewtral1 w-fit rounded-full px-2 py-2 cursor-pointer">
                                                    <AiFillPushpin size={24} />
                                                </div>
                                                <span className="opacity-60 text-center text-xs">
                                                    Ghim tin nhắn
                                                </span>
                                            </>
                                        ) : (
                                            <>
                                                <div className="dark:bg-thNewtral1 dark:hover:bg-thNewtral2 
                                                bg-light-thNewtral1
                                                hover:bg-light-thNewtral2  w-fit rounded-full px-2 py-2 cursor-pointer">
                                                    <RiUnpinFill size={24} />
                                                </div>
                                                <span className="opacity-60 text-center text-xs">
                                                    Bỏ ghim tin nhắn
                                                </span>
                                            </>
                                        )
                                    }
                                </div>
                                {detailChat?.type === CONVERSATION.TYPE.INDIVIDUAL && (
                                    <div
                                        className="flex flex-col gap-y-1 items-center w-[76px]"
                                        onClick={() => setIsShowModal("ADD_TO_GROUP")}
                                    >
                                        <div className="dark:bg-thNewtral1 bg-light-thNewtral1 dark:hover:bg-thNewtral2 hover:bg-light-thNewtral2 w-fit rounded-full px-2 py-2 cursor-pointer">
                                            <HiUsers size={24} />
                                        </div>
                                        <span className="opacity-60 text-center text-xs">
                                            Thêm vào nhóm
                                        </span>
                                    </div>
                                )}
                                {detailChat?.type === CONVERSATION.TYPE.GROUP && (
                                    <>
                                        <div
                                            className="flex flex-col gap-y-1 items-center w-[76px]"
                                            onClick={() => setIsShowModalAddMembers(true)}
                                        >
                                            <div className="dark:bg-thNewtral1 bg-light-thNewtral1 dark:hover:bg-thNewtral2 hover:bg-light-thNewtral2 w-fit rounded-full px-2 py-2 cursor-pointer">
                                                <HiOutlineUserPlus size={24} />
                                            </div>
                                            <span className="opacity-60 text-center text-xs">
                                                Thêm thành viên
                                            </span>
                                        </div>
                                        <div className="flex flex-col gap-y-1 items-center w-[76px]" onClick={() => { setIsShowManager(true); }}>
                                            <div className="dark:bg-thNewtral1 bg-light-thNewtral1 dark:hover:bg-thNewtral2 hover:bg-light-thNewtral2 w-fit rounded-full px-2 py-2 cursor-pointer">
                                                <IoSettingsOutline size={24} />
                                            </div>
                                            <span className="opacity-60 text-center text-xs">
                                                Quản lý nhóm
                                            </span>
                                        </div>
                                    </>
                                )}
                            </div>
                            <div className="mt-8 px-6">
                                {detailChat?.type === CONVERSATION.TYPE.INDIVIDUAL && (
                                    <>
                                        <div
                                            className="flex justify-between py-3"
                                            onClick={() => handleShowMenu(1)}
                                        >
                                            <p className="font-bold">Nhóm</p>
                                            {options.includes(1) ? (
                                                <FiChevronDown size={24} />
                                            ) : (
                                                // <Image
                                                //     src={GroupIcon}
                                                //     sizes="(max-width: 24px) 24px, 24px"
                                                //     alt={"avt"}
                                                //     loading="eager"
                                                //     priority
                                                //     width={24}
                                                //     height={24}
                                                //     className="rounded-full object-cover h-6"
                                                // />
                                                <MdGroups
                                                    size={24}

                                                    className="cursor-pointer m-2 block dark:text-white
                                                text-thNewtral2 rounded-full object-cover h-6"

                                                />
                                            )}
                                        </div>
                                        <div
                                            className={`pl-6 transform transition duration-500 overflow-y-hidden h-0  ${options.includes(1) && "!h-fit"
                                            }`}
                                        >
                                            <div
                                                className="flex justify-between py-3"
                                                onClick={() => setIsShowCommonGroup(true)}
                                            >
                                                <p className="font-bold">Nhóm chung</p>
                                                <Image
                                                    src={GroupIcon}
                                                    sizes="(max-width: 24px) 24px, 24px"
                                                    alt={"avt"}
                                                    onError={handleImageError}
                                                    loading="eager"
                                                    priority
                                                    width={24}
                                                    height={24}
                                                    className="rounded-full object-cover h-6"
                                                />
                                            </div>
                                            <div
                                                className="flex justify-between py-3"
                                                onClick={() => setIsShowModal("CREATE_GROUP")}
                                            >
                                                <p className="font-bold">Tạo nhóm mới</p>
                                                <Image
                                                    src={AddIcon}
                                                    sizes="(max-width: 24px) 24px, 24px"
                                                    alt={"avt"}
                                                    onError={handleImageError}
                                                    loading="eager"
                                                    priority
                                                    width={24}
                                                    height={24}
                                                    className="rounded-full object-cover h-6"
                                                />
                                            </div>
                                        </div>
                                    </>
                                )}
                                {detailChat?.type === CONVERSATION.TYPE.GROUP && (
                                    <>
                                        <div
                                            className="flex justify-between py-3 cursor-pointer"
                                            onClick={() => handleShowMenu(1)}
                                        >
                                            {/* <p className="font-bold">Thành viên nhóm</p> */}
                                            <p className="font-bold">Thành viên nhóm
                                                {detailChat?.members.find((item: any) => item.id === user.id)?.type === CONVERSATION.ROLE_TYPE.OWNER &&
                                                    <span className="px-2 bg-thRed rounded-md text-sm ml-2">
                                                        {detailChat?.members?.filter((item: any) => item.ownerAccepted === false).length}
                                                    </span>
                                                }
                                            </p>
                                            {options.includes(1) ? (
                                                <FiChevronDown size={24} />
                                            ) : (
                                                <Image
                                                    src={GroupIcon}
                                                    sizes="(max-width: 24px) 24px, 24px"
                                                    alt={"avt"}
                                                    onError={handleImageError}
                                                    loading="eager"
                                                    priority
                                                    width={24}
                                                    height={24}
                                                    className="rounded-full object-cover h-6"
                                                />
                                            )}
                                        </div>
                                        <div
                                            className={`pl-6 transform transition duration-500 overflow-y-hidden h-0  ${options.includes(1) && "!h-fit"
                                            }`}
                                        >
                                            <div
                                                className="flex flex-col justify-start py-3 cursor-pointer"
                                            >
                                                <div className="flex"
                                                    onClick={() => setIsShowMembers(true)}
                                                >
                                                    <HiOutlineUsers size={24} />
                                                    <p className="font-bold ml-2">
                                                        {detailChat?.members?.length} Thành viên
                                                    </p>
                                                </div>

                                                {
                                                    detailChat?.conversationSetting?.find((item) => item.type === 9)?.value === "true" &&
                                                    <div
                                                        className="flex flex-row gap-x-4 rounded-sm dark:hover:bg-thNewtral2
                                                         hover:bg-light-thNewtral2 w-full h-14 relative items-center hover:cursor-pointer"
                                                        onClick={handleCopyLink}
                                                    >
                                                        <div className="  flex gap-3 items-center w-full">
                                                            <AiOutlineLink size={24} />
                                                            <div className="w-[80%] truncate">
                                                                Link tham gia nhóm
                                                                <br />
                                                                <span ref={spanRef} className="text-blue-500 ">

                                                                    {`${process.env.NEXT_PUBLIC_DAK_API_SHARE_GROUP}/g/${dataGroup?.inviteId}`}
                                                                </span>
                                                            </div>
                                                            <div className="flex gap-3">
                                                                <RiFileCopyLine
                                                                    size={30}
                                                                    className="rounded-full p-1 bg-white border-[1px] text-neutral-700 hover:bg-thNewtral2"
                                                                />
                                                                <PiShareFatBold
                                                                    size={30}
                                                                    className="rounded-full p-1 bg-white border-[1px]   text-neutral-700 hover:bg-thNewtral2"
                                                                    onClick={(e: any) => handleShare(e)}
                                                                />
                                                            </div>
                                                        </div>

                                                    </div>
                                                }
                                            </div>
                                        </div>
                                    </>
                                )}
                                {
                                    detailChat?.type === CONVERSATION.TYPE.GROUP &&
                                    <>
                                        <div
                                            className="flex justify-between py-3 cursor-pointer"
                                            onClick={() => handleShowMenu(6)}
                                        >
                                            <p className="font-bold">Bảng tin nhóm
                                            </p>
                                            {options.includes(6) ? (
                                                <FiChevronDown size={24} />
                                            ) : (
                                                <FiChevronRight size={24} />
                                            )}
                                        </div>
                                        <div
                                            className={`pl-6 transform transition duration-500 overflow-y-hidden h-0  ${options.includes(6) && "!h-fit"
                                            }`}
                                        >
                                            <div className="flex items-center gap-2 cursor-pointer" onClick={() => setIsShowNewsletter(true)}>
                                                <PiNoteLight size={20} />
                                                <span>Ghi chú, ghim, bình chọn</span>
                                            </div>
                                        </div>
                                    </>
                                }
                                <div
                                    className="flex justify-between py-3"
                                    onClick={() => setIsFile(true)}
                                >
                                    <p className="font-bold">Ảnh/Video, File, Liên kết</p>
                                    {/* <Image
                                        src={GaleryIcon}
                                        sizes="(max-width: 24px) 24px, 24px"
                                        alt={"avt"}
                                        loading="eager"
                                        priority
                                        width={24}
                                        height={24}
                                        className="rounded-full object-cover h-6"
                                    /> */}
                                    <IoImage size={24} className="rounded-full object-cover h-6 dark:text-white text-light-grey" />
                                </div>
                                <div
                                    className="flex justify-between py-3"
                                    onClick={() => handleShowMenu(2)}
                                >
                                    <p className="font-bold">Đổi ảnh nền</p>
                                    {options.includes(2) ? (
                                        <FiChevronDown size={24} />
                                    ) : (
                                        <Image
                                            src={Circle}
                                            sizes="(max-width: 24px) 24px, 24px"
                                            alt={"avt"}
                                            onError={handleImageError}
                                            loading="eager"
                                            priority
                                            width={24}
                                            height={24}
                                            className="rounded-full object-cover h-6"
                                        />
                                    )}
                                </div>
                                <div
                                    className={`pl-6 transform transition duration-500 overflow-y-hidden h-0  ${options.includes(2) && "!h-fit"
                                    }`}
                                >
                                    <div className="flex justify-between py-3" onClick={() => {
                                        if (openImageUploadModal) {
                                            openImageUploadModal();
                                        }
                                    }}>
                                        <p className="font-bold cursor-pointer">Ảnh từ thiết bị</p>
                                        <Image
                                            src={ExportIcon}
                                            sizes="(max-width: 24px) 24px, 24px"
                                            alt={"avt"}
                                            onError={handleImageError}
                                            loading="eager"
                                            priority
                                            width={24}
                                            height={24}
                                            className="rounded-full object-cover h-6"
                                        />
                                    </div>
                                    <div className="flex justify-between py-3">
                                        <p className="font-bold">Ảnh trên hệ thống</p>
                                        <Image
                                            src={AddIcon}
                                            sizes="(max-width: 24px) 24px, 24px"
                                            alt={"avt"}
                                            onError={handleImageError}
                                            loading="eager"
                                            priority
                                            width={24}
                                            height={24}
                                            className="rounded-full object-cover h-6"
                                        />
                                    </div>
                                </div>
                                <div className="flex justify-between py-3">
                                    <p className="font-bold">Lưu cuộc trò chuyện</p>
                                    {/* <Image
                                        src={DownloadIcon}
                                        sizes="(max-width: 24px) 24px, 24px"
                                        alt={"avt"}
                                        loading="eager"
                                        priority
                                        width={24}
                                        height={24}
                                        className="rounded-full object-cover h-6"
                                    /> */}
                                    <LuArrowDownSquare size={24} className="rounded-full object-cover h-6 dark:text-white text-light-grey" />
                                </div>
                                <div
                                    className="flex justify-between py-3"
                                    onClick={() => handleShowMenu(3)}
                                >
                                    <p className="font-bold">Bảo mật và quyền riêng tư</p>
                                    {options.includes(3) ? (
                                        <FiChevronDown size={24} />
                                    ) : (
                                        <Image
                                            src={SecurityIcon}
                                            sizes="(max-width: 24px) 24px, 24px"
                                            alt={"avt"}
                                            onError={handleImageError}
                                            loading="eager"
                                            priority
                                            width={24}
                                            height={24}
                                            className="rounded-full object-cover h-6"
                                        />
                                    )}
                                </div>
                                <div
                                    className={`pl-6 transform transition duration-500 overflow-y-hidden h-0  ${options.includes(3) && "!h-fit"
                                    }`}
                                >
                                    {
                                        encryptionConversation ? !encryptionConversation.isDecoded ? (
                                            <Tippy content={decodeContent}>
                                                <div className="flex justify-between py-3 cursor-pointer" onClick={handleOpenModalDecodeMessage}>
                                                    <p className="font-bold">Giải mã</p>
                                                    <FaUnlockAlt size={24} />
                                                </div>
                                            </Tippy>
                                        ) : (
                                            <Tippy content={decodedContent}>
                                                <div className="flex justify-between py-3 opacity-70">
                                                    <p className="font-bold">Đã được giải mã</p>
                                                    <FaUnlockAlt size={24} />
                                                </div>
                                            </Tippy>
                                        ) : (
                                            <Tippy content={encodeContent}>
                                                <div className="flex justify-between py-3" onClick={handleOpenModalGeneratesEncryptedPasswords}>
                                                    <p className="font-bold">Mã hóa</p>
                                                    <Image
                                                        src={LockIcon}
                                                        sizes="(max-width: 24px) 24px, 24px"
                                                        alt={"avt"}
                                                        onError={handleImageError}
                                                        loading="eager"
                                                        priority
                                                        width={24}
                                                        height={24}
                                                        className="rounded-full object-cover h-6"
                                                    />
                                                </div>
                                            </Tippy>
                                        )
                                    }
                                    <div className="flex justify-between py-3">
                                        <p className="font-bold">Chặn</p>
                                        <Image
                                            src={BlockIcon}
                                            sizes="(max-width: 24px) 24px, 24px"
                                            alt={"avt"}
                                            onError={handleImageError}
                                            loading="eager"
                                            priority
                                            width={24}
                                            height={24}
                                            className="rounded-full object-cover h-6"
                                        />
                                    </div>
                                    <div className="flex justify-between py-3">
                                        <p className="font-bold">Ẩn trò chuyện</p>
                                        <InfoSwitch
                                            idConversation={idConversation}
                                            setIsOpenSetPinHidden={setIsOpenSetPinHidden}
                                            setIsHidden={setIsHidden}
                                        />
                                    </div>
                                    {isHidden && (
                                        <div className="flex justify-between py-3">
                                            <button
                                                className="border-2 border-thPrimary text-thPrimary py-2 rounded-lg hover:opacity-80 w-full"
                                                onClick={() => setIsOpenUpdatePinHidden(true)}
                                            >
                                                Đổi mã PIN
                                            </button>
                                        </div>
                                    )}
                                </div>
                                {
                                    detailChat.type === CONVERSATION.TYPE.GROUP && (
                                        <div
                                            className="flex justify-between py-3 mb-2 cursor-pointer"
                                            onClick={() => {
                                                if (detailChat?.members?.find((item: any) => item.id === user.id)?.type === CONVERSATION.ROLE_TYPE.MEMBER_WAITING_ACCEPT) {
                                                    return toast.warning("Động cần join nhóm trước khi chat");
                                                };
                                                setIsShowModalConfirm(true);
                                            }}
                                        >
                                            <span className="text-thRed font-bold">Rời nhóm</span>
                                            <PiSignOutLight size={24} className="text-thRed" />
                                        </div>
                                    )
                                }
                            </div>
                        </div>
                        <div className={`${isFile ? "block" : "hidden"}`}>
                            <div className="flex gap-x-4 items-center">
                                <div
                                    className="cursor-pointer"
                                    onClick={() => setIsFile(false)}
                                >
                                    <FiChevronLeft size={40} />
                                </div>
                                <p className="text-lg font-semibold">
                                    Ảnh/Video, File, Liên kết
                                </p>
                            </div>
                            <div className="flex justify-around py-3 dark:bg-thNewtral1 bg-light-thNewtral1 rounded mx-4">
                                <p
                                    className={`${feature.media && "dark:text-thPrimary text-light-thPrimary font-font"}`}
                                    onClick={() =>
                                        setFeature({ file: false, media: true, link: false })
                                    }
                                >
                                    Ảnh/Video
                                </p>
                                <p
                                    className={`${feature.file && "dark:text-thPrimary text-light-thPrimary font-medium"}`}
                                    onClick={() =>
                                        setFeature({ file: true, media: false, link: false })
                                    }
                                >
                                    File
                                </p>
                                <p
                                    className={`${feature.link && "dark:text-thPrimary text-light-thPrimary font-medium"}`}
                                    onClick={() =>
                                        setFeature({ file: false, media: false, link: true })
                                    }
                                >
                                    Liên kết
                                </p>
                            </div>
                            <div
                                className={`${feature.media ? "block" : "hidden"} px-4 mt-4`}
                            >
                                {/* <div className="grid grid-cols-3 gap-4 py-4 w-full h-[100px]">
                                   {data
                                       ?.filter(
                                           (message: IMessage) =>
                                               message.type === MESSAGE_TYPE.IMAGE ||
                                                message.type === MESSAGE_TYPE.MEDIA
                                       )
                                       .map((filteredMsg: IMessage) => {
                                           return filteredMsg.type === MESSAGE_TYPE.IMAGE ? (
                                               JSON.parse(filteredMsg.content).map((item: string) => {
                                                   return (
                                                       <Image
                                                           key={item}
                                                           sizes="(max-width: 96px) 96px, 96px"
                                                           alt={"avt"}
                                                           loading="eager"
                                                           priority
                                                           width={96}
                                                           height={96}
                                                           src={item}
                                                           className="w-full h-full max-h-[96px] object-cover"
                                                       />
                                                   );
                                               })
                                           ) : (
                                               <video
                                                   src={filteredMsg.content}
                                                   width={96}
                                                   height={96}
                                                   controls
                                                   className="w-full h-full object-cover"
                                               />
                                           );
                                       })}
                               </div> */}
                                <VirtuosoGrid
                                    ref={virtuoso}
                                    totalCount={dataMediaImage.length}
                                    overscan={20}
                                    style={{ height: heightVirtualList }}
                                    components={{
                                        Item: ItemContainer,

                                        List: ListContainer,
                                        ScrollSeekPlaceholder: () => (
                                            <ItemContainer>
                                                <Image
                                                    sizes="(max-width: 96px) 96px, 96px"
                                                    alt={"avt"}
                                                    onError={handleImageError}
                                                    loading="eager"
                                                    priority
                                                    width={96}
                                                    height={96}
                                                    src="/logoDAK.jpg"
                                                    className="w-full aspect-square object-cover"
                                                />
                                            </ItemContainer>
                                        )
                                    }}
                                    itemContent={(index) => {
                                        let url = "";
                                        if (dataMediaImage[index].type === MESSAGE_TYPE.IMAGE) {
                                            const parseContent = JSON.parse(
                                                dataMediaImage[index].content
                                            );
                                            url = parseContent[0];
                                        }
                                        return dataMediaImage[index].type === MESSAGE_TYPE.IMAGE ? (
                                            <Image
                                                key={url}
                                                sizes="(max-width: 96px) 96px, 96px"
                                                alt={"avt"}
                                                onError={handleImageError}
                                                loading="eager"
                                                priority
                                                width={96}
                                                height={96}
                                                src={url}
                                                className="w-full aspect-square object-cover"
                                                onClick={() => {
                                                    setIdMediaModal(dataMediaImage[index]?._id);
                                                    setUrlMediaModal(url);
                                                    setIsOpenMediaLibrary(true);
                                                }}
                                            />
                                        ) : (
                                            <div
                                                onClick={() => {
                                                    setIdMediaModal(dataMediaImage[index]?._id);
                                                    setUrlMediaModal(dataMediaImage[index].content);
                                                    setIsOpenMediaLibrary(true);
                                                }}
                                                className="relative"
                                            >
                                                <AiFillPlayCircle
                                                    size={40}
                                                    className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 text-thWhite"
                                                />

                                                <video
                                                    src={dataMediaImage[index].content}
                                                    width={300}
                                                    height={300}
                                                    className="w-full aspect-square object-cover"
                                                    style={{ pointerEvents: "none" }}
                                                />
                                            </div>
                                        );
                                    }}
                                    scrollSeekConfiguration={{
                                        enter: (velocity) => Math.abs(velocity) > 200,
                                        exit: (velocity) => Math.abs(velocity) < 30
                                    }}
                                />
                            </div>
                            <div className={`${feature.file ? "block" : "hidden"} px-4 mt-4`}>
                                {/* <div className="flex flex-col gap-y-2">
                                   {data
                                       ?.filter(
                                           (message: IMessage) =>
                                               message.type === MESSAGE_TYPE.FILE &&
                                                checkFile(message.content)
                                       )
                                       .map((filteredMsg: IMessage) => {
                                           return (
                                               <div key={filteredMsg._id}>
                                                   <p className="opacity-60 text-lg">
                                                       {moment(filteredMsg.createdAt).format("DD-MM-YYYY")}
                                                   </p>
                                                   <div
                                                       key={filteredMsg._id}
                                                       className="py-4 dark:dark:bg-thNewtral1 bg-light-thNewtral1 flex items-center gap-x-4 h-fit rounded-md px-4"
                                                   >
                                                       <BsFileEarmarkText size={40} />
                                                       <a
                                                           target="_blank"
                                                           rel="noopener noreferrer"
                                                           href={filteredMsg.content}
                                                           className="max-w-[80%] text-ellipsis overflow-x-hidden"
                                                       >
                                                           {getFileName(filteredMsg.content)}
                                                       </a>
                                                   </div>
                                               </div>
                                           );
                                       })}
                               </div> */}
                                <GroupedVirtuoso
                                    groupCounts={groupFilesCounts}
                                    style={{ height: heightVirtualList }}
                                    groupContent={(index) => {
                                        return (
                                            <div className="text-lg dark:bg-thDark-background bg-light-thDark-background w-full">
                                                <span className="opacity-60">{groupFiles[index]}</span>
                                            </div>
                                        );
                                    }}
                                    itemContent={(index) => {
                                        return (
                                            <div key={dataFiles[index]._id}>
                                                <div
                                                    key={dataFiles[index]._id}
                                                    className="py-4 dark:bg-thNewtral1 bg-light-thNewtral1 flex items-center gap-x-4 h-fit rounded-md px-4 my-2"
                                                >
                                                    <BsFileEarmarkText size={40} />
                                                    <a
                                                        target="_blank"
                                                        rel="noopener noreferrer"
                                                        href={dataFiles[index].content}
                                                        className="max-w-[80%] text-ellipsis overflow-x-hidden"
                                                    >
                                                        {getFileName(dataFiles[index].content)}
                                                    </a>
                                                </div>
                                            </div>
                                        );
                                    }}
                                />
                            </div>
                            <div className={`${feature.link ? "block" : "hidden"} px-4 mt-4`}>
                                {/* <div className="flex flex-col gap-y-2">
                                   {data
                                       ?.filter(
                                           (message: IMessage) => message.type === MESSAGE_TYPE.LINK
                                       )
                                       .map((filteredMsg: IMessage) => {
                                           return (
                                               <div key={filteredMsg._id}>
                                                   <p className="opacity-60 text-lg">
                                                       {moment(filteredMsg.createdAt).format("DD-MM-YYYY")}
                                                   </p>
                                                   <div
                                                       key={filteredMsg._id}
                                                       className="py-4 dark:bg-thNewtral1 bg-light-thNewtral1 flex items-center gap-x-4 h-fit rounded-md px-4"
                                                   >
                                                       <BsLink45Deg size={40} />
                                                       <a
                                                           target="_blank"
                                                           rel="noopener noreferrer"
                                                           href={filteredMsg.content}
                                                           className="max-w-[80%] text-ellipsis overflow-x-hidden"
                                                       >
                                                           {filteredMsg.content}
                                                       </a>
                                                   </div>
                                               </div>
                                           );
                                       })}
                               </div> */}
                                <GroupedVirtuoso
                                    groupCounts={groupLinkCounts}
                                    style={{ height: heightVirtualList }}
                                    groupContent={(index) => {
                                        return (
                                            <div className="text-lg dark:bg-thDark-background bg-light-thDark-background w-full">
                                                <span className="opacity-60">{groupLink[index]}</span>
                                            </div>
                                        );
                                    }}
                                    itemContent={(index) => {
                                        return (
                                            <div key={dataLink[index]._id}>
                                                <div
                                                    key={dataLink[index]._id}
                                                    className="py-4 dark:bg-thNewtral1 bg-light-thNewtral1 flex items-center gap-x-4 h-fit rounded-md px-4  my-2"
                                                >
                                                    <BsLink45Deg size={40} />
                                                    <a
                                                        target="_blank"
                                                        rel="noopener noreferrer"
                                                        href={dataLink[index].content}
                                                        className="max-w-[80%] text-ellipsis overflow-x-hidden"
                                                    >
                                                        {dataLink[index].content}
                                                    </a>
                                                </div>
                                            </div>
                                        );
                                    }}
                                />
                            </div>
                        </div>
                    </>
                )}
                {isShowCommonGroup && (
                    <CommonGroup
                        setIsShow={setIsShowCommonGroup}
                        setIsOpenForm={setIsShowModal}
                        data={commonGroup}
                    />
                )}
                {isShowMembers && (
                    <Members
                        close={() => setIsShowMembers(false)} setIsShowModalAddMembers={setIsShowModalAddMembers} fetchDetailChat={fetchDetailChat} setIsShowModalConfirm={setIsShowModalConfirm}
                    />
                )}
                {
                    isShowManager && <ManagerGroup close={() => setIsShowManager(false)} setIsShowModalBlockUser={setIsShowModalBlockUser} />
                }
                {
                    isShowNewsletter && <NewsletterGroup closeInfoChat={() => setIsOpenInfo(false)} setMessageScroll={setMessageScroll as React.Dispatch<any>} close={() => setIsShowNewsletter(false)} setIsShowModalVote={setIsShowModalVote} />
                }
            </div>
            {isEditUserName &&


                <div className="fixed left-0 top-[-90px] bottom-0  w-full h-[calc(100%+110px)]`} bg-[#00000080] min-h-screen  z-50 ">
                    <div ref={modalEditUserRef} >


                        <EditUserName
                            contact={contact}
                            onClose={() => {
                                setIsEditUserName(false);
                            }}
                            onSubmit={(otherUsername) => {
                                setOtherUsername(otherUsername);
                                setIsEditUserName(false);
                            }}
                            otherUsername={otherUsername}

                        />
                    </div>
                </div>


            }
            {isShowModal && (
                <FormAddGroup
                    setIsOpen={setIsShowModal}
                    idConversation={idConversation}
                    action={isShowModal}
                    detailChat={detailChat}
                    // setDetailChat={setDetailChat}
                    fetchDetailChat={fetchDetailChat}
                    setCommonGroup={setCommonGroup}
                    commonGroup={commonGroup}
                />
            )}
            {isOpenSetPinHidden && (
                <ModalSetPinHidden
                    setIsOpenSetPinHidden={setIsOpenSetPinHidden}
                    conversationId={idConversation}
                />
            )}
            {isOpenUpdatePinHidden && (
                <ModalUpdatePinHidden
                    idConversation={idConversation}
                    setIsOpenUpdatePinHidden={setIsOpenUpdatePinHidden}
                />
            )}
            {
                isShowModalChangeName && <ModalChangeName setIsShowModal={setIsShowModalChangeName} name={conversation?.otherUsername} type={detailChat?.type} idConversation={idConversation} />
            }
            {
                isShowModalNote && <ModalNote setIsShowModal={setIsShowModalNote} idConversation={idConversation} />
            }
            {
                isShowModalVote && <ModalVote setIsShowModal={setIsShowModalVote} idConversation={idConversation} />
            }
            {
                isShowModalAddMembers && <ModalAddMembers close={() => setIsShowModalAddMembers(false)} members={detailChat?.members} idConversation={idConversation} fetchDetailChat={fetchDetailChat} />
            }
            {
                isShowModalConfirm && <ModalConfirm close={() => setIsShowModalConfirm(false)} handleConfirm={handleOutGroup} message="Bạn chắc chắn muốn out group này?" />
            }
            {
                isShowModalBlockUser !== "" && <ModalManagerGroup action={isShowModalBlockUser} close={() => setIsShowModalBlockUser("")} members={detailChat?.members} />
            }
            {
                search.isOpenSearch && <SearchBoxMbl setMessageScroll={setMessageScroll as React.Dispatch<any>} closeInfoChat={() => setIsOpenInfo(false)} />
            }
        </>
    );
}
