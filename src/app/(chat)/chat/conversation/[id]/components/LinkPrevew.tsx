import { ReactElement } from "react";
import FacebookEmbedPreview from "./embed/FacebookEmbedPreview";
import TikTokEmbed from "./embed/TiktokEmbed";
import TwitterEmbed from "./embed/TwitterEmbed";
import YoutubeEmbed from "./embed/YoutubeEmbed";
import InstagramEmbed from "./embed/InstagramEmbed";
import TwitchEmbed from "./embed/TwitchEmbed";
const LinkPreview = ({ url }: { url: string}): ReactElement => {
    return (
        <>
            <div
                className={`flex justify-center  `}
            >
                {url.includes("instagram.com") && url.split("?")[0] && (
                    <InstagramEmbed url={url} />
                )}
                {(url.includes("facebook.com") || url.includes("fb.watch")) && (
                    <FacebookEmbedPreview url={url} />
                )}
                {/* {url.includes("fb.watch") && (
                    <iframe
                        src={`https://www.facebook.com/plugins/video.php?href=${url}&show_text=0`}
                        width={"100%"}
                        height={1000}
                        className="h-[45vh] max-[480px]:h-[260px] w-full"
                        loading="lazy"
                        allowFullScreen
                        title="Facebook video player"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                        scrolling="no"
                    ></iframe>
                    // <PreviewFB url={url} />
                )} */}
                {url.includes("youtube.com") && (
                    <YoutubeEmbed url={url}/>
                )}

                {url.includes("tiktok.com") && (
                    <>
                        <TikTokEmbed url={url} />
                    </>
                )}
                {(url.includes("x.com") || url.includes("twitter.com")) && (
                    <>
                        <TwitterEmbed url={url} />
                    </>
                )}
                {url.includes("twitch.tv") && (
                    <TwitchEmbed url={url} />
                )}
            </div>
        </>
    );
};

export default LinkPreview;
