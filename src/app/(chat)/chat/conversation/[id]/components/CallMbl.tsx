"use client";
import phone from "@/public/call2.svg";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { updateCalling } from "@/src/redux/slices/callSlice";
import { setUserIsCalling } from "@/src/redux/slices/userSlice";
import Image from "next/image";
import { ReactElement, SyntheticEvent, useEffect, useRef, useState } from "react";
import { useCookies } from "react-cookie";
import Draggable, { DraggableData, DraggableEvent } from "react-draggable";
import { BiSolidMicrophone, BiSolidMicrophoneOff } from "react-icons/bi";
import { BsFillCameraVideoFill, BsFillCameraVideoOffFill } from "react-icons/bs";
import { FiChevronLeft } from "react-icons/fi";
import { HiSpeakerWave, HiSpeakerXMark } from "react-icons/hi2";
import styles from "../conversation.module.css";
import LoadingCall from "./LoadingCall";
import userAvatar from "@/public/user.png";
interface FeatureState {
    mute: boolean;
    camera: boolean;
    speaker: boolean;
}

export default function CallMbl({
    isCalling,
    setIsCalling,
    isDragVideoHidden,
    setIsDragVideoHidden
}: {
    isCalling: boolean;
    setIsCalling: (isCalling: boolean) => void;
    isDragVideoHidden: boolean;
    setIsDragVideoHidden: (isDragVideoHidden: boolean) => void;
}): ReactElement {
    const [feature, setFeature] = useState<FeatureState>({
        mute: true,
        camera: true,
        speaker: false
    });
    const iframeRef = useRef(null);
    const dispatch = useAppDispatch();
    const [typeOfCalling, setTypeOfCalling] = useState<number>();
    const [cookies] = useCookies(["data"]);
    // Lấy Type cuộc gọi (Video/Voice) từ cookies
    useEffect(() => {
        if (cookies && cookies.data) {
            setTypeOfCalling(cookies.data.type);
        }
    }, [cookies]);
    const { user } = useAppSelector((state) => state.user);
    // Để component display: block trong lần khởi tạo đầu tiên
    useEffect(() => {
        setIsCalling(true);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleOpenClick = (iframeRef: any, state: boolean) => {
        // Truy cập iframe và gọi phương thức Reset()
        if (iframeRef.current) {
            const iframeContentWindow = iframeRef.current.contentWindow;
            if (iframeContentWindow.opencamera && !state) {
                iframeContentWindow.opencamera();
            } else {
                iframeContentWindow.closecamera();
            }
        }
    };
    const handleSourceClick = (iframeRef: any, state: boolean) => {
        // Truy cập iframe và gọi phương thức Reset()

        if (iframeRef.current) {
            const iframeContentWindow = iframeRef.current.contentWindow;
            if (iframeContentWindow.mute && !state) {
                iframeContentWindow.mute();
            } else {
                iframeContentWindow.unmute();
            }
        }
    };
    const handleSpeakerClick = (iframeRef: any, state: boolean) => {
        if (iframeRef.current) {
            const iframeContentWindow = iframeRef.current.contentDocument;
            if (iframeContentWindow.querySelector("audio")) {
                if (!state) {
                    iframeContentWindow.querySelector("audio").muted = true;
                } else {
                    iframeContentWindow.querySelector("audio").muted = false;
                }
            }
        }
    };
    const draggableRef = useRef<HTMLDivElement | null>(null);
    const [isDragging, setIsDragging] = useState(false);
    const [position, setPosition] = useState({ x: 0, y: 0 });
    const [screenDimensions, setScreenDimensions] = useState({
        rightScreen: 0,
        leftScreen: 0,
        topScreen: 0,
        bottomScreen: 0
    });
    useEffect(() => {
        const windowHeight = window.innerHeight;
        const windowdWidth = window.innerWidth;
        const rect = draggableRef.current?.getBoundingClientRect();
        if (rect) {

            // Khi popup được kéo ra khỏi 1/3 màn hình thì ẩn
            const ratioHidden = 0.3;

            const widthHidden = rect.width * ratioHidden;
            const heightHidden = rect.height * ratioHidden;

            const rightScreen = widthHidden;
            const leftScreen = 2 * widthHidden - windowdWidth;
            const topScreen = -heightHidden;
            const bottomScreen = windowHeight - 2 * heightHidden;
            setScreenDimensions({
                rightScreen: rightScreen,
                leftScreen: leftScreen,
                topScreen: topScreen,
                bottomScreen: bottomScreen
            });
        }
    }, [isCalling]);

    const CLICK_THRESHOLD = 10; // Adjust this threshold as needed

    const handleDrag = (e: DraggableEvent, ui: DraggableData) => {
        // Cập nhật vị trí và trạng thái khi Drag
        if (
            !isDragging &&
            (Math.abs(ui.deltaX) > CLICK_THRESHOLD ||
                Math.abs(ui.deltaY) > CLICK_THRESHOLD)
        ) {
            // If movement is above the threshold, consider it a drag
            setIsDragging(true);
        }
        const { x, y } = ui;
        setPosition({ x, y });

        // Xử lý action user Drag để ẩn VideoPopup
        if (
            x > screenDimensions.rightScreen ||
            x < screenDimensions.leftScreen ||
            y > screenDimensions.bottomScreen ||
            y < screenDimensions.topScreen
        ) {
            setIsDragVideoHidden(true);
        }
    };
    useEffect(() => {
        if (!isDragVideoHidden) {
            setPosition({ x: 0, y: 0 });
        }
    }, [isDragVideoHidden]);
    const handleStop = () => {
        // setTimeout(() => {
        if (!isDragging) {
            // Set lại position rồi ZoomOut Call
            setPosition({ x: 0, y: 0 });
            if (!isCalling) {
                setIsCalling(true);
            }
        }
        // }, 300);
        setIsDragging(false);
    };
    const [loading, setLoading] = useState(true);

    const handleLoadComplete = () => {
        ('onLoad event has finished!');
        setLoading(false);
    };
    const handleImageError = (event: SyntheticEvent<HTMLImageElement, Event>) => {

        const imgElement = event.target as HTMLImageElement;
        imgElement.src = userAvatar.src;
    };
    return (
        // Contain div bằng Draggable để có thể di chuyển component được
        <Draggable
            handle=".onDrag"
            position={position}
            onDrag={handleDrag}
            // onStop={handleStop}
            onStop={handleStop}
        >
            <div
                className={`fixed top-0 bottom-0 right-0 bg-thDark-background ${
                    // typeOfCalling = 2: Call video => Thu nhỏ/Phóng to cuộc gọi
                    // typeOfCalling != 2: Call voice => Ẩn/Hiện cuộc gọi
                    typeOfCalling === 2
                        ? isCalling
                            ? "h-[100dvh] w-[100dvw]"
                            : "h-[30%] w-[36%] rounded-xl"
                        : isCalling
                            ? "h-[100dvh] w-[100dvw] block"
                            : "h-[100dvh] w-[100dvw] hidden"
                }
        ${isDragVideoHidden ? "hidden" : "block"}
        overflow-x-hidden z-50`}
                ref={draggableRef}
            >
                {/* // Dùng để handle action "Touch and Drag" trên giao diện Mobile */}
                <div
                    className={`onDrag absolute top-0 left-0 h-full w-full opacity-0 bg-thWhite ${isCalling ? "hidden" : "block z-50"
                    }`}
                ></div>
                {
                    (loading || user.is_watting_call) &&
                   <LoadingCall/>
                }
                <iframe
                    ref={iframeRef}
                    id="resultFrame"
                    src="https://mdac.irics.vn/call"
                    className="w-full h-full absolute"
                    allow="camera; microphone; display-capture; autoplay; clipboard-write;"
                    title="Call"
                    onLoad={handleLoadComplete}
                ></iframe>
                {/* Nút Back về giao diện chatBox */}
                <div
                    className={`absolute left-4 cursor-pointer ${typeOfCalling === 2 ? (isCalling ? "block" : "hidden") : ""
                    }`}
                    onClick={() => {
                        setIsCalling(false);
                    }}
                >
                    <FiChevronLeft size={40} />
                </div>

                <div
                    className={`feature flex justify-center w-full absolute z-50 bottom-4 gap-x-4 
          ${typeOfCalling === 2 ? (isCalling ? "block" : "hidden") : ""}`}
                >
                    <div
                        className={`rounded-full ${styles.call_btn} ${feature.mute === true && styles.call_btn_active
                        }`}
                        onClick={() => {
                            setFeature((prevState) => ({
                                ...prevState,
                                mute: !prevState.mute
                            }));
                            handleSourceClick(iframeRef, !feature.mute);
                        }}
                    >
                        {feature.mute ? < BiSolidMicrophone size={32} /> : < BiSolidMicrophoneOff size={32} />}
                    </div>
                    <div
                        className={`rounded-full ${styles.call_btn} ${feature.camera === true && styles.call_btn_active
                        }`}
                        onClick={() => {
                            setFeature((prevState) => ({
                                ...prevState,
                                camera: !prevState.camera
                            }));
                            handleOpenClick(iframeRef, feature.camera);
                        }}
                    >
                        {feature.camera ? < BsFillCameraVideoFill size={32} /> : < BsFillCameraVideoOffFill size={32} />}
                    </div>
                    <div
                        className={`rounded-full ${styles.call_btn} ${feature.speaker === true && styles.call_btn_active
                        }`}
                        onClick={() => {
                            setFeature((prevState) => ({
                                ...prevState,
                                speaker: !prevState.speaker
                            }));
                            handleSpeakerClick(iframeRef, feature.speaker);
                        }}
                    >
                        {feature.speaker === true ? (
                            <HiSpeakerXMark size={32} />
                        ) : (
                            <HiSpeakerWave size={32} />
                        )}{" "}
                    </div>
                    <div
                        className={`rounded-full !bg-thRed ${styles.call_btn}`}
                        onClick={() => {
                            ("vao day");
                            setIsCalling(false);
                            dispatch(setUserIsCalling(false));
                            dispatch(updateCalling({ isCalling: false, conversationId: "", userAcceptCall: "", userCreatCall: "", deviceCall: "", deviceAcceptCall: "" }));
                        }}
                    >
                        <Image
                            src={phone}
                            sizes="32px"
                            alt={"avt"}
                            loading="eager"
                            priority
                            width={24}
                            onError={handleImageError}
                            height={24}
                            className="rounded-full h-6 cursor-pointer text-thWhite"
                        />
                    </div>
                </div>
            </div>
        </Draggable>
    );
}
