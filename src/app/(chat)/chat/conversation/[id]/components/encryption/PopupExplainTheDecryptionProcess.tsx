"use client";

import { ReactElement } from "react";

interface Props {
    setIsOpenPopupExplainTheDecryptionProcess: any;
    hanleMakeACallAfterAcceptedProcess: () => void;
}

export default function PopupExplainTheDecryptionProcess({
    setIsOpenPopupExplainTheDecryptionProcess,
    hanleMakeACallAfterAcceptedProcess
}: Props): ReactElement {
    const onClose = () => {
        setIsOpenPopupExplainTheDecryptionProcess(false);
    };
    const onAccept = () => {
        setIsOpenPopupExplainTheDecryptionProcess(false);
        hanleMakeACallAfterAcceptedProcess();
    };

    return (
        <div className={`fixed top-0 left-0 w-full h-full flex items-center z-50 justify-center`}>
            <div className="absolute w-full h-full opacity-50" onClick={onClose}></div>
            <div className="z-50 dark:bg-black bg-light-thNewtral2 p-6 rounded-md w-[300px] lg:w-[400px] flex flex-col">
                <h2 className="text-2xl font-bold mb-4 w-full text-center">Giải thích về quy trình</h2>
                <p>+ Sau khi xác nhận bạn sẽ được tạo 1 cuộc gọi trực tiếp tới partner (hoặc group owner) để trao đổi thông tin về mật khẩu.</p>
                <p>+ Sau khi nhận được mật khẩu hãy bấm vào nút &#x2033;Tiếp tục&#x2033; để nhập mật khẩu vào để decode tin nhắn.</p>

                <div className="flex justify-between mt-3">
                    <button type="button" className="bg-gray-500 text-white font-bold px-4 py-2 rounded-md cursor-pointer" onClick={onClose}>
                        Hủy
                    </button>
                    <button type="submit" className="dark:bg-thPrimary bg-light-thPrimary text-black font-bold px-4 py-2 rounded-md cursor-pointer" onClick={onAccept}>
                        Xác nhận
                    </button>
                </div>
            </div>
        </div>
    );

}
