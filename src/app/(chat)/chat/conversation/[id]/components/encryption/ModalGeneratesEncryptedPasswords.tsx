"use client";

import { useAppDispatch } from "@/src/redux/hook";
import { addIdToListIdConversationDecoded, setOpenModalGeneratesEncryptedPasswords } from "@/src/redux/slices/endToEndEncryptionSlice";
import { ReactElement, useState } from "react";
import { useForm } from 'react-hook-form';
import bcrypt from 'bcryptjs';
import { AiFillEye, AiFillEyeInvisible } from "react-icons/ai";
import { toast } from "react-toastify";

interface Props {
    idConversation: string;
}

export default function ModalGeneratesEncryptedPasswords({
    idConversation
}: Props): ReactElement {
    const dispatch = useAppDispatch();
    const [showPassword, setShowPassword] = useState(false);
    const [showConfirmPassword, setShowConfirmPassword] = useState(false);
    // const { endToEndEncryption } = useAppSelector((state) => state.user);

    const togglePasswordVisibility = () => {
        setShowPassword(!showPassword);
    };

    const toggleConfirmPasswordVisibility = () => {
        setShowConfirmPassword(!showConfirmPassword);
    };

    const { register, handleSubmit, setError, watch, formState }: any = useForm<FormData | any>();
    const { errors }: any = formState;

    const onClose = () => {
        dispatch(setOpenModalGeneratesEncryptedPasswords(false));
    };


    const onSubmit = (data: any) => {
        if (data.password !== data.confirmPassword) {
            setError('confirmPassword', {
                type: 'manual',
                message: 'Mật khẩu xác nhận không khớp.'
            });
            return;
        }

        // 10 là Số vòng lặp để tạo ra salt
        bcrypt.hash(data.password, 10, (err, hash) => {
            if (err) {
                console.error('Error hashing password:', err);
                return;
            }

            // alert("Tin nhắn đã được mã hóa !");
            toast.success("Tin nhắn đã được mã hóa !");

            dispatch(addIdToListIdConversationDecoded({
                idConversation,
                hashEncryptionPassword: hash,
                encryptionPassword: data.password,
                isDecoded: true
            }));
        });

        onClose();
    };

    return (
        <div className={`fixed top-0 left-0 w-full h-full flex items-center z-50 justify-center`}>
            <div className="absolute w-full h-full opacity-50" onClick={onClose}></div>
            <div className="z-50 dark:bg-black bg-light-thNewtral2 p-6 rounded-md w-[300px] lg:w-[400px]">
                <h2 className="text-2xl font-bold mb-4 w-full text-center">Đặt mật khẩu mã hóa</h2>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="mb-4">
                        <label htmlFor="password" className="block text-sm font-semibold mb-1">
                            Mật khẩu:
                        </label>
                        <div className="relative">
                            <input
                                type={showPassword ? 'text' : 'password'}
                                name="password"
                                id="password"
                                className={`w-full p-2 text-black border rounded-md ${errors.password ? 'border-red-500' : ''
                                }`}
                                placeholder="Nhập mật khẩu"
                                {...register('password', { required: true, minLength: 6 })}
                            />
                            <div
                                className="absolute right-2 top-1/2 transform -translate-y-1/2 cursor-pointer"
                                onClick={togglePasswordVisibility}
                            >
                                {showPassword ? (
                                    <AiFillEye size={20} className="text-thPrimary p-1 rounded-full bg-thNewtral1" />
                                ) : (
                                    <AiFillEyeInvisible size={20} className="text-thPrimary p-1 rounded-full bg-thNewtral1" />
                                )}
                            </div>
                        </div>
                        {errors.password && (
                            <p className="text-red-500 text-sm mt-1 max-w-[300px] lg:max-w-none ">
                                Mật khẩu không được trống và phải có ít nhất 6 ký tự.
                            </p>
                        )}
                    </div>
                    <div className="mb-4">
                        <label htmlFor="confirmPassword" className="block text-sm font-semibold mb-1">
                            Xác nhận mật khẩu:
                        </label>
                        <div className="relative">
                            <input
                                type={showConfirmPassword ? 'text' : 'password'}
                                name="confirmPassword"
                                id="confirmPassword"
                                className={`w-full p-2 text-black border rounded-md ${errors.confirmPassword ? 'border-red-500' : ''
                                }`}
                                placeholder="Xác nhận mật khẩu"
                                {...register('confirmPassword', {
                                    required: true,
                                    validate: (value: any) => value === watch('password') || 'Mật khẩu xác nhận không khớp.'
                                })}
                            />
                            <div
                                className="absolute right-2 top-1/2 transform -translate-y-1/2 cursor-pointer"
                                onClick={toggleConfirmPasswordVisibility}
                            >
                                {showConfirmPassword ? (
                                    <AiFillEye size={20} className="text-thPrimary p-1 rounded-full bg-thNewtral1" />
                                ) : (
                                    <AiFillEyeInvisible size={20} className="text-thPrimary p-1 rounded-full bg-thNewtral1" />
                                )}
                            </div>
                        </div>
                        {errors.confirmPassword && (
                            <p className="text-red-500 text-sm mt-1 max-w-[300px] lg:max-w-none ">
                                {errors.confirmPassword.message}
                            </p>
                        )}
                    </div>
                    <div className="flex justify-between">
                        <button type="button" className="bg-gray-500 text-white font-bold px-4 py-2 rounded-md" onClick={onClose}>
                            Hủy
                        </button>
                        <button type="submit" className="bg-thPrimary text-black font-bold px-4 py-2 rounded-md">
                            Xác nhận
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );

}
