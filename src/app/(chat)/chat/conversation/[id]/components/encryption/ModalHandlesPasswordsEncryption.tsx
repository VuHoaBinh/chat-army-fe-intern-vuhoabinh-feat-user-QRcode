"use client";

import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { findEncryptionConversationByIdConversation, setTypeOfMofalHandlesPasswordsEncryption, updateEncryptionPasswordById, updateIsDecodedOfEncryptionConversationById } from "@/src/redux/slices/endToEndEncryptionSlice";
import { ReactElement, useEffect, useState } from "react";
import { SubmitHandler, useForm } from 'react-hook-form';
import bcrypt from 'bcryptjs';
import { AiFillEye, AiFillEyeInvisible } from "react-icons/ai";
import { IConversation } from "@/src/types/Conversation";
import CryptoJS from "crypto-js";
import { toast } from "react-toastify";

interface Props {
    idConversation: string;
}

interface FormData {
    password: string;
    confirmPassword: string;
    loginPassword?: string;
    oldPassword?: string;
}
interface EncryptionConversationProp {
    idConversation: string | any,
    hashEncryptionPassword: string | any
}

export default function ModalHandlesPasswordsEncryption({
    idConversation
}: Props): ReactElement {
    const dispatch = useAppDispatch();
    const { endToEndEncryption, conversation, user } = useAppSelector((state) => state.user);
    const [showPassword, setShowPassword] = useState(false);
    const [showConfirmPassword, setShowConfirmPassword] = useState(false);
    const [showOldPassword, setShowOldPassword] = useState(false);
    const [showLoginPassword, setShowLoginPassword] = useState(false);
    const [conversationOfThisId, setConversationOfThisId] = useState<IConversation | any>(null);
    const [encrytionConversation, setEncrytionConversation] = useState<EncryptionConversationProp | null>(null);

    useEffect(() => {
        setConversationOfThisId(conversation?.listConversation?.find((c: IConversation | any) => c.id === idConversation));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [idConversation, conversation.listConversation]);

    const toggleLoginPasswordVisibility = () => {
        setShowLoginPassword(!showLoginPassword);
    };
    const toggleOldPasswordVisibility = () => {
        setShowOldPassword(!showOldPassword);
    };
    const togglePasswordVisibility = () => {
        setShowPassword(!showPassword);
    };
    const toggleConfirmPasswordVisibility = () => {
        setShowConfirmPassword(!showConfirmPassword);
    };

    const { register, handleSubmit, setError, watch, formState, reset }: any = useForm<FormData | any>();
    const { errors }: any = formState;

    const onClose = () => {
        dispatch(setTypeOfMofalHandlesPasswordsEncryption("none"));
    };

    const resetAllVisibility = () => {
        setShowPassword(false);
        setShowConfirmPassword(false);
        setShowOldPassword(false);
        setShowLoginPassword(false);
        reset();
    };
    const handleForgotPasswordClick = () => {
        const groupOwner: any = conversationOfThisId.createdByUser;
        if (conversationOfThisId !== null && conversationOfThisId?.type === 2 && groupOwner.id !== user.id) {
            alert("Tạo 1 cuộc gọi tới group owner để lấy lại mật khẩu (Group Owner : " + groupOwner?.username + ") !");
            return;
        }

        dispatch(setTypeOfMofalHandlesPasswordsEncryption('forgotPassword'));
        resetAllVisibility();
    };
    const handleChangePasswordClick = () => {
        const groupOwner: any = conversationOfThisId.createdByUser;
        if (conversationOfThisId !== null && conversationOfThisId?.type === 2 && groupOwner.id !== user.id) {
            alert("Chỉ group owner mới đc thay đổi mật khẩu (Group Owner : " + groupOwner?.username + ") !");
            return;
        }

        dispatch(setTypeOfMofalHandlesPasswordsEncryption('changePassword'));
        resetAllVisibility();
    };
    const handleEnterPasswordClick = () => {
        dispatch(setTypeOfMofalHandlesPasswordsEncryption('enterPassword'));
        resetAllVisibility();
    };

    useEffect(() => {
        setEncrytionConversation(findEncryptionConversationByIdConversation(idConversation, endToEndEncryption.listIdConversationDecoded) || null);
    }, [endToEndEncryption, endToEndEncryption.listIdConversationDecoded, idConversation]);

    // const encryptMessages = (password: string) => {
    //     const encryptedMessages = conversationOfThisId?.latestMessage.map((message: any) => {
    //         const ciphertext = CryptoJS.AES.encrypt(message.content, password).toString();
    //         return {
    //             ...message,
    //             content: ciphertext,
    //             isHashed: true
    //         };
    //     });

    //     const newConversationOfThisId = {
    //         ...conversationOfThisId,
    //         latestMessage: encryptedMessages
    //     };

    //     if(conversation && conversation.listConversation) {
    //         alert("Gui message da ma hoa len sv!");
    //         const newListConversation = conversation?.listConversation.map((l: any) => {
    //             if(l.id === newConversationOfThisId.id) {
    //                 return newConversationOfThisId;
    //             }
    //             return l;
    //         });
    //         dispatch(setListConversations(newListConversation));
    //         ('Encrypted Messages:', newListConversation);
    //     }
    // };

    // const decryptMessages = (password: string) => {
    //     try {
    //         const decryptedMessages = conversationOfThisId?.latestMessage.map((message: any) => {
    //             if(message.isHashed) {
    //                 const decryptedBytes = CryptoJS.AES.decrypt(message.content, password);
    //                 const decryptedText = decryptedBytes.toString(CryptoJS.enc.Utf8);

    //                 return {
    //                     ...message,
    //                     content: decryptedText,
    //                     isHashed: false
    //                 };
    //             }
    //             return message;
    //         });
    //         const newConversationOfThisId = {
    //             ...conversationOfThisId,
    //             latestMessage: decryptedMessages
    //         };

    //         if(conversation && conversation.listConversation) {
    //             const newListConversation = conversation?.listConversation.map((l: any) => {
    //                 if(l.id === newConversationOfThisId.id) {
    //                     return newConversationOfThisId;
    //                 }
    //                 return l;
    //             });
    //             dispatch(setListConversations(newListConversation));
    //             ('Decrypted Messages:', newListConversation);
    //             return true;
    //         }
    //         return false;
    //     } catch(e) {
    //         return false;
    //     }  
    // };

    // (conversationOfThisId);

    const decryptMessage = (password: string, message: string) => {
        try {
            const bytes = CryptoJS.AES.decrypt(message, password);
            const originalText = bytes.toString(CryptoJS.enc.Utf8);
            return originalText ? true : false;
        } catch (e) {
            // (e);
            return false;
        }
    };

    const onSubmit: SubmitHandler<FormData> = async (data: any) => {
        const thisEncryptionConversation = endToEndEncryption.listIdConversationDecoded.find((c: any) => c.idConversation === idConversation);
        if (endToEndEncryption.typeOfMofalHandlesPasswordsEncryption === "enterPassword") {
            if (encrytionConversation) {
                let hashedContent: any = conversationOfThisId?.latestMessage[conversationOfThisId?.latestMessage?.length - 1].content;
                for (let i = conversationOfThisId?.latestMessage?.length - 1; i >= 0; i--) {
                    if (conversationOfThisId?.latestMessage[i].type === 1) {
                        hashedContent = conversationOfThisId?.latestMessage[i].content;
                        break;
                    }
                }
                // const hashedContent = conversationOfThisId?.latestMessage[conversationOfThisId?.latestMessage?.length - 1].content;
                const isDecoded = decryptMessage(data.password, hashedContent);
                const isDecodedByEncryptionPassword: boolean = thisEncryptionConversation?.encryptionPassword !== "" && bcrypt.compareSync(data.password, thisEncryptionConversation?.hashEncryptionPassword);
                if (isDecoded || isDecodedByEncryptionPassword) {
                    const hashedPassword = await bcrypt.hash(data.password, 10);
                    // alert("- Tin nhắn đã được giải mã !");
                    toast.success("Tin nhắn đã được giải mã !");

                    dispatch(updateIsDecodedOfEncryptionConversationById({
                        idConversation,
                        newHashedPassword: hashedPassword,
                        newIsDecoded: true
                    }));
                    dispatch(updateEncryptionPasswordById({
                        idConversation,
                        newPassword: data.password
                    }));

                    onClose();
                } else {
                    alert("- Mật khẩu mã hóa không đúng !");
                    dispatch(updateIsDecodedOfEncryptionConversationById({
                        idConversation,
                        newHashedPassword: "",
                        newIsDecoded: false
                    }));
                }
            };
        } else if (endToEndEncryption.typeOfMofalHandlesPasswordsEncryption === "forgotPassword") {
            if (data.password !== data.confirmPassword) {
                setError('confirmPassword', {
                    type: 'manual',
                    message: 'Mật khẩu xác nhận không khớp.'
                });
                return;
            }

            alert("Tính năng quên mật khẩu đang phát triển !");

            // const userInfo: any = user;
            // (userInfo);
            // (userInfo.password);
            // (data.password);

            // const publicKey = await getLoginPublicKey("phamtandat123");
            // const hashedKey = hashLoginPublicKey(data.password, publicKey);
            // (hashedKey);

            // if(encrytionConversation) {
            //     const isPasswordMatch = bcrypt.compareSync(data.loginPassword, userInfo.password);
            //     if (isPasswordMatch) {
            //         bcrypt.hash(data.password, 10, (err, rehashedPassword) => {
            //             if (err) {
            //                 alert("- Error hashing password: " + JSON.stringify(err));
            //                 return;
            //             }

            //             alert("- Sau khi quên mk => Mật khẩu mã hóa đã được thay đổi từ : " + encrytionConversation.hashEncryptionPassword + " thành : " + data.password + "( Hashed : " + rehashedPassword + ")");
            //             dispatch(updateHashEncryptionPasswordById({
            //                 idConversation,
            //                 newHashPassword: rehashedPassword
            //             }));

            //             onClose();
            //         });
            //     } else {
            //         setError('loginPassword', {
            //             type: 'manual',
            //             message: 'Mật khẩu đăng nhập không đúng.'
            //         });
            //         alert("- Mật khẩu đăng nhập không đúng !");
            //     }
            // };
        } else {
            if (data.password !== data.confirmPassword) {
                setError('confirmPassword', {
                    type: 'manual',
                    message: 'Mật khẩu xác nhận không khớp.'
                });
                return;
            }

            alert("Tính năng đổi mật khẩu đang phát triển !");

            // if(encrytionConversation) {
            //     const result: boolean = bcrypt.compareSync(data.password, thisEncryptionConversation?.hashEncryptionPassword);
            //     if (result) {
            //         // encryptMessages(data.password);
            //         dispatch(updateIsDecodedOfEncryptionConversationById({
            //             idConversation,
            //             newHashedPassword: "",
            //             newIsDecoded: false
            //         }));

            //         dispatch(setTypeOfMofalHandlesPasswordsEncryption("enterPassword"));
            //         resetAllVisibility();
            //     } else {
            //         dispatch(updateIsDecodedOfEncryptionConversationById({
            //             idConversation,
            //             newHashedPassword: "",
            //             newIsDecoded: false
            //         }));
            //         setError('oldPassword', {
            //             type: 'manual',
            //             message: 'Mật khẩu cũ không đúng.'
            //         });
            //         alert("- Mật khẩu mã hóa cũ không đúng !");
            //     }
            // };
        }
    };

    if (endToEndEncryption.typeOfMofalHandlesPasswordsEncryption === "enterPassword") {
        return (
            <div className={`fixed top-0 left-0 w-full h-full flex items-center z-50 justify-center`}>
                <div className="absolute w-full h-full opacity-50" onClick={onClose}></div>
                <div className="z-50 dark:bg-black bg-light-thNewtral2 p-6 rounded-md w-[300px] lg:w-[400px]">
                    <h2 className="text-2xl font-bold mb-4 w-full text-center">Xác nhận mật khẩu mã hóa</h2>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div className="mb-4">
                            <label htmlFor="password" className="block text-sm font-semibold mb-1">
                                Mật khẩu:
                            </label>
                            <div className="relative">
                                <input
                                    type={showPassword ? 'text' : 'password'}
                                    name="password"
                                    id="password"
                                    className={`w-full p-2 text-black border rounded-md ${errors.password ? 'border-red-500' : ''
                                    }`}
                                    placeholder="Nhập mật khẩu"
                                    {...register('password', { required: true, minLength: 6 })}
                                />
                                <div
                                    className="absolute right-2 top-1/2 transform -translate-y-1/2 cursor-pointer"
                                    onClick={togglePasswordVisibility}
                                >
                                    {showPassword ? (
                                        <AiFillEye size={20} className="text-thPrimary p-1 rounded-full bg-thNewtral1" />
                                    ) : (
                                        <AiFillEyeInvisible size={20} className="text-thPrimary p-1 rounded-full bg-thNewtral1" />
                                    )}
                                </div>
                            </div>
                            {errors.password && (
                                <p className="text-red-500 text-sm mt-1 max-w-[300px] lg:max-w-none ">
                                    Mật khẩu không được trống và phải có ít nhất 6 ký tự.
                                </p>
                            )}
                        </div>
                        <div className="flex justify-between">
                            <button type="button" className="bg-gray-500 text-white font-bold px-4 py-2 rounded-md" onClick={onClose}>
                                Hủy
                            </button>
                            <button type="submit" className="dark:bg-thPrimary bg-light-thPrimary text-black font-bold px-4 py-2 rounded-md">
                                Xác nhận
                            </button>
                        </div>
                        <div className="mt-2 flex justify-between lg:mt-3">
                            <button
                                type="button"
                                className="dark:text-thPrimary text-light-thPrimary underline cursor-pointer"
                                onClick={handleForgotPasswordClick}
                            >
                                Quên mật khẩu
                            </button>
                            <button
                                type="button"
                                className="dark:text-thPrimary text-light-thPrimary underline cursor-pointer"
                                onClick={handleChangePasswordClick}
                            >
                                Thay đổi mật khẩu
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        );
    } else if (endToEndEncryption.typeOfMofalHandlesPasswordsEncryption === "changePassword") {
        return (
            <div className={`fixed top-0 left-0 w-full h-full flex items-center z-50 justify-center`}>
                <div className="absolute w-full h-full opacity-50" onClick={onClose}></div>
                <div className="z-50 dark:bg-black bg-light-thNewtral2 p-6 rounded-md w-[300px] lg:w-[400px]">
                    <h2 className="text-2xl font-bold mb-4 w-full text-center">Thay đổi mật khẩu mã hóa</h2>
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div className="mb-4">
                            <label htmlFor="oldPassword" className="block text-sm font-semibold mb-1">
                                Mật khẩu cũ:
                            </label>
                            <div className="relative">
                                <input
                                    type={showOldPassword ? 'text' : 'password'}
                                    name="oldPassword"
                                    id="oldPassword"
                                    className={`w-full p-2 text-black border rounded-md ${errors.oldPassword ? 'border-red-500' : ''
                                    }`}
                                    placeholder="Nhập mật khẩu cũ"
                                    {...register('oldPassword', { required: true })}
                                />
                                <div
                                    className="absolute right-2 top-1/2 transform -translate-y-1/2 cursor-pointer"
                                    onClick={toggleOldPasswordVisibility}
                                >
                                    {showOldPassword ? (
                                        <AiFillEye size={20} className="text-thPrimary p-1 rounded-full bg-thNewtral1" />
                                    ) : (
                                        <AiFillEyeInvisible size={20} className="text-thPrimary p-1 rounded-full bg-thNewtral1" />
                                    )}
                                </div>
                            </div>
                            {errors.oldPassword && (
                                <p className="text-red-500 text-sm mt-1 max-w-[300px] lg:max-w-none ">
                                    Mật khẩu cũ không đúng.
                                </p>
                            )}
                        </div>
                        <div className="mb-4">
                            <label htmlFor="password" className="block text-sm font-semibold mb-1">
                                Mật khẩu:
                            </label>
                            <div className="relative">
                                <input
                                    type={showPassword ? 'text' : 'password'}
                                    name="password"
                                    id="password"
                                    className={`w-full p-2 text-black border rounded-md ${errors.password ? 'border-red-500' : ''
                                    }`}
                                    placeholder="Nhập mật khẩu"
                                    {...register('password', { required: true, minLength: 6 })}
                                />
                                <div
                                    className="absolute right-2 top-1/2 transform -translate-y-1/2 cursor-pointer"
                                    onClick={togglePasswordVisibility}
                                >
                                    {showPassword ? (
                                        <AiFillEye size={20} className="text-thPrimary p-1 rounded-full bg-thNewtral1" />
                                    ) : (
                                        <AiFillEyeInvisible size={20} className="text-thPrimary p-1 rounded-full bg-thNewtral1" />
                                    )}
                                </div>
                            </div>
                            {errors.password && (
                                <p className="text-red-500 text-sm mt-1 max-w-[300px] lg:max-w-none ">
                                    Mật khẩu không được trống và phải có ít nhất 6 ký tự.
                                </p>
                            )}
                        </div>
                        <div className="mb-4">
                            <label htmlFor="confirmPassword" className="block text-sm font-semibold mb-1">
                                Xác nhận mật khẩu:
                            </label>
                            <div className="relative">
                                <input
                                    type={showConfirmPassword ? 'text' : 'password'}
                                    name="confirmPassword"
                                    id="confirmPassword"
                                    className={`w-full p-2 text-black border rounded-md ${errors.confirmPassword ? 'border-red-500' : ''
                                    }`}
                                    placeholder="Xác nhận mật khẩu"
                                    {...register('confirmPassword', {
                                        required: true,
                                        validate: (value: any) => value === watch('password') || 'Mật khẩu xác nhận không khớp.'
                                    })}
                                />
                                <div
                                    className="absolute right-2 top-1/2 transform -translate-y-1/2 cursor-pointer"
                                    onClick={toggleConfirmPasswordVisibility}
                                >
                                    {showConfirmPassword ? (
                                        <AiFillEye size={20} className="text-thPrimary p-1 rounded-full bg-thNewtral1" />
                                    ) : (
                                        <AiFillEyeInvisible size={20} className="text-thPrimary p-1 rounded-full bg-thNewtral1" />
                                    )}
                                </div>
                            </div>
                            {errors.confirmPassword && (
                                <p className="text-red-500 text-sm mt-1 max-w-[300px] lg:max-w-none ">
                                    {errors.confirmPassword.message}
                                </p>
                            )}
                        </div>
                        <div className="flex justify-between">
                            <button type="button" className="bg-gray-500 text-white font-bold px-4 py-2 rounded-md" onClick={onClose}>
                                Hủy
                            </button>
                            <button type="submit" className="bg-thPrimary text-black font-bold px-4 py-2 rounded-md">
                                Xác nhận
                            </button>
                        </div>
                        <div className="mt-2 flex justify-between lg:mt-3">
                            <button
                                type="button"
                                className="text-thPrimary underline cursor-pointer"
                                onClick={handleForgotPasswordClick}
                            >
                                Quên mật khẩu
                            </button>
                            <button
                                type="button"
                                className="text-thPrimary underline cursor-pointer"
                                onClick={handleEnterPasswordClick}
                            >
                                Xác nhận mật khẩu mã hóa
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
    return (
        <div className={`fixed top-0 left-0 w-full h-full flex items-center z-50 justify-center`}>
            <div className="absolute w-full h-full opacity-50" onClick={onClose}></div>
            <div className="z-50 dark:bg-black bg-light-thNewtral2 p-6 rounded-md w-[300px] lg:w-[400px]">
                <h2 className="text-2xl font-bold mb-4 w-full text-center">Quên mật khẩu mã hóa</h2>
                <form onSubmit={handleSubmit(onSubmit)}>
                    <div className="mb-4">
                        <label htmlFor="loginPassword" className="block text-sm font-semibold mb-1">
                            Mật khẩu đăng nhập:
                        </label>
                        <div className="relative">
                            <input
                                type={showLoginPassword ? 'text' : 'password'}
                                name="loginPassword"
                                id="loginPassword"
                                className={`w-full p-2 text-black border rounded-md ${errors.loginPassword ? 'border-red-500' : ''
                                }`}
                                placeholder="Nhập mật khẩu đăng nhập"
                                {...register('loginPassword', { required: true, minLength: 6 })}
                            />
                            <div
                                className="absolute right-2 top-1/2 transform -translate-y-1/2 cursor-pointer"
                                onClick={toggleLoginPasswordVisibility}
                            >
                                {showLoginPassword ? (
                                    <AiFillEye size={20} className="text-thPrimary p-1 rounded-full bg-thNewtral1" />
                                ) : (
                                    <AiFillEyeInvisible size={20} className="text-thPrimary p-1 rounded-full bg-thNewtral1" />
                                )}
                            </div>
                        </div>
                        {errors.loginPassword && (
                            <p className="text-red-500 text-sm mt-1 max-w-[300px] lg:max-w-none ">
                                Mật khẩu đăng nhập không đúng.
                            </p>
                        )}
                    </div>
                    <div className="mb-4">
                        <label htmlFor="password" className="block text-sm font-semibold mb-1">
                            Mật khẩu:
                        </label>
                        <div className="relative">
                            <input
                                type={showPassword ? 'text' : 'password'}
                                name="password"
                                id="password"
                                className={`w-full p-2 text-black border rounded-md ${errors.password ? 'border-red-500' : ''
                                }`}
                                placeholder="Nhập mật khẩu"
                                {...register('password', { required: true, minLength: 6 })}
                            />
                            <div
                                className="absolute right-2 top-1/2 transform -translate-y-1/2 cursor-pointer"
                                onClick={togglePasswordVisibility}
                            >
                                {showPassword ? (
                                    <AiFillEye size={20} className="text-thPrimary p-1 rounded-full bg-thNewtral1" />
                                ) : (
                                    <AiFillEyeInvisible size={20} className="text-thPrimary p-1 rounded-full bg-thNewtral1" />
                                )}
                            </div>
                        </div>
                        {errors.password && (
                            <p className="text-red-500 text-sm mt-1 max-w-[300px] lg:max-w-none ">
                                Mật khẩu không được trống và phải có ít nhất 6 ký tự.
                            </p>
                        )}
                    </div>
                    <div className="mb-4">
                        <label htmlFor="confirmPassword" className="block text-sm font-semibold mb-1">
                            Xác nhận mật khẩu:
                        </label>
                        <div className="relative">
                            <input
                                type={showConfirmPassword ? 'text' : 'password'}
                                name="confirmPassword"
                                id="confirmPassword"
                                className={`w-full p-2 text-black border rounded-md ${errors.confirmPassword ? 'border-red-500' : ''
                                }`}
                                placeholder="Xác nhận mật khẩu"
                                {...register('confirmPassword', {
                                    required: true,
                                    validate: (value: any) => value === watch('password') || 'Mật khẩu xác nhận không khớp.'
                                })}
                            />
                            <div
                                className="absolute right-2 top-1/2 transform -translate-y-1/2 cursor-pointer"
                                onClick={toggleConfirmPasswordVisibility}
                            >
                                {showConfirmPassword ? (
                                    <AiFillEye size={20} className="text-thPrimary p-1 rounded-full bg-thNewtral1" />
                                ) : (
                                    <AiFillEyeInvisible size={20} className="text-thPrimary p-1 rounded-full bg-thNewtral1" />
                                )}
                            </div>
                        </div>
                        {errors.confirmPassword && (
                            <p className="text-red-500 text-sm mt-1 max-w-[300px] lg:max-w-none ">
                                {errors.confirmPassword.message}
                            </p>
                        )}
                    </div>
                    <div className="flex justify-between">
                        <button type="button" className="bg-gray-500 text-white font-bold px-4 py-2 rounded-md" onClick={onClose}>
                            Hủy
                        </button>
                        <button type="submit" className="bg-thPrimary text-black font-bold px-4 py-2 rounded-md">
                            Xác nhận
                        </button>
                    </div>
                    <div className="mt-2 flex justify-between lg:mt-3">
                        <button
                            type="button"
                            className="text-thPrimary underline cursor-pointer"
                            onClick={handleChangePasswordClick}
                        >
                            Thay đổi mật khẩu
                        </button>
                        <button
                            type="button"
                            className="text-thPrimary underline cursor-pointer"
                            onClick={handleEnterPasswordClick}
                        >
                            Xác nhận mật khẩu mã hóa
                        </button>
                    </div>
                </form>
            </div>
        </div>
    );
    ;
}
