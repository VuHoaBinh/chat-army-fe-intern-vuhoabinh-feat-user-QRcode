"use client";

import { ReactElement } from "react";

interface Props {
    idConversation: string,
    setIsOpenModalExplainDeleteConversation: React.Dispatch<React.SetStateAction<boolean>>
}

export default function PopupExplainDeleteConversation({
    idConversation,
    setIsOpenModalExplainDeleteConversation
}: Props): ReactElement {
    const onClose = () => {
        setIsOpenModalExplainDeleteConversation(false);
    };
    const onAccept = () => {
        alert("Xóa cuộc trò chuyện : " + idConversation);
        onClose();
    };

    return (
        <div className={`fixed top-0 left-0 w-full h-full flex items-center z-50 justify-center`}>
            <div className="absolute w-full h-full opacity-50" onClick={onClose}></div>
            <div className="z-50 dark:bg-black bg-light-thNewtral2 p-6 rounded-md w-[300px] lg:w-[400px] flex flex-col">
                <h2 className="text-2xl font-bold mb-4 w-full text-center">Xóa cuộc trò chuyện</h2>
                <p>Bạn có chắc muốn xóa cuộc trò chuyện này ?</p>

                <div className="flex justify-between mt-3">
                    <button type="button" className="bg-gray-500 text-white font-bold px-4 py-2 rounded-md cursor-pointer" onClick={onClose}>
                        Hủy
                    </button>
                    <button type="submit" className="bg-thPrimary text-black font-bold px-4 py-2 rounded-md cursor-pointer" onClick={onAccept}>
                        Xác nhận
                    </button>
                </div>
            </div>
        </div>
    );

}
