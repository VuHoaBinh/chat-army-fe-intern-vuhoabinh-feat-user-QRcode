
import { useAppDispatch } from "@/src/redux/hook";
import { setOtherUser } from "@/src/redux/slices/conversationSlice";
import Image from "next/image";
import Link from "next/link";
import { ReactElement } from "react";
import { FiChevronLeft } from "react-icons/fi";
import { HiOutlineUserPlus } from "react-icons/hi2";
interface ICommonProps {
    setIsShow: React.Dispatch<React.SetStateAction<boolean>>,
    setIsOpenForm: React.Dispatch<React.SetStateAction<string>>,
    data: any
}
const CommonGroup = ({ setIsShow, setIsOpenForm, data }: ICommonProps): ReactElement => {
    const dispatch = useAppDispatch();
    return (
        <div className={`block h-full mt-4 relative`}>
            <div className="sticky top-0 z-50 dark:bg-thNewtral bg-light-thNewtral ">
                <div className="flex gap-x-4 items-center">
                    <div className="cursor-pointer" onClick={() => setIsShow(false)}>
                        <FiChevronLeft size={40} />
                    </div>
                    <p className="text-lg font-semibold">Nhóm chung</p>
                </div>
                <div className="flex bottom-2 justify-center w-full items-center px-2 mt-2">
                    <button className="dark:bg-thNewtral2 bg-light-thNewtral2 w-full rounded-md px-2 py-2 gap-2 flex justify-center  items-center" onClick={() => setIsOpenForm("ADD_TO_GROUP")}>
                        <div className="dark:bg-thNewtral1 hover:bg-thNewtral2 bg-light-thNewtral2 w-fit rounded-full px-2 py-2 cursor-pointer">
                            <HiOutlineUserPlus size={24} />
                        </div>
                        <span className="opacity-60 text-center text-xs" >
                            Thêm vào nhóm
                        </span>
                    </button>
                </div>
            </div>
            <div className="mt-2 overflow-y-auto pb-4">
                {
                    data?.map((item: any) => {
                        return (
                            item?.type === 2 &&
                            <Link key={item._id} href={`/chat/conversation/${item?._id}`} className="flex items-center justify-start gap-3 item dark:hover:bg-gray-500 hover:bg-light-thNewtral1 px-2 py-1"
                                onClick={() =>
                                    dispatch(
                                        setOtherUser({
                                            otherUsername: item?.name,
                                            otherAvatar: item?.avatar,
                                            lastMessage: item.lastMessage || []
                                        })
                                    )
                                }
                            >
                                <div
                                    className="rounded-full w-[50px] h-[50px] border-2 cursor-pointer overflow-hidden"
                                >
                                    <Image
                                        src={item?.avatar || "/default_avatar.jpg"}
                                        alt="avatar"
                                        loading="eager"
                                        width={50}
                                        height={50}
                                        className=" w-full h-full object-cover"
                                    />
                                </div>
                                <span className="truncate text-base font-bold">{item?.name}</span>
                            </Link>
                        );
                    })
                }
            </div>
        </div>);
};
export default CommonGroup;