import { useAppDispatch } from "@/src/redux/hook";
import {  IVote, setStartAnswersVote } from "@/src/redux/slices/newsletterSlice";
import moment from "moment";
import { ReactElement } from "react";
import { AiFillCheckCircle } from "react-icons/ai";
import { FaChartSimple } from "react-icons/fa6";

export default function VoteActivity({ item, idUser, action }: {item: IVote|any, idUser: string, action: string}): ReactElement {
    const dispatch = useAppDispatch();
    const renderVote = (item: any)=>{
        const isVoted = item.conversationVoteOptions?.some((option: any) => option.conversationVoteAnswers.some((answer: any) => answer?.user.id === idUser)) || false;
        const totalMembersVoted = 
            (item.hideResultBeforeAnswers && !isVoted) 
                ? new Set(item?.conversationVoteOptions?.map((option: any) => option.conversationVoteAnswers.map((answer: any) => answer?.user.id)).flat()).size > 1 ? 1 : 0 :
                new Set(item?.conversationVoteOptions?.map((option: any) => option.conversationVoteAnswers.map((answer: any) => answer?.user.id)).flat()).size || 1;
        return (
            <div className="rounded-md w-[80%] md:w-1/2 lg:w-1/2 border border-gray-500 p-2 mt-2" key={item.id}>
                <h2 className="font-['Roboto'] font-bold py-2 text-lg truncate max-w-[180px]">{item.question}</h2>
                {
                    item?.allowMultipleAnswers &&
                                            <p className="text-sm text-thNewtral2 py-2">Chọn nhiều phương án</p>
                }
                {
                    item?.status === 2 &&
                                            <p className="text-sm text-thNewtral2 pb-2">Kết thúc {moment(item?.updatedAt).fromNow()}</p>
                }
                <div className="flex gap-2 flex-col ">
                    {
                        item.conversationVoteOptions?.map((option: any)=>{
                            const totalVotesForOption = (item.hideResultBeforeAnswers && !isVoted) 
                                ? option.conversationVoteAnswers.some((answer: any) => answer?.user.id === idUser) ? 1 : 0 : option?.conversationVoteAnswers?.length;

                            const optionPercentage = totalMembersVoted !== 0
                                ? (totalVotesForOption / totalMembersVoted) * 100
                                : 0;
                            return (
                                <div className="cursor-pointer flex items-center gap-2" key={option.id} onClick={()=>{dispatch(setStartAnswersVote(item as any));}}>
                                    <div className="cursor-pointer flex justify-between items-center p-2 border border-thPrimary rounded-md relative flex-1">
                                        <div className="absolute inset-0 bg-thPrimary bg-opacity-60 transition-all  rounded-md" style={{ width: `${optionPercentage}%` }}></div>
                                        <span>{option?.option}</span>
                                        {
                                            (option.conversationVoteAnswers.find((item: any)=>item.user.id === idUser) ) ? 
                                                <AiFillCheckCircle
                                                    size={16}
                                                    className="cursor-pointer"
                                                /> :
                                                <div className={` h-3 w-3 border-[1.5px] border-thNewtral2 bg-transparent rounded-full cursor-pointer`}></div>
                                        }
                                    </div>
                                    <span className="text-thWhite">{totalVotesForOption }</span>
                                </div>
                            );
                        })
                    }
                </div>
                <button type="button" className="w-full mt-2 rounded-md py-2 border border-thPrimary text-thWhite hover:bg-thPrimaryHover opacity-90 hover:opacity-100 font-bold" 
                    onClick={()=>{dispatch(setStartAnswersVote(item as any));}}
                >
                    Bình chọn
                </button>
            </div>
        );
    };
    const actionText = item?.user?.id === idUser
        ? "Bạn"
        : item?.user?.username;

    const voteAction = new Date(item?.updatedAt).getTime() <= new Date(item?.createdAt).getTime()
        ? "lựa chọn"
        : "đổi lựa chọn";

    const message = `${actionText} đã ${voteAction} trong cuộc bình chọn`;

    return (
        <div className="flex w-full justify-center items-center flex-col mb-2 relative">
            <div className="flex justify-center items-center gap-2 max-[480px]:w-3/4">
                {
                    action === "CREATE" &&
                        <p className="text-center">
                            <span className="text-zinc-300 flex items-center justify-center gap-2 truncate "><FaChartSimple size={16}/>{
                                item?.createdBy?.id === idUser
                                    ? "Bạn"
                                    : item?.createdBy?.username
                            } tạo cuộc bình chọn mới {" "}
                            <button className="font-bold text-blue-600"
                                onClick={()=>{
                                    dispatch(setStartAnswersVote(item));
                                }}
                            >Xem</button>
                            </span> 
                        </p>
                }
                {
                    action === "UPDATE" &&
                    <div className=" flex justify-center items-center gap-1">
                        <FaChartSimple size={16} className="self-start"/>
                        <p className="text-zinc-300 ">
                            {message}{" "}
                            <span className="truncate font-bold inline-flex max-w-[100px]">{item?.voteItem?.question}</span>
                            {" "}
                            <button className="font-bold text-blue-600"
                                onClick={()=>{
                                    dispatch(setStartAnswersVote(item?.voteItem));
                                }}
                            >Xem</button>
                        </p> 
                    </div>
                }
            </div>
            {
                action === "CREATE" &&
                new Date(item?.createdAt).getTime() <= new Date(item?.updatedAt).getTime() &&
                renderVote(item)
            }
        </div>
    );
}