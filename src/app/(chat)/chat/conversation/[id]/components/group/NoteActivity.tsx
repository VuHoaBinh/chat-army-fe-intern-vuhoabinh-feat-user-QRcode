import { useAppDispatch } from "@/src/redux/hook";
import { INote, setStartEditNote } from "@/src/redux/slices/newsletterSlice";
import { ReactElement } from "react";
import { MdOutlineStickyNote2 } from "react-icons/md";

export default function NoteActivity({ item, idUser }: {item: INote, idUser: string}): ReactElement {
    const dispatch = useAppDispatch();
    return (
        <div className="flex w-full justify-center items-center mb-2 relative">
            <div className="flex justify-center items-center gap-2 break-words max-[480px]:w-3/4">
                <span className="text-zinc-300  flex items-center justify-center gap-2 ">
                    <MdOutlineStickyNote2 size={16}/>
                    {
                        item?.createdBy?.id === idUser
                            ? "Bạn"
                            : item?.createdBy?.username
                    } đã tạo một ghi chú mới</span> 
                <button className="font-bold text-blue-600"
                    onClick={()=>{
                        dispatch(setStartEditNote({ note: item, typeAction:"VIEW" }));
                    }}
                >Xem</button>
            </div>
        </div>
    );
}