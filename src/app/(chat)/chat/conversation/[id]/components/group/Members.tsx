'use client';
import { CONVERSATION } from "@/src/constants/chat";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { setDetailChat } from "@/src/redux/slices/detailChatSlice";
import getAxiosClient from "@/src/utils/axios/axiosClient";
import { Button, Dropdown, MenuProps } from "antd";
import Image from "next/image";
import { ReactElement, useEffect, useState } from "react";
import { FiChevronLeft, FiMoreHorizontal } from "react-icons/fi";
import { HiOutlineUserPlus } from "react-icons/hi2";
import { IoKeyOutline } from "react-icons/io5";
import { toast } from "react-toastify";
interface IMembersProps {
    close: () => void;
    fetchDetailChat: (id: string) => any,
    setIsShowModalAddMembers: React.Dispatch<React.SetStateAction<boolean>>,
    setIsShowModalConfirm: React.Dispatch<React.SetStateAction<boolean>>
}
const Members = ({ setIsShowModalAddMembers, fetchDetailChat, close, setIsShowModalConfirm }: IMembersProps): ReactElement => {
    const { user: currentUser, detailChat } = useAppSelector((state) => state.user);
    const dispatch = useAppDispatch();
    const localStorageData = useAppSelector((state: any) => state.user);
    const [dataStorage, setDataStorage] = useState<any>([{}] as any);
    useEffect(() => {
        const dataString = localStorage.getItem("Othername");

        if (dataString) {
            setDataStorage(JSON.parse(dataString));
        }
    }, [localStorageData]);
    const handleKickUser = async (id: string) => {
        try {
            const response = await getAxiosClient().delete(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/group/owner/${detailChat?.id}/kick`,
                {
                    data: {
                        ids: [id]
                    }
                }
            );
            if (response.status === 200) {
                toast.success("Kick user thành công");
                const { data: newDetailChat } = await fetchDetailChat(detailChat?.id);
                dispatch(setDetailChat(newDetailChat));
            }
        } catch (error: any) {
            toast.error(error.response.data.message);
        }
    };
    const handleAcceptAndDeny = async (id: string, action: number) => {
        try {
            if (detailChat?.members.find((item: any) => item.id === currentUser.id)?.type === CONVERSATION.ROLE_TYPE.OWNER) {
                const response = await getAxiosClient().put(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/group/decide/owner/${detailChat?.id}/${id}/${action}`, {});
                if (response.status === 200) {
                    const { data: newDetailChat } = await fetchDetailChat(detailChat?.id);
                    dispatch(setDetailChat(newDetailChat));
                    toast.success("Thành công");
                }
            }
            if (detailChat?.members.find((item: any) => item.id === currentUser.id)?.type === CONVERSATION.ROLE_TYPE.MEMBER_WAITING_ACCEPT) {
                const response = await getAxiosClient().put(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/group/decide/member/${detailChat?.id}/${action}`, {});
                if (response.status === 200) {
                    const { data: newDetailChat } = await fetchDetailChat(detailChat?.id);
                    dispatch(setDetailChat(newDetailChat));
                    toast.success("Thành công");
                }
            }
        } catch (err: any) {
            toast.error(err.response.data.message);
            console.log(err);
        }
    };
    const handleAddAdmin = async (user: string) => {
        const url = `/conversations/group/grant/${detailChat.id}`;
        try {
            const res = await getAxiosClient().put(
                `${process.env.NEXT_PUBLIC_DAK_CHAT_API}${url}`,
                { userId: user, role: CONVERSATION.ROLE_TYPE.ADMIN }
            );
            if (res.status === 200) {
                const { data: newDetailChat } = await fetchDetailChat(detailChat?.id);
                dispatch(setDetailChat(newDetailChat));
                toast.success("Thêm phó nhóm thành công");
            }
        } catch (err) {
            console.error(err);
            toast.error("Thêm phó nhóm thất bại");
            // Xử lý lỗi nếu cần thiết
        }
    };
    return (
        <div className={`block h-full mt-4 relative`}>
            <div className="flex gap-x-4 items-center">
                <div className="cursor-pointer" onClick={() => close()}>
                    <FiChevronLeft size={40} />
                </div>
                <p className="text-lg font-semibold">Thành viên nhóm</p>
            </div>
            <div className=" flex justify-center w-full items-center px-4 mt-2">
                <button className="bg-thNewtral2 w-full rounded-md px-2 py-2 flex justify-center gap-x-3 items-center" onClick={() => setIsShowModalAddMembers(true)}>
                    <div className="bg-thNewtral1 hover:bg-thNewtral2 w-fit rounded-full px-2 py-2 cursor-pointer">
                        <HiOutlineUserPlus size={20} />
                    </div>
                    <span className="opacity-60 text-center text-md " >
                        Thêm thành viên
                    </span>
                </button>
            </div>
            <div className="mt-2 overflow-y-auto pb-4">
                {
                    [...detailChat?.members]?.sort((a: any, b: any) => a.ownerAccepted - b.ownerAccepted)?.map((user: any) => {
                        return (
                            <div key={user.id} className="flex items-center justify-between hover:bg-gray-500">
                                <div className="flex items-center justify-start gap-3 item  px-2 py-1">
                                    <div className="relative">
                                        <div
                                            className="rounded-full w-[50px] h-[50px] border-2 cursor-pointer overflow-hidden"
                                        >
                                            <Image
                                                src={user?.avatar === "http://avatar.com" ? "/default_avatar.jpg" : user?.avatar}
                                                alt="avatar"
                                                loading="eager"
                                                width={50}
                                                height={50}
                                                className=" w-full h-full object-cover"
                                            />
                                        </div>
                                        {
                                            user?.type === CONVERSATION.ROLE_TYPE.OWNER &&
                                            <span className="absolute bottom-0 right-0 w-4 h-4 rounded-full z-10 bg-slate-600">
                                                <IoKeyOutline size={16} color="yellow" />
                                            </span>
                                        }
                                    </div>
                                    <div className="flex flex-col">
                                        <div className="flex items-center gap-2">
                                            <span className="truncate text-base font-bold">{dataStorage[user.id] ? dataStorage[user.id] : user?.username}</span>
                                            <span className="text-sm">{user?.id === currentUser.id && "(me)"} </span>
                                            {/* {user.type === CONVERSATION.ROLE_TYPE.MEMBER_WAITING_ACCEPT && <span className="text-sm opacity-60 bg-red-600 rounded-md px-1">request</span>} */}
                                        </div>
                                        {
                                            detailChat?.members.find((item: any) => item.id === currentUser.id)?.type === CONVERSATION.ROLE_TYPE.OWNER &&
                                            user.ownerAccepted === false &&
                                            <div className="flex items-center">
                                                <div className="flex gap-x-2">
                                                    <button className="bg-thPrimary text-thDark-background rounded-md font-bold px-2 opacity-60 hover:opacity-90"
                                                        onClick={() => handleAcceptAndDeny(user.id, 1)}
                                                    >
                                                        Đồng ý
                                                    </button>
                                                    <button className="bg-thNewtral2 text-thWhite rounded-md font-bold px-2 opacity-60 hover:opacity-90"
                                                        onClick={() => handleAcceptAndDeny(user.id, 2)}
                                                    >Từ chối</button>
                                                </div>
                                            </div>
                                        }
                                    </div>
                                </div>
                                {
                                    detailChat?.members.find((item: any) => item.id === currentUser.id)?.type === CONVERSATION.ROLE_TYPE.OWNER &&
                                    user?.id !== currentUser.id &&
                                    (
                                        <Dropdown trigger={["click"]} placement="bottomRight" className="mr-2"
                                            menu={{
                                                items: [
                                                    {
                                                        key: user?.id + '1',
                                                        label: (
                                                            "Thêm phó nhóm"
                                                        ),
                                                        onClick: () => {
                                                            handleAddAdmin(user?.id);
                                                        }
                                                    },
                                                    {
                                                        key: user?.id + '2',
                                                        label: (
                                                            "Xóa khỏi nhóm"
                                                        ),
                                                        onClick: () => {
                                                            handleKickUser(user?.id);
                                                        }
                                                    }
                                                ] as MenuProps['items']
                                            }}
                                        >
                                            <Button><FiMoreHorizontal size={24} color="white" /></Button>
                                        </Dropdown>
                                    )
                                }
                                {
                                    user?.id === currentUser.id &&
                                    <Dropdown trigger={["click"]} placement="bottomRight" className="mr-2"
                                        menu={{
                                            items: [
                                                {
                                                    key: user?.id,
                                                    label: (
                                                        "Rời nhóm"
                                                    ),
                                                    onClick: () => {
                                                        if (user?.type === CONVERSATION.ROLE_TYPE.MEMBER_WAITING_ACCEPT) {
                                                            return toast.warning("Cần join nhóm trước khi rời nhóm");
                                                        }
                                                        setIsShowModalConfirm(true);
                                                    }
                                                }
                                            ] as MenuProps['items']
                                        }}
                                    >
                                        <Button><FiMoreHorizontal size={24} color="white" /></Button>
                                    </Dropdown>
                                }
                            </div>
                        );
                    })
                }
            </div>
        </div>
    );
};
export default Members;