import Image from "next/image";
import { ReactElement, cache, useEffect, useRef, useState } from "react";
import { AiFillCheckCircle, AiOutlineCloseCircle } from "react-icons/ai";
import { BsCameraFill } from "react-icons/bs";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { Avatar } from "../../../../components/Avatar";
import { CONVERSATION } from "@/src/constants/chat";
import { setListConversations } from "@/src/redux/slices/conversationSlice";
import { toast } from "react-toastify";
import { uploadFile } from "@/src/utils/upload/getUploadToken";
import getAxiosClient from "@/src/utils/axios/axiosClient";
import { IConversation } from "@/src/types/Conversation";
import { filterConversation } from "@/src/utils/conversations/filterConversation";
import useWindowDimensions from "@/src/hook/useWindowDimension";
import { setDetailChat } from "@/src/redux/slices/detailChatSlice";
interface IFormCreateGroup {
    setIsOpen: React.Dispatch<React.SetStateAction<string>>;
    idConversation: string;
    action: string;
    detailChat: any;
    //   setDetailChat: React.Dispatch<any>;
    setCommonGroup: React.Dispatch<any>;
    commonGroup: any;
    fetchDetailChat: (id: string) => any
}
const ACTION = {
    ADDTOGROUP: "ADD_TO_GROUP",
    CREATEGROUP: "CREATE_GROUP",
    ADD_MEMBERS: "ADD_MEMBERS"
};
const fetchConversations = cache((offset: string, type: string) =>
    getAxiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations?offset=${offset}&status=[1,2]&limit=10&type=${type}`
    )
);

const FormCreateGroup = ({
    setIsOpen,
    idConversation,
    action,
    detailChat,
    // setDetailChat,
    setCommonGroup,
    commonGroup,
    fetchDetailChat
}: IFormCreateGroup): ReactElement => {
    const { conversation, user } = useAppSelector((state) => state.user);
    const [groups, setGroups] = useState<string[]>([]);
    const [avatar, setAvatar] = useState<File>();
    const [name, setName] = useState<string>("");
    const divRef = useRef<HTMLDivElement>(null);
    const [page, setPage] = useState<number>(2);
    const [hasMoreData, setHasMoreData] = useState<boolean>(true);
    const dispatch = useAppDispatch();
    const [listConversation, setListConversation] = useState<
        IConversation[] | any
    >([]);
    const localStorageData = useAppSelector((state: any) => state.user);
    const [dataStorage, setDataStorage] = useState<any>([{}] as any);
    useEffect(() => {
        const dataString = localStorage.getItem("Othername");

        if (dataString) {
            setDataStorage(JSON.parse(dataString));
        }
    }, [localStorageData]);
    useEffect(() => {
        (async () => {
            try {
                const { data }: any = await fetchConversations(
                    String(0),
                    String(
                        action === ACTION.ADDTOGROUP
                            ? CONVERSATION.TYPE.GROUP
                            : CONVERSATION.TYPE.INDIVIDUAL
                    )
                );
                setListConversation(filterConversation(data, user.id));
            } catch (err) {
                console.log(err);
            }
        })();
    }, [action, user.id]);
    const [users, setUsers] = useState<string[]>(() => {
        return action === ACTION.ADDTOGROUP || action === ACTION.ADD_MEMBERS
            ? []
            : [
                (
                    conversation.listConversation?.filter(
                        (item: any) => item._id === idConversation
                    ) as any
                )?.[0]?.idOther
            ];
    });
    const { width } = useWindowDimensions();
    const [keySearch, setKeySearch] = useState<string>("");
    const handleChoose = (item: any) => {
        // Kiểm tra xem item đã được chọn trước đó chưa
        if (action === ACTION.CREATEGROUP || action === ACTION.ADD_MEMBERS) {
            const isSelected = users?.some((userId) => userId === item?.idOther);
            if (isSelected) {
                // Nếu đã chọn rồi, thì loại bỏ item khỏi danh sách
                setUsers((prevUsers) =>
                    prevUsers?.filter((userId) => userId !== item?.idOther)
                );
            } else {
                // Nếu chưa chọn, thêm item vào danh sách
                setUsers((prevUsers) => [...(prevUsers || []), item?.idOther]);
            }
        } else {
            const isSelected = groups?.some((groupId) => groupId === item?.id);
            if (isSelected) {
                // Nếu đã chọn rồi, thì loại bỏ item khỏi danh sách
                setGroups((prevGroups) =>
                    prevGroups?.filter((groupId) => groupId !== item?.id)
                );
            } else {
                // Nếu chưa chọn, thêm item vào danh sách
                setGroups((prevGroups) => [...(prevGroups || []), item?.id]);
            }
        }
    };
    const handleCreateGroup = async () => {
        try {
            if (users.length === 0) {
                toast.error("Vui lòng chọn thành viên");
                return;
            }
            if (name === "") {
                toast.error("Vui lòng nhập tên nhóm");
                return;
            }
            if (!avatar) {
                toast.error("Vui lòng chọn ảnh nhóm");
                return;
            }
            let linkAvatar = "";
            if (avatar) {
                const formData = new FormData();
                formData.append("files", avatar);
                const data = await uploadFile(formData);
                linkAvatar = data[0].link;
            }
            toast.promise(
                async () => {
                    const result = await getAxiosClient().post(
                        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations`,
                        {
                            type: CONVERSATION.TYPE.GROUP,
                            memberIds: users,
                            name: name,
                            avatar: linkAvatar
                        }
                    );
                    if (result) {
                        const data = result.data;
                        if (data && data?.id) {
                            dispatch(
                                setListConversations([
                                    data,
                                    ...(conversation.listConversation || [])
                                ])
                            );
                            setIsOpen("");
                        }
                    }
                },
                {
                    pending: "Đang tạo nhóm",
                    success: "Tạo nhóm thành công",
                    error: "Tạo nhóm thất bại"
                }
            );
        } catch (error) {
            const errorMessage =
                error instanceof Error ? error.message : String(error);
            toast.error(errorMessage);
        }
    };
    const handleAddMembers = async () => {
        if (users.length === 0) {
            return toast.warning("Vui lòng chọn thành viên");
        }
        try {
            const response = await getAxiosClient().post(
                `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${idConversation}/add`,
                {
                    ids: [...users]
                }
            );
            if (response.status === 200) {
                toast.success("Thêm thành công");
                //         const newMenbers =
                //   conversation?.listConversation
                //       ?.filter((data: any) =>
                //           users?.some((userId) => userId === data?.idOther)
                //       )
                //       .map((item: any) => {
                //           return {
                //               id: item?.idOther,
                //               type: 3,
                //               avatar: item?.avatar,
                //               username: item?.name
                //           };
                //       }) || [];
                //         setDetailChat({
                //             ...detailChat,
                //             members: [...detailChat?.members, ...newMenbers]
                //         });
                const { data: newDetailChat } = await fetchDetailChat(detailChat?.id);
                dispatch(setDetailChat(newDetailChat));
                setIsOpen("");
            } else {
                toast.error(response.message);
            }
        } catch (err: any) {
            toast.error(err?.response.data.message);
        }
    };
    const addUserToGroup = () => {
        if (groups.length === 0) {
            return toast.warning("Vui lòng chọn nhóm");
        }
        const addMembersToGroups = async () => {
            let hasError = false;
            for (const group of groups) {
                try {
                    const idOrder = (
                        conversation.listConversation?.find(
                            (item: any) => item.id === idConversation
                        ) as any
                    )?.idOther;
                    const response = await getAxiosClient().post(
                        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${group}/add`,
                        {
                            ids: [idOrder]
                        }
                    );
                    if (response.status === 200) {
                        setCommonGroup((prev: any) => [
                            ...prev,
                            listConversation?.find((item: any) => item._id === group)
                        ]);
                        toast.success("Thêm thành công");
                        setGroups([
                            ...(groups.filter((item: any) => item !== group) || [])
                        ]);
                    } else {
                        hasError = true;
                    }
                } catch (err: any) {
                    toast.error(err?.response.data.message);
                    hasError = true;
                }
            }
            if (!hasError) {
                setIsOpen("");
            }
        };
        addMembersToGroups();
    };
    const handleSubmit = () => {
        switch (action) {
        case ACTION.CREATEGROUP:
            handleCreateGroup();
            break;
        case ACTION.ADDTOGROUP:
            addUserToGroup();
            break;
        case ACTION.ADD_MEMBERS:
            handleAddMembers();
            break;
        default:
            toast.error("Vui lòng chọn thao tác");
            break;
        }
    };
    useEffect(() => {
        const divElement = divRef.current as HTMLDivElement;
        const handleScroll = async () => {
            if (
                Math.abs(
                    divElement.scrollTop +
                    divElement.clientHeight -
                    divElement.scrollHeight
                ) < 1
            ) {
                if (hasMoreData) {
                    const { data } = await fetchConversations(
                        String((page - 1) * 10),
                        "1,2"
                    );
                    if (data?.length > 0) {
                        setListConversation([
                            ...listConversation,
                            ...filterConversation(data, user.id)
                        ]);
                        setPage((prev) => prev + 1);
                    } else {
                        setHasMoreData(false);
                    }
                }
            }
        };
        divElement?.addEventListener("scroll", handleScroll);
        return () => {
            divElement?.removeEventListener("scroll", handleScroll);
        };
    }, [hasMoreData, listConversation, page, user.id]);
    return (
        <div className="fixed top-0 left-0 right-0 z-50 w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] max-h-full flex items-center justify-center">
            <div className="relative w-full max-w-2xl max-h-full">
                <div className="relative  rounded-lg shadow dark:bg-gray-700 bg-light-thNewtral2">
                    <div className="flex items-start justify-between p-3 border-b rounded-t border-gray-600">
                        <h3 className="text-xl font-semibold text-white">
                            {action === ACTION.CREATEGROUP && "Tạo nhóm"}
                            {action === ACTION.ADDTOGROUP && "Mời tham gia nhóm"}
                            {action === ACTION.ADD_MEMBERS && "Thêm thành viên"}
                        </h3>
                        <button
                            type="button"
                            className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ml-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white"
                            data-modal-hide="defaultModal"
                            onClick={() => setIsOpen("")}
                        >
                            <svg
                                className="w-3 h-3"
                                aria-hidden="true"
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 14 14"
                            >
                                <path
                                    stroke="currentColor"
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    strokeWidth={2}
                                    d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"
                                />
                            </svg>
                            <span className="sr-only">Close modal</span>
                        </button>
                    </div>
                    {/* Modal body */}
                    <div className="px-6 py-2  flex flex-col">
                        {action === ACTION.CREATEGROUP && (
                            <div className="flex gap-x-6">
                                {avatar ? (
                                    <div className="relative mb-2">
                                        <Image
                                            className="w-[60px] h-[54px] rounded-full border-solid border-2"
                                            width={60}
                                            height={54}
                                            src={URL.createObjectURL(avatar)}
                                            alt=""
                                        />
                                        <span className="top-0 right-0 absolute  w-3.5 h-3.5 rounded-full cursor-pointer">
                                            <AiOutlineCloseCircle
                                                onClick={() => setAvatar(undefined)}
                                                size={18}
                                                color="red"
                                            />
                                        </span>
                                    </div>
                                ) : (
                                    <label
                                        htmlFor="avatar"
                                        className="block mb-2 text-sm font-medium text-gray-300 border-solid border-2 p-2 rounded-full"
                                    >
                                        <BsCameraFill size={35} />
                                    </label>
                                )}
                                <input
                                    type="file"
                                    name="file"
                                    className="hidden"
                                    id="avatar"
                                    onChange={(e: any) => setAvatar(e.target.files[0])}
                                />
                                <input
                                    type="text"
                                    name="name"
                                    className="outline-none border-none border-b-blue-500 border-b-2 bg-transparent w-full"
                                    placeholder="Nhập tên nhóm..."
                                    autoComplete="off"
                                    value={name}
                                    onChange={(e) => setName(e.target.value)}
                                />
                            </div>
                        )}
                        <div className="flex items-center justify-center">
                            <label
                                htmlFor="default-search"
                                className="mb-2 text-sm font-medium sr-only text-white"
                            >
                                Search
                            </label>
                            <div className="relative w-full">
                                <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                    <svg
                                        className="w-4 h-4 text-gray-400"
                                        aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="none"
                                        viewBox="0 0 20 20"
                                    >
                                        <path
                                            stroke="currentColor"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            strokeWidth={2}
                                            d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
                                        />
                                    </svg>
                                </div>
                                <input
                                    type="search"
                                    id="default-search"
                                    className="block w-full p-2 pl-10 text-sm border border-gray-300 rounded-full bg-transparent text-white focus:ring-blue-500 focus:border-blue-500 "
                                    placeholder="Nhập tên..."
                                    value={keySearch}
                                    onChange={(e) => setKeySearch(e.target.value)}
                                />
                            </div>
                        </div>
                        <div className="flex w-full border-t border-gray-200 pt-2 mt-2">
                            <div
                                className={`${(groups.length > 0 || users?.length > 0) && width > 480
                                    ? "w-4/6"
                                    : "w-full"
                                } h-[47vh] overflow-y-auto overflow-x-hidden mr-2`}
                                ref={divRef}
                            >
                                <h3>Trò chuyện gần đây</h3>
                                {(action === ACTION.CREATEGROUP ||
                                    action === ACTION.ADD_MEMBERS) &&
                                    listConversation
                                        .filter((item: any) =>
                                            item?.name
                                                ?.toLowerCase()
                                                .includes(keySearch.toLowerCase())
                                        )
                                        .sort((a: any, b: any) => a?.name?.localeCompare(b?.name))
                                        ?.map((conversation: any) => {
                                            const checkUserInGroup: boolean =
                                                detailChat?.members.some(
                                                    (user: any) => user?.id === conversation?.idOther
                                                ) && action === ACTION.ADD_MEMBERS;
                                            return (
                                                <div
                                                    key={conversation._id}
                                                    className={`relative z-0 flex items-center justify-start gap-2 item ${checkUserInGroup
                                                        ? "cursor-not-allowed bg-slate-400 opacity-70"
                                                        : "cursor-pointer"
                                                    } mt-2 hover:bg-gray-500`}
                                                    onClick={() => {
                                                        if (!checkUserInGroup) {
                                                            handleChoose(conversation);
                                                        }
                                                    }}
                                                >
                                                    {users?.some(
                                                        (userId) => userId === conversation?.idOther
                                                    ) || checkUserInGroup ? (
                                                            <AiFillCheckCircle
                                                                size={16}
                                                                className="cursor-pointer"
                                                            />
                                                        ) : (
                                                            <div className="h-3 w-3 border-[1.5px] border-thNewtral2 bg-transparent rounded-full cursor-pointer"></div>
                                                        )}
                                                    <div className="relative rounded-full w-10 h-10 object-cover border-2 border-thPrimary cursor-pointer overflow-hidden">
                                                        <Avatar
                                                            url={
                                                                conversation?.avatar === "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg"
                                                                    ? "/default_avatar.jpg"
                                                                    : conversation?.avatar
                                                            }
                                                        />
                                                    </div>
                                                    <span>{dataStorage[conversation?.idOther] ? dataStorage[conversation?.idOther] : conversation?.name}</span>
                                                    <span>{checkUserInGroup && "(Đã tham gia)"}</span>
                                                </div>
                                            );
                                        })}
                                {action === ACTION.ADDTOGROUP &&
                                    listConversation
                                        .filter((item: any) =>
                                            item?.name
                                                ?.toLowerCase()
                                                .includes(keySearch.toLowerCase())
                                        )
                                        ?.map((conversation: any) => {
                                            const checkCommonGroup: boolean = commonGroup?.some(
                                                (group: any) => group?._id === conversation?._id
                                            );
                                            return (
                                                <div
                                                    key={conversation._id}
                                                    className={`flex items-center justify-start gap-2 item  mt-2 dark:hover:bg-gray-500 hover:bg-light-thNewtral1 ${checkCommonGroup
                                                        ? "bg-slate-400 opacity-70 cursor-not-allowed"
                                                        : "cursor-pointer"
                                                    }`}
                                                    onClick={() => {
                                                        if (!checkCommonGroup) {
                                                            handleChoose(conversation);
                                                        }
                                                    }}
                                                >
                                                    {(groups.length > 0 &&
                                                        groups?.some(
                                                            (groupId) => groupId === conversation?.id
                                                        )) ||
                                                        checkCommonGroup ? (
                                                            <AiFillCheckCircle
                                                                size={16}
                                                                className="cursor-pointer"
                                                            />
                                                        ) : (
                                                            <div className="h-3 w-3 border-[1.5px] border-thNewtral2 bg-transparent rounded-full cursor-pointer"></div>
                                                        )}
                                                    <div className="relative rounded-full w-10 h-10 object-cover border-2 border-thPrimary cursor-pointer overflow-hidden">
                                                        <Avatar
                                                            url={
                                                                conversation?.avatar === "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg"
                                                                    ? "/default_avatar.jpg"
                                                                    : conversation?.avatar
                                                            }
                                                        />
                                                    </div>
                                                    <span>{conversation?.name}</span>
                                                    <span className="text-sm text-white">
                                                        {checkCommonGroup && "(Đã tham gia)"}
                                                    </span>
                                                </div>
                                            );
                                        })}
                            </div>
                            {width > 480 && users.length > 0 && (
                                <div className="w-2/6 flex flex-col gap-2 h-[47vh] border-solid border-2 dark:border-thPrimary border-light-thPrimary p-3 rounded-md overflow-y-auto overflow-x-hidden">
                                    <h3>Đã chọn {users.length}</h3>
                                    {listConversation.map((conversation: any) => {
                                        return (
                                            users &&
                                            users?.some(
                                                (userId) => userId === conversation?.idOther
                                            ) && (
                                                <div
                                                    key={conversation._id}
                                                    className="flex items-center justify-between gap-3 item bg-blue-400 text-blue-700 rounded-full px-2 py-1"
                                                >
                                                    <div className="rounded-full w-[30px] h-[30px] border-2 cursor-pointer overflow-hidden">
                                                        <Image
                                                            src={
                                                                conversation?.avatar === "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg"
                                                                    ? "/default_avatar.jpg"
                                                                    : conversation?.avatar
                                                            }
                                                            alt="avatar"
                                                            loading="eager"
                                                            width={30}
                                                            height={30}
                                                            className=" w-full h-full object-cover"
                                                        />
                                                    </div>
                                                    <span className="truncate text-sm font-bold">
                                                        {conversation.name}
                                                    </span>
                                                    <AiOutlineCloseCircle
                                                        size={16}
                                                        className="cursor-pointer"
                                                        onClick={() => handleChoose(conversation)}
                                                        width={16}
                                                        height={16}
                                                    />
                                                </div>
                                            )
                                        );
                                    })}
                                </div>
                            )}
                            {width > 480 && groups.length > 0 && (
                                <div className="w-2/6 flex flex-col gap-2 h-[47vh] border-solid border-2 border-thPrimary p-3 rounded-md overflow-y-auto overflow-x-hidden">
                                    <h3>Đã chọn {groups.length}</h3>
                                    {listConversation.map((conversation: any) => {
                                        return (
                                            groups?.some((group) => group === conversation._id) && (
                                                <div
                                                    key={conversation._id}
                                                    className="flex items-center justify-between gap-3 item bg-blue-400 text-blue-700 rounded-full px-2 py-1"
                                                >
                                                    <div className="rounded-full w-[30px] h-[30px] border-2 cursor-pointer overflow-hidden">
                                                        <Image
                                                            src={
                                                                conversation?.avatar === "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg"
                                                                    ? "/default_avatar.jpg"
                                                                    : conversation?.avatar
                                                            }
                                                            alt="avatar"
                                                            loading="eager"
                                                            width={30}
                                                            height={30}
                                                            className=" w-full h-full object-cover"
                                                        />
                                                    </div>
                                                    <span className="truncate text-sm font-bold">
                                                        {conversation.name}
                                                    </span>
                                                    <AiOutlineCloseCircle
                                                        size={16}
                                                        className="cursor-pointer"
                                                        onClick={() => handleChoose(conversation)}
                                                        width={16}
                                                        height={16}
                                                    />
                                                </div>
                                            )
                                        );
                                    })}
                                </div>
                            )}
                        </div>
                    </div>
                    {/* Modal footer */}
                    <div className="flex items-center justify-end p-3 space-x-2 border-t border-gray-200 rounded-b dark:border-gray-600">
                        <button
                            className="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600"
                            onClick={() => {
                                setIsOpen("");
                            }}
                        >
                            Hủy
                        </button>
                        <button
                            className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                            onClick={handleSubmit}
                        >
                            {action === ACTION.CREATEGROUP
                                ? "Tạo nhóm"
                                : action === ACTION.ADD_MEMBERS
                                    ? "Xác nhận"
                                    : "Mời"}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default FormCreateGroup;
