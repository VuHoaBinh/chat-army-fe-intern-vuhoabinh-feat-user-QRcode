import Image from "next/image";
import { Dispatch, ReactElement, SetStateAction } from "react";
import { useCookies } from "react-cookie";
import { PiPhone, PiPhoneSlash } from "react-icons/pi";
import { IUserProfile } from "@/src/types/User";
function PopupCallInComing({ callInComing, setCallInComing, handCallAccept, handleCallDeny }: {callInComing: any, setCallInComing: Dispatch<SetStateAction<any>>, userLogin: IUserProfile, handCallAccept: any, handleCallDeny: any}): ReactElement {
    // const { push } = useRouter();
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [cookies, setCookie] = useCookies(['data']);

    return (
        <div className="absolute top-16 right-2 bg-lime-600 z-50 rounded-md p-2 text-white-1 container-popup-call">
            <div className="w-[300px] h-fit  bg-lime-600	 flex rounded-md items-center gap-3 container-popup-call">
                <Image className="rounded-full" src={callInComing.avatar} alt="avatar-call" height={60} width={60}/>
                <span className="flex flex-col w-full">
                    <span className="w-full truncate text-xl font-bold">{callInComing.username}</span>
                    <span className="w-full truncate text-sm">{callInComing.type == 1 ? "Cuộc gọi thoại" : "Cuộc gọi video"}</span>
                </span>
                <div className="flex items-center gap-2">
                    <div className="rounded-full bg-red-600 p-2 cursor-pointer" onClick={() => {
                        handleCallDeny();
                        setCallInComing(undefined);
                    }}>
                        <PiPhoneSlash className="w-[25px] h-[25px]"/>
                    </div>
                    <div className="rounded-full bg-green-800 p-2 cursor-pointer" onClick={() => {
                        // setCookie('data', JSON.stringify({
                        //     conversationId: callInComing.conversationId, 
                        //     username: userLogin.name,
                        //     type: callInComing.type
                        // }), { sameSite: 'strict', secure: true, maxAge: 30 * 24 * 60 * 60 });
                        handCallAccept() ;
                        setCallInComing(undefined);
                    }}>
                        <PiPhone className="w-[25px] h-[25px]"/>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default PopupCallInComing;