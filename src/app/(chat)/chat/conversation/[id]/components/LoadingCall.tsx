import { Avatar, CircularProgress } from '@mui/material';
import React, { ReactElement } from 'react';

// interface LoadingCallProps {
//   message?: string;
// }

export default function LoadingCall(): ReactElement {
    return (
        <div className="absolute inset-0 lg:right-4  rounded-lg flex items-center justify-center z-50 bg-thDark-background lg:bg-thNewtral1">
            <div className="bg-thNewtral1 lg:bg-thNewtral2 border border-[#fcd535] p-4 rounded-lg w-[80%] h-[50%] lg:h-4/5 bg-opacity-50 shadow-md flex justify-center items-center flex-col relative">
                <div className='w-full flex justify-center items-center relative'>
                    <Avatar src="/logoDAK.jpg" sx={{ width:"25%", height:"auto", '@media (max-width: 480px)': {
                        width: '40%'  // Set the width to 100% for screens with a width of 600px or less (adjust this breakpoint as needed)
                    } }}  className='border-[#fcd535] border-2'/>
                    <div className="absolute inset-0 flex items-center justify-center">
                        <CircularProgress color="success" variant="indeterminate"  />
                    </div>
                </div>
                <div className="text-white font-bold flex flex-col flex-wrap items-center justify-center gap-x-2 mt-2">
                    <span>Xin vui lòng chờ trong giây lát !! </span>
                    <span>Cuộc gọi đang được mã hoá...</span>
                </div>
            </div>
        </div>
    );
}
