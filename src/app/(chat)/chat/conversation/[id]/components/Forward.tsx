"use client";
import Image from "next/image";
import { ReactElement, SyntheticEvent, cache, use, useState } from 'react';
// import Pattern from "@/public/pattern.png";
// import Pattern2 from "@/public/pattern2.jpg";
// import Pattern3 from "@/public/pattern3.jpg";
import { useAppDispatch, useAppSelector } from '@/src/redux/hook';
import { IConversation } from '@/src/types/Conversation';
import { IForwardMessageData, IMessage } from '@/src/types/Message';
import { AiFillCheckCircle } from "react-icons/ai";
import { Socket } from 'socket.io-client';
// import { sendMessage } from '@/src/utils/messages/sendMessage';
import { EVENT_NAMES } from '@/src/constants/chat';
import { setLastMessage, setMessageForward } from '@/src/redux/slices/conversationSlice';
import getAxiosClient from '@/src/utils/axios/axiosClient';
import { filterConversation, sortConversationByNewest } from '@/src/utils/conversations/filterConversation';
import { toast } from "react-toastify";
import userAvatar from "@/public/user.png";

interface Content {
    message: IMessage,
    socket: Socket<any, any>,
    close: () => void
}
const fetchConversations = cache((offset: number) =>
    getAxiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations?offset=${offset}&status=[2]&limit=20`
    )
);
export default function Forward({ message, socket, close }: Content): ReactElement {
    const [isChecked, setIsChecked] = useState<string[]>([]);
    const { user } = useAppSelector(state => state.user);
    const { data }: any = use(fetchConversations(0));
    const [listConversation, setListConversation] = useState<IConversation[] | any>(() => {
        return sortConversationByNewest(filterConversation(data, user.id));
    });
    const dispatch = useAppDispatch();
    const [inputSearch, setInputSearch] = useState<string>("");
    const searchListConversation = listConversation?.filter((conversation: IConversation | any) => {
        return conversation?.name?.toLowerCase().includes(inputSearch.toLowerCase());
    });
    const handleForward = () => {

        if (isChecked.length === 0) {
            return;
        }
        for (const id of isChecked) {
            const forwardMessage: IForwardMessageData = {
                content: message.content,
                type: message.type,
                forward_conversation_id: id
            };
 
            socket.emit(EVENT_NAMES.CONVERSATION.FORWARD_MESSAGE, forwardMessage, (data: any) => {
                if (data && data.success) {
                    dispatch(setMessageForward(data.data));
                    if (id === isChecked[isChecked.length - 1]) {
                        dispatch(setLastMessage(data.data));
                    }
                } else {
                    toast.error("Không thể chia sẻ tin nhắn");
                }
            });
        }
        toast.success("Chia sẻ thành công");
        close();
    };
    const [page, setPage] = useState(2);
    const [hasMorePage, setHasMorePage] = useState(true);
    const handleAddConversation = (e: any) => {
        const target = e.target as HTMLDivElement;
        if (Math.abs(target.scrollTop + target.clientHeight - target.scrollHeight) < 1 && hasMorePage && listConversation.length > 19) {
            (async () => {
                try {
                    const { data } = await fetchConversations((page - 1) * 20);
                    if (data.length === 0) {
                        setHasMorePage(false);
                    } else {
                        setListConversation(sortConversationByNewest([...listConversation || [], ...filterConversation(data, user.id)]));
                        setPage(page + 1);
                    }
                } catch (error) {
                    console.error("Error fetching conversations:", error);
                    setHasMorePage(false);
                }
            })();
        }
    };
    const handleImageError = (event: SyntheticEvent<HTMLImageElement, Event>) => {

        const imgElement = event.target as HTMLImageElement;
        imgElement.src = userAvatar.src;
    };
    return (
        <>
            <div className='bg-thNewtral1 rounded-xl px-4'>
                <p className='py-1.5 text-center text-lg'>Chia sẻ</p>
                <div className='w-full dark:bg-thNewtral bg-light-thNewtral rounded'>
                    <input type="text" placeholder='tìm kiếm bạn bè' className='px-2 py-2 bg-transparent outline-none ' value={inputSearch} onChange={(e) => setInputSearch(e.target.value)} />
                </div>
                <div className='list_user py-4 max-h-[600px] overflow-y-auto h-[50vh]'
                    onScroll={handleAddConversation}
                >
                    {
                        searchListConversation ? (searchListConversation.map((conversation: IConversation | any) => (
                            <div key={conversation._id} className='py-2 pr-2 flex items-center gap-x-2 md:gap-x-10 justify-between'>
                                <div className='flex gap-x-3 items-center flex-1'>
                                    <Image
                                        src={conversation?.avatar === "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg" ? "/default_avatar.jpg" : conversation?.avatar}
                                        sizes="(max-width: 44px) 44px, 44px"
                                        alt={"avt"}
                                        onError={handleImageError}
                                        loading="eager"
                                        priority
                                        width={44}
                                        height={44}
                                        className="rounded-full h-full aspect-square object-cover cursor-pointer"
                                    />
                                    <div className='flex-1 flex flex-col min-w-0'>
                                        <p className='text-sm font-normal truncate  '>{conversation?.name}</p>
                                        <span className='text-xs opacity-60 truncate'>{conversation?.name}</span>
                                    </div>
                                </div>
                                {
                                    isChecked.includes(conversation?.id)
                                        ? (<AiFillCheckCircle size={24} onClick={() => setIsChecked(isChecked.filter(id => id !== conversation?.id))} className='cursor-pointer' />)
                                        : (<div className='h-6 w-6 border-[1.5px] border-thNewtral2 bg-transparent rounded-full cursor-pointer shrink-0 ' onClick={() => setIsChecked([...isChecked, conversation?.id])}>
                                        </div>)
                                }
                            </div>
                        ))) : (
                            // Xử lý khi conversation hoặc conversation.listConversation là undefined
                            <p>Loading or handle error here</p>)
                    }
                </div>
                <div className='py-3'>
                    <button className=' dark:bg-thPrimary bg-light-thPrimary text-thDark-background w-full py-1 rounded-md' onClick={handleForward}>Gửi</button>
                </div>
                {/* {
                    isShowPopUp && <PopUp isShow={isShowPopUp} setIsShow={setIsShowPopUp} isUploading={false} isError={error ? "Đã có lỗi xảy ra" : ""} message={!error ? "Chia sẽ thành công" : 'Đã có lỗi xảy ra'}/>
                } */}
            </div>
        </>
    );
}