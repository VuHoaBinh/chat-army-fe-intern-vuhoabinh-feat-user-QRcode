import { ReactElement } from "react";
import SearchBox from "./SearchBox";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { setIdConversation, toggleOpenSearch } from "@/src/redux/slices/searchSlice";
import { IMessage } from "@/src/types/Message";
import { useRouter } from "next/navigation";
interface Props {
    setMessageScroll: React.Dispatch<any>,
    closeInfoChat: () => void
}
export default function SearchBoxMbl({
    setMessageScroll,
    closeInfoChat
}: Props): ReactElement {
    const dispatch = useAppDispatch();
    const { search, detailChat } = useAppSelector((state) => state.user);
    const handleClickCloseSearchBox = () => {
        if (search.isOpenSearch) {
            dispatch(toggleOpenSearch());
            dispatch(setIdConversation(""));
        }
    };
    const router = useRouter();
    const handelClickSearchItem = (item: IMessage) => {
        if(item.conversationId === detailChat?.id){
            closeInfoChat();
            dispatch(toggleOpenSearch());
            dispatch(setIdConversation(item.conversationId || ""));
            setMessageScroll(item);
        }else{
            router.push(`/chat/conversation/${item.conversationId}`);
            closeInfoChat();
            dispatch(toggleOpenSearch());
            dispatch(setIdConversation(item.conversationId || ""));
            setMessageScroll(item);
        }
    };
    return <div
        id="default-modal"
        tabIndex={-1}
        className=" overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0  justify-center items-center w-full md:inset-0 h-[calc(100%-1rem)] max-h-full z-40"
    >
        <div className="fixed inset-0 w-full h-screen opacity-40" onClick={() => handleClickCloseSearchBox()}>
        </div>
        <div className="relative z-50 w-full max-w-md max-h-full  max-[480px]:w-[90%] p-0 m-auto text-thWhite bg-thDark-background border border-thPrimary rounded-md mt-5">
            <SearchBox handelClickSearchItem={handelClickSearchItem}/>
        </div>
    </div>;
}