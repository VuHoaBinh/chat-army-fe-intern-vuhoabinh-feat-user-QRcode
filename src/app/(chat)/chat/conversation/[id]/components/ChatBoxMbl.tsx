/* eslint-disable react-hooks/exhaustive-deps */
"use client";
// import info from "@/public/info-circle.svg";
import Send from "@/public/send-2.svg";
import Slute from "@/public/slute.png";
import { CONVERSATION, EVENT_NAMES, MESSAGE_TYPE } from "@/src/constants/chat";
import useCreateSocket from "@/src/hook/useCreateSocket";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import {
    setLastMessage,
    setListConversations,
    setMessageForward,
    setMessageReaction,
    setTotalUnseenMessage
} from "@/src/redux/slices/conversationSlice";
import { setSearchItem } from "@/src/redux/slices/searchSlice";
import { setUserIsCalling, setWaitCalling } from "@/src/redux/slices/userSlice";
import { getCookie } from "@/src/services/cookie";
import { IConversation } from "@/src/types/Conversation";
import { IMessage, IMessageData } from "@/src/types/Message";
import { ISeenUser, IUserProfile } from "@/src/types/User";
import { getLoginCookies } from "@/src/utils/auth/handleCookies";
import axiosClient from "@/src/utils/axios/axiosClient";
import { filterConversation, sortConversationByNewest } from "@/src/utils/conversations/filterConversation";
import { decompressMessage } from "@/src/utils/messages/decompressMessage";
import {
    getFileIcon,
    getFileName,
    isUrl
} from "@/src/utils/messages/handleUrl";
import { sendMessage } from "@/src/utils/messages/sendMessage";
import { sortByTime } from "@/src/utils/messages/sortMessage";
import { uploadFile, validateFiles } from "@/src/utils/upload/getUploadToken";
import CryptoJS from "crypto-js";
import { Theme } from "emoji-picker-react";
import moment from "moment";
import dynamic from "next/dynamic";
import Image from "next/image";
import { useRouter } from "next/navigation";
import ModalInforGroupChat from "src/app/(chat)/chat/components/Modal/ModalnforGroupChat";

import { STATUS_USER } from "@/src/constants/user";
import { updateCalling } from "@/src/redux/slices/callSlice";
import { setDetailChat } from "@/src/redux/slices/detailChatSlice";
import { EncryptionConversationProp, addIdToListIdConversationDecoded, findEncryptionConversationByIdConversation, setTypeOfMofalHandlesPasswordsEncryption } from "@/src/redux/slices/endToEndEncryptionSlice";
import { IVote, setEndEditNote, setNotes, setPinMessages, setVotes } from "@/src/redux/slices/newsletterSlice";
import { IActivityVote, IMessageMerge, INoteMerge, IVoteMerge } from "@/src/types/NewsLetter";
import getAxiosClient from "@/src/utils/axios/axiosClient";
import { sortNotes, sortVotes } from "@/src/utils/newsletter/sortHepper";
import React, {
    ChangeEvent,
    ReactElement,
    SyntheticEvent,
    cache,
    // use,
    useCallback,
    useEffect,
    useLayoutEffect,
    useRef,
    useState
} from "react";
import { useCookies } from "react-cookie";
import { AiFillExclamationCircle, AiOutlineSmile } from "react-icons/ai";
import {
    BsChevronDoubleDown,
    BsFileEarmarkText,
    BsImage,
    BsLink45Deg,
    BsReplyAllFill
} from "react-icons/bs";
import { FaCheckCircle, FaLock, FaRegCheckCircle, FaVideo } from "react-icons/fa";
import { FiChevronLeft } from "react-icons/fi";
import { IoIosCloseCircleOutline, IoMdClose } from "react-icons/io";
import { IoCall } from "react-icons/io5";
import { MdLock } from "react-icons/md";
import { Id, toast } from "react-toastify";
import { Virtuoso } from "react-virtuoso";
import { Socket, io } from "socket.io-client";
import useSound from "use-sound";
import ModalInforConversation from "../../../components/Modal/ModalInforConversation";
import styles from "../conversation.module.css";
import Forward from "./Forward";
import ImageUploadModal from "./ImageUploadModal";
import InfoChatMbl from "./InfoChatMbl";
import ModalAnswersVote from "./Modal/ModalAnswersVote";
import ModalMediaLibraryMbl from "./Modal/ModalMediaLibraryMbl";
import ModalNote from "./Modal/ModalNote";
import ModalAddMemberToCall from "./ModalAddMemberToCall";
import ModalTurnOffNotifications from "./ModalTurnOffNotifications";
import OwnerText from "./OwnerText";
import PartnerText from "./PartnerText";
import PinnedComponent from "./PinnedComponent";
import ModalGeneratesEncryptedPasswords from "./encryption/ModalGeneratesEncryptedPasswords";
import ModalHandlesPasswordsEncryption from "./encryption/ModalHandlesPasswordsEncryption";
import PopupExplainTheDecryptionProcess from "./encryption/PopupExplainTheDecryptionProcess";
import NoteActivity from "./group/NoteActivity";
import VoteActivity from "./group/VoteActiviti";
import userAvatar from "@/public/user.png";
const Picker = dynamic(
    () => {
        return import("emoji-picker-react");
    },
    { ssr: false }
);
interface ChatBoxProps {
    listMessage: IMessage[];
    idConversation: string;
}
const fetchDetailChat = cache((id: string) =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${id}`
    )
);
const fetchCommonGroup = cache((userId: string) =>
    getAxiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${userId}/same-group?sort=asc&limit=100`
    )
);
const fetchPinMessages = cache((id: string) =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${id}/message-pin?limit=10&offset=0&sort=desc`
    )
);
const fetchNotes = cache((id: string) =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/note/${id}?limit=20&offset=0`
    )
);
const fetchVotes = cache((id: string) =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/vote/${id}?limit=20&offset=0`
    )
);
export default function ChatBoxMbl({
    listMessage,
    idConversation
}: ChatBoxProps): ReactElement {
    const { user, conversation, search, call, endToEndEncryption, notify, statusSeen } = useAppSelector((state) => state.user);
    const { newsletter } = useAppSelector((state) => state.user);
    const { detailChat } = useAppSelector((state) => state.user);

    const dispatch = useAppDispatch();
    const [statusUser, setStatusUser] = useState<boolean>(false);
    const [lastLogin, setLastLogin] = useState<string>("");
    const [page, setPage] = useState<number>(2);
    const [commonGroup, setCommonGroup] = useState<any>([]);
    const [messages, setMessages] = useState<IMessage[]>([]);
    // const [detailChat, setDetailChat] = useState<any>({});
    const router = useRouter();
    const [isBottom, setIsBottom] = useState<boolean>(false);
    const [isFocused, setIsFocused] = useState<boolean>(false);
    const [isOpenEmoji, setIsOpenEmoji] = useState<boolean>(false);
    const [isOpenInfo, setIsOpenInfo] = useState<boolean>(false);
    const [forwardContent, setForwardContent] = useState<IMessage | any>({});
    const divVirtualListRef = useRef<HTMLDivElement>(null);
    const inputRef = useRef<HTMLInputElement>(null);
    const socket: Socket<any, any> = useCreateSocket(idConversation);
    const [reactionID, setReactionID] = useState<string>("");
    const [isSubmitting, setIsSubmitting] = useState<boolean>(false);
    const [messageReply, setMessageReply] = useState<IMessage | any>({});
    const [messageScroll, setMessageScroll] = useState<IMessage | any>({});
    const virtuoso = useRef(null);
    const [heightVirtualList, setHeightVirtualList] = useState<number>(0);
    const [isScroll, setIsScroll] = useState<boolean>(false);
    const [isUploading, setIsUploading] = useState<boolean>(false);
    const [selectedImage, setSelectedImage] = useState<File[]>([]);
    const [fileError, setFileError] = useState<File[]>([]);
    const [selectedFile, setSelectedFile] = useState<File[]>([]);
    const [isShowButtonDown, setIsShowButtonDown] = useState<boolean>(false);
    const [isScrollMessageReply, setIsScrollMessageReply] =
        useState<boolean>(false);
    const [nextMessage, setNextMessage] = useState<IMessage>();
    const [prevMessage, setPrevMessage] = useState<IMessage>();
    const [hasMoreNextMessage, setHasMoreNextMessage] = useState<boolean>(true);
    const [hasMorePrevMessage, setHasMorePrevMessage] = useState<boolean>(true);
    const [hasMorePage, setHasMorePage] = useState<boolean>(true);
    const [isOpenInforConversation, setIsOpenInforConversation] = useState<boolean>(false);
    const [userLogin, setUserLogin] = useState<IUserProfile>();
    const { push } = useRouter();
    const [socketCall, setSocketCall] = useState<Socket<any, any> | any>(null);
    const cookieData = getLoginCookies();
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [cookies, setCookie] = useCookies(["data"]);
    const [play] = useSound("../../watting_respond.mp3", { interrupt: true });
    const [isRinging, setIsRinging] = useState(false);
    const [isOpenInfoGroupChat, setIsOpenInfoGroupChat] =
        useState<boolean>(false);
    const [isOpenModalAddMemberToCall, setIsOpenModalAddMemberToCall] =
        useState<boolean>(false);
    const [listMember, setListMember] = useState([]);
    const [conversationOfThisMessage, setConversationOfThisMessage] = useState<
        IConversation | any
    >(null);
    const [sendMediaHd, setSendMediaHd] = useState<boolean>(false);
    // type của cuộc gọi : 1 là voice call, 2 là video call
    const [typeOfCallGroup, setTypeOfCallGroup] = useState<number>(0);
    const [isOpenMediaLibrary, setIsOpenMediaLibrary] = useState<boolean>(false);
    const [idMediaModal, setIdMediaModal] = useState<string>("");
    const [urlMediaModal, setUrlMediaModal] = useState<string>("");
    const [dataStorage, setDataStorage] = useState<any>([]);
    const localStorageData = useAppSelector((state: any) => state.user);
    useEffect(() => {
        const dataString = localStorage.getItem("Othername");
        if (dataString) {
            setDataStorage(JSON.parse(dataString));
        }
    }, [localStorageData]); const [isShowNewsletter, setIsShowNewsletter] = useState<boolean>(false);
    // useEffect(() => {
    //     (async () => {
    //         const { data } = await (fetchDetailChat(idConversation)) as any;
    //         dispatch(setDetailChat(data));
    //     })();
    // }, [idConversation]);
    useEffect(() => {
        (async () => {
            try {
                const { data } = await (fetchPinMessages(idConversation)) as any;
                dispatch(setPinMessages(data));
            } catch (err) {
                console.log(err);
            }
        })();
        return () => {
            dispatch(setPinMessages([]));
        };
    }, [idConversation]);
    useEffect(() => {
        (async () => {
            try {
                const response = await fetchNotes(idConversation);
                if (response.status === 200) {
                    dispatch(setNotes(sortNotes(response.data)));
                }
                const response2 = await fetchVotes(idConversation);
                if (response2.status === 200) {
                    dispatch(setVotes(sortVotes(response2.data)));
                }
            } catch (error) {
                console.log(error);
            }
        })();
        return () => {
            dispatch(setNotes([]));
            dispatch(setVotes([]));
        };
    }, [idConversation, dispatch]);
    useEffect(() => {
        if (conversation.listConversation) {
            const thisConversation =
                conversation?.listConversation.find(
                    (c: any) => c?._id === idConversation
                ) ||
                search?.arrayValueToSearch.find((c: any) => c?.id === idConversation)
                    ?.data;
            setConversationOfThisMessage(thisConversation ? thisConversation : null);
        }
    }, [idConversation, conversation.listConversation, statusSeen]);
    useEffect(() => {
        const conversationData: any = conversation.listConversation?.find((item: any) => item._id === idConversation);
        if (detailChat?.type === CONVERSATION.TYPE.INDIVIDUAL) {
            (async () => {
                const { data } = await fetchCommonGroup(conversationData?.idOther);
                setCommonGroup(data);
            })();
        }
    }, [conversation.listConversation, detailChat?.type, idConversation]);
    // useEffect(() => {
    //     (async () => {
    //         try {
    //             const { data: detail }: any = await (fetchDetailChat(idConversation));
    //             setDetailChat(detail);
    //         } catch (err) {
    //             (err);
    //         }
    //     })();
    // }, [idConversation]);
    const handleOpenModalAddMemberToCall = () => {
        setListMember(conversationOfThisMessage?.member);
        setIsOpenModalAddMemberToCall(true);
    };
    const handleVoiceCall = () => {
        // có thuộc tính member là group chat
        // nếu là gọi cá nhân không phải gr thì gọi luôn ko chọn thành viên tham gia
        if (
            conversationOfThisMessage !== null &&
            !conversationOfThisMessage?.member
        ) {
            handleCall(1);
        } else if (
            conversationOfThisMessage !== null &&
            conversationOfThisMessage?.member
        ) {
            setTypeOfCallGroup(1);
            handleOpenModalAddMemberToCall();
        }
    };
    const handleVideoCall = () => {
        if (
            conversationOfThisMessage !== null &&
            !conversationOfThisMessage?.member
        ) {
            handleCall(2);
        } else if (
            conversationOfThisMessage !== null &&
            conversationOfThisMessage?.member
        ) {
            setTypeOfCallGroup(2);
            handleOpenModalAddMemberToCall();
        }
    };

    const prevIndex: any = useRef();
    const scrollToMessage = async (index: any) => {
        if (virtuoso.current) {
            if (prevIndex.current === index) {
                dispatch(setSearchItem(null));
                prevIndex.current = -1;
            }
            await (virtuoso.current as any).scrollToIndex(index, "smooth");
            prevIndex.current = index;
        }
    };
    useEffect(() => {
        if (search.arrayValueToSearch.length > 0) {
            // kiểm tra tin nhắn được chọn có nằm trong đoạn chat này không
            const isThisMessages = search.arrayValueToSearch?.find(
                (m: any) => m?.id === search.searchItem?.conversationId
            );
            const isConversation =
                idConversation === search.searchItem?.conversationId;

            if (search.searchItem !== null && isThisMessages && isConversation) {
                const index = isThisMessages.data.latestMessage?.findIndex(
                    (m: IMessage | any) =>
                        m.id === search.searchItem?._id &&
                        m.content.toLowerCase() === search.searchItem?.content.toLowerCase()
                );

                scrollToMessage(index);
            }
        }
    }, [
        search.searchItem,
        search.arrayValueToSearch,
        idConversation,
        virtuoso.current
    ]);
    // useEffect(() => {
    //     if(messages.length > 0) {
    //         // kiểm tra tin nhắn được chọn có nằm trong đoạn chat này không
    //         const isThisMessages = messages?.find((m: IMessage) => m?.conversationId === search.searchItem?.conversationId);

    //         if(search.searchItem !== null && isThisMessages ) {
    //             let index: number = -1;

    //             index = messages?.findIndex((m: IMessage) => m._id === search.searchItem?._id && m.content.toLowerCase() === search.searchItem?.content.toLowerCase());

    //             if(index !== -1) {
    //                 scrollToMessage(index);
    //             } else {
    //                 alert("Không có tin nhắn trong đoạn chat");
    //             }
    //         }
    //     }
    // }, [search.searchItem, messages, virtuoso.current]);
    useEffect(() => {
        let intervalId: string | number | NodeJS.Timeout | undefined;

        if (isRinging) {
            // Bắt đầu phát tiếng chuông
            play();
            // Thiết lập một độ trễ để phát lại tiếng chuông sau khi kết thúc
            intervalId = setInterval(() => play(), 3000); // Chạy lại tiếng chuông sau mỗi giây (tùy chỉnh theo nhu cầu)
        } else {
            // Dừng tiếng chuông và xóa độ trễ
            clearInterval(intervalId);
        }

        // Hủy kết thúc độ trễ khi component bị hủy
        return () => clearInterval(intervalId);
    }, [isRinging, play]);

    // Tính height cho Virtuallist dựa vào div contain
    useLayoutEffect(() => {
        const handleWindowResize = () => {
            if (divVirtualListRef.current) {
                // Div parent có pading-y là 12 nên phải trừ đi
                setHeightVirtualList(divVirtualListRef.current.clientHeight - 25);
            }
        };
        handleWindowResize();
        window.addEventListener("resize", handleWindowResize);
        return () => {
            window.removeEventListener("resize", handleWindowResize);
        };
    }, []);

    const handleFollowOutputHandler = useCallback(
        (isAtBottom: boolean) => {

            if (
                conversation.lastMessage.conversationId === idConversation &&
                (isAtBottom || isScroll)
            ) {
                setIsScroll(false);
                return "smooth";
            }
            return "smooth";
        },
        [conversation.lastMessage, isScroll]
    );
    useEffect(() => {
        let intervalId: string | number | NodeJS.Timeout | undefined;

        if (isRinging) {
            // Bắt đầu phát tiếng chuông
            play();
            // Thiết lập một độ trễ để phát lại tiếng chuông sau khi kết thúc
            intervalId = setInterval(() => play(), 3000); // Chạy lại tiếng chuông sau mỗi giây (tùy chỉnh theo nhu cầu)
        } else {
            // Dừng tiếng chuông và xóa độ trễ
            clearInterval(intervalId);
        }

        // Hủy kết thúc độ trễ khi component bị hủy
        return () => clearInterval(intervalId);
    }, [isRinging, play]);
    /**
   * Khi chat box được cuộn lên trên cùng -> call API load thêm  tin nhắn
   */
    const handleScrollOnTop = async () => {
        try {
            if (hasMoreNextMessage && nextMessage?._id) {
                const response = await axiosClient().get(
                    `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${idConversation}/message/${nextMessage?._id}?limit=20&type=3`
                );
                if (response.data && response.data.length > 0) {
                    setNextMessage(response.data[response.data.length - 1]);
                    const messageList = sortByTime(response.data);
                    setMessages([...messageList, ...messages]);
                    if (virtuoso.current !== null) {
                        (virtuoso.current as any).scrollToIndex({
                            index: 19,
                            align: "start",
                            behavior: "auto"
                        });
                    }
                } else {
                    setHasMoreNextMessage(false);
                    setNextMessage(undefined);
                }
            } else if (hasMorePage) {
                const response = await axiosClient().get(
                    `${process.env.NEXT_PUBLIC_DAK_CHAT_API
                    }/conversations/${idConversation}/messages?sort=desc&limit=20&offset=${(page - 1) * 20
                    }`
                );
                if (response.data && response.data.length > 0) {
                    setMessages([...sortByTime(response.data), ...messages]);
                    setPage(page + 1);
                    if (virtuoso.current !== null) {
                        (virtuoso.current as any).scrollToIndex({
                            index: 19,
                            align: "start"
                        });
                    }
                } else {
                    setHasMorePage(false);
                }
            }
        } catch (error) {
            console.log(error);
        }
    };
    const handleScrollOnBottom = async () => {
        if (prevMessage?._id && hasMorePrevMessage) {
            try {
                const response = await axiosClient().get(
                    `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${idConversation}/message/${prevMessage._id}?sort=desc&limit=20&type=1`
                );
                if (response.data && response.data.length > 0) {
                    setPrevMessage(response.data[0]);
                    const messageList = sortByTime(response.data);
                    setMessages([...messages, ...messageList]);
                } else {
                    setPrevMessage(undefined);
                    setHasMorePrevMessage(false);
                    setIsScrollMessageReply(false);
                }
            } catch (error) {
                console.log(error);
            }
        }
    };
    useEffect(() => {
        setMessages(listMessage);
    }, [listMessage]);

    useEffect(() => {
        if (Object.keys(messageScroll).length > 0) {
            const indexMessage = messages.findIndex((item: IMessage) => {
                return item._id === messageScroll.id;
            });
            if (indexMessage !== -1) {
                if (virtuoso.current !== null) {
                    (virtuoso.current as any).scrollToIndex({
                        index: indexMessage,
                        align: "start",
                        behavior: "auto"
                    });
                }
            } else {
                (async () => {
                    try {
                        const response = await axiosClient().get(
                            `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${idConversation}/message/${messageScroll.id}?sort=desc&limit=5&type=2`
                        );
                        if (response.data && response.data.length > 0) {
                            setNextMessage(response.data[response.data.length - 1]);
                            setPrevMessage(response.data[0]);
                            const messageList = sortByTime(response.data);
                            setMessages(messageList);
                            setIsScrollMessageReply(true);
                            const indexMessage = messageList.findIndex((item: IMessage) => {
                                return item._id === messageScroll._id;
                            });
                            if (virtuoso.current !== null) {
                                (virtuoso.current as any).scrollToIndex({
                                    index: indexMessage,
                                    align: "center",
                                    behavior: "auto"
                                });
                            }
                        }
                    } catch (error) {
                        console.log(error);
                    }
                })();
            }
            setMessageScroll({});
        }
    }, [messageScroll]);

    // useEffect(() => {
    //     if (divVirtualListRef.current && !isBottom) {
    //         divVirtualListRef.current.scrollTop =
    //             divVirtualListRef.current.scrollHeight;
    //     }
    // }, [messages]);
    useEffect(() => {
        dispatch(setUserIsCalling(call.isCalling));
        if (!call.isCalling) {
            stopRingtone();
        }
        if (call.userAcceptCall != "") {
            stopRingtone();
            clearMyTimeout();
        }


    }, [call]);
    useEffect(() => {
        (async () => {
            try {
                if (!isBottom) return;
                const response = await axiosClient().get(
                    `${process.env.NEXT_PUBLIC_DAK_CHAT_API
                    }/conversations/${idConversation}/messages?sort=desc&limit=20&offset=${(page - 1) * 20
                    }`
                );

                if (response.data && response.data.length > 0) {
                    setMessages([...sortByTime(response.data), ...messages]);
                    setPage(page + 1);
                }

        
            } catch (error) {
                console.log(error);
            }
        })();
    }, [isBottom]);

    useEffect(() => {
        const handleAddMessages = () => {
            if (!divVirtualListRef.current) return;
            const divEl = divVirtualListRef.current;
            const isBottomScroll = divEl.scrollTop === 0;
            setIsBottom(isBottomScroll);
        };
        divVirtualListRef.current?.addEventListener("scroll", handleAddMessages);
        return () => {
            divVirtualListRef.current?.removeEventListener(
                "scroll",
                handleAddMessages
            );
        };
    }, []);

    //add emoji to input
    const onEmojiClick = (emojiObject: any) => {
        if (inputRef.current !== null) {
            const value = inputRef.current.value;
            const selectionStart = inputRef.current.selectionStart;
            // Thêm emoji vào vị trí con trỏ bất kỳ
            if (selectionStart !== null) {
                const updatedValue =
                    value.substring(0, selectionStart) +
                    emojiObject.emoji +
                    value.substring(selectionStart);
                // Cập nhật giá trị và vị trí con trỏ mới
                inputRef.current.value = updatedValue;
                inputRef.current.setSelectionRange(
                    selectionStart + emojiObject.emoji.length,
                    selectionStart + emojiObject.emoji.length
                );
                inputRef.current.focus();
            }
        }
    };

    const encryptMessage = (password: string, message: string) => {
        return CryptoJS.AES.encrypt(message, password).toString();
    };
    const decryptMessage = (password: string, message: string) => {
        try {
            const bytes = CryptoJS.AES.decrypt(message, password);
            const originalText = bytes.toString(CryptoJS.enc.Utf8);
            return originalText;
        } catch (e) {
            console.log(e);
        }
    };
    const handleSubmit = async () => {
        const checkRole = detailChat?.members.find((item: any) => item.id === user.id)?.type;
        if (checkRole === CONVERSATION.ROLE_TYPE.MEMBER_WAITING_ACCEPT) {
            return toast.warning(" bạn cần join vào group trước khi chat");
        };
        if (detailChat.conversationSetting?.find(item => item.type === 6)?.value === false && checkRole !== CONVERSATION.ROLE_TYPE.OWNER && checkRole !== CONVERSATION.ROLE_TYPE.ADMIN) {
            return toast.warning(" Chỉ có chủ nhóm và phó nhóm mới được nhắn tin");
        };
        if (isSubmitting) {
            alert("You are messaging too fast, take a break! ");
            return;
        }

        // Mã hóa đầu cuối => kiểm tra trước khi gửi nếu chưa nhập pass, encode toàn bộ tin nhắn nếu đã nhập pass
        const listEncryptionConversation = findEncryptionConversationByIdConversation(idConversation, endToEndEncryption.listIdConversationDecoded) || null;
        if (listEncryptionConversation && listEncryptionConversation.encryptionPassword === "") {
            alert("Vui lòng nhập mật khóa mã hóa để tiếp tục cuộc trò chuyện !");
            dispatch(setTypeOfMofalHandlesPasswordsEncryption("enterPassword"));
            if (inputRef.current && inputRef.current.value) {
                inputRef.current.value = '';
            }
            return;
        }
        setIsSubmitting(true);
        // Nếu input không rỗng, gửi tin nhắn văn bản hoặc liên kết.
        if (inputRef.current?.value) {
            // const messageData: IMessageData = {
            //     content: inputRef.current?.value || "",
            //     replyTo: messageReply._id || null,
            //     conversationId: idConversation,
            //     type: isUrl(inputRef.current?.value)
            //         ? MESSAGE_TYPE.LINK
            //         : MESSAGE_TYPE.TEXT,
            //     exceptSocketId: user.socket?.id
            // };
            // (messageData);
            let messageData: IMessageData | any;
            if (listEncryptionConversation) {
                messageData = {
                    content: encryptMessage(listEncryptionConversation.encryptionPassword, inputRef.current?.value || ""),
                    isHashed: true,
                    replyTo: messageReply._id || null,
                    conversationId: idConversation,
                    type: isUrl(inputRef.current?.value)
                        ? MESSAGE_TYPE.LINK
                        : MESSAGE_TYPE.TEXT,
                    exceptSocketId: user.socket?.id
                };
            } else {
                messageData = {
                    content: inputRef.current?.value || "",
                    replyTo: messageReply._id || null,
                    conversationId: idConversation,
                    type: isUrl(inputRef.current?.value)
                        ? MESSAGE_TYPE.LINK
                        : MESSAGE_TYPE.TEXT,
                    exceptSocketId: user.socket?.id
                };
            }
            sendMessage(
                EVENT_NAMES.CONVERSATION.SEND_MESSAGE,
                socket,
                () => {
                    if (inputRef.current !== null) {
                        inputRef.current.value = "";
                    }
                    setMessageReply({});
                    setIsFocused(false);
                },
                messageData
            );
            setIsScroll(true);
        }
        if (selectedImage.length > 0 || selectedFile.length > 0) {
            const id = toast.loading("Uploading...");
            // Nếu có hình ảnh hoặc tệp được chọn, tải chúng lên và gửi tin nhắn đa phương tiện.
            try {
                // Kiểm tra tính hợp lệ của các tệp được chọn.
                const { invalidFiles, errors } = await validateFiles([
                    ...selectedImage,
                    ...selectedFile
                ]);
                if (errors.length > 0 || invalidFiles.length > 0) {
                    setFileError(invalidFiles);
                    toast.error(errors.join("\n"));
                    return;
                }
                // Tạo một biểu mẫu dữ liệu để tải lên các tệp.
                const formData = new FormData();
                for (const file of selectedImage) {
                    formData.append("files", file);
                }
                for (const file of selectedFile) {
                    formData.append("files", file);
                }
                // Hiển thị popup đang tải lên
                setIsUploading(true);
                const data = await uploadFile(formData, sendMediaHd);
                // Nếu tải lên thành công, gửi tin nhắn đa phương tiện.
                if (data && data.length > 0) {
                    const images = data.filter((result) => result.type === 2);
                    const fileMedia = data.filter((result) => result.type !== 2);
                    if (images && images.length > 0) {
                        const linkImages = images.map((item) => item.link);
                        // const messageData: IMessageData = {
                        //     content: JSON.stringify(linkImages),
                        //     replyTo: messageReply._id || null,
                        //     conversationId: idConversation,
                        //     type: MESSAGE_TYPE.IMAGE,
                        //     exceptSocketId: user.socket?.id
                        // };
                        let messageData: IMessageData | any;
                        if (listEncryptionConversation) {
                            messageData = {
                                content: encryptMessage(listEncryptionConversation.encryptionPassword, JSON.stringify(linkImages)),
                                isHashed: true,
                                replyTo: messageReply._id || null,
                                conversationId: idConversation,
                                type: MESSAGE_TYPE.IMAGE,
                                exceptSocketId: user.socket?.id
                            };
                        } else {
                            messageData = {
                                content: JSON.stringify(linkImages),
                                replyTo: messageReply._id || null,
                                conversationId: idConversation,
                                type: MESSAGE_TYPE.IMAGE,
                                exceptSocketId: user.socket?.id
                            };
                        }
                        sendMessage(
                            EVENT_NAMES.CONVERSATION.SEND_MESSAGE,
                            socket,
                            () => { },
                            messageData
                        );
                    }
                    for (const item of fileMedia) {
                        // const messageData: IMessageData = {
                        //     content: item.link,
                        //     replyTo: messageReply._id || null,
                        //     conversationId: idConversation,
                        //     type: item.type === 1 ? MESSAGE_TYPE.MEDIA : MESSAGE_TYPE.FILE,
                        //     exceptSocketId: user.socket?.id
                        // };
                        let messageData: IMessageData | any;
                        if (listEncryptionConversation) {
                            messageData = {
                                content: encryptMessage(listEncryptionConversation.encryptionPassword, item.link),
                                isHashed: true,
                                replyTo: messageReply._id || null,
                                conversationId: idConversation,
                                type: item.type === 1 ? MESSAGE_TYPE.MEDIA : MESSAGE_TYPE.FILE,
                                exceptSocketId: user.socket?.id
                            };
                        } else {
                            messageData = {
                                content: item.link,
                                replyTo: messageReply._id || null,
                                conversationId: idConversation,
                                type: item.type === 1 ? MESSAGE_TYPE.MEDIA : MESSAGE_TYPE.FILE,
                                exceptSocketId: user.socket?.id
                            };
                        }
                        sendMessage(
                            EVENT_NAMES.CONVERSATION.SEND_MESSAGE,
                            socket,
                            () => { },
                            messageData
                        );
                    }
                    toast.update(id, {
                        render: "Tải file thành công",
                        type: "success",
                        isLoading: false,
                        autoClose: 3000,
                        closeButton: true
                    });
                    setIsUploading(false);
                    setSelectedImage([]);
                    setSelectedFile([]);
                    setIsScroll(true);
                    setMessageReply({});
                }
            } catch (error) {
                // Xử lý lỗi kiểm tra tính hợp lệ hoặc lỗi tải lên.
                setIsUploading(false);
                const errorMessage =
                    error instanceof Error ? error.message : String(error);
                toast.update(id as Id, {
                    render: errorMessage,
                    type: "error",
                    isLoading: false,
                    autoClose: 3000,
                    closeButton: true
                });
            }
        }
        setTimeout(() => {
            setIsSubmitting(false);
        }, 300);
    };

    useEffect(() => {
        if (!socket) return;
        socket.on(EVENT_NAMES.CONVERSATION.SENDED, (msg: IMessage) => {
            if (msg.isCompressed) {
                decompressMessage(msg);
            }
            if (msg.conversationId === idConversation) {
                setMessages((prevState) => [...prevState, msg]);
                setIsScroll(true);
            }
            dispatch(setLastMessage(msg));
        });
        return () => {
            socket.off(EVENT_NAMES.CONVERSATION.SENDED);
        };
    }, [socket]);

    useEffect(() => {
        if (!user.socket) return;
        user.socket.on(
            EVENT_NAMES.CONVERSATION.RECEIVE_MESSAGE,
            (msg: IMessage | any) => {
                // Mã hóa đầu cuối => logic
                if (msg.isHashed) {
                    const haveEncrytionConversation: any = findEncryptionConversationByIdConversation(msg.conversationId, endToEndEncryption.listIdConversationDecoded);
                    if (!haveEncrytionConversation) {
                        dispatch(addIdToListIdConversationDecoded({
                            idConversation: msg.conversationId,
                            hashEncryptionPassword: "",
                            encryptionPassword: "",
                            isDecoded: false
                        }));
                    }
                };
                if (msg.isCompressed) {
                    decompressMessage(msg);
                }
                if (msg.conversationId === idConversation) {
                    setMessages((prevState) => [...prevState, msg]);
                    setIsScroll(true);
                }
                dispatch(setLastMessage(msg));
                // Gửi dữ liệu từ Next.js đến ứng dụng Flutter
                // Gửi sự kiện từ trang web Next.js đến ứng dụng Flutter
                // (`${msg.conversationId}-${msg.createdBy.username}-receiMessage`);
                // window.postMessage(`${msg.conversationId}-${msg.createdBy.username}-receiMessage`, '*');


            }
        );
        user.socket.on(EVENT_NAMES.CONVERSATION.REACTION, (reaction: any) => {
            const data = messages.find(
                (mess: IMessage) => mess._id === reaction.messageId
            );
            if (!data) return;
            const newData = { ...data, reactions: [reaction, ...data.reactions] };
            setMessages((prevArray) =>
                prevArray.map((mess: IMessage) => {
                    return mess._id === reaction.messageId ? newData : mess;
                })
            );
        });
        if (
            messages.length > 0 &&
            !messages[messages.length - 1].seen.find(
                (item: ISeenUser) => item._id === user.id
            )
        ) {
            user.socket.emit(
                EVENT_NAMES.CONVERSATION.SEEN_MESSAGE,
                idConversation,
                (data: any) => {
                    if (data.success === true) {
                        dispatch(
                            setTotalUnseenMessage(
                                (conversation?.totalUnseenMessage as number) === 0
                                    ? 0
                                    : (((conversation?.totalUnseenMessage as number) -
                                        (
                                            conversation.listConversation?.find(
                                                (item: IConversation) => item._id === idConversation
                                            ) as IConversation
                                        )?.unSeenMessageTotal) as number)
                            )
                        );
                        const conversationData =
                            (conversation?.listConversation?.find(
                                (item: IConversation) => item._id === idConversation
                            ) as IConversation) || {};
                        if (!conversationData) return;
                        const newData = { ...conversationData, unSeenMessageTotal: 0 };
                        dispatch(
                            setListConversations(
                                sortConversationByNewest(
                                    conversation?.listConversation?.map((item: IConversation) => {
                                        return item._id === idConversation ? newData : item;
                                    }) || []
                                )
                            )
                        );
                        const newUserSeen: ISeenUser = {
                            _id: user.id,
                            username: user.name,
                            avatar: user.avatar,
                            id: user.id,
                            time: ""
                        };
                        const lastMessage = messages[messages.length - 1];
                        const newSeen = lastMessage.seen.find(
                            (item: ISeenUser) => item._id === newUserSeen._id
                        )
                            ? lastMessage.seen
                            : [...lastMessage.seen, newUserSeen];
                        const newLastMessage = {
                            ...lastMessage,
                            seen: [...newSeen]
                        };
                        setMessages((prevState) => {
                            return prevState.map((mess: IMessage) => {
                                return mess._id === newLastMessage._id ? newLastMessage : mess;
                            });
                        });
                    }
                }
            );
        }
        return () => {
            user.socket.off(EVENT_NAMES.CONVERSATION.RECEIVE_MESSAGE);
            user.socket.off(EVENT_NAMES.CONVERSATION.REACTION);
            user.socket.off(EVENT_NAMES.CONVERSATION.SEEN_MESSAGE);
        };
    }, [user.socket, messages]);
    useEffect(() => {
        if (!user.socket || messages.length === 0) return;
        user.socket.on(EVENT_NAMES.CONVERSATION.SEENED_MESSAGE, (data: any) => {
            if (
                data.conversationId === idConversation &&
                data.userSeenId !== user.id
            ) {
                const newUserSeen: ISeenUser = {
                    _id: data?.userSeenId,
                    username: data?.userSeenUsername,
                    avatar: data?.userSeenAvatar,
                    id: data?.userSeenId,
                    time: ""
                };
                const lastMessage = messages[messages.length - 1];
                const newSeen = lastMessage.seen.find(
                    (item: ISeenUser) => item._id === newUserSeen._id
                )
                    ? lastMessage.seen
                    : [...lastMessage.seen, newUserSeen];
                const newLastMessage = {
                    ...lastMessage,
                    seen: [...newSeen]
                };
                if (
                    lastMessage.seen.find(
                        (item: ISeenUser) => item._id === newUserSeen._id
                    )
                ) {
                    return;
                }
                setMessages((prevState) => {
                    return prevState.map((mess: IMessage) => {
                        return mess._id === newLastMessage._id ? newLastMessage : mess;
                    });
                });
            }
        });
        return () => {
            user.socket.off(EVENT_NAMES.CONVERSATION.SEENED_MESSAGE);
        };
    }, [user.socket, messages]);
    //   EVENT SOCKET USER ONLINE
    useEffect(() => {
        const socketConversation = io(
            `${process.env.NEXT_PUBLIC_DAK_CHAT_SOCKET}/chat-${idConversation}`,
            {
                transports: ["websocket"],
                auth: {
                    token: `${cookieData?.token_chat}`
                    // xsrf_token: "nixT9tY1nA4Ub0ZeRh5a8RMA"
                },
                withCredentials: true
            }
        );
        socketConversation.on("connect", () => {
            socketConversation.emit("CHECK_STATUS_MEMBER", (data: any) => {
                if (userLogin?.id) {
                    const checkOnine = data?.data.find(
                        (item: any) => item?.userId !== userLogin?.id
                    );
                    if (checkOnine?.online || data.data.length > 2) {
                        setStatusUser(true);
                    } else {
                        const currentTime = moment();
                        const lastLoginTime = moment(checkOnine?.lastLogin);
                        const duration = moment.duration(currentTime.diff(lastLoginTime));
                        if (duration.asHours() < 24) {
                            setLastLogin(STATUS_USER.ONLINE + lastLoginTime.fromNow());
                        } else {
                            setLastLogin(STATUS_USER.OFFLINE);
                        }
                        setStatusUser(false);
                    }
                }
            });
        });
    }, [idConversation, userLogin?.id]);
    useEffect(() => {
        if (!user.socket) return;

        user.socket.on("USER_ONLINE", (msg: any) => {
            if (msg === conversationOfThisMessage?.idOther) {
                setStatusUser(true);
            }
        });
        user.socket.on("USER_OFFLINE", (msg: any) => {
            if (msg === conversationOfThisMessage?.idOther) {
                setStatusUser(false);
                setLastLogin("Offline");
            }
        });
        return () => {
            user.socket.off("USER_ONLINE");
            user.socket.off("USER_OFFLINE");
        };
    }, [user.socket, conversationOfThisMessage]);

    useEffect(() => {
        (() => {
            try {
                const socketConversation = io(
                    `${process.env.NEXT_PUBLIC_DAK_CHAT_SOCKET}`,
                    {
                        auth: { token: `${cookieData?.token_chat}` },
                        autoConnect: true,
                        transports: ["websocket"],
                        withCredentials: true
                    }
                );
                socketConversation.on("connect", () => {
                    setSocketCall(socketConversation);
                    // socketConversation.on(
                    //     EVENT_NAMES.INDIVIDUAL_CALL.CALL_INCOMING_DEMO,
                    //     (data: any) => {
                    //         window.postMessage("ddsa-dsadsa-IncomingCall", "*");
                    //         setIsPopup(data);
                    //     }
                    // );
                    // socketConversation.on(
                    //     EVENT_NAMES.INDIVIDUAL_CALL.CALL_ACCEPT_DEMO,
                    //     (data: any) => {
                    //         (" đã nhận dồng ý cuộc gọi", data);
                    //         stopRingtone();
                    //         socketConversation.emit(
                    //             EVENT_NAMES.INDIVIDUAL_CALL.CALL_MUTE_DEMO,
                    //             { userId: userLogin?.id, conversationId: cookies.data.conversationId },
                    //             (data: any) => {
                    //                 ("emit mute", data);
                    //             }
                    //         );
                    //     }
                    // );
                    // socketConversation.on(
                    //     EVENT_NAMES.INDIVIDUAL_CALL.CALL_DENY_DEMO,
                    //     (data: any) => {
                    //         ("từ chối", data);
                    //         stopRingtone();
                    //         dispatch(setUserIsCalling(false));
                    //         socketConversation.emit(
                    //             EVENT_NAMES.INDIVIDUAL_CALL.CALL_MUTE_DEMO,
                    //             { userId: userLogin?.id, conversationId: cookies.data.conversationId },
                    //             (data: any) => {
                    //                 ("emit mute", data);
                    //             }
                    //         );
                    //     }
                    // );
                    // socketConversation.on(
                    //     EVENT_NAMES.INDIVIDUAL_CALL.CALL_OUT_DEMO,
                    //     () => {
                    //         dispatch(setUserIsCalling(false));
                    //     }
                    // );

                    // window.addEventListener('message', (event) => {
                    //     const data: string = event.data;
                    //     ("typeofData", typeof data, data)
                    //     if (!(data.includes("DENY") || data.includes("ACCEPT"))) return;
                    //     try {
                    //         if (!data.includes("DENY")) {
                    //             socketConversation.emit(
                    //                 EVENT_NAMES.INDIVIDUAL_CALL.CALL_ACCEPT_DEMO,
                    //                 {
                    //                     userId: event.data,
                    //                     conversationId: idConversation,
                    //                     exceptSocketId: socketConversation.id
                    //                 },
                    //                 (data: any) => {
                    //                     ("emit accept call", data);
                    //                     dispatch(setUserIsCalling(true));
                    //                     setIsPopup(false);
                    //                 }
                    //             );
                    //             return;
                    //         }
                    //         socketConversation.emit(
                    //             EVENT_NAMES.INDIVIDUAL_CALL.CALL_DENY_DEMO,
                    //             {
                    //                 userId: data.split("_")[1],
                    //                 conversationId: idConversation,
                    //                 exceptSocketId: socketConversation.id
                    //             },
                    //             (data: any) => {
                    //                 ("emit accept call", data);
                    //                 dispatch(setUserIsCalling(false));
                    //                 setIsPopup(false);
                    //             }
                    //         );

                    //     } catch (error) {
                    //         (error);
                    //     }
                    // });

                });
            } catch (error: any) {
                throw new Error(error);
            }
        })();
    }, []);
    useEffect(() => {
        if (!socketCall) return;

        return () => {
            socket.off(EVENT_NAMES.INDIVIDUAL_CALL.CALL_INCOMING_DEMO);
        };
    }, [socketCall]);
    const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === "Enter") {
            handleSubmit();
        }
    };
    const handleSlute = () => {
        const checkRole = detailChat?.members.find((item: any) => item.id === user.id)?.type;
        if (checkRole === CONVERSATION.ROLE_TYPE.MEMBER_WAITING_ACCEPT) {
            return toast.warning(" bạn cần join vào group trước khi chat");
        };
        if (detailChat.conversationSetting?.find(item => item.type === 6)?.value === false && checkRole !== CONVERSATION.ROLE_TYPE.OWNER && checkRole !== CONVERSATION.ROLE_TYPE.ADMIN) {
            return toast.warning(" Chỉ có chủ nhóm và phó nhóm mới được nhắn tin");
        };
        // const messageData: IMessageData = {
        //     content: `${process.env.NEXT_PUBLIC_DAK_API_SLUTE}/slute.png`,
        //     replyTo: messageReply._id || null,
        //     conversationId: idConversation,
        //     type: MESSAGE_TYPE.STICKER,
        //     exceptSocketId: user.socket?.id
        // };

        // Mã hóa đầu cuối => logic
        const listEncryptionConversation = findEncryptionConversationByIdConversation(idConversation, endToEndEncryption.listIdConversationDecoded) || null;
        let messageData: IMessageData | any;
        if (listEncryptionConversation) {
            messageData = {
                content: encryptMessage(listEncryptionConversation.encryptionPassword, `${process.env.NEXT_PUBLIC_DAK_API_SLUTE}/slute.png`),
                isHashed: true,
                replyTo: messageReply._id || null,
                conversationId: idConversation,
                type: MESSAGE_TYPE.STICKER,
                exceptSocketId: user.socket?.id
            };
        } else {
            messageData = {
                content: `${process.env.NEXT_PUBLIC_DAK_API_SLUTE}/slute.png`,
                replyTo: messageReply._id || null,
                conversationId: idConversation,
                type: MESSAGE_TYPE.STICKER,
                exceptSocketId: user.socket?.id
            };
        }
        sendMessage(
            EVENT_NAMES.CONVERSATION.SEND_MESSAGE,
            socket,
            () => {
                setIsScroll(true);
            },
            messageData
        );
    };
    const handleCall = async (type: number) => {
        await onClickVideoCall(idConversation, type);
    };
    const handleRepeat = () => {
        startRingtone();
    };
    // const handleCallAccept = () => {
    //     try {
    //         socketCall.emit(
    //             EVENT_NAMES.INDIVIDUAL_CALL.CALL_ACCEPT_DEMO,
    //             {
    //                 userId: userLogin?.id,
    //                 conversationId: cookies.data.conversationId,
    //                 exceptSocketId: socketCall.id
    //             },
    //             (data: any) => {
    //                 ("emit accept call", data);
    //                 dispatch(setUserIsCalling(true));
    //             }
    //         );
    //     } catch (error) {
    //         (error);
    //     }
    // };
    // const handleCallDeny = () => {
    //     try {
    //         socketCall.emit(
    //             EVENT_NAMES.INDIVIDUAL_CALL.CALL_DENY_DEMO,
    //             {
    //                 userId: userLogin?.id,
    //                 conversationId: cookies.data.conversationId,
    //                 exceptSocketId: socketCall.id
    //             },
    //             (data: any) => {
    //                 ("emit accept call", data);
    //                 dispatch(setUserIsCalling(false));
    //             }
    //         );
    //     } catch (error) {
    //         (error);
    //     }
    // };
    // const handleCallOut = () => {
    //     try {
    //         ("handle out");
    //         socketCall.emit(
    //             EVENT_NAMES.INDIVIDUAL_CALL.CALL_OUT_DEMO,
    //             {
    //                 userId: userLogin?.id,
    //                 conversationId: cookies.data.conversationId,
    //                 exceptSocketId: socketCall.id
    //             },
    //             (data: any) => {
    //                 ("out call", data);
    //             }
    //         );
    //     } catch (error) {
    //         (error);
    //     }
    // };
    useEffect(() => {
        if (!user.is_calling) {
            clearMyTimeout();
        }
    }, [user.is_calling]);
    const handleImageChange = (event: ChangeEvent<HTMLInputElement>) => {
        if (selectedFile && selectedFile.length > 0) {
            toast.warning("Không thể chọn ảnh");
        } else {
            const files = event.target.files;
            if (files && files.length > 0) {
                // Validate that all files are images or videos
                const validFiles = Array.from(files).filter((file) => {
                    const mimeType = file.type;
                    return mimeType.startsWith("image/") || mimeType.startsWith("video/");
                });

                // If there are any invalid files, display an error
                if (validFiles.length !== files.length) {
                    toast.error("Chỉ có thể chọn ảnh hoặc video");
                    return;
                }

                // If all files are valid, set the selectedImage state
                setSelectedImage([...selectedImage, ...validFiles]);
            }
        }
        event.target.value = "";
    };
    const handleDeleteimg = (index: number) => {
        const newArray = selectedImage.filter((_, i) => i !== index);
        setSelectedImage(newArray);
    };
    const handleFileChange = (event: ChangeEvent<HTMLInputElement>) => {
        if (selectedImage && selectedImage.length > 0) {
            // Display an error if the user has already selected images
            toast.warning("Không thể chọn file khi đã chọn ảnh");
            return;
        }
        const files = event.target.files;
        if (files && files.length > 0) {
            const validFiles = Array.from(files).filter((file) => {
                const mimeType = file.type;
                return !mimeType.startsWith("image/") && !mimeType.startsWith("video/");
            });
            if (validFiles.length !== files.length) {
                toast.error("Chỉ có thể chọn các file khác ảnh hoặc video");
                return;
            }
            setSelectedFile([...selectedFile, ...validFiles]);
        }
        event.target.value = "";
    };
    const handleDeleteFile = (index: number) => {
        const newArray = selectedFile.filter((_, i) => i !== index);
        if (fileError && fileError.length > 0) {
            const newArrayError = fileError.filter((item) =>
                newArray.find((n) => n.name === item.name)
            );
            setFileError(newArrayError);
        }
        setSelectedFile(newArray);
    };
    const handleDeleteAll = () => {
        if (!isUploading) {
            setSelectedImage([]);
            setSelectedFile([]);
        }
    };
    useEffect(() => {
        if (selectedFile.length > 0 && selectedImage.length > 0) {
            setIsFocused(true);
        }
        if (messageReply.id && inputRef.current) {
            inputRef.current?.focus();
        }
    }, [selectedFile, selectedImage, messageReply]);
    useEffect(() => {
        if (
            conversation.messageReaction &&
            Object.keys(conversation.messageReaction).length > 0
        ) {
            const reaction = conversation.messageReaction;
            const data = messages.find(
                (mess: IMessage) => mess._id === reaction.messageId
            );
            if (!data) return;
            const newData = { ...data, reactions: [reaction, ...data.reactions] };
            setMessages((prevArray) =>
                prevArray.map((mess: IMessage) => {
                    return mess._id === reaction.messageId ? newData : mess;
                })
            );
            dispatch(
                setMessageReaction({
                    userId: "",
                    icon: "",
                    messageId: ""
                })
            );
        }
    }, [conversation.messageReaction]);
    useEffect(() => {
        if (
            conversation.messageForward &&
            conversation.messageForward?.conversationId === idConversation &&
            Object.keys(conversation.messageForward).length > 0
        ) {
            setMessages([...messages, conversation.messageForward]);
            dispatch(setMessageForward({}));
        }
    }, [conversation.messageForward]);
    const handleClickDown = async () => {
        if (virtuoso.current !== null) {
            if (!isScrollMessageReply) {
                (virtuoso.current as any).scrollToIndex({
                    index: messages.length - 1,
                    align: "end",
                    behavior: "auto"
                });
            } else
                try {
                    const response = await axiosClient().get(
                        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${idConversation}/messages?sort=desc&limit=20&offset=0`
                    );
                    if (response.data && response.data.length > 0) {
                        setMessages([...sortByTime(response.data)]);
                        setIsScrollMessageReply(false);
                        setNextMessage(undefined);
                        setPrevMessage(undefined);
                        if (virtuoso.current !== null) {
                            (virtuoso.current as any).scrollToIndex({
                                index: 19,
                                align: "end",
                                behavior: "auto"
                            });
                        }
                    }
                } catch (error) {
                    console.log(error);
                }
        }
    };
    useEffect(() => {
        (async function () {
            const userLogin = await getCookie("userLogin");
            if (!userLogin) return push("/");
            setUserLogin(JSON.parse(userLogin));
        })();
    }, [push]);
    const startRingtone = () => {
        setIsRinging(true);
    };

    const stopRingtone = () => {
        setIsRinging(false);
    };
    const [timeoutId, setTimeoutId] = useState<any>(null);
    const startTimeout = (callback: any) => {
        const id = setTimeout(() => {
            ('Timeout completed!');
            callback();
        }, 15000);

        // Save the timeout ID in the state
        setTimeoutId(id);
    };

    // Function to clear the timeout
    const clearMyTimeout = () => {
        if (timeoutId) {
            clearTimeout(timeoutId);
        }
    };
    function onClickVideoCall(conversationActive: string, type: number) {
        // setConversationsCalling(conversationActive);
        // setShowScreenConversationsCalling(true);

        setCookie(
            "data",
            JSON.stringify({
                conversationId: conversationActive,
                username: userLogin?.name,
                type: type,
                usercall: conversation.otherUsername
            }),
            { sameSite: "strict", secure: true, maxAge: 30 * 24 * 60 * 60 }
        );
        try {
            socketCall.emit(
                EVENT_NAMES.INDIVIDUAL_CALL.CALL_INCOMING_DEMO,
                {
                    conversationId: conversationActive,
                    username: userLogin?.name,
                    avatar: userLogin?.avatar,
                    type: type,
                    userId: userLogin?.id
                },

                (data: any) => {
                    dispatch(setWaitCalling(true));
                    dispatch(setUserIsCalling(true));
                    dispatch(updateCalling({ isCalling: true, conversationId: conversationActive || "", userCreatCall: user?.id || "" }));
                    if (!data) {
                        alert(`${conversation?.otherUsername} đang ngoại tuyến`);
                    } else {
                        try {
                            handleRepeat();
                            startTimeout(() => {
                                dispatch(setUserIsCalling(false));
                                stopRingtone();
                                socketCall.emit(EVENT_NAMES.INDIVIDUAL_CALL.DEMO_INCOMING_CANCEL, {
                                    conversationId: conversationActive,
                                    avatar: user?.avatar,
                                    username: user.username
                                }, () => {});
                                socketCall.emit(
                                    EVENT_NAMES.INDIVIDUAL_CALL.CALL_OUT_DEMO,
                                    {
                                        userId: user?.id,
                                        conversationId: conversationActive,
                                        exceptSocketId: socketCall.id
                                    },
                                    () => {
                                        // dispatch(setUserIsCalling(false));
                                    }
                                );
                                // socketCall.on(
                                //     EVENT_NAMES.INDIVIDUAL_CALL.CALL_MUTE_DEMO,
                                //     (data: any) => {
                                //         clearMyTimeout();
                                //         ("emit mute", data);
                                //     }
                                // );
                            });
                        } catch (error) {
                            console.log(error);
                        }
                        dispatch(setUserIsCalling(true));
                    }
                }
            );
        } catch (error) {
            console.log(error);
        }

        // push("/media");
    }
    const isGroupRef = detailChat.type === CONVERSATION.TYPE.GROUP ? true : false;
    const handleAcceptAndDeny = async (action: number) => {
        try {
            if (detailChat?.members.find((item: any) => item.id === user.id)?.type === CONVERSATION.ROLE_TYPE.MEMBER_WAITING_ACCEPT) {
                const response = await axiosClient().put(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/group/decide/member/${idConversation}/${action}`, {});
                if (response.status === 200) {
                    toast.success("Thành công");
                    if (action === 1) {
                        const { data: newDetailChat } = await fetchDetailChat(idConversation);
                        dispatch(setDetailChat(newDetailChat));
                    }
                    if (action === 2) {
                        push("/chat");
                        const newListConversations = conversation?.listConversation?.filter((item: any) => item._id !== detailChat?.id);
                        dispatch(
                            setListConversations(
                                sortConversationByNewest(filterConversation(newListConversations, user.id))
                            )
                        );
                    }
                }
            }
        } catch (err: any) {
            toast.error(err.response.data.message);
        }
    };
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [listMemberCallGroup, setListMemberCallGroup] = useState<any[]>([]);
    const [mergeAllList, setMergeAllList] = useState<(IMessageMerge | INoteMerge | IVoteMerge | IActivityVote)[]>([]);
    // Chia nhỏ quy trình
    const processVoteAnswers = (voteList: IVote[]) => {
        // Map to keep track of unique user IDs and their latest answer
        const userAnswersMap: Record<string, IActivityVote> = {};
        // Process each vote item
        voteList.forEach(voteItem => {
            voteItem.conversationVoteOptions.forEach(option => {
                option.conversationVoteAnswers.forEach(answer => {
                    const answerUserId = answer.user.id;
                    const answerId = voteItem.id + answerUserId;
                    // If user ID is not in the map or the current answer has a later updatedAt, update the map
                    if (!userAnswersMap[answerUserId] || userAnswersMap[answerUserId].updatedAt < answer.updatedAt) {
                        userAnswersMap[answerUserId] = {
                            itemType: "voteAnswers",
                            ...answer,
                            id: answerId,
                            voteItem
                        };
                    }
                });
            });
        });
        // Convert map values to an array
        return Object.values(userAnswersMap);
    };
    useEffect(() => {
        const firstMessageTime = new Date(messages[0]?.createdAt).getTime();
        const mergeArr: (IMessageMerge | INoteMerge | IVoteMerge | IActivityVote)[] = [
            ...messages.map(message => ({ ...message, itemType: "message" })) as IMessageMerge[],
            ...newsletter.noteList.map((note: any) => ({ ...note, itemType: "note" })).filter((item: any) => new Date(item.createdAt).getTime() > firstMessageTime) as INoteMerge[],
            ...newsletter.voteList.map((vote: any) => ({ ...vote, itemType: "vote" })).filter((item: any) => new Date(item.createdAt).getTime() > firstMessageTime) as IVoteMerge[],
            ...processVoteAnswers(newsletter.voteList).filter((answer) => new Date(answer.createdAt).getTime() > firstMessageTime) as IActivityVote[]
            // ) as IActivityVote[]
        ].sort((a, b) => new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime());
        setMergeAllList(mergeArr);
        return () => {
            setMergeAllList([]);
        };
    }, [messages, newsletter.noteList, newsletter.voteList]);
    const handleShowInfoGroupChat = async (conversation: any) => {
        const { data } = await (fetchDetailChat(idConversation)) as any;
        dispatch(setDetailChat(data));
        if (conversation.type == 2) {
            setIsOpenInfoGroupChat(true);
        }
        else {
            setIsOpenInforConversation(true);
        }
    };

    // Mã hóa đầu cuối => logic

    const [isOpenPopupExplainTheDecryptionProcess, setIsOpenPopupExplainTheDecryptionProcess] = useState<boolean>(false);
    const handleClickRequestPassword = () => {
        setIsOpenPopupExplainTheDecryptionProcess(true);
    };
    const hanleMakeACallAfterAcceptedProcess = () => {
        if (conversationOfThisMessage && conversationOfThisMessage?.type === 1) {
            const partner: any = conversationOfThisMessage?.createdByUser?.id !== user.id ? conversationOfThisMessage?.createdByUser : conversationOfThisMessage?.directUser;
            if (partner) {
                alert("Tạo cuộc gọi tới partner : " + partner?.username);
                handleCall(1);
            }
        } else if (conversationOfThisMessage && conversationOfThisMessage?.type === 2) {
            const groupOwner: any = conversationOfThisMessage?.createdByUser;
            if (groupOwner) {
                alert("Tạo cuộc gọi tới group owner : " + groupOwner?.username);
                setTypeOfCallGroup(1);
            }
        }
    };
    const handleClickContinue = () => {
        dispatch(setTypeOfMofalHandlesPasswordsEncryption("enterPassword"));
    };

    const [encryptionConversation, setEncryptionConversation] = useState<EncryptionConversationProp | null>(null);

    useEffect(() => {
        setEncryptionConversation(findEncryptionConversationByIdConversation(idConversation, endToEndEncryption?.listIdConversationDecoded));
    }, [endToEndEncryption, idConversation, messages]);

    const { isOpenModalTurnOffNotification } = notify;
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [backgroundImage, setBackgroundImage] = useState<string | null>(null);
    useEffect(() => {
        // Load background image URL from localStorage on component mount
        const storedBackgroundImage = localStorage.getItem(idConversation + 'backgroundImage');
        if (storedBackgroundImage) {
            setBackgroundImage(storedBackgroundImage);
        }
    }, [isModalOpen, idConversation]);
    const openImageUploadModal = () => {
        setIsModalOpen(true);
    };
    const closeImageUploadModal = () => {
        setIsModalOpen(false);
    };
    const handleImageError = (event: SyntheticEvent<HTMLImageElement, Event>) => {

        const imgElement = event.target as HTMLImageElement;
        imgElement.src = userAvatar.src;
    };
    return (
        <>
            <div className={`w-screen overflow-x-hidden flex relative`}>
                {isOpenModalAddMemberToCall && (
                    <ModalAddMemberToCall
                        listMember={listMember}
                        setIsOpenModalAddMemberToCall={setIsOpenModalAddMemberToCall}
                        typeOfCallGroup={typeOfCallGroup}
                        handleCall={handleCall}
                    />
                )}
                {
                    isOpenModalTurnOffNotification && <ModalTurnOffNotifications idConversation={idConversation} />
                }
                {isModalOpen && <ImageUploadModal isOpen={isModalOpen} onClose={closeImageUploadModal} idConversation={idConversation} />}
                {/* Mã hóa đầu cuối => logic */}
                {
                    endToEndEncryption?.isOpenModalGeneratesEncryptedPasswords && <ModalGeneratesEncryptedPasswords idConversation={idConversation} />
                }
                {
                    endToEndEncryption?.typeOfMofalHandlesPasswordsEncryption !== "none" && <ModalHandlesPasswordsEncryption idConversation={idConversation} />
                }
                {
                    isOpenPopupExplainTheDecryptionProcess && <PopupExplainTheDecryptionProcess hanleMakeACallAfterAcceptedProcess={hanleMakeACallAfterAcceptedProcess} setIsOpenPopupExplainTheDecryptionProcess={setIsOpenPopupExplainTheDecryptionProcess} />
                }
                <div
                    className={`h-[92dvh] flex flex-col relative flex-shrink-0 w-screen`}
                >
                    <div className="relative chat_header py-2 px-4 dark:bg-thNewtral1 bg-light-thNewtral1 flex justify-between items-center">
                        <div className="flex gap-x-2 items-center">
                            <div
                                className="relative cursor-pointer"
                                onClick={() => {
                                    if (isGroupRef) {
                                        router.push(`/chat?option=group`);
                                    } else {
                                        router.push(`/chat`);
                                    }
                                }}
                            >
                                <FiChevronLeft size={40} />
                            </div>
                            <div className="relative">
                                <Image
                                    src={
                                        conversation?.otherAvatar === "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg"
                                            ? "/default_avatar.jpg"
                                            : conversation?.otherAvatar
                                    }
                                    sizes="(max-width: 44px) 44px, 44px"
                                    alt={"avt"}
                                    loading="eager"
                                    priority
                                    width={44}
                                    height={44}
                                    onError={handleImageError}
                                    className="rounded-full object-cover h-11"
                                    onClick={() => handleShowInfoGroupChat(conversationOfThisMessage)}
                                />
                                {conversationOfThisMessage?.member ? null : (
                                    <div
                                        className={`${statusUser ? `bg-green-500` : `bg-red-500`
                                        } w-3 h-3 rounded-full absolute bottom-0 right-0 border-2 border-thNewtral1`}
                                    ></div>
                                )}
                                {/* <div
                  className={`${
                    statusUser ? ` dark:bg-thPrimary bg-light-thPrimary` : `bg-red-500`
                  } w-3 h-3 rounded-full absolute bottom-0 right-0 border-2 border-thNewtral1`}
                ></div> */}
                            </div>
                            <div className="flex flex-col dark:text-thWhite text-light-thWhite">
                                {/* Mã hóa đầu cuối => logic */}
                                {
                                    encryptionConversation ? (
                                        <p className="text-base font-semibold flex items-center mb-1">
                                            {conversation?.otherUsername || ""}
                                            {/* <Image
                                                src={LockIcon}
                                                sizes="(max-width: 16px) 16px, 16px"
                                                alt={"avt"}
                                                loading="eager"
                                                priority
                                                width={20}
                                                height={20}
                                                className="rounded-full object-cover h-4 ml-[7px] -translate-y-[3px]"
                                            /> */}
                                            <MdLock size={20} className="rounded-full object-cover h-4 ml-[7px] -translate-y-[3px] dark:text-white text-light-thNewtral2" />
                                        </p>
                                    ) : (
                                        <p className="text-base font-semibold">
                                            {conversationOfThisMessage?.type == 1 ? dataStorage[conversationOfThisMessage?.idOther] || conversation?.otherUsername : conversation?.otherUsername}
                                        </p>
                                    )
                                }
                                <span className="text-xs font-normal opacity-70">
                                    {/* online now{" "} */}
                                    {conversationOfThisMessage?.member
                                        ? null
                                        : statusUser
                                            ? "Online Now"
                                            : lastLogin}
                                </span>
                            </div>
                        </div>
                        {
                            isOpenInforConversation && (
                                <ModalInforConversation
                                    isOpen={isOpenInforConversation}
                                    onClose={() => setIsOpenInforConversation(false)}
                                    conversation={conversationOfThisMessage}

                                    reminiscentName={dataStorage[conversationOfThisMessage?.idOther] || conversation?.otherUsername}
                                    commonGroup={commonGroup}
                                    setIsOpenInfo={setIsOpenInfo}

                                />
                            )
                        }
                        {
                            isOpenInfoGroupChat && (
                                <ModalInforGroupChat
                                    isOpen={isOpenInfoGroupChat}
                                    onClose={() => setIsOpenInfoGroupChat(false)}
                                    dataGroup={conversationOfThisMessage}
                                    socket={socket}
                                    reminiscentName={dataStorage[conversationOfThisMessage?.idOther] || conversationOfThisMessage?.name}
                                    setIsOpenInfo={setIsOpenInfo}
                                />
                            )
                        }
                        <div className="flex gap-x-3 items-center">
                            {/* <Image
                                src={phone}
                                sizes="(max-width: 24px) 24px, 24px"
                                alt={"avt"}
                                loading="eager"
                                priority
                                width={24}
                                height={24}
                                className="rounded-full h-10 cursor-pointer"
                                onClick={handleVoiceCall}
                            />
                            <Image
                                src={videoCall}
                                sizes="(max-width: 24px) 24px, 24px"
                                alt={"avt"}
                                loading="eager"
                                priority
                                width={24}
                                height={24}
                                className="rounded-full h-10 cursor-pointer"
                                onClick={handleVideoCall}
                            />
                            {/* <Image
                                src={info}
                                sizes="(max-width: 24px) 24px, 24px"
                                alt={"avt"}
                                loading="eager"
                                priority
                                width={24}
                                height={24}
                                className="rounded-full h-10 cursor-pointer"
                                onClick={() => setIsOpenInfo(true)}
                            /> */}

                            <IoCall size={24} className="rounded-full h-10 cursor-pointer text-light-thPrimary dark:text-thPrimary"
                                onClick={handleVoiceCall} />
                            <FaVideo size={24} className="rounded-full h-10 cursor-pointer text-light-thPrimary dark:text-thPrimary" onClick={handleVideoCall} />
                            {/* <IoIosInformationCircle size={24} className="rounded-full h-10 cursor-pointer text-light-thPrimary dark:text-thPrimary" onClick={() => setIsOpenInfo(true)} /> */}
                            <AiFillExclamationCircle
                                size={24}
                                onClick={() => setIsOpenInfo(true)}
                                className={`rounded-full h-10 text-light-thPrimary dark:text-thPrimary cursor-pointer ${(detailChat?.members.find((item: any) => item.id === user.id)?.type === CONVERSATION.ROLE_TYPE.OWNER &&
                                    detailChat?.members?.filter((item: any) => item.ownerAccepted === false).length > 0) ? styles.blinkingText : ""}`}

                            />
                        </div>
                        {
                            encryptionConversation && encryptionConversation?.isDecoded === false && (
                                <div className="absolute top-full left-0 w-full min-h-[50px] flex items-center px-1 z-40" style={{ backgroundColor: "#DC143C" }}>
                                    <span className="font-bold text-sm text-white">Cuộc trò chuyện đã được mã hóa hãy nhập mật khẩu để giải mã : </span>
                                    <div className="flex justify-between items-center">
                                        <button type="button" className="text-xs bg-none underline text-black font-bold px-1 rounded-md mr-[5px]" onClick={handleClickRequestPassword}>
                                            Yêu cầu mật khẩu
                                        </button>
                                        <button type="submit" className="text-xs bg-none underline text-black font-bold px-1 mr-1 rounded-md" onClick={handleClickContinue}>
                                            Nhập mật khẩu
                                        </button>
                                    </div>
                                </div>
                            )
                        }
                    </div>
                    <div
                        style={{ backgroundImage: `url(${backgroundImage})` }}
                        className={`h-full flex flex-col overflow-y-auto snap-end py-3 ${styles.inboxList} relative bg-contain bg-center`}
                        ref={divVirtualListRef}
                        onClick={() => setIsOpenEmoji(false)}
                    >
                        {isShowButtonDown && (
                            <button
                                className="absolute bottom-2 right-10 w-10 h-10 rounded-full bg-gray-300 z-50 flex justify-center items-center animate-bounce"
                                onClick={handleClickDown}
                            >
                                <BsChevronDoubleDown size={20} color="black" />
                            </button>
                        )}
                        <PinnedComponent
                            setIsShowNewsletter={setIsShowNewsletter}
                            setIsOpenInfo={setIsOpenInfo}
                            setMessageScroll={setMessageScroll}
                        />
                        <div className="mt-auto">
                            {/* {isPopup && (
                                <PopupCallInComing
                                    callInComing={isPopup}
                                    setCallInComing={setIsPopup}
                                    handCallAccept={handleCallAccept}
                                    handleCallDeny={handleCallDeny}
                                    userLogin={userLogin as IUserProfile}
                                />
                            )} */}

                            <div className="socket_conversation flex flex-col gap-y-2 px-4">
                                {mergeAllList && mergeAllList.length > 0 && (
                                    <Virtuoso
                                        data={mergeAllList}
                                        ref={virtuoso}
                                        style={{ height: heightVirtualList }}
                                        followOutput={handleFollowOutputHandler}
                                        initialTopMostItemIndex={mergeAllList?.length}
                                        onScroll={(e) => {
                                            const element = e.target as HTMLDivElement;
                                            if (
                                                Math.abs(
                                                    element.scrollTop +
                                                    element.clientHeight -
                                                    element.scrollHeight
                                                ) < 1
                                            ) {
                                                handleScrollOnBottom();
                                            }
                                            if (
                                                Math.abs(
                                                    element.scrollTop +
                                                    element.clientHeight -
                                                    element.scrollHeight
                                                ) > 250
                                            ) {
                                                setIsShowButtonDown(true);
                                            } else {
                                                setIsShowButtonDown(false);
                                            }
                                            if (element.scrollTop === 0) {
                                                handleScrollOnTop();
                                            }
                                        }}
                                        itemContent={(index, messageData: any) => {
                                            const curMsgDate = moment(messageData.createdAt).format(
                                                "DD-MM-YYYY"
                                            );

                                            const nextMsgDate =
                                                messages[index + 1] &&
                                                moment(messages[index + 1].createdAt).format(
                                                    "DD-MM-YYYY"
                                                );

                                            {/* Mã hóa đầu cuối => logic */ }
                                            let newMessageData: any;
                                            const encryptionConversation = endToEndEncryption.listIdConversationDecoded.find((c: any) => c.idConversation === idConversation);
                                            if (messageData.isHashed) {
                                                if (encryptionConversation?.isDecoded) {
                                                    newMessageData = {
                                                        ...messageData,
                                                        content: decryptMessage(encryptionConversation?.encryptionPassword, messageData.content)
                                                    };
                                                } else {
                                                    if (messageData.type !== 1) {
                                                        newMessageData = {
                                                            ...messageData,
                                                            content: "File đã bị mã hóa !",
                                                            type: 1
                                                        };
                                                    } else {
                                                        newMessageData = {
                                                            ...messageData,
                                                            content: "Tin nhắn đã bị mã hóa",
                                                            type: 1
                                                        };
                                                    }
                                                }
                                            } else {
                                                newMessageData = messageData;
                                            };
                                            return (
                                                <div key={`${messageData.id}${index}`}>
                                                    {index === 0 && (
                                                        <div className="my-6">
                                                            <p className="text-sm w-fit mx-auto dark: bg-thNewtral2 bg-light-thNewtral2 px-1.5 rounded opacity-60 py-3">
                                                                {curMsgDate}
                                                            </p>
                                                        </div>
                                                    )}
                                                    {
                                                        messageData.itemType === "message" &&
                                                        <>
                                                            {(messageData?.createdBy as ISeenUser)._id !==
                                                                user.id &&
                                                                (messageData?.createdBy as string) !== user.id ? (
                                                                    <div
                                                                        key={(messageData?.createdBy as ISeenUser)._id}
                                                                    >
                                                                        <PartnerText
                                                                            socket={socket}
                                                                            reactionID={reactionID}
                                                                            setReactionID={setReactionID}
                                                                            message={newMessageData}
                                                                            setForwardContent={setForwardContent}
                                                                            setMessageReply={setMessageReply}
                                                                            setMessageScroll={setMessageScroll}
                                                                            isGroup={isGroupRef}
                                                                            messagePrev={messages[index - 1]}
                                                                            setIdMediaModal={setIdMediaModal}
                                                                            setUrlMediaModal={setUrlMediaModal}
                                                                            setIsOpenMediaLibrary={setIsOpenMediaLibrary}
                                                                        />
                                                                    </div>
                                                                ) : (
                                                                    <div
                                                                        key={(messageData?.createdBy as ISeenUser)._id}
                                                                    >
                                                                        <OwnerText
                                                                            socket={socket}
                                                                            reactionID={reactionID}
                                                                            setReactionID={setReactionID}
                                                                            message={newMessageData}
                                                                            isLastMsg={
                                                                                messages[messages.length - 1]._id ===
                                                                            messageData?._id
                                                                            }
                                                                            setForwardContent={setForwardContent}
                                                                            setMessageReply={setMessageReply}
                                                                            setMessageScroll={setMessageScroll}
                                                                            isGroup={isGroupRef}
                                                                            setIdMediaModal={setIdMediaModal}
                                                                            setUrlMediaModal={setUrlMediaModal}
                                                                            setIsOpenMediaLibrary={setIsOpenMediaLibrary}
                                                                        />
                                                                    </div>
                                                                )}
                                                        </>
                                                    }
                                                    {
                                                        messageData?.itemType === "note" && detailChat?.type === CONVERSATION.TYPE.GROUP &&
                                                        <NoteActivity item={messageData} idUser={user.id} />
                                                    }
                                                    {
                                                        messageData?.itemType === "vote" && detailChat?.type === CONVERSATION.TYPE.GROUP &&
                                                        <VoteActivity item={messageData} idUser={user.id} action="CREATE" />
                                                    }
                                                    {
                                                        messageData?.itemType === "voteAnswers" && detailChat?.type === CONVERSATION.TYPE.GROUP &&
                                                        <VoteActivity item={messageData} idUser={user.id} action="UPDATE" />
                                                    }
                                                    {index >= 1 &&
                                                        curMsgDate !== nextMsgDate &&
                                                        nextMsgDate && (
                                                        <div className="my-6">
                                                            <p className="text-sm w-fit mx-auto dark: bg-thNewtral2 bg-light-thNewtral2 px-1.5 py-1 rounded opacity-60">
                                                                {nextMsgDate}
                                                            </p>
                                                        </div>
                                                    )}
                                                </div>
                                            );
                                        }}
                                    />
                                )}
                            </div>
                        </div>
                    </div>
                    <div className="absolute bottom-16 right-6">
                        {isOpenEmoji && (
                            <Picker
                                onEmojiClick={onEmojiClick}
                                height={337}
                                width={340}
                                searchDisabled={true}
                                theme={Theme.DARK}
                            />
                        )}
                    </div>
                    {Object.keys(forwardContent).length > 0 && (
                        <>
                            <div
                                className={`overlay absolute h-full w-full z-10 ${styles.glass} cursor-pointer`}
                                onClick={() => setForwardContent({})}
                            ></div>
                            <div className="w-[85vw] absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 z-50">
                                <Forward
                                    message={forwardContent}
                                    socket={socket}
                                    close={() => setForwardContent({})}
                                />
                            </div>
                        </>
                    )}

                    <div className="chat_area mt-auto py-4 px-4 ">
                        {(selectedImage.length > 0 || selectedFile.length > 0) && (
                            <div className="py-2">
                                <div
                                    className="flex justify-center items-center"
                                    onClick={() => {
                                        setSendMediaHd(!sendMediaHd);
                                    }}
                                >
                                    {sendMediaHd ? (
                                        <FaCheckCircle size={24} className="text-thPrimary mr-2" />
                                    ) : (
                                        <FaRegCheckCircle size={24} className="text-thWhite mr-2" />
                                    )}
                                    HD
                                </div>
                                <div className="flex items-center justify-between">
                                    <p className="mb-2 text-sm">
                                        <span className="text-thPrimary">
                                            {selectedImage.length + selectedFile.length}
                                        </span>
                                        Ảnh được chọn 👇
                                    </p>
                                    <div className="cursor-pointer" onClick={handleDeleteAll}>
                                        <IoIosCloseCircleOutline size={24} />
                                    </div>
                                </div>

                                <div className="flex gap-2 w-full overflow-x-auto ">
                                    {selectedImage?.map((image, index) => {
                                        return (
                                            <div
                                                className="w-24 h-24 rounded-md overflow-hidden relative"
                                                key={image?.name}
                                            >
                                                {image.type.startsWith("image") ? (
                                                    <Image
                                                        src={URL.createObjectURL(image)}
                                                        sizes="300px"
                                                        alt={"img"}
                                                        loading="eager"
                                                        priority
                                                        fill
                                                        className="object-cover dark:bg-thNewtral bg-light-thNewtral"
                                                    />
                                                ) : (
                                                    <video
                                                        src={URL.createObjectURL(image)}
                                                        controls
                                                        width={300}
                                                        height={300}
                                                        className="w-full h-full"
                                                    />
                                                )}

                                                {!isUploading && (
                                                    <div
                                                        className={`${styles.call_btn} absolute top-1 right-1 rounded-full !p-1`}
                                                        onClick={() => handleDeleteimg(index)}
                                                    >
                                                        <IoMdClose
                                                            size={24}
                                                            className="opacity-40 hover:opacity-100"
                                                        />
                                                    </div>
                                                )}
                                            </div>
                                        );
                                    })}
                                    {selectedFile?.map((file, index) => {
                                        return (
                                            <div
                                                className={`w-32 h-24 rounded-md overflow-hidden relative ${fileError &&
                                                    fileError.length > 0 &&
                                                    fileError.findIndex((f) => f.name === file.name) !==
                                                    -1 &&
                                                    " text-red-500 border-yellow-400"
                                                }`}
                                                key={file?.name}
                                            >
                                                {
                                                    // eslint-disable-next-line @next/next/no-img-element
                                                    <img
                                                        src={getFileIcon(
                                                            file.name.split(".").pop() as string
                                                        )}
                                                        alt="file icon"
                                                        width={50}
                                                        height={50}
                                                    />
                                                }
                                                <p className="text-xs">{file.name}</p>

                                                {!isUploading && (
                                                    <div
                                                        className={`${styles.call_btn} absolute top-1 right-1 rounded-full !p-1 `}
                                                        onClick={() => handleDeleteFile(index)}
                                                    >
                                                        <IoMdClose
                                                            size={24}
                                                            className="opacity-40 hover:opacity-100"
                                                        />
                                                    </div>
                                                )}
                                            </div>
                                        );
                                    })}
                                </div>
                            </div>
                        )}
                        <div className="flex gap-x-4 items-center">
                            <div className=" flex-col items-center justify-between w-full">
                                {messageReply && Object.keys(messageReply).length > 0 && (
                                    <div className="py-2 bg-gray-600 text-white rounded-md mb-2 flex justify-between">
                                        <div className="flex-col gap-2 w-full overflow-x-auto ml-2 border-l-blue-200 border-l-4 pl-2">
                                            <div className="flex">
                                                <BsReplyAllFill size={20} />{" "}
                                                <span>
                                                    Trả lời{" "}
                                                    <b>{`${messageReply?.createdBy.username || ""}`}</b>
                                                </span>
                                            </div>
                                            {messageReply.type === MESSAGE_TYPE.STICKER ? (
                                                <Image
                                                    alt={messageReply.content}
                                                    src={messageReply.content}
                                                    className="w-[40px] h-[40px]"
                                                    width={40}
                                                    height={40}
                                                />
                                            ) : messageReply.type === MESSAGE_TYPE.IMAGE ? (
                                                <div className={`flex gap-1 justify-start`}>
                                                    {JSON.parse(messageReply.content).map(
                                                        (item: string) => {
                                                            return (
                                                                <Image
                                                                    key={item}
                                                                    alt={item}
                                                                    src={item}
                                                                    className={`object-fill ${JSON.parse(messageReply.content).length >= 2
                                                                        ? "w-[50px] h-[50px]"
                                                                        : "w-[50px] h-[50px]"
                                                                    }`}
                                                                    width={
                                                                        JSON.parse(messageReply.content).length >= 2
                                                                            ? 70
                                                                            : 50
                                                                    }
                                                                    height={
                                                                        JSON.parse(messageReply.content).length >= 2
                                                                            ? 70
                                                                            : 50
                                                                    }
                                                                />
                                                            );
                                                        }
                                                    )}
                                                </div>
                                            ) : messageReply.type === MESSAGE_TYPE.TEXT ? (
                                                <span className="text-[.9375rem]">
                                                    {messageReply.content}
                                                </span>
                                            ) : messageReply.type === MESSAGE_TYPE.MEDIA ? (
                                                <video
                                                    src={messageReply.content}
                                                    width={100}
                                                    height={100}
                                                    className="w-[100px] h-auto"
                                                ></video>
                                            ) : messageReply.type === MESSAGE_TYPE.LINK ? (
                                                <span className="flex items-center text-blue-600 underline">
                                                    <BsLink45Deg size={30} /> {messageReply.content}
                                                </span>
                                            ) : (
                                                <span className="flex items-center">
                                                    {/* eslint-disable-next-line @next/next/no-img-element */}
                                                    <img
                                                        src={getFileIcon(
                                                            messageReply.content.split(".").pop() as string
                                                        )}
                                                        alt="icon"
                                                        width={50}
                                                        height={50}
                                                    />
                                                    <p className="ml-2">
                                                        {getFileName(messageReply.content)}
                                                    </p>
                                                </span>
                                            )}
                                        </div>
                                        <div className="flex items-center ">
                                            <p className="mb-2 text-sm"> </p>
                                            <div
                                                className="cursor-pointer px-2"
                                                onClick={() => setMessageReply({})}
                                            >
                                                <IoIosCloseCircleOutline size={24} />
                                            </div>
                                        </div>
                                    </div>
                                )}
                                <div className="dark:bg-thNewtral1 bg-light-thPrimary border-black dark:border-none w-full py-3 px-2 flex gap-x-4 justify-between items-center rounded-2xl relative">
                                    {
                                        (detailChat?.type === CONVERSATION.TYPE.GROUP && detailChat.members.find((u: any) => u?.id === user?.id)?.type === CONVERSATION.ROLE_TYPE.MEMBER_WAITING_ACCEPT) ?
                                            <div className="absolute inset-0 bg-thNewtral2 opacity-80 w-full h-full z-50 rounded-2xl flex items-center justify-center gap-2 p-2 text-xs">
                                                <FaLock size={20} /> <span className="text-center">Bạn cần chấp nhận vào group mới có thể chat</span>
                                                <button className="bg-thPrimary text-thDark-background rounded-md font-bold px-2 opacity-70 hover:opacity-100"
                                                    onClick={() => handleAcceptAndDeny(1)}
                                                >
                                                    Đồng ý
                                                </button>
                                                <button className="bg-thRed text-white rounded-md font-bold px-2 opacity-70 hover:opacity-100"
                                                    onClick={() => handleAcceptAndDeny(2)}>
                                                    Từ chối
                                                </button>
                                            </div> :
                                            detailChat.conversationSetting?.find(item => item.type === 6)?.value === false && detailChat?.members.find((item: any) => item.id === user.id)?.type === CONVERSATION.ROLE_TYPE.MEMBER &&
                                            <div className="absolute inset-0 bg-thNewtral2 opacity-80 w-full h-full z-50 rounded-2xl flex items-center justify-center gap-2 p-2 text-center">
                                                <FaLock size={16} /> <span>Tính năng gửi tin nhắn đã bị khóa</span>
                                            </div>

                                    }
                                    <input
                                        type="text"
                                        ref={inputRef}
                                        className="px-4 outline-none border-none w-full bg-transparent"
                                        placeholder="nhập gì đó..."
                                        onFocus={() => setIsFocused(true)}
                                        onBlur={() => {
                                            if (selectedFile.length > 0 || selectedImage.length > 0) {
                                                return;
                                            }
                                        }}
                                        onKeyDown={handleKeyDown}
                                    />
                                    <ul
                                        className={`flex items-center gap-x-3 opacity-60 ${styles.input_feature}`}
                                    >
                                        <li onClick={() => setIsOpenEmoji(!isOpenEmoji)}>
                                            <AiOutlineSmile size={20} />
                                        </li>
                                        <li>
                                            <label
                                                htmlFor="image-input"
                                                className="cursor-pointer"
                                                onClick={() => setSendMediaHd(false)}
                                            >
                                                <input
                                                    id="image-input"
                                                    type="file"
                                                    accept="image/*,video/*"
                                                    className="hidden"
                                                    multiple
                                                    onChange={handleImageChange}
                                                />
                                                <BsImage size={20} className="cursor-pointer" />
                                            </label>
                                        </li>
                                        <li>
                                            <label htmlFor="file-input" className="cursor-pointer">
                                                <input
                                                    id="file-input"
                                                    type="file"
                                                    accept=".doc,.docx,.pdf,.zip,.txt,.rar,.xlsx,.xlsm,.7z,.xz,.gz,.psd,.dng,.heic,.mp3,.wav,.ogg"
                                                    className="hidden"
                                                    multiple
                                                    onChange={handleFileChange}
                                                />
                                                <BsFileEarmarkText size={20} />
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div>
                                {isFocused ? (
                                    <div onClick={handleSubmit}>
                                        <Image
                                            src={Send}
                                            sizes="(max-width: 40px) 40px, 40px"
                                            alt={"avt"}
                                            loading="eager"
                                            priority
                                            width={40}
                                            height={40}
                                            onError={handleImageError}
                                            className="rounded-full h-10 cursor-pointer"
                                        />
                                    </div>
                                ) : (
                                    <div onClick={handleSlute}>
                                        <Image
                                            src={Slute}
                                            sizes="(max-width: 40px) 40px, 40px"
                                            alt={"avt"}
                                            loading="eager"
                                            priority
                                            width={40}
                                            onError={handleImageError}
                                            height={40}
                                            className="rounded-full h-10 cursor-pointer"
                                        />
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
                {isOpenInfo && (
                    <InfoChatMbl
                        idConversation={idConversation}
                        isOpenInfo={isOpenInfo}
                        setIsOpenInfo={setIsOpenInfo}
                        setIdMediaModal={setIdMediaModal}
                        setUrlMediaModal={setUrlMediaModal}
                        setIsOpenMediaLibrary={setIsOpenMediaLibrary}
                        heightVirtualList={heightVirtualList}
                        dataGroup={conversationOfThisMessage}
                        socket={socket}
                        isOpenNewsletter={isShowNewsletter}
                        setMessageScroll={setMessageScroll}
                        openImageUploadModal={openImageUploadModal}
                    />
                )}
                {isOpenMediaLibrary && (
                    <ModalMediaLibraryMbl
                        socket={socket}
                        idConversation={idConversation}
                        idMediaModal={idMediaModal}
                        urlMediaModal={urlMediaModal}
                        setIsOpenMediaLibrary={setIsOpenMediaLibrary}
                        setForwardContent={setForwardContent}
                        reactionID={reactionID}
                        setReactionID={setReactionID}
                        message={messages}
                        otherUsername={conversation?.otherUsername || ""}
                    />
                )}
                {
                    newsletter?.actionForNote?.isShowEditNote === true && (
                        <ModalNote
                            setIsShowModal={() => dispatch(setEndEditNote())}
                            idConversation={idConversation}
                        />
                    )
                }
                {
                    newsletter.isShowModalAnswersVote &&
                    newsletter.answersVoteItem &&
                    Object.keys(newsletter.answersVoteItem).length > 0 &&
                    <ModalAnswersVote />
                }
            </div>
        </>
    );
}
