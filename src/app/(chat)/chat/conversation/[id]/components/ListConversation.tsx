import { useEffect, useState } from "react";
import { filterConversation } from "@/src/utils/conversations/filterConversation";
import { MESSAGE_TYPE } from "@/src/constants/chat";
import { AiFillCheckCircle } from "react-icons/ai";
import { IConversation } from "@/src/types/Conversation";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { setOtherUser } from "@/src/redux/slices/conversationSlice";
import Link from "next/link";
import Image from "next/image";
import moment from "moment";
import axios from "axios";

interface ListConversationProps {
    listConversation: IConversation[],
    setListConversation: any,
    fetchConversations: any
}

function ListConversation({ listConversation, setListConversation, fetchConversations }: ListConversationProps): any {
    const { user, conversation } = useAppSelector((state) => state.user);
    const dispatch = useAppDispatch();
    const currentDateTime = moment();
    const unreadMessages = listConversation.reduce((total: number, conversation: IConversation) => total + conversation.unSeenMessageTotal, 0);
    const localStorageData = useAppSelector((state: any) => state.user);
    const [dataStorage, setDataStorage] = useState<any>([{}] as any);
    useEffect(() => {
        const dataString = localStorage.getItem("Othername");

        if (dataString) {
            setDataStorage(JSON.parse(dataString));
        }
    }, [localStorageData]);
    useEffect(() => {
        ("test");
        // if(Object.keys(conversation.lastMessage) || !conversation.lastMessage) return;
        (async () => {
            try {
                const { data } = await axios.get("/api/conversations");
                await fetchConversations();
                setListConversation(filterConversation(data, user.id));
            } catch (error) {
                console.log(error);
            }
        })();
    }, [conversation.lastMessage]);

    return (
        <div className="p-4 flex flex-col gap-y-6 flex-shrink-0 rounded-2xl  h-[90vh] lg:h-[85vh] mx-4 mt-4 overflow-y-auto">
            <div className="flex gap-x-1 items-center">
                <h1 className="text-xl font-semibold">Message</h1>
                <p className="bg-thRed font-medium text-sm px-2 h-fit rounded grid place-items-center">{unreadMessages}</p>
            </div>
            <div className="flex flex-col gap-y-2">
                {
                    listConversation.map((c: IConversation | any) => {
                        const checkNewMsg = conversation.lastMessage?.conversationId === c._id;
                        const userNewMessage = conversation.lastMessage?.createdBy?._id === user.id ? "Bạn: " : `${conversation.lastMessage?.createdBy?.username}: `;
                        const userMessage = c.latestMessage?.[c?.latestMessage?.length - 1]?.createdBy?._id === user.id ? "Bạn: " : `${c?.latestMessage?.[c?.latestMessage?.length - 1]?.createdBy?.username}: `;
                        const targetDate = moment(c?.latestMessage?.createdAt);
                        const checkDate = targetDate.isBefore(currentDateTime, 'day');
                        return (
                            <Link href={`/chat/conversation/${c._id}`} key={c._id}>
                                <div onClick={() => dispatch(setOtherUser({
                                    otherUsername: c?.name,
                                    otherAvatar: c?.avatar,
                                    lastMessage: conversation.lastMessage || {}
                                }))} className="flex gap-x-2 items-center py-2 px-2 dark:hover:bg-thNewtral1 hover:bg-light-thNewtral1  rounded-lg">

                                    <Image
                                        src={c?.avatar}
                                        sizes="(max-width: 56px) 56px, 56px"
                                        alt={"Logo"}
                                        loading="eager"
                                        priority
                                        width={56}
                                        height={56}
                                        className="rounded-full object-cover h-[56px]"
                                    />
                                    <div className="flex gap-x-8 justify-between h-full items-center flex-grow">
                                        <div>
                                            <p className="text-[.9375rem] font-semibold md:max-w-[72px] lg:max-w-[185px] text-ellipsis line-clamp-1">{dataStorage[c?.idOrder] ? dataStorage[c?.idOrder] : c?.name}</p>
                                            <span className={`${checkNewMsg && "font-bold"} text-[.8125rem] opacity-80 max-w-[195px] line-clamp-1`}>
                                                {checkNewMsg ?
                                                    (conversation.lastMessage.type === MESSAGE_TYPE.IMAGE || conversation.lastMessage.type === MESSAGE_TYPE.STICKER ? <Image alt="test" src={conversation.lastMessage.content} width={30} height={30} /> : <span>{`${userNewMessage}${conversation.lastMessage.content}`}</span>)
                                                    :
                                                    (c?.latestMessage?.type === MESSAGE_TYPE.IMAGE || c?.latestMessage?.type === MESSAGE_TYPE.STICKER ? <Image alt="test" src={c?.latestMessage?.content} width={30} height={30} /> : <span>{`${userMessage}: ${c?.latestMessage?.[c?.latestMessage?.length - 1]?.content ?? ''}`}</span>)}
                                            </span>
                                        </div>
                                        <div className="flex flex-col justify-between gap-y-1 items-end lg:ml-3">

                                            <span className="text-xs opacity-60">{!checkDate ? moment(c?.latestMessage?.createdAt).format('hh:mm') : moment(c?.latestMessage?.createdAt).format('DD/MM')}</span>
                                            {c?.latestMessage?.seen.length > 1 ? (
                                                <>
                                                    <Image
                                                        src={user.avatar}
                                                        sizes="(max-width: 16px) 16px, 16px"
                                                        alt={"Logo"}
                                                        loading="eager"
                                                        priority
                                                        width={16}
                                                        height={16}
                                                        className="rounded-full object-cover h-4"
                                                    />
                                                </>
                                            ) : (
                                                <>
                                                    <AiFillCheckCircle size={16} className="dark:text-thPrimary text-light-thPrimary" />
                                                </>
                                            )}
                                        </div>
                                    </div>
                                </div>
                            </Link>
                        );
                    })
                }
            </div>
        </div>
    );
}

export default ListConversation;
