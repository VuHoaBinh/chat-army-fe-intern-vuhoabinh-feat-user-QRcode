import { ReactElement } from "react";

export default function YoutubeEmbed({ url }: { url: string }): ReactElement {
    return (
        <div className="w-full inline-block">
            <iframe
                width="100%"
                height="315"
                src={`https://www.youtube.com/embed/${
                    /https?:\/\/(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:watch\?v=|embed\/|shorts\/))([a-zA-Z0-9-_\-]+)/.exec(
                        url
                    )?.[1]
                }`}
                title="YouTube video player"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen
            ></iframe>
        </div>
    );
}