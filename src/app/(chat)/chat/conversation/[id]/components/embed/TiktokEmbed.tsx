import useWindowDimensions from '@/src/hook/useWindowDimension';
import React, { useState, useRef, useEffect } from 'react';

const TikTokEmbed = ({ url }: { url: string }): React.ReactElement => {
    const [embedTikTok, setEmbedTikTok] = useState<string | null>(null);
    const tiktokRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
    // Chỉ fetch embed tiktok nếu url chứa "tiktok.com"
        if (url.includes('tiktok.com')) {
            (async () => {
                try {
                    const res = await fetch(`https://www.tiktok.com/oembed?url=${url}`);
                    const data = await res.json();
                    setEmbedTikTok(data.html);
                } catch (error) {
                    console.log(error);
                }
            })();
        }
    }, [url]);
    const windowDimensions = useWindowDimensions();
    useEffect(() => {
        if (!tiktokRef.current && !embedTikTok) return;
        const script = document.createElement('script');
        script.src = 'https://www.tiktok.com/embed.js';
        script.async = true;
        const divEl = document.createElement('div');
        if(windowDimensions.width <= 480){
            const regex = /style=\"max-width: ([0-9]+)px;min-width: ([0-9]+)px;\"/g;
            const newStyle = 'style=\"max-width: 100%;min-width: 300px;\"';
            const newText = embedTikTok?.replace(regex, newStyle);
            divEl.innerHTML = newText as string;
        }else{
            divEl.innerHTML = embedTikTok as string;
        }
        tiktokRef.current?.appendChild(divEl);
        tiktokRef.current?.appendChild(script);
    }, [embedTikTok, windowDimensions.width]);

    if (!embedTikTok) {
        return <div>Loading TikTok embed...</div>;
    }
    

    return <div className=''
        ref={tiktokRef}
    />;
};

export default TikTokEmbed;