export default function TwitchEmbed({
    url
}: {
  url: string;
}): React.ReactElement {
    const src = url.includes("clips.twitch.tv") ? url.split("/").pop() : url;
    const srcLink = url.includes("category") ? `https://player.twitch.tv/?collection=${url.split("/").pop()}` : url.includes("videos") ? `https://player.twitch.tv/?video=${url.split("/").pop()}` : `https://player.twitch.tv/?channel=${url.split("/").pop()}`;
    return (
        <div className="w-full inline-block">
            {
                url.includes("clips.twitch.tv") ?
                    <iframe
                        src={`https://clips.twitch.tv/embed?clip=${src}&parent=${window.location.hostname}`}
                        allowFullScreen={true}
                        scrolling="no"
                        width="100%"
                        title="Twitch video player"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; fullscreen"
                        className="h-[250px] max-[480px]:h-[200px] w-full"
                    ></iframe> :
                    <iframe
                        src={`${srcLink}&autoplay=false&parent=${window.location.hostname}&muted=true`}
                        width="400px"
                        title="Twitch video player"
                        allowFullScreen={true}
                        className="h-[250px] max-[480px]:h-[200px] w-full"
                    >
                    </iframe>
            }
        </div>
    );
}
