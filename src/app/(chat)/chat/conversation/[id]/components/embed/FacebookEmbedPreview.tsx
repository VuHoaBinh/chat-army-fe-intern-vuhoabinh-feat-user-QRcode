import useWindowDimensions from "@/src/hook/useWindowDimension";
import { ReactElement, useRef } from "react";
import { FacebookEmbed } from "react-social-media-embed";
export default function FacebookEmbedPreview({ url }: { url: string }): ReactElement {
    const divRef = useRef<HTMLDivElement>(null);
    const windowDimensions = useWindowDimensions();
    return (
        <div
            className=" inline-flex justify-center max-[480px]:w-full max-w-[480px]"
            ref={divRef}
        >
            {windowDimensions.width <= 480 &&
                (url.includes("watch") || url.includes("video")) ? (
                    <iframe
                        src={`https://www.facebook.com/plugins/video.php?height=200&href=${url}&show_text=false&width=270&t=0`}
                        width="270"
                        height="200"
                        style={{ border: "none", overflow: "hidden" }}
                        scrolling="no"
                        allowFullScreen={true}
                        allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"
                        title="VieBuzzDatVietVAC"
                        className="max-w-[400px] max-[480px]:max-w-full overflow-hidden h-[200px] !inline-block"
                    ></iframe>
                ) : url.includes("fb.watch") || url.includes("reel") ? (
                    <iframe
                        src={`https://www.facebook.com/plugins/video.php?height=476&href=${url}&show_text=false&width=476&t=0`}
                        style={{ border: "none", overflow: "hidden" }}
                        scrolling="no"
                        allowFullScreen={true}
                        allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"
                        title="VieBuzzDatVietVAC"
                        className="max-w-full max-[480px]:max-w-full overflow-hidden h-[300px] !inline-block"
                    ></iframe>
                ) : (
                    <div className="bg-white">
                        <FacebookEmbed url={url}   width={windowDimensions.width <= 480 ? windowDimensions.width - 80 : 325}/>
                    </div>
                )}
        </div>
    );
}
