import { ReactElement } from "react";

export default function InstagramEmbed({ url }: { url: string }): ReactElement {
    return (
        <div className="w-full inline-flex justify-center" >
            <iframe
                src={url.split("?")[0] + "embed?show_text=false"}
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen={true}
                title="Embedded Instagram"
                width={"100%"}
                className="h-[600px] max-[480px]:h-[430px] w-[310px] max-[480px]:w-full"
                loading="lazy"
                scrolling="no"
            ></iframe>
        </div>
    );
}
