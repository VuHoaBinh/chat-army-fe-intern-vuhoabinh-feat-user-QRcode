"use client";
import phone from "@/public/call2.svg";
// import { useAppSelector } from "@/src/redux/hook";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { setUserIsCalling } from "@/src/redux/slices/userSlice";
import Image from "next/image";
import { ReactElement, useRef, useState } from "react";
import { BiSolidMicrophone, BiSolidMicrophoneOff } from "react-icons/bi";
import { BsFillCameraVideoFill, BsFillCameraVideoOffFill } from "react-icons/bs";
import { HiSpeakerWave, HiSpeakerXMark } from "react-icons/hi2";
import styles from "../conversation.module.css";

import { updateCalling } from "@/src/redux/slices/callSlice";
import LoadingCall from "./LoadingCall";
// import { convertLegacyOperators } from "@mui/x-data-grid/internals";
interface FeatureState {
    mute: boolean;
    camera: boolean;
    speaker: boolean;
}

export default function Call(): ReactElement {
    const { user } = useAppSelector((state) => state.user);
    const [feature, setFeature] = useState<FeatureState>({
        mute: true,
        camera: true,
        speaker: false
    });
    const dispatch = useAppDispatch();
    const iframeRef = useRef(null);
    const handleOpenClick = (iframeRef: any, state: boolean) => {
        // Truy cập iframe và gọi phương thức Reset()
        if (iframeRef.current) {
            const iframeContentWindow = iframeRef.current.contentWindow;
            if (iframeContentWindow?.opencamera && !state) {
                iframeContentWindow.opencamera();
            } else {
                iframeContentWindow.closecamera();
            }
        }
    };
    const handleSourceClick = (iframeRef: any, state: boolean) => {
        // Truy cập iframe và gọi phương thức Reset()
        // (state);
        if (iframeRef.current) {
            const iframeContentWindow = iframeRef.current.contentWindow;
            if (iframeContentWindow?.mute && !state) {
                iframeContentWindow.mute();
            } else {
                iframeContentWindow.unmute();
            }
        }
    };
    const handleSpeakerClick = (iframeRef: any, state: boolean) => {
        // Truy cập iframe và gọi phương thức Reset()
        // (state);
        if (iframeRef.current) {
            const iframeContentWindow = iframeRef.current.contentDocument;
            if (iframeContentWindow?.querySelector("audio")) {
                if (!state) {
                    iframeContentWindow.querySelector("audio").muted = true;
                } else {
                    iframeContentWindow.querySelector("audio").muted = false;

                }
            }



        };
    }
        ;
    const [loading, setLoading] = useState(true);

    const handleLoadComplete = () => {
        // Your logic to be executed when onLoad finishes
        ('onLoad event has finished!');
        setLoading(false);
    };
    return (<div className={` w-full relative`}>
        <iframe ref={iframeRef} id="resultFrame" onLoad={handleLoadComplete} src="https://mdac.irics.vn/call" className="w-full h-full" allow="camera; microphone; display-capture; autoplay; clipboard-write;" title="call"></iframe>
        {
            (loading || user.is_watting_call) &&
            <LoadingCall />
        }
        <div className='feature flex justify-center w-full absolute z-50 bottom-4 gap-x-4'>
            <div className={`rounded-full ${styles.call_btn}`} onClick={() => { setFeature(prevState => ({ ...prevState, mute: !prevState.mute })); handleSourceClick(iframeRef, !feature.mute); }}> {feature.mute ? < BiSolidMicrophone size={24} className="dark:text-white text-light-thNewtral2" /> : < BiSolidMicrophoneOff size={24} className="dark:text-white text-light-thNewtral2" />}</div>
            <div className={`rounded-full ${styles.call_btn} `} onClick={() => { setFeature(prevState => ({ ...prevState, camera: !prevState.camera })); handleOpenClick(iframeRef, feature.camera); }}>{feature.camera ? < BsFillCameraVideoFill size={24} className="dark:text-white text-light-thNewtral2" /> : < BsFillCameraVideoOffFill className="dark:text-white text-light-thNewtral2" size={24} />}</div>
            <div className={`rounded-full ${styles.call_btn} `} onClick={() => { setFeature(prevState => ({ ...prevState, speaker: !prevState.speaker })); handleSpeakerClick(iframeRef, feature.speaker); }}>
                {
                    feature.speaker === true ? (< HiSpeakerXMark size={24} className="dark:text-white text-light-thNewtral2" />) : (< HiSpeakerWave size={24} className="dark:text-white text-light-thNewtral2" />)
                } </div><div className={`rounded-full !bg-thRed ${styles.call_btn}`} onClick={() => {
                // handleCallOut();

                dispatch(updateCalling({ isCalling: false, conversationId: "", userAcceptCall: "", userCreatCall: "", deviceCall: "", deviceAcceptCall: "" }));

                dispatch(setUserIsCalling(false));
            }}><Image src={phone} sizes="32px" alt={"avt"} loading="eager" priority width={24} height={24} className="rounded-full h-6 cursor-pointer text-thWhite" /></div></div></div>);
};