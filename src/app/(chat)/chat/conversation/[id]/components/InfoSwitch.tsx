import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { setListConversations, setListHiddenConversation } from "@/src/redux/slices/conversationSlice";
// import { IConversation } from "@/src/types/Conversation";
import axiosClient from "@/src/utils/axios/axiosClient";
import { filterConversation, sortConversationByNewest } from "@/src/utils/conversations/filterConversation";
import React, { ReactElement, cache, useCallback, useEffect, useState } from "react";
interface Props {
    idConversation: string;
    setIsOpenSetPinHidden: (isOpenSetPinHidden: boolean) => void;
    setIsHidden: (isHidden: boolean) => void;
}

const useConfirm = () => {
    const confirmAction = useCallback((message: string) => {
        return new Promise<void>((resolve, reject) => {
            const result = window.confirm(message);
            if (result) {
                resolve();
            } else {
                reject();
            }
        });
    }, []);

    return confirmAction;
};
const fetchConversations = cache(() =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations?offset=0&status=[2]&limit=20&type=1`
    )
);

const InfoSwitch = ({
    idConversation,
    setIsOpenSetPinHidden,
    setIsHidden
}: Props): ReactElement => {
    const [isChecked, setIsChecked] = useState(false);
    const conversation = useAppSelector((state) => state.user.conversation);
    const dispatch = useAppDispatch();

    const confirmAction = useConfirm();
    const user = useAppSelector((state) => state.user.user);

    const fetchData = useCallback(async () => {
        const { data } = await fetchConversations();
        return filterConversation(data, user.id);
    }, [user.id]);
    const handleToggle = async () => {
        if (isChecked) {
            try {
                await confirmAction("Bạn có muốn bỏ ẩn trò chuyện?");
                const body: {
                    conversationId: string;
                } = {
                    conversationId: idConversation
                };
                axiosClient()
                    .post(
                        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/unhidden`,
                        body
                    )
                    .then(() => {
                        if (conversation.listHiddenConversation) {
                            const filteredConversations =
                                conversation.listHiddenConversation.filter(
                                    (conversation) => conversation._id !== idConversation
                                );
                            dispatch(setListHiddenConversation(filteredConversations));
                        }
                        setIsChecked(false);
                        setIsHidden(false);
                        fetchData().then((data) => {
                            dispatch(setListConversations(sortConversationByNewest(filterConversation(data, user.id))));
                        });
                    })
                    .catch((error: any) => {
                        console.log(error);
                    });
            } catch (e) {
                console.log(e);
            }
        } else {
            setIsOpenSetPinHidden(true);
        }
    };
    // Update listConver
    // const user = useAppSelector((state) => state.user.user);

    // const fetchData = useCallback(async () => {
    //     const { data } = await fetchConversations();
    //     return filterConversation(data, user.id);
    // }, [user.id]);
    // useEffect(() => {
    //     fetchData().then((data) => {
    //         setListConversation(sortConversationByNewest(data));
    //     });
    // eslint-disable-next-line react-hooks/exhaustive-deps
    // }, [user.id]);
    // const [listConversation, setListConversation] = useState<IConversation[]>(
    //     sortConversationByNewest(
    //         filterConversation(conversation.listConversation, user.id)
    //     )
    // );
    // useEffect(() => {
    //     dispatch(setListConversations(listConversation));
    // // eslint-disable-next-line react-hooks/exhaustive-deps
    // }, [listConversation]);

    useEffect(() => {
        if (conversation.listHiddenConversation) {
            const isIdInHiddenConversations =
                conversation.listHiddenConversation.some(
                    (item: any) => item.id === idConversation
                );
            if (isIdInHiddenConversations) {
                setIsChecked(true);
                setIsHidden(true);
            } else {
                setIsChecked(false);
                setIsHidden(false);
            }
        }
    }, [conversation.listHiddenConversation]);

    return (
        <label className="relative inline-flex items-center cursor-pointer">
            <input
                type="checkbox"
                value=""
                className="sr-only peer"
                checked={isChecked}
                onChange={handleToggle}
                id="toggle"
            />
            <div className="w-11 h-6 dark: bg-thNewtral2 bg-light-thNewtral2 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full  after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white  after:rounded-full after:h-5 after:w-5 after:transition-all  peer-checked:dark:bg-thPrimary peer-checked:bg-light-thPrimary"></div>
        </label>
    );
};

export default InfoSwitch;
