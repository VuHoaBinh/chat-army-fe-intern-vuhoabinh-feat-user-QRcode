"use client";

import { IBaseUser } from "@/src/types/User";
import Image from "next/image";
import { ChangeEvent, ReactElement, useEffect, useState } from "react";
import { useDebounce } from "use-debounce";

interface Props {
    listMember: any;
    setIsOpenModalAddMemberToCall: React.Dispatch<React.SetStateAction<boolean>>;
    handleCall: (type: number) => void;
    typeOfCallGroup: number;
};

export default function ModalAddMemberToCall({ listMember, setIsOpenModalAddMemberToCall, handleCall, typeOfCallGroup }: Props): ReactElement {
    const [inputSearchValue, setInputSearchValue] = useState<string>("");
    const [value] = useDebounce(inputSearchValue, 200);

    const [listResultSearch, setListResultSearch] = useState<IBaseUser[]>(listMember);
    const [listCheckedMember, setListCheckedMember] = useState<IBaseUser[]>([]);

    useEffect(() => {
        const newListMember = listMember.filter((m: IBaseUser) => m?.username?.includes(value));
        setListResultSearch(newListMember);
    }, [value, listMember]);

    const handleCheckedMember = (member: IBaseUser) => {
        // nếu có rồi là bỏ check => xóa member trong list
        if (listCheckedMember.find(m => m.id === member.id)) {
            const newListCheckedMember = listCheckedMember.filter(m => m.id !== member.id);
            setListCheckedMember(newListCheckedMember);
            return;
        }
        // check => bỏ member vô list
        setListCheckedMember(prev => [...prev, member]);
    };
    const handleCheckAll = (e: ChangeEvent<HTMLInputElement>) => {
        if (e.target.checked) {
            setListCheckedMember([...listResultSearch]);
        } else {
            setListCheckedMember([]);
        }
    };
    const handleClickStartCall = () => {
        if (listCheckedMember.length > 0) {
            // list user được add vào cuộc gọi là : listCheckedMember

            setIsOpenModalAddMemberToCall(false);
            if (typeOfCallGroup !== 0) {
                handleCall(typeOfCallGroup);
            }
        } else {
            alert("Hãy chọn thành viên để bắt đầu cuộc gọi !");
        }
    };

    return (
        <div className="w-screen h-screen fixed left-0 top-0 z-50 flex justify-center items-center" onClick={() => setIsOpenModalAddMemberToCall(false)}>
            <div className="bg-black w-[300px] h-auto max-h-[400px] overflow-hidden rounded-md" onClick={(e) => e.stopPropagation()}>
                <div id="dropdownSearch" className="z-10 p-2 pb-3 w-full h-full bg-black rounded-lg shadow">
                    <div className="p-2">
                        <label htmlFor="input-group-search" className="sr-only">Tìm tên</label>
                        <div className="relative dark:bg-thNewtral1 bg-light-thNewtral1 rounded-lg">
                            <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                <svg className="w-4 h-4 text-gray-500" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                                    <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z" />
                                </svg>
                            </div>
                            <input value={inputSearchValue} onChange={(e) => {
                                if (e.target.value.trim() === "") {
                                    setInputSearchValue("");
                                    return;
                                };
                                setInputSearchValue(e.target.value);
                            }} type="text" id="input-group-search" className="bg-thNewtral1 text-white text-sm rounded-lg block w-full pl-10 p-2 outline-none " placeholder="Search user" />
                        </div>
                    </div>
                    <ul className="h-[250px] max-h-[250px] px-3 pb-3 overflow-y-auto text-sm text-white" aria-labelledby="dropdownSearchButton">
                        <p className="text-left text-sm font-bold text-white">Thành viên nhóm ({listMember.length})</p>
                        {
                            // chưa search gì
                            listMember.length === listResultSearch.length && (
                                <li>
                                    <div className="flex items-center p-2 rounded hover:bg-thNewtral1">
                                        <input id="selectAll" onChange={handleCheckAll} checked={listResultSearch.length === listCheckedMember.length} type="checkbox" value="" className="w-4 h-4 text-thPrimary bg-gray-100 border-gray-300 rounded focus:text-thPrimary checked: dark:bg-thPrimary bg-light-thPrimary " />
                                        <label htmlFor="selectAll" className="ml-2 text-left text-sm text-white cursor-pointer">Chọn tất cả</label>
                                    </div>
                                </li>
                            )
                        }
                        {
                            listResultSearch?.map((member: IBaseUser) => {
                                return (
                                    <li key={member?.id}>
                                        <div className="flex items-center p-2 rounded hover:bg-thNewtral1">
                                            <input onChange={() => handleCheckedMember(member)} checked={listCheckedMember.find(m => m.id === member.id) ? true : false} id={`${member?.id}`} type="checkbox" value="" className="w-4 h-4 text-thPrimary bg-gray-100 border-gray-300 rounded focus:text-thPrimary checked: dark:bg-thPrimary bg-light-thPrimary " />
                                            <label htmlFor={`${member?.id}`} className="w-full ml-2 text-sm font-medium text-white rounded cursor-pointer flex items-center">
                                                <Image
                                                    src={member?.avatar === "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg"
                                                        ? "/default_avatar.jpg"
                                                        : member?.avatar}
                                                    width={30}
                                                    height={30}
                                                    alt="Avatar of the member"
                                                    className="rounded-full object-cover"
                                                />
                                                <p className="ml-2">{member?.username}</p>
                                            </label>
                                        </div>
                                    </li>
                                );
                            })
                        }
                    </ul>
                    <div className="bg-thNewtral1 text-thPrimary font-bold text-center py-2 rounded-3xl hover:bg-thNewtral2">
                        <button onClick={handleClickStartCall}>Bắt đầu gọi</button>
                    </div>
                </div>
            </div>
        </div>
    );
};