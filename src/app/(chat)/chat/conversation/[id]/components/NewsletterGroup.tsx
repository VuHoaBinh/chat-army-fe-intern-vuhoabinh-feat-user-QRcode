import { CONVERSATION } from "@/src/constants/chat";
import useWindowDimensions from "@/src/hook/useWindowDimension";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { setNotes, setStartAnswersVote, setStartEditNote, setVotes } from "@/src/redux/slices/newsletterSlice";
import getAxiosClient from "@/src/utils/axios/axiosClient";
import { sortNotes, sortVotes } from "@/src/utils/newsletter/sortHepper";
import { Popconfirm, message } from "antd";
import moment from "moment";
import Image from "next/image";
import { ReactElement, SyntheticEvent, cache, useEffect, useState } from "react";
import { AiFillCheckCircle, AiOutlineMessage } from "react-icons/ai";
import { FiChevronLeft, FiTrash2 } from "react-icons/fi";
import { MdOutlineStickyNote2 } from "react-icons/md";
import userAvatar from "@/public/user.png";
interface IProps{
    close: () => void,
    option?: number,
    setIsShowModalVote: React.Dispatch<React.SetStateAction<boolean>>,
    setMessageScroll: React.Dispatch<any>,
    closeInfoChat: () => void
}
const fetchNotes = cache((id: string) =>
    getAxiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/note/${id}?limit=20&offset=0`
    )
);
const fetchVotes = cache((id: string) =>
    getAxiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/vote/${id}?limit=20&offset=0`
    )
);
export default function NewsletterGroup({ close, option = 0, setIsShowModalVote, setMessageScroll, closeInfoChat }: IProps): ReactElement {
    const [tab, setTab] = useState(option);
    const { newsletter, detailChat, user } = useAppSelector((state) => state.user);
    const dispatch = useAppDispatch();
    useEffect(()=>{
        (async()=>{
            try {
                const response = await fetchNotes(detailChat?.id);
                if(response.status === 200){
                    dispatch(setNotes(sortNotes(response.data)));
                }
                const response2 = await fetchVotes(detailChat?.id);
                if(response2.status === 200){
                    dispatch(setVotes(sortVotes(response2.data)));
                }
            } catch (error) {
                console.log(error);
            }
        })();
    }, [detailChat?.id, dispatch]);
    const [mergeList, setMergeList] = useState<any[]>([]);
    useEffect(()=>{
        const mergeAllList = [
            ...[...newsletter.messagePinned || []].map((item) => ({
                ...item,
                item: "messagePinned"
            })),
            ...[...newsletter.noteList || []].map((item) => {
                return {
                    ...item,
                    item: "notePinned"
                };
            }),
            ...[...newsletter.voteList || []].map((item) => ({
                ...item,
                item: "votePinned"
            }))
        ];
        setMergeList([...mergeAllList].sort((a, b) => new Date(b?.createdAt).getTime() - new Date(a?.createdAt).getTime()));
    }, [newsletter.messagePinned, newsletter.noteList, newsletter.voteList]);
    const handleDeleteNote = async(id: string) => {
        try {
            const response = await getAxiosClient().delete(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/note/${id}`);
            if(response.status === 200){
                const newListNotes = newsletter?.noteList?.filter((item) => item.id !== id);
                dispatch(setNotes(sortNotes(newListNotes || [])));
                message.success("Xoá ghi chú thành công");
            }
        } catch (error) {
            console.log(error);
        }
    };
    const handleImageError = (event: SyntheticEvent<HTMLImageElement, Event>) => {

        const imgElement = event.target as HTMLImageElement;
        imgElement.src = userAvatar.src;
    };
    const windowDimensions = useWindowDimensions();
    const renderMessagePin = (item: any)=>{
        return (
            <div className="rounded-md border border-gray-500 p-2 mt-2" key={item?.id}>
                <div className="text-sm flex items-center gap-2"><AiOutlineMessage size={16}/> <span>Tin ghim</span></div>
                <div className="flex gap-2 items-center">
                    <Image className="rounded-full" src={item.message.createdBy?.avatar} alt="avt"  onError={handleImageError} width={42} height={42} />
                    <div>
                        <p className="font-['Roboto'] font-bold ">{item.message.createdBy?.username}</p>
                        <p className="font-['Roboto'] ">{item.message?.content}</p>
                    </div>
                </div>
                <div className="w-full flex items-center gap-2">
                    <span className="text-thNewtral2">{moment(item.createdAt).fromNow()}</span>|
                    <button className="text-blue-600 font-semibold"
                        onClick={() => {
                            if(windowDimensions.width < 1024){
                                close();
                                closeInfoChat();
                            }
                            setMessageScroll(item.message);
                        }}
                    >Xem tin nhắn gốc</button>
                </div>
            </div>
        );
    };
    const renderNote = (item: any)=>{
        return (
            <div className="rounded-md border border-gray-500 p-2 mt-2" key={item?.id}>
                <div className="text-sm flex items-center gap-2"><MdOutlineStickyNote2 size={16}/> <span>Ghi chú</span></div>
                <div className="flex gap-2 items-center">
                    <Image className="rounded-full" src={item?.createdBy?.avatar} alt="avt" onError={handleImageError} width={42} height={42} />
                    <div>
                        <p className="font-['Roboto'] font-bold ">{item.createdBy?.username}</p>
                        <p className="font-['Roboto'] truncate max-w-[180px] ">{item?.content}</p>
                    </div>
                </div>
                <div className="w-full flex items-center gap-2 divide-x-2 divide-gray-600">
                    <span className="text-thNewtral2">{moment(item?.createdAt).fromNow()}</span>
                    <button className="px-2 text-blue-600 font-semibold" onClick={()=>dispatch(setStartEditNote({ note: item, typeAction:"VIEW" }))}>Xem ghi chú</button>
                    {
                        (isOwner || isAdmin || item?.createdBy?.id === user?.id) &&
                            <Popconfirm
                                title="Xóa ghi chú"
                                description="Bạn có muốn xóa ghi chú?"
                                okText="Xóa"
                                cancelText="Hủy"
                                onConfirm={()=>handleDeleteNote(item.id)}
                                okButtonProps={
                                    {
                                        className: "text-red-600 font-semibold"
                                    }
                                }
                            >
                                <button className="text-red-600 font-semibold px-2">
                                    <FiTrash2 />
                                </button>
                            </Popconfirm>
                    }
                </div>
            </div>
        );
    };
    const renderVote = (item: any)=>{
        const isVoted = item.conversationVoteOptions?.some((option: any) => option.conversationVoteAnswers.some((answer: any) => answer?.user.id === user.id)) || false;
        const totalMembersVoted = 
            (item.hideResultBeforeAnswers && !isVoted) 
                ? new Set(item?.conversationVoteOptions?.map((option: any) => option.conversationVoteAnswers.map((answer: any) => answer?.user.id)).flat()).size > 1 ? 1 : 0 :
                new Set(item?.conversationVoteOptions?.map((option: any) => option.conversationVoteAnswers.map((answer: any) => answer?.user.id)).flat()).size || 1;
        return (
            <div className="rounded-md border border-gray-500 p-2 mt-2" key={item.id}>
                <h2 className="font-['Roboto'] font-bold py-2 text-lg truncate max-w-[180px]">{item.question}</h2>
                {
                    item?.allowMultipleAnswers &&
                                            <p className="text-sm text-thNewtral2 py-2">Chọn nhiều phương án</p>
                }
                {
                    item?.status === 2 &&
                                            <p className="text-sm text-thNewtral2 pb-2">Kết thúc {moment(item?.updatedAt).fromNow()}</p>
                }
                <div className="flex gap-2 flex-col ">
                    {
                        item.conversationVoteOptions?.map((option: any)=>{
                            const totalVotesForOption = (item.hideResultBeforeAnswers && !isVoted) 
                                ? option.conversationVoteAnswers.some((answer: any) => answer?.user.id === user.id) ? 1 : 0 : option?.conversationVoteAnswers?.length;

                            const optionPercentage = totalMembersVoted !== 0
                                ? (totalVotesForOption / totalMembersVoted) * 100
                                : 0;
                            return (
                                <div className="cursor-pointer flex items-center gap-2" key={option.id} onClick={()=>{dispatch(setStartAnswersVote(item as any));}}>
                                    <div className="cursor-pointer flex justify-between items-center p-2 border border-thPrimary rounded-md relative flex-1">
                                        <div className="absolute inset-0 bg-thPrimary bg-opacity-60 transition-all  rounded-md" style={{ width: `${optionPercentage}%` }}></div>
                                        <span>{option?.option}</span>
                                        {
                                            (option.conversationVoteAnswers.find((item: any)=>item.user.id === user?.id) ) ? 
                                                <AiFillCheckCircle
                                                    size={16}
                                                    className="cursor-pointer"
                                                /> :
                                                <div className={` h-3 w-3 border-[1.5px] border-thNewtral2 bg-transparent rounded-full cursor-pointer`}></div>
                                        }
                                    </div>
                                    <span className="text-thWhite">{totalVotesForOption }</span>
                                </div>
                            );
                        })
                    }
                </div>
                <button type="button" className="w-full mt-2 rounded-md py-2 border border-thPrimary text-thWhite hover:bg-thPrimaryHover opacity-90 hover:opacity-100 font-bold" 
                    onClick={()=>{dispatch(setStartAnswersVote(item as any));}}
                >
                                            Đổi lựa chọn
                </button>
            </div>
        );
    };
    const isTypeWaitting = detailChat?.members.find((item: any) => item.id === user?.id)?.type === CONVERSATION.ROLE_TYPE.MEMBER_WAITING_ACCEPT;
    const isOwner  = detailChat?.members.find((item: any) => item.id === user?.id)?.type === CONVERSATION.ROLE_TYPE.OWNER;
    const isAdmin = detailChat?.members.find((item: any) => item.id === user?.id)?.type === CONVERSATION.ROLE_TYPE.ADMIN;
    return (
        <div className={`block h-full mt-4 relative overflow-x-hidden max-[480px]:px-2`}>
            <div className="flex gap-x-4 items-center">
                <div className="cursor-pointer" onClick={() => close()}>
                    <FiChevronLeft size={40} />
                </div>
                <p className="text-lg font-semibold">Bảng tin nhóm</p>
            </div>
            <div className="mt-2 overflow-hidden p-2 relative">
                <div className="text-sm font-medium text-center text-gray-500 border-b border-gray-200 dark:text-gray-400 dark:border-gray-700">
                    <ul className="flex -mb-px gap-2">
                        <li>
                            <button onClick={()=>setTab(0)} className={`inline-block whitespace-nowrap p-4 md:p-2 lg:p-1 ${tab === 0 ? 'text-blue-600 border-blue-600 active' : 'hover:text-gray-300 border-transparent'} border-b-2  rounded-t-lg `}>Tất cả</button>
                        </li>
                        <li>
                            <button onClick={()=>setTab(1)} className={`inline-block whitespace-nowrap p-4 md:p-2 lg:p-1 ${tab === 1 ? 'text-blue-600  border-blue-600 active' : 'hover:text-gray-300 border-transparent'} border-b-2 rounded-t-lg `}>Tin ghim</button>
                        </li>
                        <li>
                            <button onClick={()=>setTab(2)} className={`inline-block whitespace-nowrap p-4 md:p-2 lg:p-1 ${tab === 2 ? 'text-blue-600 border-blue-600 active' : 'hover:text-gray-300 border-transparent'} border-b-2  rounded-t-lg `}>Ghi chú</button>
                        </li>
                        <li>
                            <button onClick={()=>setTab(3)} className={`inline-block whitespace-nowrap p-4 md:p-2 lg:p-1 ${tab === 3 ? 'text-blue-600 border-blue-600 active' : 'hover:text-gray-300 border-transparent'} border-b-2  rounded-t-lg `}>Bình chọn</button>
                        </li>
                    </ul>
                </div>
                {
                    tab === 0 && 
                    <div className="w-full h-[70vh] overflow-y-auto p-2 pt-0">
                        <div className="flex justify-between items-center p-2 flex-col gap-2 sticky top-0 bg-thNewtral z-10">
                            {
                                (detailChat?.conversationSetting?.find((item) => item.type === 4)?.value === true && !isTypeWaitting) ||
                                (isOwner || isAdmin) ?
                                    <button type="button" className="w-full rounded-md py-2 bg-thNewtral2 text-thWhite hover:bg-thPrimaryHover opacity-90 hover:opacity-100 font-bold" 
                                        onClick={()=>{dispatch(setStartEditNote({ note: undefined, typeAction: "CREATE" }));}}
                                    >
                                    Tạo ghi chú
                                    </button> :
                                    null
                            }
                            {
                                (detailChat?.conversationSetting?.find((item) => item.type === 5)?.value === true && !isTypeWaitting) ||
                                (isOwner || isAdmin) ?
                                    <button type="button" className="w-full rounded-md py-2 bg-thNewtral2 text-thWhite hover:bg-thPrimaryHover opacity-90 hover:opacity-100 font-bold" 
                                        onClick={()=>{setIsShowModalVote(true);}}
                                    >
                                        Tạo bình chọn
                                    </button> : null
                            }
                        </div>
                        {
                            mergeList?.map((item: any)=>{
                                if (item?.item === "messagePinned")
                                {
                                    return renderMessagePin(item);
                                }else if(item?.item === "notePinned"){
                                    return renderNote(item);
                                }
                                return renderVote(item);
                            })
                        }
                    </div>
                }
                {
                    tab === 1 &&
                    <div className="w-full h-[70vh] overflow-y-auto p-2">
                        {
                            [...newsletter.messagePinned]?.sort((a, b) => new Date(b?.createdAt).getTime() - new Date(a?.createdAt).getTime())?.map((item)=>{
                                return (
                                    renderMessagePin(item)
                                );
                            })
                        }
                    </div>
                }
                {
                    tab === 2 && 
                    <div className="w-full h-[70vh] overflow-y-auto p-2 relative pt-0">
                        <div className="flex justify-between items-center p-2 flex-col gap-2 sticky top-0 bg-thNewtral z-10">
                            {
                                (detailChat?.conversationSetting?.find((item) => item.type === 4)?.value === true && !isTypeWaitting) ||
                                (isOwner || isAdmin) ?
                                    <button type="button" className="w-full rounded-md py-2 bg-thNewtral2 text-thWhite hover:bg-thPrimaryHover opacity-90 hover:opacity-100 font-bold" 
                                        onClick={()=>{dispatch(setStartEditNote({ note: undefined, typeAction: "CREATE" }));}}
                                    >
                                    Tạo ghi chú
                                    </button> :
                                    null
                            }
                        </div>
                        {
                            newsletter.noteList?.map((item)=>{
                                return (
                                    renderNote(item)
                                );
                            })
                        }
                    </div>
                }
                {
                    tab === 3 &&
                    <div className="w-full h-[70vh] overflow-y-auto p-2 relative pt-0">
                        <div className="flex justify-between items-center p-2 flex-col gap-2 sticky top-0 bg-thNewtral z-10">
                            {
                                (detailChat?.conversationSetting?.find((item) => item.type === 5)?.value === true && !isTypeWaitting) ||
                                (isOwner || isAdmin) ?
                                    <button type="button" className="w-full rounded-md py-2 bg-thNewtral2 text-thWhite hover:bg-thPrimaryHover opacity-90 hover:opacity-100 font-bold" 
                                        onClick={()=>{setIsShowModalVote(true);}}
                                    >
                                        Tạo bình chọn
                                    </button> : null
                            }
                        </div>
                        {
                            [...newsletter.voteList || []]?.sort((a, b) => {
                                const createdAtA = new Date(a.createdAt).getTime();
                                const createdAtB = new Date(b.createdAt).getTime();
                                // Sort in descending order (newest first)
                                return createdAtB - createdAtA;
                            })?.map((item)=>{
                                // const totalMembersVoted = new Set(item?.conversationVoteOptions?.map((option) => option.conversationVoteAnswers.map((answer) => answer?.user.id)).flat()).size || 1;

                                return (
                                    renderVote(item)
                                );
                            })
                        }
                    </div>
                }
            </div>
        </div>
    );
}