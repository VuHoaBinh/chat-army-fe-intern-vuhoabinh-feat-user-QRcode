/* eslint-disable @next/next/no-img-element */
import {
    Dispatch,
    ReactElement,
    SetStateAction,
    cache,
    useCallback,
    useEffect,
    useState
} from "react";

import { CONVERSATION } from "@/src/constants/chat";
import useWindowDimensions from "@/src/hook/useWindowDimension";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { setOtherUser } from "@/src/redux/slices/conversationSlice";
import { setSearchItem } from "@/src/redux/slices/searchSlice";
import { IConversation } from "@/src/types/Conversation";
import { IMessage } from "@/src/types/Message";
import axiosClient from "@/src/utils/axios/axiosClient";
import {
    filterConversation,
    sortConversationByNewest
} from "@/src/utils/conversations/filterConversation";
import * as bcrypt from "bcryptjs";
import moment from "moment";
import Image from "next/image";
import Link from "next/link";

const fetchConversations = cache(() =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations?offset=0&status=[2]`
    )
);
interface Props {
    value: string;
    setInputSearchValue: Dispatch<SetStateAction<string>>;
}

export default function HeaderSearchBox({
    value,
    setInputSearchValue
}: Props): ReactElement {
    const { user, search } = useAppSelector((state) => state.user);
    const dispatch = useAppDispatch();
    const [listResultSearch, setListResultSearch] = useState<any>([]);
    const [listResultContact, setListResultContact] = useState<any>([]);
    const [listResultGroup, setListResultGroup] = useState<any>([]);
    const [listResultHidden, setListResultHidden] = useState<any>([]);
    const [listAllResult, setListAllResult] = useState<any>([]);
    const currentDateTime = moment();
    const { width } = useWindowDimensions();
    const localStorageData = useAppSelector((state: any) => state.user);
    const [dataStorage, setDataStorage] = useState<any>([{}] as any);
    useEffect(() => {
        const dataString = localStorage.getItem("Othername");

        if (dataString) {
            setDataStorage(JSON.parse(dataString));
        }
    }, [localStorageData]);
    const [selectToViewResults, setSelectToViewResults] = useState<
        "all" | "converstions" | "messages"
    >("all");
    const fetchData = useCallback(async () => {
        const { data } = await fetchConversations();
        return filterConversation(data, user.id);
    }, [user.id]);
    const [conversationsWithHidden, setConversationsWithHiddenFlag] = useState<
        IConversation[]
    >([]);

    const [listIdHiddenMatching, setListIdHiddenMatching] = useState<string[]>(
        []
    );

    useEffect(() => {
        // Effect để lấy listHidden mỗi khi bắt đầu search
        fetchData().then((data) => {
            const conversationsWithHiddenFlag = sortConversationByNewest(data).filter(
                (item: any) => item.isConversationHidden
            );
            setConversationsWithHiddenFlag(conversationsWithHiddenFlag);
        });
    }, []);

    useEffect(() => {
        // Loại bỏ các conversation bị ẩn khỏi listContact và listGroup
        if (listIdHiddenMatching?.length !== 0) {
            const newListResultContact = listResultContact.filter(
                (obj: any) => !listIdHiddenMatching.includes(obj.conversationId)
            );
            const newListResultGroup = listResultGroup.filter(
                (obj: any) => !listIdHiddenMatching.includes(obj.conversationId)
            );
            setListResultContact(newListResultContact);
            setListResultGroup(newListResultGroup);
        }
    }, [listIdHiddenMatching]);

    const compareAndFilterIds = () => {
        if (conversationsWithHidden) {
            const promises = conversationsWithHidden.map((conversation) => {
                return new Promise<string | undefined>((resolve, reject) => {
                    bcrypt.compare(
                        value,
                        conversation.isConversationHidden.pin,
                        (err, result) => {
                            if (err) {
                                console.error(err);
                                reject(err);
                            } else {
                                resolve(result ? conversation._id : undefined);
                            }
                        }
                    );
                });
            });

            Promise.all(promises)
                .then((filteredIds) => {
                    const finalIds = filteredIds.filter(
                        (id) => id !== undefined
                    ) as string[];
                    setListIdHiddenMatching(finalIds);
                    const newListResultHidden: any = [];
                    if (search.arrayValueToSearch?.length > 0) {
                        search.arrayValueToSearch.forEach((message: any) => {
                            // nếu ko phải group thì thêm người còn lại không phải mình trong cuộc trò chuyện vào danh sách contact

                            if (
                                !message.data.member &&
                                message.data.createdByUser &&
                                message.data.createdByUser.id !== user?.id &&
                                finalIds.includes(message.id)
                            ) {
                                newListResultHidden.push({
                                    ...message.data.createdByUser,
                                    conversationId: message.id,
                                    latestMessage: message?.data?.latestMessage
                                });
                            }
                            if (
                                !message.data.member &&
                                message.data.directUser &&
                                message.data.directUser.id !== user?.id &&
                                finalIds.includes(message.id)
                            ) {
                                newListResultHidden.push({
                                    ...message.data.directUser,
                                    conversationId: message.id,
                                    latestMessage: message?.data?.latestMessage
                                });
                            }
                            // nếu là group thì duyệt hết member
                            if (
                                message.data &&
                                message.data.member &&
                                message?.data.name &&
                                finalIds.includes(message.id)
                            ) {
                                newListResultHidden.push({
                                    ...message.data,
                                    conversationId: message.id
                                });
                            }
                        });
                        const sortedNewListResultHidden = newListResultHidden.sort(
                            (a: any, b: any) => {
                                const dateA = Date.parse(
                                    a?.latestMessage[a?.latestMessage?.length - 1]?.createdAt
                                );
                                const dateB = Date.parse(
                                    b?.latestMessage[b?.latestMessage?.length - 1]?.createdAt
                                );

                                return dateB - dateA;
                            }
                        );
                        setListResultHidden(sortedNewListResultHidden);
                    }
                })
                .catch((error) => {
                    console.error("Error during promise execution:", error);
                });
        }
    };


    useEffect(() => {
        compareAndFilterIds();
    }, [value, conversationsWithHidden]);

    // xử lý cho việc tìm kiếm trong phần chatbox trên header
    useEffect(() => {
        const newListResultSearch: any = [];
        const newListResultContact: any = [];
        const newListResultGroup: any = [];
        const newListAllResult: any = [];
        if (search.arrayValueToSearch?.length > 0) {
            // nếu nhập ngày vd như 20-10 , 20/10 hoặc 20-10-2023, 20/10/2023
            if (checkDate(value)) {
                const standardizedDate = convertToStandardDate(value);
                // duyệt qua hết tin nhắn kiểm tra nếu ngày tạo tin nhắn trùng thì thêm vô kết quả
                search.arrayValueToSearch.forEach((message: any) => {
                    message.data.latestMessage.forEach((m: IMessage) => {
                        const messageDate = new Date(m.createdAt)
                            .toISOString()
                            .split("T")[0];

                        if (standardizedDate === messageDate) {
                            newListResultSearch.push(m);
                        }
                    });
                });
            } else {
                search.arrayValueToSearch.forEach((message: any) => {
                    // nếu ko phải group thì thêm người còn lại không phải mình trong cuộc trò chuyện vào danh sách contact

                    if (
                        !message.data.member &&
                        message.data.createdByUser &&
                        message.data.createdByUser.id !== user?.id &&
                        (message?.data.createdByUser.username
                            .toLowerCase()
                            .includes(value.toLowerCase()) || dataStorage[message?.data.createdByUser.id || ""]
                            ?.toLowerCase()
                            .includes(value.toLowerCase()))
                    ) {

                        newListResultContact.push({
                            ...message.data.createdByUser,
                            conversationId: message.id,
                            latestMessage: message?.data?.latestMessage
                        });
                    }
                    if (
                        !message.data.member &&
                        message.data.directUser &&
                        message.data.directUser.id !== user?.id &&
                        (message?.data.directUser.username
                            .toLowerCase()
                            .includes(value.toLowerCase()) || dataStorage[message?.data.directUser?.id || ""]
                            ?.toLowerCase()
                            .includes(value.toLowerCase()))
                    ) {
                        newListResultContact.push({
                            ...message.data.directUser,
                            conversationId: message.id,
                            latestMessage: message?.data?.latestMessage
                        });
                    }

                    // nếu là group thì duyệt hết member
                    if (
                        message.data &&
                        message.data.member &&
                        message?.data.name &&
                        message?.data.name.toLowerCase().includes(value.toLowerCase())
                    ) {
                        newListResultGroup.push({
                            ...message.data,
                            conversationId: message.id
                        });
                    }

                    // thêm các tin nhắn trùng với input value
                    message.data.latestMessage.forEach((m: IMessage) => {
                        if (m.content.toLowerCase().includes(value.toLowerCase())) {
                            // nếu có member thì là tin nhắn trong group
                            if (message.data?.member) {
                                newListResultSearch.push({
                                    ...m,
                                    nameGroup: message.data.name,
                                    avatarGroup: message.data.avatar
                                });
                            } else {
                                newListResultSearch.push(m);
                            }
                        }
                    });
                });
            }
        }

        // thêm tất cả cuộc trò chuyện vào
        newListAllResult.push(...newListResultSearch);
        newListAllResult.push(...newListResultContact);
        newListAllResult.push(...newListResultGroup);
        const sortedNewListAllResult = newListAllResult.sort((a: any, b: any) => {
            let createdAtA = "";
            let createdAtB = "";

            if (a?.createdAt) {
                createdAtA = a?.createdAt;
            } else {
                // nếu latestMessage trong group hay contact thì phải lấy ra cái latestMessage
                createdAtA = a?.latestMessage && a?.latestMessage?.createdAt;
            }
            if (b?.createdAt) {
                createdAtB = b?.createdAt;
            } else {
                // nếu latestMessage trong group hay contact thì phải lấy ra cái latestMessage
                createdAtB = b?.latestMessage && b?.latestMessage?.createdAt;
            }

            const dateA = Date.parse(createdAtA);
            const dateB = Date.parse(createdAtB);

            return dateB - dateA;
        });
        setListAllResult(sortedNewListAllResult);

        const sortedNewListResultSearch = newListResultSearch.sort(
            (a: IMessage, b: IMessage) => {
                const dateA = Date.parse(a.createdAt);
                const dateB = Date.parse(b.createdAt);

                return dateB - dateA;
            }
        );
        const sortedNewListResultGroup = newListResultGroup.sort(
            (a: any, b: any) => {
                const dateA = Date.parse(
                    a?.latestMessage[a?.latestMessage?.length - 1]?.createdAt
                );
                const dateB = Date.parse(
                    b?.latestMessage[b?.latestMessage?.length - 1]?.createdAt
                );

                return dateB - dateA;
            }
        );
        const sortedNewListResultContact = newListResultContact.sort(
            (a: any, b: any) => {
                const dateA = Date.parse(a?.latestMessage?.createdAt);
                const dateB = Date.parse(b?.latestMessage?.createdAt);

                return dateB - dateA;
            }
        );

        setListResultGroup(sortedNewListResultGroup);
        setListResultSearch(sortedNewListResultSearch);
        setListResultContact(sortedNewListResultContact);
    }, [search, user.id, value, conversationsWithHidden]);

    const checkDate = (inputValue: string) => {
        const pattern = /(\d{1,2})[-/](\d{1,2})(?:[-/](\d{4}))?/;
        return pattern.test(inputValue);
    };
    const convertToStandardDate = (inputValue: string) => {
        const [day, month, year] = inputValue.split(/[-/]/);
        if (year) {
            // Ngày tháng năm đầy đủ
            return `${year}-${month.padStart(2, "0")}-${day.padStart(2, "0")}`;
        }
        // Chỉ có ngày tháng
        const currentYear = new Date().getFullYear();
        return `${currentYear}-${month.padStart(2, "0")}-${day.padStart(2, "0")}`;
    };
    const handleOnclickItem = () => {
        setInputSearchValue("");
    };

    const handleOnclickMessage = (message: IMessage | any) => {
        const infoOfThisMessage = search.arrayValueToSearch.find(
            (conversation: IConversation | any) => {
                return conversation.id === message.conversationId;
            }
        );

        if (message?.nameGroup && message?.avatarGroup) {
            dispatch(
                setOtherUser({
                    otherUsername: message?.nameGroup,
                    otherAvatar: message?.avatarGroup
                })
            );
        } else {
            const infoOtherUser =
                infoOfThisMessage.data.createdByUser.id !== user.id
                    ? infoOfThisMessage.data.createdByUser
                    : infoOfThisMessage.data.directUser;
            dispatch(
                setOtherUser({
                    otherUsername: infoOtherUser?.username,
                    otherAvatar: infoOtherUser?.avatar
                })
            );
        }
        dispatch(setSearchItem(message));
    };
    const handleOnclickContact = (contact: any) => {
        dispatch(
            setOtherUser({
                otherUsername: contact?.username || contact?.name,
                otherAvatar: contact?.avatar
            })
        );
    };

    const handleClickOption = (select: "all" | "converstions" | "messages") => {
        setSelectToViewResults(select);
    };

    if (value.trim() === "") return <></>;
    return (
        <div
            className={
                width > 768
                    ? " absolute top-full left-0 w-full py-2 z-50 mt-1 flex flex-col justify-start flex-shrink-0 rounded-md overflow-y-auto overflow-x-hidden dark:bg-thNewtral1 bg-light-thNewtral1"
                    : "fixed top-[6vh] left-6 right-6 width-[100% - 24px - 24px] py-2 z-50 mt-1 flex flex-col justify-start flex-shrink-0 rounded-md overflow-y-auto overflow-x-hidden dark:bg-thNewtral1 bg-light-thNewtral1"
            }
        >
            <div className="flex flex-col h-full  py-2">
                {listAllResult?.length > 0 || listResultHidden?.length > 0 ? (
                    <div className="flex justify-evenly pb-2">
                        <button
                            className={`text-md relative font-bold cursor-pointer ${selectToViewResults === "all"
                                ? "dark:text-thPrimary text-light-thPrimary"
                                : "text-gray-400"
                            }`}
                            onClick={() => handleClickOption("all")}
                        >
                            <span>Tất cả</span>
                            <span
                                className={`absolute bottom-0 left-0 w-full h-[2px]  dark:bg-thPrimary bg-light-thPrimary ${selectToViewResults === "all" ? "block" : "hidden"
                                }`}
                            />
                        </button>
                        <button
                            className={`text-md relative font-bold cursor-pointer ${selectToViewResults === "converstions"
                                ? "text-thPrimary"
                                : "text-gray-400"
                            }`}
                            onClick={() => handleClickOption("converstions")}
                        >
                            <span>Trò chuyện</span>
                            <span
                                className={`absolute bottom-0 left-0 w-full h-[2px]  dark:bg-thPrimary bg-light-thPrimary ${selectToViewResults === "converstions" ? "block" : "hidden"
                                }`}
                            />
                        </button>
                        <button
                            className={`text-md relative font-bold cursor-pointer ${selectToViewResults === "messages"
                                ? "text-thPrimary"
                                : "text-gray-400"
                            }`}
                            onClick={() => handleClickOption("messages")}
                        >
                            <span>Tin nhắn</span>
                            <span
                                className={`absolute bottom-0 left-0 w-full h-[2px]  dark:bg-thPrimary bg-light-thPrimary ${selectToViewResults === "messages" ? "block" : "hidden"
                                }`}
                            />
                        </button>
                    </div>
                ) : (
                    <h2 className="w-full py-2 font-bold text-center">
                        Không tìm thấy kết quả!
                    </h2>
                )}
                {selectToViewResults === "all" && (
                    <div className="flex flex-col h-full">
                        {listAllResult === 0 && (
                            <h3 className="w-full text-center">Không tìm thấy kết quả!</h3>
                        )}
                        {listAllResult?.length > 0 && (
                            <div className="flex flex-col items-start">
                                <div className="w-full max-h-[75vh] lg:max-h-[70vh] overflow-y-scroll overflow-x-hidden">
                                    {listAllResult?.map((c: any) => {
                                        const m: any = c;
                                        let avatar: any;
                                        let name: any;
                                        let username: string =
                                            user.id === c?.createdBy?.id
                                                ? "Bạn"
                                                : dataStorage[c?.createdBy?.id] ? dataStorage[c?.createdBy?.id] : c?.createdBy?.username;
                                        let content: string = c?.content;

                                        // nếu là message thì có createAt và content còn ko có thì là group hoặc contact
                                        if (c?.createdAt && c?.content) {
                                            avatar = c?.createdBy?.avatar;
                                            name = dataStorage[c?.createdBy?.id] ? dataStorage[c?.createdBy?.id] : c?.createdBy?.username;

                                            // nếu có thì là tin nhắn trong group
                                            if (c?.nameGroup && c?.avatarGroup) {
                                                avatar = c?.avatarGroup;
                                                name = c?.nameGroup;
                                            }
                                        } else {
                                            avatar = c?.avatar;
                                            // chat cá nhân là username ,    chat nhóm là name

                                            name = dataStorage[c?.id] || c?.username || c?.name;

                                            if (c?.latestMessage) {
                                                username =
                                                    user.id ===
                                                        c?.latestMessage[c?.latestMessage?.length - 1]
                                                            ?.createdBy?.id
                                                        ? "Bạn"
                                                        : dataStorage[c?.latestMessage[c?.latestMessage?.length - 1]?.createdBy?.id] ? dataStorage[c?.latestMessage[c?.latestMessage?.length - 1]?.createdBy?.id] : c?.latestMessage[c?.latestMessage?.length - 1]
                                                            ?.createdBy?.username;
                                                content =
                                                    c?.latestMessage[c?.latestMessage?.length - 1]
                                                        ?.content;
                                            }
                                        }

                                        const targetDate = moment(m?.createdAt);
                                        const checkDate = targetDate.isBefore(
                                            currentDateTime,
                                            "day"
                                        );

                                        return (
                                            <Link
                                                key={c?.id}
                                                href={`/chat/conversation/${c?.conversationId}`}
                                                onClick={() => {
                                                    handleOnclickItem();
                                                    handleOnclickMessage(c);
                                                }}
                                            >
                                                <div className="flex w-full gap-x-2 items-center py-2 px-2 dark:hover:bg-thNewtral1 hover:bg-light-thNewtral2 rounded-lg cursor-pointer">
                                                    <Image
                                                        src={
                                                            avatar !== ""
                                                                ? avatar === "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg"
                                                                    ? "/default_avatar.jpg"
                                                                    : avatar
                                                                : null
                                                        }
                                                        sizes="(max-width: 56px) 56px, 56px"
                                                        alt={"Logo"}
                                                        loading="eager"
                                                        priority
                                                        width={56}
                                                        height={56}
                                                        className="rounded-full object-cover h-[56px] min-w-[56px]"
                                                    />
                                                    <div className="flex w-full justify-between">
                                                        <div>
                                                            <p className="text-[.9375rem] font-semibold md:max-w-[72px] lg:max-w-[185px] text-ellipsis line-clamp-1">
                                                                {name}
                                                            </p>
                                                            <span className="font-bold text-[.8125rem] opacity-80 max-w-[175px] line-clamp-1 pt-1">
                                                                {username} : {content}
                                                            </span>
                                                        </div>
                                                        <div className="flex flex-col justify-between gap-y-1 items-end lg:ml-3">
                                                            <span className="text-xs opacity-60">
                                                                {!checkDate
                                                                    ? moment(m?.createdAt).format("hh:mm")
                                                                    : moment(m?.createdAt).format("DD/MM")}
                                                            </span>
                                                            <svg
                                                                stroke="currentColor"
                                                                fill="currentColor"
                                                                strokeWidth="0"
                                                                viewBox="0 0 1024 1024"
                                                                className="dark:text-thPrimary text-light-thPrimary"
                                                                height="16"
                                                                width="16"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                            >
                                                                <path d="M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448 448-200.6 448-448S759.4 64 512 64zm193.5 301.7l-210.6 292a31.8 31.8 0 0 1-51.7 0L318.5 484.9c-3.8-5.3 0-12.7 6.5-12.7h46.9c10.2 0 19.9 4.9 25.9 13.3l71.2 98.8 157.2-218c6-8.3 15.6-13.3 25.9-13.3H699c6.5 0 10.3 7.4 6.5 12.7z"></path>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Link>
                                        );
                                    })}
                                </div>
                            </div>
                        )}
                    </div>
                )}
                {selectToViewResults === "converstions" && (
                    <div className="flex flex-col h-full">
                        {listResultHidden?.length === 0 &&
                            listResultContact?.length === 0 &&
                            listResultGroup?.length === 0 && (
                            <h3 className="w-full text-center">Không tìm thấy kết quả!</h3>
                        )}

                        <div className="flex flex-col items-start">
                            {listResultHidden?.length > 0 && (
                                <h2 className="w-full py-2 font-bold text-center">
                                    Trò chuyện ẩn
                                </h2>
                            )}
                            <div className="w-full max-h-[35vh] lg:max-h-[35vh] overflow-y-scroll overflow-x-hidden">
                                {listResultHidden?.length > 0 &&
                                    listResultHidden?.map((conversation: any) => {
                                        const latestMessage =
                                            conversation?.latestMessage[
                                                conversation?.latestMessage?.length - 1
                                            ];
                                        const targetDate = moment(latestMessage?.createdAt);
                                        const checkDate = targetDate.isBefore(
                                            currentDateTime,
                                            "day"
                                        );

                                        return conversation.type === CONVERSATION.TYPE.GROUP ? (
                                            <Link
                                                key={conversation?.id}
                                                href={`/chat/conversation/${conversation?.conversationId}`}
                                                onClick={() => {
                                                    handleOnclickItem();
                                                    handleOnclickContact(conversation);
                                                }}
                                            >
                                                <div className="flex w-full gap-x-2 items-center py-2 px-2 dark:hover:bg-thNewtral1 hover:bg-light-thNewtral2 rounded-lg cursor-pointer">
                                                    <Image
                                                        src={
                                                            conversation?.avatar !== ""
                                                                ? conversation?.avatar === "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg"
                                                                    ? "/default_avatar.jpg"
                                                                    : conversation?.avatar
                                                                : null
                                                        }
                                                        sizes="(max-width: 56px) 56px, 56px"
                                                        alt={"Logo"}
                                                        loading="eager"
                                                        priority
                                                        width={56}
                                                        height={56}
                                                        className="rounded-full object-cover h-[56px] min-w-[56px]"
                                                    />
                                                    <div className="flex w-full justify-between">
                                                        <div>
                                                            <p className="text-[.9375rem] font-semibold md:max-w-[72px] lg:max-w-[185px] text-ellipsis line-clamp-1">
                                                                {dataStorage[conversation.idOrder] ? dataStorage[conversation.idOrder] : conversation?.name}
                                                            </p>
                                                            <span className="font-bold text-[.8125rem] opacity-80 max-w-[175px] line-clamp-1 pt-1">
                                                                {user.id === latestMessage?.createdBy?.id
                                                                    ? "Bạn"
                                                                    : latestMessage?.createdBy?.username}{" "}
                                                                : {latestMessage?.content}
                                                            </span>
                                                        </div>
                                                        <div className="flex flex-col justify-between gap-y-1 items-end lg:ml-3">
                                                            <span className="text-xs opacity-60">
                                                                {!checkDate
                                                                    ? moment(latestMessage?.createdAt).format(
                                                                        "hh:mm"
                                                                    )
                                                                    : moment(latestMessage?.createdAt).format(
                                                                        "DD/MM"
                                                                    )}
                                                            </span>
                                                            <svg
                                                                stroke="currentColor"
                                                                fill="currentColor"
                                                                strokeWidth="0"
                                                                viewBox="0 0 1024 1024"
                                                                className="dark:text-thPrimary text-light-thPrimary"
                                                                height="16"
                                                                width="16"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                            >
                                                                <path d="M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448 448-200.6 448-448S759.4 64 512 64zm193.5 301.7l-210.6 292a31.8 31.8 0 0 1-51.7 0L318.5 484.9c-3.8-5.3 0-12.7 6.5-12.7h46.9c10.2 0 19.9 4.9 25.9 13.3l71.2 98.8 157.2-218c6-8.3 15.6-13.3 25.9-13.3H699c6.5 0 10.3 7.4 6.5 12.7z"></path>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Link>
                                        ) : (
                                            <Link
                                                key={conversation?.id}
                                                href={`/chat/conversation/${conversation?.conversationId}`}
                                                onClick={() => {
                                                    handleOnclickItem();
                                                    handleOnclickContact(conversation);
                                                }}
                                            >
                                                <div className="flex w-full gap-x-2 items-center py-2 px-2 dark:hover:bg-thNewtral1 hover:bg-light-thNewtral2 rounded-lg cursor-pointer">
                                                    <Image
                                                        src={
                                                            conversation?.avatar !== ""
                                                                ? conversation?.avatar === "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg"
                                                                    ? "/default_avatar.jpg"
                                                                    : conversation?.avatar
                                                                : null
                                                        }
                                                        sizes="(max-width: 56px) 56px, 56px"
                                                        alt={"Logo"}
                                                        loading="eager"
                                                        priority
                                                        width={56}
                                                        height={56}
                                                        className="rounded-full object-cover h-[56px] min-w-[56px]"
                                                    />
                                                    <div className="flex w-full justify-between">
                                                        <div>
                                                            <p className="text-[.9375rem] font-semibold md:max-w-[72px] lg:max-w-[185px] text-ellipsis line-clamp-1">
                                                                {conversation?.username}
                                                            </p>
                                                            <span className="font-bold text-[.8125rem] opacity-80 max-w-[175px] line-clamp-1">
                                                                {user.id === latestMessage?.createdBy?.id
                                                                    ? "Bạn"
                                                                    : latestMessage?.createdBy?.username}{" "}
                                                                : {latestMessage?.content}
                                                            </span>
                                                        </div>
                                                        <div className="flex flex-col justify-between gap-y-1 items-end lg:ml-3">
                                                            <span className="text-xs opacity-60">
                                                                {!checkDate
                                                                    ? moment(latestMessage?.createdAt).format(
                                                                        "hh:mm"
                                                                    )
                                                                    : moment(latestMessage?.createdAt).format(
                                                                        "DD/MM"
                                                                    )}
                                                            </span>
                                                            <svg
                                                                stroke="currentColor"
                                                                fill="currentColor"
                                                                strokeWidth="0"
                                                                viewBox="0 0 1024 1024"
                                                                className="dark:text-thPrimary text-light-thPrimary"
                                                                height="16"
                                                                width="16"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                            >
                                                                <path d="M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448 448-200.6 448-448S759.4 64 512 64zm193.5 301.7l-210.6 292a31.8 31.8 0 0 1-51.7 0L318.5 484.9c-3.8-5.3 0-12.7 6.5-12.7h46.9c10.2 0 19.9 4.9 25.9 13.3l71.2 98.8 157.2-218c6-8.3 15.6-13.3 25.9-13.3H699c6.5 0 10.3 7.4 6.5 12.7z"></path>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Link>
                                        );
                                    })}
                            </div>
                        </div>
                        <div className="flex flex-col items-start">
                            {listResultContact?.length > 0 && (
                                <h2 className="w-full py-2 font-bold text-center">Cá nhân</h2>
                            )}
                            <div className="w-full max-h-[35vh] lg:max-h-[35vh] overflow-y-scroll overflow-x-hidden">
                                {listResultContact?.length > 0 &&
                                    listResultContact?.map((contact: any) => {
                                        const latestMessage =
                                            contact?.latestMessage[
                                                contact?.latestMessage?.length - 1
                                            ];
                                        const targetDate = moment(latestMessage?.createdAt);
                                        const checkDate = targetDate.isBefore(
                                            currentDateTime,
                                            "day"
                                        );

                                        return (
                                            <Link
                                                key={contact?.id}
                                                href={`/chat/conversation/${contact?.conversationId}`}
                                                onClick={() => {
                                                    handleOnclickItem();
                                                    handleOnclickContact(contact);
                                                }}
                                            >
                                                <div className="flex w-full gap-x-2 items-center py-2 px-2 dark:hover:bg-thNewtral1 hover:bg-light-thNewtral2 rounded-lg cursor-pointer">
                                                    <Image
                                                        src={
                                                            contact?.avatar !== ""
                                                                ? contact?.avatar === "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg"
                                                                    ? "/default_avatar.jpg"
                                                                    : contact?.avatar
                                                                : null
                                                        }
                                                        sizes="(max-width: 56px) 56px, 56px"
                                                        alt={"Logo"}
                                                        loading="eager"
                                                        priority
                                                        width={56}
                                                        height={56}
                                                        className="rounded-full object-cover h-[56px] min-w-[56px]"
                                                    />
                                                    <div className="flex w-full justify-between">
                                                        <div>
                                                            <p className="text-[.9375rem] font-semibold md:max-w-[72px] lg:max-w-[185px] text-ellipsis line-clamp-1">
                                                                {dataStorage[contact?.id] ? dataStorage[contact?.id] : contact?.username}
                                                            </p>
                                                            <span className="font-bold text-[.8125rem] opacity-80 max-w-[175px] line-clamp-1">
                                                                {user.id === latestMessage?.createdBy?.id
                                                                    ? "Bạn"
                                                                    : latestMessage?.createdBy?.username}{" "}
                                                                : {latestMessage?.content}
                                                            </span>
                                                        </div>
                                                        <div className="flex flex-col justify-between gap-y-1 items-end lg:ml-3">
                                                            <span className="text-xs opacity-60">
                                                                {!checkDate
                                                                    ? moment(latestMessage?.createdAt).format(
                                                                        "hh:mm"
                                                                    )
                                                                    : moment(latestMessage?.createdAt).format(
                                                                        "DD/MM"
                                                                    )}
                                                            </span>
                                                            <svg
                                                                stroke="currentColor"
                                                                fill="currentColor"
                                                                strokeWidth="0"
                                                                viewBox="0 0 1024 1024"
                                                                className="dark:text-thPrimary text-light-thPrimary"
                                                                height="16"
                                                                width="16"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                            >
                                                                <path d="M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448 448-200.6 448-448S759.4 64 512 64zm193.5 301.7l-210.6 292a31.8 31.8 0 0 1-51.7 0L318.5 484.9c-3.8-5.3 0-12.7 6.5-12.7h46.9c10.2 0 19.9 4.9 25.9 13.3l71.2 98.8 157.2-218c6-8.3 15.6-13.3 25.9-13.3H699c6.5 0 10.3 7.4 6.5 12.7z"></path>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Link>
                                        );
                                    })}
                            </div>
                        </div>
                        <div className="flex flex-col items-start">
                            {listResultGroup?.length > 0 && (
                                <h2 className="w-full py-2 font-bold text-center">Nhóm</h2>
                            )}
                            <div className="w-full max-h-[30vh] lg:max-h-[35vh] overflow-y-scroll overflow-x-hidden">
                                {listResultGroup?.length > 0 &&
                                    listResultGroup?.map((group: any) => {
                                        const latestMessage =
                                            group?.latestMessage[group?.latestMessage?.length - 1];
                                        const targetDate = moment(latestMessage?.createdAt);
                                        const checkDate = targetDate.isBefore(
                                            currentDateTime,
                                            "day"
                                        );

                                        return (
                                            <Link
                                                key={group?.id}
                                                href={`/chat/conversation/${group?.conversationId}`}
                                                onClick={() => {
                                                    handleOnclickItem();
                                                    handleOnclickContact(group);
                                                }}
                                            >
                                                <div className="flex w-full gap-x-2 items-center py-2 px-2 dark:hover:bg-thNewtral1 hover:bg-light-thNewtral2 rounded-lg cursor-pointer">
                                                    <Image
                                                        src={
                                                            group?.avatar !== ""
                                                                ? group?.avatar === "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg"
                                                                    ? "/default_avatar.jpg"
                                                                    : group?.avatar
                                                                : null
                                                        }
                                                        sizes="(max-width: 56px) 56px, 56px"
                                                        alt={"Logo"}
                                                        loading="eager"
                                                        priority
                                                        width={56}
                                                        height={56}
                                                        className="rounded-full object-cover h-[56px] min-w-[56px]"
                                                    />
                                                    <div className="flex w-full justify-between">
                                                        <div>
                                                            <p className="text-[.9375rem] font-semibold md:max-w-[72px] lg:max-w-[185px] text-ellipsis line-clamp-1">
                                                                {group?.name}
                                                            </p>
                                                            <span className="font-bold text-[.8125rem] opacity-80 max-w-[175px] line-clamp-1 pt-1">
                                                                {user.id === latestMessage?.createdBy?.id
                                                                    ? "Bạn"
                                                                    : latestMessage?.createdBy?.username}{" "}
                                                                : {latestMessage?.content}
                                                            </span>
                                                        </div>
                                                        <div className="flex flex-col justify-between gap-y-1 items-end lg:ml-3">
                                                            <span className="text-xs opacity-60">
                                                                {!checkDate
                                                                    ? moment(latestMessage?.createdAt).format(
                                                                        "hh:mm"
                                                                    )
                                                                    : moment(latestMessage?.createdAt).format(
                                                                        "DD/MM"
                                                                    )}
                                                            </span>
                                                            <svg
                                                                stroke="currentColor"
                                                                fill="currentColor"
                                                                strokeWidth="0"
                                                                viewBox="0 0 1024 1024"
                                                                className="dark:text-thPrimary text-light-thPrimary"
                                                                height="16"
                                                                width="16"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                            >
                                                                <path d="M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448 448-200.6 448-448S759.4 64 512 64zm193.5 301.7l-210.6 292a31.8 31.8 0 0 1-51.7 0L318.5 484.9c-3.8-5.3 0-12.7 6.5-12.7h46.9c10.2 0 19.9 4.9 25.9 13.3l71.2 98.8 157.2-218c6-8.3 15.6-13.3 25.9-13.3H699c6.5 0 10.3 7.4 6.5 12.7z"></path>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Link>
                                        );
                                    })}
                            </div>
                        </div>
                    </div>
                )}
                {selectToViewResults === "messages" && (
                    <div className="flex flex-col h-full">
                        {listResultSearch?.length === 0 &&
                            listResultHidden?.length === 0 && (
                            <h3 className="w-full text-center">Không tìm thấy kết quả!</h3>
                        )}
                        {listResultSearch?.length > 0 && (
                            <div className="flex flex-col items-start">
                                <h2 className="w-full py-2 font-bold text-center">Tin nhắn</h2>
                                <div className="w-full max-h-[70vh] lg:max-h-[65vh] overflow-y-scroll overflow-x-hidden">
                                    {listResultSearch?.map((message: any) => {
                                        const targetDate = moment(message?.createdAt);
                                        const checkDate = targetDate.isBefore(
                                            currentDateTime,
                                            "day"
                                        );

                                        let avatar = message?.createdBy.avatar;
                                        let name = dataStorage[message?.createdBy.id] ? dataStorage[message?.createdBy.id] : message?.createdBy.username;

                                        // nếu có thì là tin nhắn trong group
                                        if (message?.nameGroup && message?.avatarGroup) {
                                            avatar = message?.avatarGroup;
                                            name = message?.nameGroup;
                                        }

                                        return (
                                            <Link
                                                key={message?.id}
                                                href={`/chat/conversation/${message?.conversationId}`}
                                                onClick={() => {
                                                    handleOnclickItem();
                                                    handleOnclickMessage(message);
                                                }}
                                            >
                                                <div className="flex w-full gap-x-1 items-center py-2 px-2 dark:hover:bg-thNewtral1 hover:bg-light-thNewtral2 rounded-lg cursor-pointer">
                                                    <Image
                                                        src={
                                                            avatar !== ""
                                                                ? avatar === "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg"
                                                                    ? "/default_avatar.jpg"
                                                                    : avatar
                                                                : null
                                                        }
                                                        sizes="(max-width: 56px) 56px, 56px"
                                                        alt={"Logo"}
                                                        loading="eager"
                                                        priority
                                                        width={56}
                                                        height={56}
                                                        className="rounded-full object-cover h-[56px] min-w-[56px]"
                                                    />
                                                    <div className="flex w-full justify-between">
                                                        <div>
                                                            <p className="text-[.9375rem] font-semibold md:max-w-[72px] lg:max-w-[165px] text-ellipsis line-clamp-1">
                                                                {name}
                                                            </p>
                                                            <span className="font-bold text-[.8125rem] opacity-80 max-w-[175px] line-clamp-1">
                                                                {user.id === message?.createdBy?.id
                                                                    ? "Bạn"
                                                                    : dataStorage[message?.createdBy?.id] ? dataStorage[message?.createdBy?.id] : message?.createdBy?.username}{" "}
                                                                : {message?.content}
                                                            </span>
                                                        </div>
                                                        <div className="flex flex-col justify-between gap-y-1 items-end lg:ml-3">
                                                            <span className="text-xs opacity-60">
                                                                {!checkDate
                                                                    ? moment(message?.createdAt).format("hh:mm")
                                                                    : moment(message?.createdAt).format("DD/MM")}
                                                            </span>
                                                            <svg
                                                                stroke="currentColor"
                                                                fill="currentColor"
                                                                strokeWidth="0"
                                                                viewBox="0 0 1024 1024"
                                                                className="dark:text-thPrimary text-light-thPrimary"
                                                                height="16"
                                                                width="16"
                                                                xmlns="http://www.w3.org/2000/svg"
                                                            >
                                                                <path d="M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448 448-200.6 448-448S759.4 64 512 64zm193.5 301.7l-210.6 292a31.8 31.8 0 0 1-51.7 0L318.5 484.9c-3.8-5.3 0-12.7 6.5-12.7h46.9c10.2 0 19.9 4.9 25.9 13.3l71.2 98.8 157.2-218c6-8.3 15.6-13.3 25.9-13.3H699c6.5 0 10.3 7.4 6.5 12.7z"></path>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                </div>
                                            </Link>
                                        );
                                    })}
                                </div>
                            </div>
                        )}
                    </div>
                )}
            </div>
        </div>
    );
}
