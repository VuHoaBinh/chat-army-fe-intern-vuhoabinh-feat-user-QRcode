'use client';
import Image from 'next/image';
import Link from 'next/link';
import React, { ReactElement, cache, useEffect, useState } from 'react';
import Logo from "@/public/DP 1.png";
import { QRCode } from 'antd';

import { IInforGroup } from '@/src/types/Conversation';
import axios from 'axios';
import { toast } from 'react-toastify';
interface IMessageLinkProps {
    url?: string
}
const checkLinkGroup = (url: string): boolean => {
    if (url.includes('http') && url.includes('/g/')) {
        return true;
    }
    return false;
};
const fetchInforGroup = cache(async (id: string) => {
 
    try {
        if (checkLinkGroup(id)) {
            const idInvite = id.split('/').pop();
            const response = await axios.get(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/group/invite/${idInvite}`);
            return response.data;
        }


    } catch (err) {
        console.log(err);
    }
});
export default function MessageLink({ url }: IMessageLinkProps): ReactElement {
    const [inforGroup, setInforGroup] = useState<IInforGroup | null>(null);
    const [checkLinkValid, setCheckLinkValid] = useState<boolean>(false);
    useEffect(() => {
        (async () => {

            const data: IInforGroup = await fetchInforGroup(url || "");
            setInforGroup(data);
        })();

    }, [url]);

    return (
        <div className='md:max-w-xs max-w-[280px] flex flex-col gap-y-2 '>
            <Link href={`${url}`} target={checkLinkValid ? '_blank' : '_self'} onClick={async (event: any) => {
                event.preventDefault();
                const clickedUrl = event.currentTarget.href;
                const data = await fetchInforGroup(clickedUrl);
                setCheckLinkValid(data ? true : false);
                if (!data) {
                    toast.error('Nhóm không tồn tại!');
                } else {
                    window.open(clickedUrl, '_blank');
                }
            }}>
                <div className='text-blue-500 md:hover:underline underline md:no-underline' >{url} </div>
                {checkLinkGroup(url || '') ? (<>
                    <div className='p-4 bg-gradient-to-r from-yellow-300 to-yellow-500 rounded-md mt-2'>
                        <>
                            {inforGroup !== undefined ? (<><div className='flex gap-2 items-center'>
                                <Image
                                    src={inforGroup?.avatar === "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg" ? "/default_avatar.jpg" : inforGroup?.avatar || Logo}
                                    alt={"Logo"}
                                    loading="eager"
                                    priority
                                    width={40}
                                    height={40}
                                    className="bg-thNewtral1 rounded-full object-cover   w-10 h-10"

                                />
                                <div className='font-bold text-xl truncate text-white'>{inforGroup?.name}</div>
                            </div>
                            <div className='flex items-center justify-end rounded-sm'>
                                <QRCode value={`${url}`} className='w-full h-full p-0 border-none' icon="/DP 1.png" size={100} iconSize={20} bordered={false} type='svg' bgColor='white' style={{ width: '100px', height: '100px' }} />
                            </div></>) : (<div className='min-h-[100px] flex items-center justify-center'>

                                <Image
                                    src={Logo}
                                    alt={"Logo"}
                                    loading="eager"
                                    priority
                                    width={80}
                                    height={80}
                                    className="bg-thNewtral1 rounded-full object-cover   w-10 h-10"

                                />
                            </div>)}
                        </>

                    </div>

                    <div className='mt-2'><Link href={`${url}`} className='hover:text-blue-600 font-semibold '>{inforGroup !== undefined ? `DAC - ${inforGroup?.name}` : `DAC APP`} </Link></div>
                    <div className='text-thNewtral2 sm:truncate'>Bấm vào đây để tham gia nhóm trên DAC</div></>) : null}

            </Link>
        </div >
    );
}
