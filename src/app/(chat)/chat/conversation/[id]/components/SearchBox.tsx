/* eslint-disable @next/next/no-img-element */
'use client';
import { ReactElement, useEffect, useState } from "react";

import { DatePicker, Space } from "antd";
import Image from "next/image";
const { RangePicker } = DatePicker;
import { useDebounce } from "use-debounce";
import { IMessage } from "@/src/types/Message";
import moment from "moment";

import dayjs, { Dayjs } from "dayjs";
import isBetween from "dayjs/plugin/isBetween";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import {
    setIdConversation,
    setSearchItem,
    toggleOpenSearch
} from "@/src/redux/slices/searchSlice";
import { IConversation } from "@/src/types/Conversation";
import useWindowDimensions from "@/src/hook/useWindowDimension";
dayjs.extend(isBetween);

export default function SearchBox({
    handelClickSearchItem
}: {handelClickSearchItem?: (item: IMessage) => void}): ReactElement {
    const [isOpenDateSearch, setIsOpenDateSearch] = useState<boolean>(false);
    const [dateSearchValue, setDateSearchValue] = useState<[Dayjs, Dayjs] | null>(
        null
    );
    const [inputSearchValue, setInputSearchValue] = useState<string>("");
    const [value] = useDebounce(inputSearchValue, 200);

    const { search } = useAppSelector((state: any) => state.user);
    const dispatch = useAppDispatch();

    const [listUser, setListUser] = useState<any>([]);
    const [listResultSearch, setListResultSearch] = useState<any>([]);
    const [selectedValue, setSelectedValue] = useState("default");
    const currentDateTime = moment();
    const localStorageData = useAppSelector((state: any) => state.user);
    const [dataStorage, setDataStorage] = useState<any>([{}] as any);
    useEffect(() => {
        const dataString = localStorage.getItem("Othername");

        if (dataString) {
            setDataStorage(JSON.parse(dataString));
        }
    }, [localStorageData]);
    // xử lý cho việc tìm kiếm trong phần chatbox
    useEffect(() => {
        // lấy ra conversation đang trò chuyện để tìm
        const searchConversation = search?.arrayValueToSearch.find(
            (value: IConversation | any) => {
                return value.id === search.idConversation;
            }
        );

        if (!searchConversation) return;

        const newListUser = [];
        newListUser.push("Người gửi");
        if (searchConversation.data && searchConversation.data.member) {
            // nếu là group thì duyệt hết member
            searchConversation?.data.member.forEach((user: any) => {
                newListUser.push(dataStorage[user.id] ? dataStorage[user.id] : user?.username);
            });
        } else {
            // nếu là trò chuyện cá nhân thì lấy ra 2 người
            newListUser.push(dataStorage[searchConversation?.data.createdByUser.id] ? dataStorage[searchConversation?.data.createdByUser.id] : searchConversation?.data.createdByUser.username || null);
            newListUser.push(dataStorage[searchConversation?.data.directUser.id] ? dataStorage[searchConversation?.data.directUser.id] : searchConversation?.data.directUser.username || null);
        }

        setListUser(newListUser);

        const newResultSearch = [];
        if (value.trim() !== "") {
            if (dateSearchValue !== null) {
                if (newListUser.length > 1 && selectedValue !== "default") {
                    const searchResult = searchConversation?.data.latestMessage.filter(
                        (message: IMessage | any) => {
                            const inputDate = new Date(message.createdAt);

                            if (message.filename) {
                                return (
                                    message.filename
                                        .toLowerCase()
                                        .includes(value.toLowerCase()) &&
                                    isDateInRange(inputDate, dateSearchValue) && (
                                        message.createdBy.username === selectedValue || dataStorage[message.createdBy.id] === selectedValue)
                                );
                            }

                            return (
                                message.content.toLowerCase().includes(value.toLowerCase()) &&
                                isDateInRange(inputDate, dateSearchValue) &&
                                (message.createdBy.username === selectedValue || dataStorage[message.createdBy.id] === selectedValue)
                            );
                        }
                    );

                    newResultSearch.push(...searchResult);
                } else {
                    const searchResult = searchConversation?.data.latestMessage.filter(
                        (message: IMessage | any) => {
                            const inputDate = new Date(message.createdAt);

                            if (message.filename) {
                                return (
                                    message.filename
                                        .toLowerCase()
                                        .includes(value.toLowerCase()) &&
                                    isDateInRange(inputDate, dateSearchValue)
                                );
                            }

                            return (
                                message.content.toLowerCase().includes(value.toLowerCase()) &&
                                isDateInRange(inputDate, dateSearchValue)
                            );
                        }
                    );
                    newResultSearch.push(...searchResult);
                }
            } else {
                if (newListUser.length > 1 && selectedValue !== "default") {
                    const searchResult = searchConversation?.data.latestMessage.filter(
                        (message: IMessage | any) => {
                            if (message.filename) {
                                return (
                                    message.filename
                                        .toLowerCase()
                                        .includes(value.toLowerCase()) && (
                                        message.createdBy.username === selectedValue || dataStorage[message.createdBy.id] === selectedValue)
                                );
                            }

                            return (
                                message.content.toLowerCase().includes(value.toLowerCase()) &&
                                (message.createdBy.username === selectedValue || dataStorage[message.createdBy.id] === selectedValue)
                            );
                        }
                    );
                    newResultSearch.push(...searchResult);
                } else {
                    const searchResult = searchConversation?.data.latestMessage.filter(
                        (message: IMessage | any) => {
                            if (message.filename) {
                                return message.filename
                                    .toLowerCase()
                                    .includes(value.toLowerCase());
                            }

                            return message.content
                                .toLowerCase()
                                .includes(value.toLowerCase());
                        }
                    );
                    newResultSearch.push(...searchResult);
                }
            }
        } else {
            if (dateSearchValue !== null) {
                if (newListUser.length > 1 && selectedValue !== "default") {
                    const searchResult = searchConversation?.data.latestMessage.filter(
                        (message: IMessage | any) => {
                            const inputDate = new Date(message.createdAt);
                            return (
                                isDateInRange(inputDate, dateSearchValue) &&
                                (message.createdBy.username === selectedValue || dataStorage[message.createdBy.id] === selectedValue)
                            );
                        }
                    );
                    newResultSearch.push(...searchResult);
                } else {
                    const searchResult = searchConversation?.data.latestMessage.filter(
                        (message: IMessage | any) => {
                            const inputDate = new Date(message.createdAt);
                            return isDateInRange(inputDate, dateSearchValue);
                        }
                    );
                    newResultSearch.push(...searchResult);
                }
            } else {
                if (newListUser.length > 1 && selectedValue !== "default") {
                    const searchResult = searchConversation?.data.latestMessage.filter(
                        (message: IMessage | any) => {
                            return (message.createdBy.username === selectedValue || dataStorage[message.createdBy.id] === selectedValue);
                        }
                    );
                    newResultSearch.push(...searchResult);
                } else {
                    setListResultSearch([]);
                }
            }
        }

        const sortedNewResultSearch = newResultSearch.sort(
            (a: IMessage, b: IMessage) => {
                const dateA = Date.parse(a.createdAt);
                const dateB = Date.parse(b.createdAt);

                return dateB - dateA;
            }
        );

        setListResultSearch(sortedNewResultSearch);
    }, [value, search, dateSearchValue, selectedValue, dataStorage]);

    const handleChangeSearchFollowUser = (e: any) => {
        setSelectedValue(e.target.value);
    };
    const isDateInRange = (date: Date, range: [Dayjs, Dayjs]) => {
        const startDate = dayjs(range[0]);
        const endDate = dayjs(range[1]).add(12, "hour");
        const targetDate: Dayjs = dayjs(date);

        return targetDate.isBetween(startDate, endDate);
    };
    const disabledDate = (current: Dayjs) => {
        // Lấy ngày hiện tại
        const today = moment();
        // Nếu current (ngày hiện tại) sau hoặc bằng ngày hiện tại thì cho phép chọn
        return current && current > today;
    };
    const handleClickCloseSearchBox = () => {
        if (search.isOpenSearch) {
            dispatch(toggleOpenSearch());
            dispatch(setIdConversation(""));

            setIsOpenDateSearch(false);
            setInputSearchValue("");
            setListResultSearch([]);
        }
    };
    const handleChangeDateSearch = (dates: any) => {
        setDateSearchValue(dates);
    };
    const windowDimensions = useWindowDimensions();
    const handleOnclickMessage = (message: IMessage) => {
        if(windowDimensions.width > 480){
            dispatch(setSearchItem(message));
        }else{
            (handelClickSearchItem as any)(message);
        }
    };
    return (
        <div className="p-4 flex flex-col gap-y-6 flex-shrink-0 rounded-2xl  h-[90vh] lg:h-[85vh] mx-4 mt-4 overflow-y-auto ">
            <div className="flex flex-col gap-x-1 items-center">
                {/* <h1 className="text-xl font-semibold mb-2">Search Box</h1> */}
                <div className="w-full flex flex-col justify-center pt-2">
                    <div className="w-full flex">
                        <input
                            className="w-auto flex-1 py-1 px-2 text-sm rounded-md text-black bg-white border-black border-2 "
                            type="text"
                            placeholder="search..."
                            value={inputSearchValue}
                            onChange={(e) => setInputSearchValue(e.target.value)}
                        />
                        <button
                            className={`text-thDark-background flex ml-3 py-1 px-2 bg-gray-300 border border-gray-300 text-sm rounded-lg`}
                            onClick={handleClickCloseSearchBox}
                        >
                            Đóng
                        </button>
                    </div>

                    <div className="relative w-full mt-3 flex items-center justify-around">
                        <span className="mr-2">Lọc : </span>
                        <div className="w-2/5">
                            <select
                                value={selectedValue}
                                onChange={handleChangeSearchFollowUser}
                                id="countries"
                                className="py-1 px-2 bg-gray-300 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                            >
                                {/* <option value="default" selected>Người gửi</option> */}
                                {listUser.map((username: string) => {
                                    return (
                                        <option
                                            value={username === "Người gửi" ? "default" : username}
                                            key={username}
                                        >
                                            {username}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button
                            className={`text-thDark-background flex ml-3 py-1 px-2 bg-gray-300 border border-gray-300 text-sm rounded-lg`}
                            onClick={() => setIsOpenDateSearch(!isOpenDateSearch)}
                        >
                            Ngày gửi
                        </button>

                        {isOpenDateSearch && (
                            <div className="absolute flex items-center top-full left-0 right-0 w-full h-10 rounded-md z-50">
                                <Space direction="vertical" size={12}>
                                    <RangePicker
                                        onChange={(dates) => handleChangeDateSearch(dates)}
                                        disabledDate={disabledDate}
                                    />
                                </Space>
                            </div>
                        )}
                    </div>
                </div>
            </div>
            <div className="flex flex-col gap-y-2 pt-4 pb-5">
                {listResultSearch?.length === 0 && (
                    <h3 className="w-full text-center">Không tìm thấy kết quả!</h3>
                )}
                {listResultSearch?.length > 0 && (
                    <div>
                        <h3 className="w-full text-center">Tin nhắn</h3>
                        {listResultSearch?.map((message: any) => {
                            const targetDate = moment(message?.createdAt);
                            const checkDate = targetDate.isBefore(currentDateTime, "day");

                            return (
                                <div
                                    key={message?.id}
                                    className="flex gap-x-2 items-center py-2 px-2 dark:hover:bg-thNewtral1 hover:bg-light-thNewtral1  rounded-lg cursor-pointer"
                                    onClick={() => handleOnclickMessage(message)}
                                >
                                    <Image
                                        src={
                                            message?.createdBy.avatar !== ""
                                                ? message?.createdBy?.avatar === "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg"
                                                    ? "/default_avatar.jpg"
                                                    : message?.createdBy?.avatar
                                                : null
                                        }
                                        sizes="(max-width: 56px) 56px, 56px"
                                        alt={"Logo"}
                                        loading="eager"
                                        priority
                                        width={56}
                                        height={56}
                                        className="rounded-full object-cover h-[56px] min-w-[56px]"
                                    />
                                    <div className="flex w-full justify-between">
                                        <div>
                                            <p className="text-[.9375rem] font-semibold md:max-w-[72px] lg:max-w-[160px] text-ellipsis line-clamp-1">
                                                {dataStorage[message?.createdBy.id] ? dataStorage[message?.createdBy.id] : message?.createdBy.username}
                                            </p>
                                            <span className="font-bold text-[.8125rem] opacity-80 max-w-[160px] line-clamp-1">
                                                <span>{message?.content}</span>
                                            </span>
                                        </div>
                                        <div className="flex flex-col justify-between gap-y-1 items-end lg:ml-3">
                                            <span className="text-xs opacity-60">
                                                {!checkDate
                                                    ? moment(message?.createdAt).format("hh:mm")
                                                    : moment(message?.createdAt).format("DD/MM")}
                                            </span>
                                            <svg
                                                stroke="currentColor"
                                                fill="currentColor"
                                                strokeWidth="0"
                                                viewBox="0 0 1024 1024"
                                                className="text-thPrimary"
                                                height="16"
                                                width="16"
                                                xmlns="http://www.w3.org/2000/svg"
                                            >
                                                <path d="M512 64C264.6 64 64 264.6 64 512s200.6 448 448 448 448-200.6 448-448S759.4 64 512 64zm193.5 301.7l-210.6 292a31.8 31.8 0 0 1-51.7 0L318.5 484.9c-3.8-5.3 0-12.7 6.5-12.7h46.9c10.2 0 19.9 4.9 25.9 13.3l71.2 98.8 157.2-218c6-8.3 15.6-13.3 25.9-13.3H699c6.5 0 10.3 7.4 6.5 12.7z"></path>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            );
                        })}
                    </div>
                )}
            </div>
        </div>
    );
}
