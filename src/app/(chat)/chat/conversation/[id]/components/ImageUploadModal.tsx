import { useState } from 'react';
import Cropper from 'react-easy-crop';
import { cropImage } from '@/src/utils/upload/getCroppedImg';
import Image from "next/image";

import {
    Button,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions
} from "@mui/material";

import ImageUploading from "react-images-uploading";

const ImageUploadingButton = ({ value, onChange, ...props }: any) => {
    return (
        <div className='w-full flex justify-center'>
            <ImageUploading value={value} onChange={onChange}>
                {({ onImageUpload, onImageUpdate }) => (
                    <button
                        className="text-thPrimary font-bold ml-3 px-4 py-2 rounded"
                        onClick={value ? onImageUpload : () => onImageUpdate(0)}
                        {...props}
                    >
                        Selected Image
                    </button>
                )}
            </ImageUploading>
        </div>
    );
};

const ImageCropper = ({
    open,
    image,
    onComplete,
    containerStyle,
    ...props
}: any) => {
    const [crop, setCrop] = useState({ x: 0, y: 0 });
    const [zoom, setZoom] = useState(1);
    const [croppedAreaPixels, setCroppedAreaPixels] = useState<any>(null);

    return (
        <Dialog open={open} maxWidth="sm" fullWidth>
            <DialogTitle>Crop Image</DialogTitle>

            <DialogContent>
                <div style={containerStyle}>
                    <Cropper
                        image={image}
                        crop={crop}
                        zoom={zoom}
                        aspect={1}
                        onCropChange={setCrop}
                        onCropComplete={(_, croppedAreaPixels) => {
                            setCroppedAreaPixels(croppedAreaPixels);
                        }}
                        onZoomChange={setZoom}
                        {...props}
                    />
                </div>
            </DialogContent>

            <DialogActions>
                <Button
                    color="primary"
                    onClick={() =>
                        onComplete(cropImage(image, croppedAreaPixels, console.log))
                    }
                >
                    Finish
                </Button>
            </DialogActions>
        </Dialog>
    );
};

interface ImageUploadModalProps {
    isOpen: boolean;
    onClose: () => void;
    idConversation: string;
}

const ImageUploadModal: React.FC<ImageUploadModalProps> = ({ isOpen, onClose, idConversation }) => {
    const [image, setImage] = useState<any>([]);
    const [croppedImage, setCroppedImage] = useState<any>(null);
    const [dialogOpen, setDialogOpen] = useState<boolean>(false);
    const [uploading, setUploading] = useState<boolean>(false);

    // eslint-disable-next-line require-await
    const handleUpload = async () => {
        if (croppedImage) {
            localStorage.setItem(idConversation + 'backgroundImage', croppedImage);
            setUploading(true);

            setTimeout(() => {
                setUploading(false);
                onClose();
            }, 2000);
        };
    };

    return (
        <div className="fixed w-screen h-screen left-0 top-0 z-50 flex justify-center items-center" onClick={onClose}>
            <div className={`modal ${isOpen ? 'block' : 'hidden'} p-5 bg-thNewtral1 w-[400px] lg:w-[600px] rounded-lg`} onClick={(e) => e.stopPropagation()}>
                <div className="modal-overlay" onClick={onClose}></div>
                <div className="modal-container">
                    <div className="modal-content">
                        <h1 className="text-xl text-thPrimary mb-4">Upload Image</h1>
                        <ImageUploadingButton
                            value={image}
                            onChange={(newImage: any) => {
                                setDialogOpen(true);
                                setImage(newImage);
                            }}
                        />
                        {croppedImage && <div className="flex flex-col items-center justify-center mt-4">
                            <h2 className="mb-2 text-white">Preview:</h2>
                            <Image src={croppedImage} alt="Preview" width={400} height={400} className='max-h-[500px] object-contain' />
                        </div>}
                        <div className="mt-4 flex justify-end">
                            <button className="bg-thNewtral2 text-thPrimary px-4 py-2 rounded" onClick={onClose}>
                                Cancel
                            </button>
                            <button
                                className={`bg-thPrimary text-thNewtral1 ml-3 px-4 py-2 rounded ${croppedImage === null ? 'opacity-50 cursor-not-allowed' : ''} `}
                                onClick={handleUpload}
                                disabled={croppedImage === null}
                            >
                                {uploading ? 'Uploading...' : 'Upload'}
                            </button>
                        </div>
                    </div>
                </div>
                <ImageCropper
                    open={dialogOpen}
                    image={image.length > 0 && image[0].dataURL}
                    onComplete={(imagePromisse: any) => {
                        imagePromisse.then((image: any) => {
                            setCroppedImage(image);
                            setDialogOpen(false);
                        });
                    }}
                    containerStyle={{
                        position: "relative",
                        width: "100%",
                        height: 300,
                        background: "#333"
                    }}
                />
            </div>
        </div>
    );
};

export default ImageUploadModal;
