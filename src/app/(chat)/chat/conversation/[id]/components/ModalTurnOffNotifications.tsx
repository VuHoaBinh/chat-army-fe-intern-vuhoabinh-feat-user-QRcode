"use client";

import { useAppDispatch } from "@/src/redux/hook";
import { addIdToListIdConversationTurnOffNotify, removeIdToListIdConversationTurnOffNotify, setOpenModalTurnOffNotifications } from "@/src/redux/slices/NotifyOfConversationSlice";
import axiosClient from "@/src/utils/axios/axiosClient";
import { ReactElement, cache, useState } from "react";

interface Props {
  idConversation: string;
}

const fetchConversations = cache(() =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations?offset=0&status=[2]&limit=10`
    )
);

export default function ModalTurnOffNotifications({
    idConversation
}: Props): ReactElement {
    const [radioInputValue, setRadioInputValue] = useState<string>("1hour");
    const dispatch: any = useAppDispatch();

    const handleRadioChange = (value: string) => {
        setRadioInputValue(value);
    };

    const handleTurnOffNotification = async (value: any) => {
        let duration: any;
        if(value === "1hour") {
            duration = 3600000;
        } else if(value === "4hours") {
            duration = 14400000;
        } else {
            duration = "untilTurnOn";
        }

        const now = Date.now();
        if(duration !== "untilTurnOn") {
            const turnOffTime = now + duration;
            try {
                const result = await axiosClient().put(
                    `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/setting/notification/${idConversation}/off`, {
                        time: turnOffTime.toString()
                    }
                );
                if (result) {
                    const { data } = await fetchConversations();
                    console.log(123);
                    console.log(turnOffTime);
                    console.log(result);
                    console.log(data);
                    console.log(123);
                    dispatch(addIdToListIdConversationTurnOffNotify(idConversation));
                }
            } catch (err: any) {
                console.log(err);
            }

            setTimeout(async () => {
                try {
                    const now = Date.now();
                    const result = await axiosClient().put(
                        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/setting/notification/${idConversation}/on`, {
                            time: now.toString()
                        }
                    );
                    if (result) {
                        dispatch(removeIdToListIdConversationTurnOffNotify(idConversation));
                    }
                } catch (err: any) {
                    console.log(err);
                }
            }, duration);
        } else {
            try {
                const result = await axiosClient().put(
                    `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/setting/notification/${idConversation}/off`
                );
                if (result) {
                    dispatch(addIdToListIdConversationTurnOffNotify(idConversation));
                }
            } catch (err: any) {
                console.log(err);
            }
        }

        dispatch(setOpenModalTurnOffNotifications(false));
    };

    return (
        <div className="fixed w-screen h-screen left-0 top-0 z-50 flex justify-center items-center" onClick={() => dispatch(setOpenModalTurnOffNotifications(false))}>
            <div className="bg-black w-[300px] h-auto max-h-[400px] overflow-hidden rounded-md p-3" onClick={(e) => e.stopPropagation()}>
                <div>
                    <h3 className="text-sm font-bold mb-3">
                        Bạn có muốn tắt thông báo hội thoại này:
                    </h3>
                    <div className="flex items-center mb-2">
                        <input
                            onChange={() => handleRadioChange("1hour")}
                            id="default-radio-1"
                            type="radio"
                            value=""
                            name="default-radio"
                            className="w-4 h-4 text-thPrimary bg-gray-100 border-gray-300 focus:text-thPrimary"
                            checked={radioInputValue === "1hour"}
                        />
                        <label
                            htmlFor="default-radio-1"
                            className="ms-2 text-sm font-medium text-white"
                        >
                            Trong 1 giờ
                        </label>
                    </div>
                    <div className="flex items-center mb-2">
                        <input
                            onChange={() => handleRadioChange("4hours")}
                            id="default-radio-2"
                            type="radio"
                            value=""
                            name="default-radio"
                            className="w-4 h-4 text-thPrimary bg-gray-100 border-gray-300 focus:text-thPrimary"
                            checked={radioInputValue === "4hours"}
                        />
                        <label
                            htmlFor="default-radio-2"
                            className="ms-2 text-sm font-medium text-white"
                        >
                            Trong 4 giờ
                        </label>
                    </div>
                    <div className="flex items-center">
                        <input
                            onChange={() => handleRadioChange("untilTurnOn")}
                            id="default-radio-3"
                            type="radio"
                            value=""
                            name="default-radio"
                            className="w-4 h-4 text-thPrimary bg-gray-100 border-gray-300 focus:text-thPrimary"
                            checked={radioInputValue === "untilTurnOn"}
                        />
                        <label
                            htmlFor="default-radio-3"
                            className="ms-2 text-sm font-medium text-white"
                        >
                            Cho đến khi được mở lại
                        </label>
                    </div>
                </div>
                <div className="flex justify-around mt-5">
                    <button
                        className="px-3 py-1 font-bold bg-gray-300 text-thNewtral1 rounded-md"
                        onClick={() => dispatch(setOpenModalTurnOffNotifications(false))}
                    >
                        Hủy
                    </button>
                    <button
                        className="px-3 py-1 font-bold bg-thNewtral1 text-thPrimary rounded-md"
                        onClick={() => handleTurnOffNotification(radioInputValue)}
                    >
                        Xác nhận
                    </button>
                </div>
            </div>
        </div>
    );
}
