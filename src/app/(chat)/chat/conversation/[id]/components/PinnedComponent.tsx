import { CONVERSATION } from "@/src/constants/chat";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { setNotes, setStartAnswersVote, setStartEditNote, setVotes, unPinMessageItem } from "@/src/redux/slices/newsletterSlice";
import getAxiosClient from "@/src/utils/axios/axiosClient";
import { sortNotes } from "@/src/utils/newsletter/sortHepper";
import { Dropdown, message } from "antd";
import React, { ReactElement, memo, useEffect, useState } from "react";
import { AiOutlineMessage } from "react-icons/ai";
import { FaAngleRight, FaCaretDown, FaCaretUp } from "react-icons/fa";
import { FaChartSimple } from "react-icons/fa6";
import { IoIosMore } from "react-icons/io";
import { MdOutlineStickyNote2 } from "react-icons/md";
import { toast } from "react-toastify";
interface IPinnedComponentProps {
    setIsShowNewsletter: React.Dispatch<React.SetStateAction<boolean>>,
    setIsOpenInfo: React.Dispatch<React.SetStateAction<boolean>>,
    setMessageScroll: React.Dispatch<any>
}
function PinnedComponent({ setIsShowNewsletter, setIsOpenInfo, setMessageScroll }: IPinnedComponentProps): ReactElement {
    const { newsletter, detailChat } = useAppSelector((state) => state.user);
    const dispatch = useAppDispatch();
    const handleUnPinMessage = async (messagePin: any) => {
        try {
            const res = await getAxiosClient().delete(
                `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${detailChat?.id}/message-pin/${messagePin?._id}`
            );
            if (res.status === 200) {
                dispatch(unPinMessageItem(messagePin?._id));
                message.success("Bỏ ghim thành công");
            }
        } catch (e) {
            message.success("Bỏ ghim thất bại");
        }
    };
    const [isShowListPinMessage, setIsShowListPinMessage] = useState<boolean>(false);
    const [mergeList, setMergeList] = useState<any[]>([]);
    const [newItem, setNewItem] = useState<any>({});
    useEffect(()=>{
        const mergeAllList = [
            ...[...newsletter.messagePinned || []].map((item) => ({
                ...item,
                item: "messagePinned"
            })),
            ...[...newsletter.noteList || []].filter((item) => item?.isPinned).map((item) => {
                return {
                    ...item,
                    item: "notePinned"
                };
            }),
            ...[...newsletter.voteList || []].filter((item) => item?.isPinned).map((item) => ({
                ...item,
                item: "votePinned"
            }))
        ];
        setMergeList([...mergeAllList].sort((a, b) => new Date(b?.createdAt).getTime() - new Date(a?.createdAt).getTime()));
        setNewItem([...mergeAllList].sort((a, b) => new Date(b?.createdAt).getTime() - new Date(a?.createdAt).getTime()).shift());
    }, [newsletter.messagePinned, newsletter.noteList, newsletter.voteList]);
    const handleUnpinNote = async (note: any) => {
        try{
            const endpoint =
            `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/note/${note?.id}`;
            const response = await getAxiosClient().request({
                method: "PUT",
                url: endpoint,
                data: {
                    content: note?.content,
                    isPinned: false
                }
            });
            if (response.status === 200) {
                const findNote = [...newsletter.noteList as any]?.find((item) => item.id === note?.id);
                const newNote = {
                    ...findNote,
                    isPinned: false
                };
                const newListNote = newsletter.noteList?.map((item) => {
                    if(item.id === note?.id){
                        return newNote;
                    }
                    return item;
                });
                dispatch(setNotes(sortNotes(newListNote) as any));
                message.success("Bỏ ghim thành công");
            }
        }catch(e: any){
            message.error("Bỏ ghim thất bại");
        }
    };
    const handleOpenNewsletter = () => {
        setIsOpenInfo(true);
        setIsShowNewsletter(true);
    };
    const handleUnPinVote = async (voteItem: any)=>{
        try{

            const res = await getAxiosClient().put(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/vote/${voteItem?.id}/${2}`);
            if(res.status === 200){
                const newVote = {
                    ...voteItem,
                    isPinned: false 
                };
                const newListVote = [...newsletter.voteList as any].map((item) => {
                    if(item.id === newVote.id){
                        return newVote;
                    }
                    return item;
                });
                dispatch(setVotes(newListVote as any));
            }
        }catch(err: any){
            toast.error(err.response.data.message);
        }
    };
    return (
        <>
            {   newItem && Object.keys(newItem).length > 0 &&
                !isShowListPinMessage && (
                <div className="bg-thNewtral2 cursor-pointer text-white border-solid border border-t-0 border-thPrimary rounded-b-lg gap-2 px-4 py-2 absolute top-0 left-0 right-0 z-10 flex justify-between items-center">
                    {
                        newItem?.item === "messagePinned" ?
                            <AiOutlineMessage size={32} /> :
                            newItem?.item === "notePinned" ?
                                <MdOutlineStickyNote2 size={32}/> : <FaChartSimple size={32}/>
                    }
                    <div className="flex-1 truncate" onClick={()=>{
                        if(newItem?.item === "messagePinned"){
                            setMessageScroll(newItem.message);
                        }
                        if(newItem?.item === "votePinned"){
                            dispatch(setStartAnswersVote(newItem as any));
                        }
                        if(newItem?.item === "notePinned"){
                            dispatch(setStartEditNote({ note: newItem, typeAction:"VIEW" }));
                        }
                    }}>
                        <span>{newItem?.item === "messagePinned" ? "Tin ghim" : newItem?.item === "notePinned" ? "Ghi chú ghim" : "Bình chọn"}</span>
                        <p className="truncate">
                            {
                                newItem?.item === "messagePinned" ?
                                    newItem?.message?.createdBy?.username :
                                    newItem?.item === "notePinned" ?
                                        newItem?.createdBy?.username
                                        : ""
                            }
                            {newItem.item !== "votePinned" && ":"}
                            {
                                newItem?.item === "messagePinned" ?

                                    newItem?.message?.content :
                                    newItem?.item === "notePinned" ?
                                        newItem?.content
                                        : newItem?.question
                            }
                        </p>
                    </div>
                    <div className="flex gap-2 justify-center items-center">
                        <Dropdown
                            menu={{
                                items: 
                                detailChat.type === CONVERSATION.TYPE.INDIVIDUAL ? 
                                    [
                                        {
                                            key: "3",
                                            label: (
                                                <span className="flex gap-2 justify-start items-center  ">
                                            Bỏ ghim
                                                </span>
                                            ),
                                            onClick: () => {
                                                if (
                                                    newItem?.item === "messagePinned" 
                                                ) {
                                                    handleUnPinMessage(
                                                        newItem
                                                    );
                                                }
                                            }
                                        }
                                    ] :
                                    [
                                        {
                                            key: "3",
                                            label: (
                                                <span className="flex gap-2 justify-start items-center border-b-2 border-black pb-2  ">
                                            Bỏ ghim
                                                </span>
                                            ),
                                            onClick: () => {
                                                if (
                                                    newItem?.item === "messagePinned" 
                                                ) {
                                                    handleUnPinMessage(
                                                        newItem
                                                    );
                                                }
                                                if(
                                                    newItem?.item === "notePinned"
                                                ){
                                                    handleUnpinNote(newItem);
                                                }
                                                if(
                                                    newItem?.item === "votePinned"
                                                ){
                                                    handleUnPinVote(newItem);
                                                }
                                            }
                                        },
                                        {
                                            key: "4",
                                            label: (
                                                <span className={`flex gap-2 justify-start items-center`}>
                                                Mở bản tin nhóm
                                                </span>
                                            ),
                                            onClick: () => {
                                                handleOpenNewsletter();
                                            }
                                        }
                                    ]
                            }}
                            placement="bottomLeft"
                            arrow
                            trigger={["click"]}
                        >
                            <div className=" p-2 bg-neutral-400 bg-opacity-70 rounded-full cursor-pointer">
                                <IoIosMore />
                            </div>
                        </Dropdown>
                        {mergeList.length > 1 && (
                            <button
                                className="px-2 text-white border-thPrimary border hover:bg-slate-400 rounded-md flex items-center gap-2"
                                onClick={() => setIsShowListPinMessage(true)}
                            >
                                <FaCaretDown /> {mergeList.length - 1} ghim khác
                            </button>
                        )}
                    </div>
                </div>
            )}
            {isShowListPinMessage && mergeList?.length > 0 && (
                <div className="bg-thNewtral2 bg-opacity-20  absolute top-0 left-0 inset-0 z-10">
                    <div
                        className="absolute top-0 left-0 w-full h-full opacity-20 "
                        onClick={() => setIsShowListPinMessage(false)}
                    ></div>
                    <div className="relative w-full max-h-[calc(100%-100px)]  z-50 bg-thNewtral2 text-white px-4 py-2">
                        <div className="flex justify-between items-center gap-x-2 text-sm">
                            <span>Danh sách ghim ({mergeList?.length})</span>
                            <button
                                className="flex items-center gap-2"
                                onClick={() => setIsShowListPinMessage(false)}
                            >
                                Thu gọn <FaCaretUp />
                            </button>
                        </div>
                        <div className="flex-col flex max-h-[300px] overflow-y-auto">
                            {mergeList?.map((item: any) => {
                                return (
                                    <div
                                        key={item.id}
                                        className=" flex justify-between items-center gap-2 border-b-2 border-black pb-2 my-2"
                                    >
                                        {
                                            item?.item === "messagePinned" ?
                                                <div className="flex gap-2 items-center w-[80%]" >
                                                    <AiOutlineMessage size={32} />
                                                    <div className="flex-1 justify-self-start max-w-[75%]"
                                                        onClick={()=>{
                                                            if(item?.item === "messagePinned"){
                                                                setMessageScroll(item.message);
                                                            }
                                                        }}
                                                    >
                                                        <span>Tin nhắn</span>
                                                        <p>
                                                            {item.message.createdBy?.username}:{" "}
                                                            {item.message.content}
                                                        </p>
                                                    </div>
                                                </div> : item?.item === "notePinned" ?
                                                    <div className="flex gap-2 items-center w-[80%]">
                                                        <MdOutlineStickyNote2 size={32} />
                                                        <div className="flex-1 justify-self-start max-w-[75%]" 
                                                            onClick={()=>{
                                                                if(item?.item === "notePinned"){
                                                                    dispatch(setStartEditNote({ note: item, typeAction:"VIEW" }));
                                                                }
                                                            }}
                                                        >
                                                            <span>Ghi chú</span>
                                                            <p className="truncate ">
                                                                {item?.createdBy?.username}:{" "}
                                                                {item?.content}
                                                            </p>
                                                        </div>
                                                    </div> : 
                                                    <div className="flex gap-2 items-center w-[80%]">
                                                        <FaChartSimple size={32} />
                                                        <div className="flex-1 justify-self-start max-w-[75%]" onClick={()=>{
                                                            if(item?.item === "votePinned"){
                                                                dispatch(setStartAnswersVote(item as any));
                                                            }
                                                        }}>
                                                            <span>Bình chọn</span>
                                                            <div className="">
                                                                <p className="truncate break-words">
                                                                    {item.question}
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div> 
                                        }
                                        <div className="flex gap-2 justify-center items-center relative z-10">
                                            <Dropdown
                                                menu={{
                                                    items: 
                                                    detailChat.type === CONVERSATION.TYPE.GROUP ? 
                                                        [
                                                            {
                                                                key: "3",
                                                                label: (
                                                                    <span className="flex gap-2 justify-start items-center border-b-2 border-black pb-2  ">
                                                                    Bỏ ghim
                                                                    </span>
                                                                ),
                                                                onClick: () => {
                                                                    if (
                                                                        item?.item === "messagePinned" 
                                                                    ) {
                                                                        handleUnPinMessage(
                                                                            newItem
                                                                        );
                                                                    }
                                                                    if(
                                                                        item?.item === "notePinned"
                                                                    ){
                                                                        handleUnpinNote(newItem);
                                                                    }
                                                                    if(
                                                                        item?.item === "votePinned"
                                                                    ){
                                                                        handleUnPinVote(newItem);
                                                                    }
                                                                }
                                                            },
                                                            {
                                                                key: "4",
                                                                label: (
                                                                    <span className="flex gap-2 justify-start items-center">
                                                                    Mở bản tin nhóm
                                                                    </span>
                                                                ),
                                                                onClick: () => {
                                                                    handleOpenNewsletter();
                                                                }
                                                            }
                                                        ] : [
                                                            {
                                                                key: "3",
                                                                label: (
                                                                    <span className="flex gap-2 justify-start items-center  ">
                                                                    Bỏ ghim
                                                                    </span>
                                                                ),
                                                                onClick: () => {
                                                                    if (
                                                                        item?.item === "messagePinned" 
                                                                    ) {
                                                                        handleUnPinMessage(
                                                                            newItem
                                                                        );
                                                                    }
                                                                }
                                                            }
                                                        ]
                                                }}
                                                placement="bottomLeft"
                                                arrow
                                                trigger={["click"]}
                                            >
                                                <div className=" p-2 bg-neutral-400 bg-opacity-70 rounded-full cursor-pointer">
                                                    <IoIosMore />
                                                </div>
                                            </Dropdown>
                                        </div>
                                    </div>
                                );
                            })}
                        </div>
                        {
                            detailChat?.type === CONVERSATION.TYPE.GROUP &&
                            <div
                                className="flex justify-center items-center gap-2 border-t-1 border-t-gray-500 cursor-pointer"
                                onClick={handleOpenNewsletter}
                            >
                            Xem tất cả ở bản tin nhóm <FaAngleRight />
                            </div>
                        }
                    </div>
                </div>
            )}
        </>
    );
}
export default memo(PinnedComponent);