import React, { ReactElement, useEffect, useRef, useState } from "react";
import styles from "../conversation.module.css";
import {
    AiFillEyeInvisible,
    AiFillMessage,
    AiFillPlayCircle,
    AiFillSmile
} from "react-icons/ai";
import { RiShareForwardFill } from "react-icons/ri";
import { CONVERSATION, EMOJI, MESSAGE_TYPE, SOCIAL_REGEX_LIST } from "@/src/constants/chat";
import Image from "next/image";
import moment from "moment";
import { IMessage } from "@/src/types/Message";
import ReactionBox from "./ReactionBox";
import { Socket } from "socket.io-client";
import { getFileIcon, getFileName } from "@/src/utils/messages/handleUrl";
import { BsLink45Deg, BsPinAngleFill } from "react-icons/bs";
import LinkPreview from "./LinkPrevew";
import useWindowDimensions from "@/src/hook/useWindowDimension";
import MessageLink from "./MessageLink";

import { Tooltip, message as notify } from "antd";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import getAxiosClient from "@/src/utils/axios/axiosClient";
import { setPinMessages } from "@/src/redux/slices/newsletterSlice";
interface Text {
    message: IMessage;
    setForwardContent: (forwardContent: IMessage) => void;
    reactionID: string;
    setReactionID: (reactionID: string) => void;
    socket?: Socket<any, any>;
    setMessageReply: React.Dispatch<any>;
    setMessageScroll: React.Dispatch<any>;
    isGroup?: boolean;
    messagePrev?: IMessage;
    setIdMediaModal: (idMediaModal: string) => void;
    setUrlMediaModal: (urlMediaModal: string) => void;
    setIsOpenMediaLibrary: (isOpenMediaLibrary: boolean) => void;
}
export default function PartnerText({
    message,
    setForwardContent,
    reactionID,
    setReactionID,
    socket,
    setMessageReply,
    setMessageScroll,
    messagePrev,
    setIdMediaModal,
    setUrlMediaModal,
    setIsOpenMediaLibrary
}: Text): ReactElement {
    const [isReact, setIsReact] = useState<boolean>(false);
    const [isShowLink, setIsShowLink] = useState<boolean>(false);
    const localStorageData = useAppSelector((state: any) => state.user);
    const [dataStorage, setDataStorage] = useState<any>([]);
    const handleReaction = () => {
        if (isReact === true && reactionID === message?._id) {
            setIsReact(false);
            setReactionID("");
            return;
        }
        if (isReact === true && reactionID !== message?._id) {
            setReactionID(message._id);
            return;
        }

        setIsReact(true);
        setReactionID(message?._id);
    };
    // Mã hóa đầu cuối => logic
    let dataImages: string[] = [];
    if (message.type === MESSAGE_TYPE.IMAGE) {
        try {
            dataImages = JSON.parse(message.content);
        } catch (error) {
            console.error("Error parsing JSON:", error);
        }
    }
    const messageReply =
        message.replyTo &&
        Object.keys(message.replyTo).length > 0 &&
        message.replyTo;
    const reactionBoxRef = useRef<HTMLDivElement>(null);
    useEffect(() => {
        const dataString = localStorage.getItem("Othername");
        if (dataString) {
            setDataStorage(JSON.parse(dataString));
        }
    }, [localStorageData]);
    useEffect(() => {
        const handleClickOutside = (event: MouseEvent) => {
            if (
                reactionBoxRef.current &&
                !reactionBoxRef.current.contains(event.target as Node)
            ) {
                setIsReact(false);
            }
        };

        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, []);
    const windowDimensions = useWindowDimensions();
    const dispatch = useAppDispatch();
    const { newsletter, detailChat } = useAppSelector((state) => state.user);
    const { user } = useAppSelector((state) => state.user);
    const handlePinMessage = async (message: IMessage) => {
        try {
            const res = await getAxiosClient().post(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${message.conversationId}/message-pin/${message._id}`);
            if (!newsletter.messagePinned?.find((item) => item.message?.id === res.data.messageId)) {
                dispatch(setPinMessages([...newsletter.messagePinned || [], {
                    _id: res.data._id,
                    conversationId: res.data.conversationId,
                    createdAt: res.data.createdAt,
                    updatedAt: res.data.updatedAt,
                    id: res.data.id,
                    message
                }]));
                notify.success("Đã ghim tin nhắn");
            } else {
                notify.warning("Tin nhắn đã được ghim trước đó");
            }
        } catch (e) {
            notify.error("Ghim tin nhắn thất bại");
        }
    };

    const isOwner = detailChat?.members.find((item: any) => item.id === user?.id)?.type === CONVERSATION.ROLE_TYPE.OWNER;
    const isAdmin = detailChat?.members.find((item: any) => item.id === user?.id)?.type === CONVERSATION.ROLE_TYPE.ADMIN;
    const isTypeWaitting = detailChat?.members.find((item: any) => item.id === user?.id)?.type === CONVERSATION.ROLE_TYPE.MEMBER_WAITING_ACCEPT;
    const isGroup = detailChat?.type === CONVERSATION.TYPE.GROUP;
    return (
        <>

            <div
                className={`flex max-[480px]:flex max-[480px]:flex-col max-[480px]:!items-start w-full gap-6 max-[480px]:gap-1  items-center ${styles.text_box} relative`}
                onMouseLeave={() => setIsReact(false)}
            >
                {windowDimensions.width <= 480 &&
                    isReact &&
                    reactionID === message?._id && (
                    <ReactionBox
                        messageId={message._id}
                        conversationSocket={socket}
                        position="left-1"
                    />
                )}
                <div className="flex items-end gap-x-1 max-w-[75%] max-[480px]:max-w-full">
                    <div
                        className={`dark:bg-thNewtral bg-light-thNewtral md:dark:bg-thDark-background md:bg-light-thNewtral1 lg:dark:bg-thDark-background lg:bg-light-thNewtral1 dark:text-thWhite text-black w-full p-3 ${isShowLink && "max-[480px]:p-0.5"
                        } ${styles.partner_box} flex flex-col gap-y-2`}
                    >
                        {
                            <div className=" text-gray-600 flex gap-2 items-center justify-between">
                                {message?.createdBy?.id !== messagePrev?.createdBy?.id && (
                                    <div className="gap-2 items-center flex">
                                        {
                                            <div className="min-w-4 w-4 h-4">
                                                {message?.createdBy?.id !==
                                                    messagePrev?.createdBy?.id && (
                                                    <Image
                                                        alt={"avatar"}
                                                        src={message?.createdBy?.avatar}
                                                        className="w-4  rounded-full object-cover aspect-square"
                                                        width={16}
                                                        height={16}
                                                        sizes="( max-width: 480px) 16px, 16px"
                                                    />
                                                )}
                                            </div>
                                        }
                                        <span className="text-sm max-w-[150px] truncate">
                                            {dataStorage[message.createdBy.id] ? dataStorage[message.createdBy.id] : message.createdBy.username}
                                        </span>
                                    </div>
                                )}
                                {windowDimensions.width <= 480 && (
                                    <div className={`relative `}>
                                        <div
                                            className={`${styles.icon_area} flex items-center gap-x-2 opacity-25 `}
                                        >
                                            <AiFillSmile size={20} onClick={handleReaction} />
                                            <AiFillMessage
                                                size={20}
                                                onClick={() => setMessageReply(message)}
                                            />
                                            <RiShareForwardFill
                                                size={20}
                                                onClick={() => setForwardContent(message)}
                                            />
                                            {
                                                (detailChat?.conversationSetting?.find((item) => item.type === 3)?.value === true && !isTypeWaitting) ||
                                                    ((isAdmin || isOwner) || !isGroup) ? (
                                                        <Tooltip placement="bottomRight" title={"Ghim tin nhắn"}>
                                                            <BsPinAngleFill size={20} onClick={() => handlePinMessage(message)} />
                                                        </Tooltip>
                                                    ) : null
                                            }
                                            {
                                                detailChat?.type !== CONVERSATION.TYPE.INDIVIDUAL &&
                                                <Tooltip placement="bottomRight" title={"Ghim tin nhắn"}>
                                                    <BsPinAngleFill size={20} onClick={() => handlePinMessage(message)} />
                                                </Tooltip>
                                            }
                                        </div>
                                    </div>
                                )}
                            </div>
                        }
                        {messageReply && (
                            <div
                                className="py-2 bg-gray-600 text-white rounded-md mb-2 flex justify-between cursor-pointer"
                                onClick={() => setMessageScroll(messageReply)}
                            >
                                <div
                                    className={`flex-col gap-2 w-full overflow-x-hidden  ${messageReply.type !== MESSAGE_TYPE.IMAGE &&
                                        " border-l-blue-200 border-l-4 ml-2"
                                    } px-2`}
                                >
                                    {messageReply.type === MESSAGE_TYPE.STICKER ? (
                                        <Image
                                            alt={messageReply.content}
                                            src={messageReply.content}
                                            className="w-[40px] h-[40px]"
                                            width={40}
                                            height={40}
                                        />
                                    ) : messageReply.type === MESSAGE_TYPE.IMAGE ? (
                                        <div
                                            className={`grid gap-1 ${JSON.parse(messageReply.content).length % 2 === 0
                                                ? "grid-cols-2"
                                                : JSON.parse(messageReply.content).length % 3 === 0 ||
                                                    JSON.parse(messageReply.content).length >= 3
                                                    ? "grid-cols-3"
                                                    : "grid-cols-1"
                                            }`}
                                        >
                                            {JSON.parse(messageReply.content).map((item: string) => {
                                                return (
                                                    <Image
                                                        key={item}
                                                        alt={item}
                                                        src={item}
                                                        className={`object-fill ${JSON.parse(messageReply.content).length >= 2
                                                            ? "w-[100px] h-[100px]"
                                                            : "w-[50px] h-[50px]"
                                                        }`}
                                                        width={
                                                            JSON.parse(messageReply.content).length >= 2
                                                                ? 70
                                                                : 200
                                                        }
                                                        height={
                                                            JSON.parse(messageReply.content).length >= 2
                                                                ? 70
                                                                : 200
                                                        }
                                                    />
                                                );
                                            })}
                                        </div>
                                    ) : messageReply.type === MESSAGE_TYPE.TEXT ? (
                                        <span className="text-[.9375rem]">
                                            {messageReply.content}
                                        </span>
                                    ) : messageReply.type === MESSAGE_TYPE.MEDIA ? (
                                        <video
                                            src={messageReply.content}
                                            width={100}
                                            height={100}
                                            className="w-[100px] h-auto"
                                        ></video>
                                    ) : messageReply.type === MESSAGE_TYPE.LINK ? (
                                        <span className="flex items-center text-blue-600 underline">
                                            <BsLink45Deg size={30} /> {messageReply.content}
                                        </span>
                                    ) : (
                                        <span className="flex items-center">
                                            {/* eslint-disable-next-line @next/next/no-img-element */}
                                            <img
                                                src={getFileIcon(
                                                    messageReply.content.split(".").pop() as string
                                                )}
                                                alt="icon"
                                                width={50}
                                                height={50}
                                            />
                                            <p className="ml-2">
                                                {getFileName(messageReply.content)}
                                            </p>
                                        </span>
                                    )}
                                </div>
                            </div>
                        )}
                        {message.type === MESSAGE_TYPE.STICKER ? (
                            <Image
                                alt={message.content}
                                src={message.content}
                                className="w-full h-auto"
                                width={40}
                                height={40}
                            />
                        ) : message.type === MESSAGE_TYPE.IMAGE ? (
                            <div
                                className={`grid gap-1 ${dataImages.length % 2 === 0
                                    ? "grid-cols-2"
                                    : dataImages.length % 3 === 0 || dataImages.length >= 3
                                        ? "grid-cols-3"
                                        : "grid-cols-1"
                                }`}
                            >
                                {dataImages.map((item: string) => {
                                    return (
                                        <Image
                                            key={item}
                                            alt={item}
                                            src={item}
                                            className={`object-cover ${dataImages.length >= 2
                                                ? "w-[100px] h-[100px]"
                                                : "w-full h-full"
                                            }`}
                                            width={dataImages.length >= 2 ? 300 : 200}
                                            height={dataImages.length >= 2 ? 300 : 200}
                                            onClick={() => {
                                                setIdMediaModal(message?._id);
                                                setUrlMediaModal(item);
                                                setIsOpenMediaLibrary(true);
                                            }}
                                        />
                                    );
                                })}
                            </div>
                        ) : message.type === MESSAGE_TYPE.TEXT ? (
                            <span className="text-[.9375rem] opacity-80 break-words">
                                {message.content}
                            </span>
                        ) : message.type === MESSAGE_TYPE.MEDIA ? (
                            <div
                                onClick={() => {
                                    setIdMediaModal(message?._id);
                                    setUrlMediaModal(message.content);
                                    setIsOpenMediaLibrary(true);
                                }}
                                className="relative"
                            >
                                <AiFillPlayCircle
                                    size={40}
                                    className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 text-thWhite"
                                />
                                <video
                                    src={message.content}
                                    width={300}
                                    height={300}
                                    className="w-[300px] h-auto"
                                    style={{ pointerEvents: "none" }}
                                />
                            </div>
                        ) : message.type === MESSAGE_TYPE.LINK ? (
                            <div className=" flex flex-col">
                                <a
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    href={message.content}
                                    className="break-words max-[480px]:truncate"
                                >
                                    {<MessageLink url={message.content} />}
                                </a>
                                {SOCIAL_REGEX_LIST.some((regex) =>
                                    regex.test(message.content)
                                ) && (
                                    <>
                                        {!isShowLink && (
                                            <div className="flex justify-start mt-2">
                                                <button
                                                    onClick={() => setIsShowLink(true)}
                                                    className="w-fit rounded-md dark: bg-thNewtral2 bg-light-thNewtral2 text-white px-2 py-1"
                                                >
                                                        Xem trước
                                                </button>
                                            </div>
                                        )}
                                        {isShowLink && <LinkPreview url={message.content} />}
                                        {isShowLink && (
                                            <div className="flex justify-end mt-2">
                                                <button
                                                    onClick={() => setIsShowLink(false)}
                                                    className="w-fit rounded-md dark: bg-thNewtral2 bg-light-thNewtral2 text-white px-2 py-1"
                                                >
                                                    <AiFillEyeInvisible size={24} />
                                                </button>
                                            </div>
                                        )}
                                    </>
                                )}
                            </div>
                        ) : (
                            <a
                                target="_blank"
                                rel="noopener noreferrer"
                                href={message.content}
                                className="flex items-center"
                            >
                                {/* eslint-disable-next-line @next/next/no-img-element */}
                                <img
                                    src={getFileIcon(message.content.split(".").pop() as string)}
                                    alt="icon"
                                    width={50}
                                    height={50}
                                />
                                <p className="ml-2 truncate">{getFileName(message.content)}</p>
                            </a>
                        )}
                        <div className="flex justify-between items-center gap-x-2 mt-2">
                            <span className="text-[.775rem] opacity-50">
                                {moment(message?.createdAt).format("HH:mm")}
                            </span>
                            {message?.reactions?.length > 0 && (
                                <div className="flex justify-center items-center gap-2 dark:bg-thNewtral1 bg-light-thNewtral1 rounded-lg px-1.5 py-1">
                                    {message?.reactions?.map((react: any, index: number) => {
                                        if (index <= 1) {
                                            return (
                                                <span key={react.icon}>
                                                    {EMOJI.find((e) => e.id === react.icon)?.src || ""}
                                                </span>
                                            );
                                        }
                                    })}
                                    <span className="text-[12px] opacity-60">
                                        {message?.reactions?.length}
                                    </span>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
                {windowDimensions.width > 480 && (
                    <div className={`relative `} ref={reactionBoxRef}>
                        {isReact && reactionID === message?._id && (
                            <ReactionBox
                                messageId={message._id}
                                conversationSocket={socket}
                                position="left-1 -translate-x-[0.99rem]"
                            />
                        )}
                        <div
                            className={`${styles.icon_area} flex items-center gap-x-2 opacity-25 `}
                        >
                            <AiFillSmile size={20} onClick={handleReaction} />
                            <AiFillMessage
                                size={20}
                                onClick={() => setMessageReply(message)}
                            />
                            <RiShareForwardFill
                                size={20}
                                onClick={() => setForwardContent(message)}
                            />
                            {
                                (detailChat?.conversationSetting?.find((item) => item.type === 3)?.value === true && !isTypeWaitting) ||
                                    ((isAdmin || isOwner) || !isGroup) ? (
                                        <Tooltip placement="bottomRight" title={"Ghim tin nhắn"}>
                                            <BsPinAngleFill size={20} onClick={() => handlePinMessage(message)} />
                                        </Tooltip>
                                    ) : null
                            }
                        </div>
                    </div>
                )}
            </div>
            <div className="w-full py-1"></div>
        </>
    );
}
