import { ReactElement } from 'react';
import styles from "../conversation.module.css";
import { Socket } from 'socket.io-client';
import { EMOJI, EVENT_NAMES } from '@/src/constants/chat';
import { useAppSelector } from '@/src/redux/hook';

type Reaction = {
    conversationSocket?: Socket<any, any>,
    messageId: string,
    position: string
}

export default function ReactionBox( { conversationSocket, messageId, position }: Reaction): ReactElement { 
    const { user } = useAppSelector(state => state.user);

    const reactMessage = (iconId: string) => {
        ("test emit");
        if(!conversationSocket) return;
        conversationSocket.emit(EVENT_NAMES.CONVERSATION.REACTION, {
            userId: user.id,
            icon: iconId,
            messageId
        });
    };

    return (
        <div className={`absolute z-10 -top-8 flex-col lg:flex-row lg:translate-x-0 -translate-y-36 lg:-translate-y-8 flex gap-2 w-fit rounded-full h-fit py-2 px-2 ${styles.reaction_box} ${position}`}>
            {EMOJI.map((emote: any) => {
                return (
                    <div onClick={() => reactMessage(emote.id)} key={emote.id} className='w-6 h-6 lg:w-10 lg:h-10 cursor-pointer'><p className={`text-base lg:text-3xl  text-center ${styles.reaction}`}>{emote.src}</p></div>
                );
            })}
        </div>
    );
}