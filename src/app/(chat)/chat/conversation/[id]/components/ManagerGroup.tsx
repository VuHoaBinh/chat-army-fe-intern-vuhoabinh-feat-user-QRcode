

import { ChangeEvent, ReactElement, useState } from "react";
import { BiSolidLock } from "react-icons/bi";
import { FiChevronLeft, FiMoreHorizontal } from "react-icons/fi";
import { LiaKeySolid, LiaUserLockSolid } from "react-icons/lia";
import { RiGroupLine } from "react-icons/ri";
import Image from "next/image";
import { CONVERSATION } from "@/src/constants/chat";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { setDetailChat } from "@/src/redux/slices/detailChatSlice";
import getAxiosClient from "@/src/utils/axios/axiosClient";
import { IoIosRemoveCircleOutline } from "react-icons/io";
import { Dropdown, MenuProps, Tooltip, message } from "antd";
import { CiCircleQuestion } from "react-icons/ci";
import { useRouter } from "next/navigation";
import { toast } from "react-toastify";
import { setListConversations } from "@/src/redux/slices/conversationSlice";
import { filterConversation, sortConversationByNewest } from "@/src/utils/conversations/filterConversation";
import ModalConfirm from "./Modal/ModalConfirm";
interface IManagerGroupProps {
    close: () => void;
    setIsShowModalBlockUser: React.Dispatch<React.SetStateAction<string>>
}
const ManagerGroup = ({ close, setIsShowModalBlockUser }: IManagerGroupProps): ReactElement => {
    const { detailChat, user, conversation } = useAppSelector(state => state.user);
    const [isShowBlock, setIsShowBlock] = useState(false);
    const [isShowAdmin, setIsShowAdmin] = useState(false);
    const [isShowModalConfirm, setIsShowModalConfirm] = useState(false);
    const router = useRouter();
    const dispatch = useAppDispatch();
    const isOwner  = detailChat?.members.find((item: any) => item.id === user?.id)?.type === CONVERSATION.ROLE_TYPE.OWNER;
    const isAdmin = detailChat?.members.find((item: any) => item.id === user?.id)?.type === CONVERSATION.ROLE_TYPE.ADMIN;
    
    const handleChange = async(e: ChangeEvent<HTMLInputElement>, type: number, action: string) => {
        if(!isOwner && !isAdmin) return ;
        try{
            const isChecked = e.target.checked ;
            const res = await getAxiosClient().put(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/setting/group/${action}/${detailChat.id}/${isChecked ? "on" : "off"}`);
            if(res.status === 200){
                dispatch(
                    setDetailChat({
                        ...detailChat,
                        conversationSetting: detailChat?.conversationSetting
                            ? detailChat.conversationSetting.map((item) =>
                                item.type === type
                                    ? { ...item, value:type === 9 ? isChecked ? "true" : "false"  : isChecked }
                                    : item
                            )
                            : []
                    })
                );
            }
        }catch(err){
            console.log(err);
        }
    };
    const permissionList = [
        // {
        //     id: 1,
        //     label: "Thay đổi tên và ảnh đại diện của nhóm",
        //     name: "allow-avatar",
        //     checked: detailChat?.conversationSetting?.find((item) => item.type === 1)?.value,
        //     type:1,
        //     action:''
        // },
        {
            id: 2,
            label: "Ghim tin nhắn, ghi chú, bình chọn",
            name: "allow-note",
            checked: detailChat?.conversationSetting?.find((item) => item.type === 3)?.value,
            type:3,
            action:'pin'
        },
        {
            id: 3,
            label: "Tạo mới ghi chú",
            name: "create-note",
            checked: detailChat?.conversationSetting?.find((item) => item.type === 4)?.value,
            type:4,
            action:'note'
        },
        {
            id: 4,
            label: "Tạo mới bình chọn",
            name: "allow-vote",
            checked: detailChat?.conversationSetting?.find((item) => item.type === 5)?.value,
            type:5,
            action:'vote'
        },
        {
            id: 5,
            label: "Gửi tin nhắn",
            name: "allow-chat",
            checked: detailChat?.conversationSetting?.find((item) => item.type === 6)?.value,
            type:6,
            action:'send-msg'
        }
    ];
    
    const handleRemoveBlockUser = async(id: string) => {
        if(!isOwner && !isAdmin) return;
        try{
            const res = await getAxiosClient().put(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/setting/group/unprevent-join/${detailChat.id}`, {
                preventId: id
            });
            if(res.status === 200){
                const { data :newDetailChat } = await getAxiosClient().get(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${detailChat.id}`);
                dispatch(setDetailChat(newDetailChat));
                message.success("Bỏ chặn thành công");
            }
        }catch(err){
            console.log(err);
            message.error("thất bại");
        }
    };
    const handleDisbandGroup = async() => {
        if(!isOwner) return;
        try{
            const res = await getAxiosClient().delete(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/group/disband/${detailChat.id}`);
            if(res.status === 200){
                toast.success("Giải tán nhóm thành công");
                router.push("/chat");
                const newListConversations = conversation?.listConversation?.filter((item: any) => item._id !== detailChat.id);
                dispatch(
                    setListConversations(
                        sortConversationByNewest(filterConversation(newListConversations, user.id))
                    )
                );
            }
        }catch(err){
            console.log(err);
        }
    };
    const handleRemoveAdmin = async(id: string) => {
        if(!isOwner) return;
        try{
            const res = await getAxiosClient().put(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/group/grant/${detailChat.id}`, {
                userId: id, role:CONVERSATION.ROLE_TYPE.MEMBER
            });
            if(res.status === 200){
                const { data :newDetailChat } = await getAxiosClient().get(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${detailChat.id}`);
                dispatch(setDetailChat(newDetailChat));
                message.success("Xóa admin thành công");
            }
        }catch(err){
            console.log(err);
            message.error("Xóa admin thất bại");
        }
    };
    const handleKickUser = async(id: string) => {
        try {
            const response = await getAxiosClient().delete(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/group/owner/${detailChat?.id}/kick`,
                {
                    data: {
                        ids: [id]
                    }
                }
            );
            if(response.status === 200){
                message.success("Kick user thành công");
                const { data :newDetailChat } = await getAxiosClient().get(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${detailChat.id}`);
                dispatch(setDetailChat(newDetailChat));
            }
        } catch (error: any) {
            toast.error(error.response.data.message);
        }
    };
    return (
        <>
            {
                // Hiển thị danh sách chặn
                isShowBlock && !isShowAdmin && (
                    <div className={`block h-full mt-4 relative overflow-x-hidden max-[480px]:px-2`}>
                        <div className="flex gap-x-4 items-center ">
                            <div className="cursor-pointer" onClick={() => setIsShowBlock(false)}>
                                <FiChevronLeft size={40} />
                            </div>
                            <p className="text-lg font-semibold">Chặn khỏi nhóm</p>
                        </div>
                        <div>
                            {
                                (detailChat.conversationSetting?.find(item=>item.type === 8)?.value as any)?.length > 0 ?
                                    (detailChat.conversationSetting?.find(item=>item.type === 8)?.value as any)?.map((item: any)=>{
                                        return (
                                            <div key={item?.userPrevented?.id} className="flex items-center justify-between gap-3 item hover:bg-gray-500 px-2 py-1">
                                                <div className="flex items-center gap-2">
                                                    <div className="max-w-[50px]">
                                                        <Image src={item?.userPrevented?.avatar} alt="avatar" width={50} height={50} className="w-full h-full object-cover rounded-full"/>
                                                    </div>
                                                    <p>{item?.userPrevented?.username}</p>
                                                </div>
                                                <button type="button" onClick={() => handleRemoveBlockUser(item?.id)}>
                                                    <IoIosRemoveCircleOutline size={20} />
                                                </button>
                                            </div>
                                        );
                                    }) :
                                    <>
                                        <div className="flex  justify-center items-center w-full gap-3 py-3">
                                            <LiaUserLockSolid size={100}/>
                                        </div>
                                        <p className="w-full">
                                            Những người đã bị chặn không thể tham gia lại nhóm, trừ khi được trưởng, phó nhóm bỏ chặn hoặc thêm lại vào nhóm.
                                        </p>
                                    </>

                            }
                            <div className="flex justify-between items-center mt-5 p-2">
                                <button type="button" className="w-full rounded-md py-2 bg-pink-400 text-red-700 font-bold" onClick={() => setIsShowModalBlockUser("BLOCK_USER")}>
                                    Thêm vào danh sách chặn
                                </button>
                            </div>
                        </div>
                    </div>
                )
            }
            {
                // Hiển thị quản lý nhóm
                !isShowBlock && !isShowAdmin && (
                    <div className={`block h-full mt-4 relative overflow-x-hidden max-[480px]:px-2`}>
                        <div className="flex gap-x-4 items-center">
                            <div className="cursor-pointer" onClick={() => close()}>
                                <FiChevronLeft size={40} />
                            </div>
                            <p className="text-lg font-semibold">Quản lý nhóm</p>
                        </div>
                        <div className="mt-2 overflow-y-auto p-2 relative">
                            {
                                !isOwner && !isAdmin &&
                                <div className="w-full absolute h-full inset-0 bg-gray-400 z-40 opacity-40 cursor-not-allowed"></div>
                            }
                            <div className="flex  justify-center items-center gap-3 bg-thNewtral2 p-3 z-50"> 
                                <BiSolidLock size={24} />Tính năng chỉ dành cho quản trị viên
                            </div>
                            <div className="mt-2 ">
                                <h3 className="font-semibold ">Cho phép các thành viên trong nhóm</h3>
                                <ul className="mt-3 flex flex-col gap-5">
                                    {
                                        permissionList.map((permission) => (
                                            <li className="flex items-center justify-between" key={permission.id}>
                                                <label htmlFor={permission.name}>{permission.label}</label>
                                                <input
                                                    type="checkbox"
                                                    id={permission.name}
                                                    className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                                                    onChange={(e)=>handleChange(e, permission.type, permission.action)}
                                                    checked={permission.checked as boolean}
                                                />
                                            </li>
                                        ))
                                    }
                                </ul>
                            </div>

                            <div className="flex justify-between items-center mt-3 border-t-4 border-gray-500 py-2">
                                <span className="flex items-center gap-1">Chế độ phê duyệt thành viên mới
                                    <Tooltip placement="top" title={"Khi bật, thành viên mới phải đươc trưởng/ phó nhóm duyệt mới có thể tham gia nhóm."}>
                                        <CiCircleQuestion />
                                    </Tooltip>
                                </span>
                                <label className="relative inline-flex items-center cursor-pointer">
                                    <input type="checkbox" value="" id="small-toggle" className="sr-only peer" 
                                        checked = {detailChat?.conversationSetting?.find((item) => item.type === 7)?.value as boolean}
                                        onChange={(e)=>handleChange(e, 7, "review-member")}
                                    />
                                    <div className="w-9 h-5 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-4 after:w-4 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600" />
                                    <span className=""></span>
                                </label>
                            </div>
                            <div className="flex justify-between items-center border-t-2 border-gray-500 py-2">
                                <span className="flex items-center gap-1">Cho phép dùng link tham gia nhóm
                                    <Tooltip placement="top" title={"Khi bật, tất cả thành viên trong nhóm đều có thể chia sẻ Link tham gia với người khác."}>
                                        <CiCircleQuestion />
                                    </Tooltip>
                                </span>
                                <label className="relative inline-flex items-center cursor-pointer">
                                    <input type="checkbox" value="" id="small-toggle" className="sr-only peer" 
                                        checked = {detailChat?.conversationSetting?.find((item) => item.type === 9)?.value === "true"}
                                        onChange={(e)=>handleChange(e, 9, "join-link-invite")}
                                    />
                                    <div className="w-9 h-5 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-4 after:w-4 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600" />
                                    <span className=""></span>
                                </label>
                            </div>
                            <div className=" border-t-4 border-gray-500">
                                {
                                    (isOwner || isAdmin) &&
                                    <div className="py-2 flex items-center justify-start cursor-pointer">
                                        <RiGroupLine size={24}/>
                                        <span className="ml-2 font-thin" onClick={() => setIsShowBlock(true)}>Chặn khỏi nhóm</span>
                                    </div>
                                }
                                {
                                    isOwner &&
                                    <div className="py-2 flex items-center justify-start cursor-pointer" onClick={() => setIsShowAdmin(true)}>
                                        <LiaKeySolid size={24}/>
                                        <span className="ml-2 font-thin">Trưởng và phó nhóm</span>
                                    </div>
                                }
                            </div>
                            {
                                isOwner && 
                                <div className="flex justify-between items-center mt-5 p-2">
                                    <button type="button" className="w-full rounded-md py-2 bg-red-500 text-gray-800 opacity-90 hover:opacity-100 font-bold" 
                                        onClick={()=>setIsShowModalConfirm(true)}
                                    >
                                    Giải tán nhóm 
                                    </button>
                                </div>
                            }
                        </div>
                    </div>
                )
            }
            {
                // Hiển thị thêm phó nhóm
                isShowAdmin && !isShowBlock && (
                    <div className={`block h-full mt-4 relative overflow-x-hidden max-[480px]:px-2`}>
                        <div className="flex gap-x-4 items-center ">
                            <div className="cursor-pointer" onClick={() => setIsShowAdmin(false)}>
                                <FiChevronLeft size={40} />
                            </div>
                            <p className="text-lg font-semibold">Trưởng phó nhóm</p>
                        </div>
                        {
                            detailChat?.members?.map((item) => {
                                return (
                                    (item.type === CONVERSATION.ROLE_TYPE.OWNER || item.type === CONVERSATION.ROLE_TYPE.ADMIN ) &&
                                            <div className="flex items-center justify-between gap-3 item hover:bg-gray-500 px-2 py-1">
                                                <div className="flex items-center gap-3 ">
                                                    <div className="max-w-[50px]">
                                                        <Image src={item?.avatar} alt="avatar" width={50} height={50} className="w-full h-full object-cover rounded-full"/>
                                                    </div>
                                                    <div>
                                                        <p>{item?.username}</p>
                                                        <p className="text-sm">{
                                                            item?.type === CONVERSATION.ROLE_TYPE.OWNER ? "Trưởng nhóm" : "Phó nhóm"
                                                        }</p>
                                                    </div>
                                                </div>
                                                {
                                                    user.id !== item?.id && (
                                                        <Dropdown trigger={["click"]} placement="bottomRight" className="mr-2" 
                                                            menu={{
                                                                items: [
                                                                    {
                                                                        key: item?.id + '1',
                                                                        label: (
                                                                            "Xóa phó nhóm"
                                                                        ),
                                                                        onClick: () => {
                                                                            handleRemoveAdmin(item?.id);
                                                                        }
                                                                    },
                                                                    {
                                                                        key: item?.id + '2',
                                                                        label: (
                                                                            "Kick khỏi nhóm"
                                                                        ),
                                                                        onClick: () => {
                                                                            handleKickUser(item?.id);
                                                                        }
                                                                    }
                                                                ] as MenuProps['items']
                                                            }}
                                                        >
                                                            <span className="cursor-pointer p-2 bg-thNewtral2 rounded-full"><FiMoreHorizontal size={20} color="white"/></span>
                                                        </Dropdown>
                                                    )
                                                }
                                            </div>
                                );
                            })
                        }
                        <div className="flex justify-between items-center mt-5 p-2 flex-col">
                            <button type="button" className="w-full rounded-md py-2 bg-thNewtral2 text-thDark-background font-bold" onClick={() => setIsShowModalBlockUser("ADD_ADMIN")}>
                                    Thêm phó nhóm
                            </button>
                            <button type="button" className="w-full mt-2 rounded-md py-2 bg-thNewtral2 text-thDark-background font-bold" 
                                onClick={() => setIsShowModalBlockUser("CHANGE_OWNER")}
                            >
                                    Chuyển quyền nhóm trưởng
                            </button>
                        </div>
                    </div>
                )
            }
            {
                isShowModalConfirm && (
                    <ModalConfirm
                        close={() => setIsShowModalConfirm(false)}
                        message={"Mời tất cả mọi người rời nhóm và xóa tin nhắn? Nhóm đã giải tán sẽ KHÔNG THỂ khôi phục."}
                        handleConfirm={handleDisbandGroup}
                        titleSubmit="Giải tán nhóm"
                        titleCancel="Không"
                    />
                )
            }
        </>
    );
};
export default ManagerGroup;