"use client";
import React, { ReactElement, cache, useCallback, useEffect, useState } from "react";
import ChatBox from "./components/ChatBox";
import useWindowDimensions from "../../../../../hook/useWindowDimension";
import ChatBoxMbl from "./components/ChatBoxMbl";
// import { IMessage } from '@/src/types/Message';
import axiosClient from "@/src/utils/axios/axiosClient";
import { decompressListMessage } from "@/src/utils/messages/decompressMessage";
import { IMessage } from "@/src/types/Message";
import { useAppSelector } from "@/src/redux/hook";
import ModalInputPinHidden from "./components/Modal/ModalInputPinHidden";
import { filterConversation, sortConversationByNewest } from "@/src/utils/conversations/filterConversation";

const fetchMessages = cache((id: string) =>
    axiosClient().get(
        `${
            process.env.NEXT_PUBLIC_DAK_CHAT_API
        }/conversations/${id}/messages?limit=20&offset=${0}&sort=desc`
    )
);
// const fetchMessages = cache((id: string) => axiosClient().get(`${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${id}/messages?sort=asc`));
const fetchConversations = cache(() =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations?offset=0&status=[2]`
    )
);
export default function Page({
    params
}: {
  params: { id: string };
}): ReactElement {
    const { width } = useWindowDimensions();
    const [listMessage, setListMessage] = useState<IMessage[]>([]);
    const conversation = useAppSelector((state) => state.user.conversation);
    const [pinHidden, setPinHidden] = useState<string>("");
    const [isHidden, setIsHidden] = useState<boolean>(false);
    const fetchData = useCallback(async () => {
        const { data } = await fetchConversations();
        return filterConversation(data, params.id);
    }, [params.id]);
    useEffect(() => {
        (async () => {
            let { data } = await fetchMessages(params.id);
            // Khi tải tin từ dtb, arrMess bị ngược nên thực hiện reverse lại mảng
            data = [...data].reverse();
            decompressListMessage(data);
            // (data);
            setListMessage(data);
        })();
        if(conversation.listHiddenConversation){
            const hiddenConversation = conversation.listHiddenConversation.find(item => item._id === params.id);
            if(hiddenConversation){
                setIsHidden(true);
                setPinHidden(hiddenConversation?.isConversationHidden.pin);
            }
        }
    }, [params.id]);

    useEffect(()=>{
        fetchData().then((data) => {
            const conversationsWithHiddenFlag = sortConversationByNewest(data).filter(
                (item: any) => item.isConversationHidden
            );
            
            if(conversationsWithHiddenFlag){
                const hiddenConversation = conversationsWithHiddenFlag.find(item => item._id === params.id);
                if(hiddenConversation){
                    setIsHidden(true);
                    setPinHidden(hiddenConversation?.isConversationHidden.pin);
                }
            }
        });

    }, []);
    return (
        <>
            {isHidden ? (
                <ModalInputPinHidden pinHidden={pinHidden} setIsHidden={setIsHidden} idConversation={params.id}/>
            ) : (
                <>
                    {width > 768 ? (
                        <ChatBox idConversation={params.id} listMessage={listMessage} />
                    ) : (
                        <ChatBoxMbl idConversation={params.id} listMessage={listMessage} />
                    )}
                </>
            )}
        </>
    );
}
