"use client";
import React, { ReactElement } from "react";
import { DataGrid, GridColDef, viVN } from "@mui/x-data-grid";
import { Box, Typography } from "@mui/material";
import TableToolbar from "./TableToolbar";
import "../Setting.module.css";

interface TableProps {
    title: string;
    type: number;
    columns: GridColDef[];
    rows?: any[];
}

const TableView = ({
    title,
    type,
    columns,
    rows
}: TableProps): ReactElement => {
    const [selectedRows, setSelectedRows] = React.useState<readonly string[]>([]);

    const onRowsSelectionHandler = (ids: any) => {
        const selectedRowsData = ids.map((id: any) =>
            rows?.find((row) => row.id === id)
        );
        setSelectedRows(selectedRowsData);
    };

    return (
        <Box sx={{ width: "100%" }} className="dark:text-white text-black">
            <div className="flex justify-between items-center dark:text-white text-black">
                <Typography>{title}</Typography>
                <TableToolbar numSelected={selectedRows.length} type={type} />
            </div>
            <DataGrid
                className="dark:text-white text-black"
                rows={rows || []}
                columns={columns}
                initialState={{
                    pagination: {
                        paginationModel: { page: 0, pageSize: 5 }
                    }
                }}
                pageSizeOptions={[5, 10]}
                checkboxSelection
                disableColumnFilter={true}
                disableColumnMenu={true}
                disableRowSelectionOnClick
                onRowSelectionModelChange={(ids) => onRowsSelectionHandler(ids)}
                localeText={viVN.components.MuiDataGrid.defaultProps.localeText}
                sx={{
                    "& .MuiDataGrid-cell:focus-within, .MuiDataGrid-cell:focus": {
                        outlineWidth: 0
                    },
                    "& .MuiDataGrid-row": {
                        "&.Mui-selected": {
                            backgroundColor: "rgba(169, 255, 54, 0.08)",
                            "&:hover": {
                                backgroundColor: "rgba(169, 255, 54, 0.12)"
                            }
                        },
                        "& .MuiDataGrid-withBorderColor": {
                            borderColor: "#FDFDFD66" // border bottom - Body
                        }
                    },
                    "& .MuiCheckbox-root": {
                        "& .MuiSvgIcon-root[data-testid='CheckBoxOutlineBlankIcon']": {
                            color: "inherit"
                        },
                        "& .MuiSvgIcon-root": {
                            color: "inherit"
                        },
                        "&:hover": {
                            backgroundColor: "rgba(169, 255, 54, 0.05)"
                        }
                    },
                    "& .MuiDataGrid-columnHeaders": {
                        "&:hover .MuiDataGrid-columnSeparator": {
                            visibility: "hidden" // no display separator
                        },
                        borderColor: "#FDFDFD66" // border bottom - Header
                    },
                    "& .MuiDataGrid-columnHeader": {
                        "&:focus": {
                            outlineWidth: 0
                        },
                        "&:focus-within": {
                            outlineWidth: 0
                        },
                        "& .MuiDataGrid-columnHeaderTitle": {
                            fontWeight: "600"
                        }
                    },
                    "& .MuiDataGrid-footerContainer": {
                        borderTopWidth: 0
                    },
                    "& .MuiTablePagination-root": {
                        color: "inherit",
                        fontFamily: "Inter",
                        "& .MuiSvgIcon-root": {
                            color: "inherit"
                        },
                        "& .Mui-disabled": {
                            "& .MuiSvgIcon-root": {
                                color: "inherit"
                            }
                        }
                    },
                    color: "inherit",
                    fontFamily: "Inter",
                    borderWidth: 0,
                    height: 400
                }}
            />
        </Box>
    );
};

export default TableView;
