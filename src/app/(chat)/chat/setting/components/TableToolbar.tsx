import { IconButton, Toolbar, Tooltip } from "@mui/material";
import React, { ReactElement, useEffect } from "react";
import { VscListFilter, VscAdd } from "react-icons/vsc";
import { PiTrashSimpleFill } from "react-icons/pi";
import { IoLogOutOutline } from "react-icons/io5";
import axiosClient from "@/src/utils/axios/axiosClient";
import { Socket, io } from "socket.io-client";
import { useDispatch, useSelector } from "react-redux";
import { deleteCookie } from "@/src/services/cookie";
import { addListIdToListIdConversationDecoded } from "@/src/redux/slices/endToEndEncryptionSlice";
import { deleteLoginCookies } from "@/src/utils/auth/handleCookies";
import { clearDataInIndexDB } from "@/src/utils/search/useIndexDB";
import { useClearCache } from "react-clear-cache";
import { useCookies } from "react-cookie";
import { useRouter } from "next/navigation";
import { IUserSocket } from "@/src/types/User";
import { useAppSelector } from "@/src/redux/hook";

interface TableToolbarProps {
  type: number;
  numSelected: number;
  idUser: string;
}

const TableToolbar = ({
  numSelected,
  type,
  idUser,
}: TableToolbarProps): ReactElement => {
  const router = useRouter();
  const dispatch = useDispatch();
  const { emptyCacheStorage } = useClearCache();
  const [cookies] = useCookies();
  const socket = io("http://localhost:8080");
  const userId = useAppSelector((state) => state.user.user.id);
  const handleSignOutAll = async () => {
    localStorage.removeItem("listEndToEndEncryptionConversation");
    dispatch(addListIdToListIdConversationDecoded([]));
    await deleteLoginCookies();
    const cookieNames = Object.keys(cookies);
    cookieNames.forEach(async (cookieName: any) => {
      await deleteCookie(cookieName);
    });
    clearDataInIndexDB();
    emptyCacheStorage();
    router.push("/login");
    try {
      const result = await axiosClient().post(
        `${process.env.NEXT_PUBLIC_DAK_API}/auth/logout`
      );
      if (result) {
        deleteLoginCookies();
        if (socket) socket.disconnect();
        router.push("/login");
      }
    } catch (err) {
      console.log(err);
    }
  };
  const handleClick = () => {
    socket.emit("log-out-all", userId);
  };
  socket.on("log-out", async (userId) => {
    if (userId === userId) {
      // Kiểm tra nếu userId nhận được trùng với userId của người dùng hiện tại
      await handleSignOutAll(); // Thực hiện đăng xuất tài khoản
    }
  });
  return (
    <Toolbar
      disableGutters={true} // disables gutter padding
      className="dark:text-white text-black"
    >
      <Tooltip title="Sign out all">
        <IconButton>
          <IoLogOutOutline
            onClick={handleClick}
            size={24}
            className="dark:text-white text-black"
          />
        </IconButton>
      </Tooltip>
      {numSelected > 0 ? (
        <Tooltip title="Delete">
          <IconButton>
            <PiTrashSimpleFill
              size={24}
              className="dark:text-white text-black"
            />
          </IconButton>
        </Tooltip>
      ) : (
        <>
          {type !== 1 && (
            <Tooltip title="Add">
              <IconButton>
                <VscAdd size={24} className="dark:text-white text-black" />
              </IconButton>
            </Tooltip>
          )}
          <Tooltip title="Filter list">
            <IconButton>
              <VscListFilter size={24} className="dark:text-white text-black" />
            </IconButton>
          </Tooltip>
        </>
      )}
    </Toolbar>
  );
};

export default TableToolbar;
