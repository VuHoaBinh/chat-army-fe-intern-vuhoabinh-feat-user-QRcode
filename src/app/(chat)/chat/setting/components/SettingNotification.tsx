import React, { ReactElement, useState } from "react";
import ProfileRadioGroup from "../../profile/[id]/components/ProfileRadioGroup";
import SettingSwitch from "./SettingSwitch";
import ModalResetPinHidden from "../../conversation/[id]/components/Modal/ModalResetPinHidden";

const SettingNotification = (): ReactElement => {
    const [isOpenResetPinHidden, setIsOpenResetPinHidden] = useState<boolean>(false);

    return (
        <div className="flex flex-col gap-6 px-6 p-8">
            <ProfileRadioGroup
                labelText="Thông báo"
                options={[
                    {
                        label: "All way on",
                        value: 0
                    },
                    {
                        label: "Turn off 15 minutes",
                        value: 1
                    },
                    {
                        label: "Turn off 2 hours",
                        value: 2
                    },
                    {
                        label: "Turn off 8 hours",
                        value: 3
                    },
                    {
                        label: "Until i turn it back",
                        value: 4
                    }
                ]}
            />
            <div className="flex justify-between items-center">
                <span>Hiện trạng thái &quot;Đã xem&quot;</span>
                <SettingSwitch />
            </div>
            <button
                className="bg-thRed py-2 rounded-lg hover:opacity-80"
                onClick={()=>setIsOpenResetPinHidden(true)}
            >
        Đặt lại mã PIN
            </button>
            {/* <div className='flex justify-between items-center'>
                <span>Ẩn trò chuyện</span>
                <SettingSwitch />
            </div>
            <div>
                <ProfileInput title='Mã PIN' type='text' maxLength={5} />
            </div> */}
            {isOpenResetPinHidden && (
                <ModalResetPinHidden
                    setIsOpenResetPinHidden={setIsOpenResetPinHidden}
                />
            )}
        </div>
    );
};

export default SettingNotification;
