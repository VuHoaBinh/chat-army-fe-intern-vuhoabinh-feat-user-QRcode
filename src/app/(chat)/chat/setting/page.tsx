"use client";

import React, { ReactElement, useState, useEffect } from "react";
import LayoutBox from "../components/LayoutBox";
import SettingNotification from "./components/SettingNotification";
import TableView from "./components/TableView";
import { GridColDef } from "@mui/x-data-grid";
import axiosClient from "@/src/utils/axios/axiosClient";
import { useRouter } from "next/navigation";
import { FiChevronLeft } from "react-icons/fi";

// Sample Data
const columnsLoginHistory: GridColDef[] = [
    { field: "time", headerName: "Time", width: 250, sortable: false },
    { field: "device", headerName: "Device", width: 250, sortable: false },
    { field: "method", headerName: "Login Methods", width: 250, sortable: false },
    { field: "location", headerName: "Location", width: 250, sortable: false }
];

// const rowsLoginHistory = [
//     { id: 1, time: '15 May 2020 9:30 am', device: 'Iphone 13', method: 'Login Methods', location: 'Location' },
//     { id: 2, time: '15 May 2020 9:30 am', device: 'Window', method: 'Login Methods', location: 'Location' },
//     { id: 3, time: '15 May 2020 9:30 am', device: 'Samsung Galaxy S22', method: 'Login Methods', location: 'Location' },
//     { id: 4, time: '15 May 2020 9:30 am', device: 'Oppo', method: 'Login Methods', location: 'Location' },
//     { id: 5, time: '15 May 2020 9:30 am', device: 'Snow', method: 'Login Methods', location: 'Location' },
//     { id: 6, time: '15 May 2020 9:30 am', device: 'Snow', method: 'Login Methods', location: 'Location' },
//     { id: 7, time: '15 May 2020 9:30 am', device: 'Snow', method: 'Login Methods', location: 'Location' },
//     { id: 8, time: '15 May 2020 9:30 am', device: 'Snow', method: 'Login Methods', location: 'Location' }
// ];

const columnsWalletAddress: GridColDef[] = [
    { field: "time", headerName: "Time", width: 250, sortable: false },
    { field: "address", headerName: "Address", width: 350, sortable: false }

];

// const rowsWalletAddress = [

//     {
//         id: 1,
//         time: "15 May 2020 9:30 am",
//         address: "19GfBxN2QxFgD8UHQWme2p5My4Sagm7Nip"
//     },
//     {
//         id: 2,
//         time: "15 May 2020 9:30 am",
//         address: "19GfBxN2QxFgD8UHQWme2p5My4Sagm7Nip"
//     },
//     {
//         id: 3,
//         time: "15 May 2020 9:30 am",
//         address: "19GfBxN2QxFgD8UHQWme2p5My4Sagm7Nip"
//     }
// ];
interface IHistoryData {
    id: string;
    login_at: number;
    region: string;
    type: number;
    platform: number;
    browser: number;
    user_agent: string;
    device_type: number;
    city: string;
}
const Setting = (): ReactElement => {
    const router = useRouter();
    const [historyData, setHistoryData] = useState<IHistoryData[]>([]);
    useEffect(() => {
        const fetchHistory = async () => {
            try {
                const res = await axiosClient().get(
                    `${process.env.NEXT_PUBLIC_DAK_API}/auth/history/login`
                );
                const dataHistory = [
                    ...res.data.data.isActive,
                    ...res.data.data.historyHasLogout
                ];
                setHistoryData(dataHistory);
            } catch (err) {
                console.log(err);
            }
        };
        fetchHistory();
    }, []);

    const formatDate = (unixTimestamp: number): string => {
        const date = new Date(unixTimestamp);
        const day = String(date.getDate()).padStart(2, "0");
        const month = date.toLocaleString("default", { month: "long" });
        const year = date.getFullYear();
        const hour = String(date.getHours()).padStart(2, "0");
        const minute = String(date.getMinutes()).padStart(2, "0");
        const period = date.getHours() >= 12 ? "pm" : "am";
        return `${day} ${month} ${year} ${hour}:${minute} ${period}`;
    };

    const getDeviceTypeName = (deviceType: number): string => {
        switch (deviceType) {
        case 1:
            return "Mobile";
        case 2:
            return "Web";
        default:
            return "Unknown";
        }
    };

    const getTypeName = (type: number): string => {
        switch (type) {
        case 1:
            return "Password";
        case 2:
            return "QRcode";
        case 3:
            return "Google";
        default:
            return "Unknown";
        }
    };

    const rowsLoginHistory = historyData.map((data) => ({
        id: data.id,
        time: formatDate(data.login_at),
        device: getDeviceTypeName(data.device_type),
        method: getTypeName(data.type),
        location: data.city
    }));

    return (
        <div className="flex flex-col gap-8 px-8 pb-8 text-base lg:text-lg dark:text-white text-black">
            <div className="sticky top-0 z-10 rounded-md py-4 dark:bg-thNewtral1 bg-light-thNewtral1 flex items-center px-6 mt-4">
                <div
                    className="relative cursor-pointer block  lg:hidden"
                    onClick={() => router.push("/chat")}
                >
                    <FiChevronLeft size={40} />
                </div>
                <h1 className="font-bold text-2xl">Setting</h1>
            </div>

            {/* Login history*/}
            <LayoutBox>
                <div className="flex flex-1 flex-col px-6 pt-4 pb-2 dark:text-white text-black">
                    <TableView
                        title="Lịch sử đăng nhập"
                        type={1} //login history
                        columns={columnsLoginHistory}
                        rows={rowsLoginHistory}

                    />
                </div>
            </LayoutBox>
            
            {/* Wallet and notifi */}
            <div className="grid grid-cols-1 xl:grid-cols-3 gap-8">
                <div className="col-span-1 xl:col-span-2 flex flex-1 flex-col gap-8">
                    <LayoutBox>
                        <div className="flex flex-1 flex-col px-6 pt-4 pb-2">
                            <TableView
                                title="Lịch sử ví"
                                type={2} //wallet history
                                columns={columnsWalletAddress}

                            />
                        </div>
                    </LayoutBox>
                </div>
                <div className="col-span-1">
                    <LayoutBox>
                        <SettingNotification />
                    </LayoutBox>
                </div>
            </div>
        </div>
    );
};

export default Setting;
