"use client";
import { ReactElement } from "react";
import Home from "./components/Home";
import useWindowDimensions from "../../../hook/useWindowDimension";
import LeftBarMbl from "./components/LeftBarMbl";
import Loading from "./loading";

export default function Page(): ReactElement {
    const { width } = useWindowDimensions();
    return (
        <div className="">
            <div className="w-full">
                {
                    !width ? <Loading />
                        : width > 768 ? <Home /> : <LeftBarMbl />
                }
            </div>
        </div>
    );
}