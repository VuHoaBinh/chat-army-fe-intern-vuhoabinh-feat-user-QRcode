'use client';
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { updateUserCreateGroup } from "@/src/redux/slices/listUserCreateGroupSlice";
import { IUserProfile } from "@/src/types/User";
import { memo, useEffect, useState } from "react";
import { FaTimes } from "react-icons/fa";
import { Avatar } from "../../components/Avatar";
type ListUser = {
    user: IUserProfile;
};
const ListUserCreateGroup = () => {
    const { listUserCreateGroup, localStorageData } = useAppSelector((state) => state.user) as any;
    const dispatch = useAppDispatch();

    const [dataStorage, setDataStorage] = useState<any>([{}] as any);
    useEffect(() => {
        const dataString = localStorage.getItem("Othername");

        if (dataString) {
            setDataStorage(JSON.parse(dataString));
        }
    }, [localStorageData]);
    const handleRemoveUser = (userClick: ListUser) => {
        if (
            listUserCreateGroup.find(
                (user: any) => user.user.id === userClick.user.id
            )
        ) {
            dispatch(
                updateUserCreateGroup(
                    listUserCreateGroup.filter(
                        (checkedItem: any) => checkedItem.user.id !== userClick.user.id
                    )
                )
            );
        }
    };
    return (
        <div className="h-[96%] rounded-md border-[1px] border-neutral-400 p-3 flex flex-col gap-y-4 overflow-y-scroll">
            <span>Đã chọn {listUserCreateGroup.length}/100</span>
            {listUserCreateGroup?.map((contact: any) => (
                <div
                    key={contact.user.id}
                    className="rounded-3xl text-sm px-2 bg-[#d5eaf5] flex  items-center gap-x-1 justify-between"
                >
                    <div className="flex md:gap-x-3 gap-x-1 items-center">
                        <div className="relative w-5 h-5 overflow-hidden rounded-full min-w-[20px]">
                            <Avatar
                                url={
                                    contact.user.avatar === "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg"
                                        ? "/default_avatar.jpg"
                                        : contact.user.avatar
                                }
                            />
                        </div>
                        <span className="font-medium leading-6 text-blue-500 truncate w-16 md:w-full">
                            {dataStorage[contact?.user?.idOther || ""] ? dataStorage[contact?.user?.idOther || ""] : contact?.user?.name}
                        </span>
                    </div>

                    <FaTimes
                        size={12}
                        color={"black"}
                        className="font-bold hover:cursor-pointer min-w-[10px]"
                        onClick={() => handleRemoveUser(contact)}
                    />
                </div>
            ))}
        </div>
    );
};

export default memo(ListUserCreateGroup);
