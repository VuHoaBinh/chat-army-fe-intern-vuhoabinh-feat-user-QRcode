import Image from 'next/image';
import React, { ReactElement } from 'react';
import Search from '../../../../../../public/search_no_result.svg';

const SearchNoResult = (): ReactElement => {
    return (
        <div className='relative h-full'>
            <div className='flex flex-col items-center justify-center select-none absolute inset-x-0 top-4'>
                <Image src={Search} alt='no search result icon' width={230} height={200} loading="lazy" />
                <span className='font-bold mt-2'>Không tìm thấy kết quả</span>
                <span className='text-[#b5b5b5] mt-2'>Vui lòng thử lại từ khóa hoặc bộ lọc khác</span>
            </div>
        </div>
    );
};

export default SearchNoResult;