/* eslint-disable @typescript-eslint/no-unused-vars */
"use client";
import { useAppSelector } from "@/src/redux/hook";
import { IUserProfile } from "@/src/types/User";

import axiosClient from "@/src/utils/axios/axiosClient";
import { filterConversation } from "@/src/utils/conversations/filterConversation";
import { useRouter } from "next/navigation";
import { ChangeEvent, ReactElement, cache, useEffect, useRef, useState } from "react";
import { BiSearch } from "react-icons/bi";
import { IoMdCloseCircle } from "react-icons/io";
import { MdOutlineArrowBackIosNew, MdOutlineGroupAdd, MdPersonAddAlt } from "react-icons/md";
import GroupByAlphabetContact from "./GroupByAlphabetContact";
import ModalAddContact from "./Modal/ModalAddContact";
import ModalCreateGroup from "./Modal/ModalCreateGroup";
import SearchNoResult from "./SearchNoResult";
import SortSelectBox from "./SortSelectBox";
type SortOrder = "A-Z" | "Z-A";

type ListUser = {
    user: IUserProfile;
};

interface GroupedContactsProps {
    [letter: string]: ListUser[];
}
const fetchContacts = cache(() =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations?offset=0&status=[2]&limit=10&type=1`
    )
);
interface IListContact {
    nameList?: string;
    serchUserGroup?: string;
}
const ListContact = ({
    nameList,
    serchUserGroup
}: IListContact): ReactElement => {
    const [dataStorage, setDataStorage] = useState<any>([{}] as any);
    const localStorageData = useAppSelector((state: any) => state.user);

    const { user } = useAppSelector((state) => state.user) as any;
    useEffect(() => {
        const dataString = localStorage.getItem("Othername");

        if (dataString) {
            setDataStorage(JSON.parse(dataString));
        }
    }, [localStorageData]);

    const [data, setData] = useState<any>(null);

    useEffect(() => {
        const fetchData = async () => {
            const { data } = await fetchContacts();
            setData(data);
        };

        fetchData();
    }, []); // Run once on mount
    const [conversation, setConversation] = useState<any>(
        filterConversation(data, user.id).filter((item: any) => {
            return item.type !== 2;
        })
    );
    useEffect(() => {
        setConversation(
            filterConversation(data, user.id).filter((item: any) => {
                return item.type !== 2;
            })
        );
    }, [data]);

    const [sortType, setSortType] = useState<SortOrder>("A-Z");
    const [sortedAlphabetList, setSortedAlphabetList] =
        useState<GroupedContactsProps>({});
    const [searchTerm, setSearchTerm] = useState<string>("");
    const [showModalAddContact, setShowModalAddContact] =
        useState<boolean>(false);
    const [showModalCreateGroup, setShowModalCreateGroup] =
        useState<boolean>(false);
    const ToogleModalGroup = () => {
        setShowModalCreateGroup(!showModalCreateGroup);
    };
    const router = useRouter();
    const [contactList, setContactList] = useState<ListUser[]>([]);
    const handleAddContact = () => {
        setShowModalAddContact(!showModalAddContact);
    };

    useEffect(() => {
        const users: any = [];
        if (conversation) {
            conversation.forEach((data: any) => {
                const elem = { user: data };

                users.push(elem);
            });

            setContactList(users);
        }
    }, [conversation, localStorageData]);

    const groupContactsByAlphabet = (contacts: ListUser[]) => {
        const groupedContacts: GroupedContactsProps = {};
        for (const contact of contacts) {
            const firstLetter = dataStorage[contact.user.idOther || contact.user.id]
                ? dataStorage[contact.user.idOther || ""][0].toUpperCase()
                : contact?.user?.name[0]?.toUpperCase();

            if (!groupedContacts[firstLetter]) {
                groupedContacts[firstLetter] = [];
            }
            groupedContacts[firstLetter].push(contact);
        }

        return groupedContacts;
    };

    const sortContacts = (
        contacts: GroupedContactsProps,
        sortType: SortOrder
    ) => {
        const sortedKeys =
            sortType === "A-Z"
                ? Object.keys(contacts).sort()
                : Object.keys(contacts).sort().reverse();
        const sortedContacts: GroupedContactsProps = {};

        for (const letter of sortedKeys) {
            sortedContacts[letter] = contacts[letter].sort((a, b) =>
                sortType === "A-Z"
                    ? dataStorage[a?.user?.idOther || ""] && dataStorage[b?.user?.idOther || ""]
                        ? dataStorage[a.user.idOther || ""].localeCompare(dataStorage[b.user.idOther || ""])
                        : a?.user?.name?.localeCompare(b.user.name)
                    : b?.user?.name?.localeCompare(a.user.name)
            );
        }

        return sortedContacts;
    };
    useEffect(() => {
        const groupedContacts = groupContactsByAlphabet(contactList);
        const sortedContacts = sortContacts(groupedContacts, sortType);
        setSortedAlphabetList(sortedContacts);
    }, [sortType, contactList, localStorageData]);

    const handleSearch = (e: ChangeEvent<HTMLInputElement>): void => {
        setSearchTerm(e.target.value);

    };

    const filteredAlphabetList: GroupedContactsProps = Object.fromEntries(
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        Object.entries(sortedAlphabetList).filter(([letter, contacts]) => {
            if (nameList == "listFriend") {
                return contacts.some((contact) =>
                    dataStorage[contact.user.idOther || ""]
                        ? dataStorage[contact.user.idOther || ""]
                            .toLowerCase()
                            .includes(serchUserGroup?.toLowerCase())
                        : contact.user?.name
                            ?.toLowerCase()
                            .includes(serchUserGroup!.toLowerCase())
                );
            }
            return contacts.some((contact) =>
                dataStorage[contact.user.idOther || ""]
                    ? dataStorage[contact.user.idOther || ""]
                        .toLowerCase()
                        .includes(searchTerm.toLowerCase())
                    : contact.user?.name?.toLowerCase().includes(searchTerm.toLowerCase())
            );
        })
    );

    const modalAddContactRef = useRef<any>();
    const modalCreateGroupRef = useRef<any>();
    const handleClickOutside = (event: { target: any }) => {
        if (
            modalAddContactRef.current &&
            !modalAddContactRef.current.contains(event.target)
        ) {
            setShowModalAddContact(false);
        }
        if (
            modalCreateGroupRef.current &&
            !modalCreateGroupRef.current.contains(event.target)
        ) {
            setShowModalCreateGroup(false);
        }
    };

    useEffect(() => {
        document.addEventListener("mousedown", handleClickOutside);

        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, []);


    return (
        <>
            {nameList && nameList === "listFriend" ? (
                <div>
                    {Object.keys(filteredAlphabetList).length === 0 ? (
                        <>
                            <SearchNoResult />
                        </>
                    ) : (
                        Object.entries(filteredAlphabetList).map(([letter, contacts]) => (
                            <GroupByAlphabetContact
                                nameList={nameList}
                                key={letter}
                                title={letter}
                                contactData={contacts.filter((contact) =>
                                    dataStorage[contact.user.idOther || ""]
                                        ? dataStorage[contact.user.idOther || ""].toLowerCase()
                                            .includes(serchUserGroup?.toLowerCase())
                                        : contact?.user?.name
                                            .toLowerCase()
                                            .includes(serchUserGroup!.toLowerCase())
                                )}

                            />
                        ))
                    )}
                </div>
            ) : (
                <div>
                    <div className="flex items-center justify-between mb-4 mx-7 ">
                        <div className="flex items-center gap-3 ml-[-4px]">
                            <MdOutlineArrowBackIosNew
                                size={24}

                                className="cursor-pointer block sm:hidden md:hidden lg:hidden "
                                onClick={() => router.push("/chat")}
                            />
                            <h1 className="font-bold text-2xl">Contact</h1>
                        </div>
                        <div className="flex items-center gap-x-3">
                            {/* <Image
                                src={AddIcon}
                                width={28}
                                alt="Add contact"
                                className="rounded-md cursor-pointer m-2 block md:hidden lg:hidden"
                                onClick={handleAddContact}
                            /> */}
                            <MdPersonAddAlt
                                className="rounded-md cursor-pointer m-2 block md:hidden lg:hidden dark:text-thPrimary text-light-thPrimary"

                                size={30}
                                onClick={handleAddContact} />
                            <MdOutlineGroupAdd
                                size={30}

                                className="cursor-pointer m-2 block md:hidden lg:hidden dark:text-thPrimary text-light-thPrimary"
                                onClick={ToogleModalGroup}
                            />
                        </div>
                    </div>

                    <div className="flex flex-col gap-8 relative h-full">
                        {/* filter container */}
                        <div className="flex gap-2 md:gap-4 flex-col md:flex-row md:mx-8 mx-2 justify-center items-center">
                            {/* search bar */}
                            <div className="flex items-center border border-thNewtral1 rounded-md relative w-80">
                                <BiSearch className="ml-3 text-[#b5b5b5]" />
                                <input
                                    type="text"
                                    placeholder="Tìm Bạn"
                                    value={searchTerm}
                                    onChange={handleSearch}
                                    className="bg-transparent outline-none p-2 w-full pr-12 pl-3"
                                />
                                {searchTerm !== "" && (
                                    <IoMdCloseCircle
                                        className="absolute right-3 cursor-pointer text-[#b5b5b5] hover:text-white"
                                        size={24}
                                        onClick={() => setSearchTerm("")}
                                    />
                                )}
                            </div>
                            {/* select bar */}
                            <SortSelectBox sortType={sortType} setSortType={setSortType} />

                            {/* <div className="hidden md:block lg:block">
                                <Image
                                    src={AddIcon}
                                    width={28}
                                    alt="Add contact"
                                    className="rounded-md cursor-pointer m-2"
                                    onClick={handleAddContact}
                                />
                            </div> */}

                            {/* <AiOutlineUsergroupAdd
                                size={30}
                                color="#FCD535"
                                className="cursor-pointer m-0 md:m-2 hidden md:block lg:block min-w-[30px]"
                                onClick={ToogleModalGroup}
                            /> */}
                            <MdPersonAddAlt
                                className="cursor-pointer m-0 md:m-2 hidden md:block lg:block min-w-[30px]"
                                color="#FCD535"
                                size={30}
                                onClick={handleAddContact} />
                            <MdOutlineGroupAdd
                                size={30}
                                color="#FCD535"
                                className="cursor-pointer m-0 md:m-2 hidden md:block lg:block min-w-[30px]"
                                onClick={ToogleModalGroup}
                            />
                        </div>


                        {Object.keys(filteredAlphabetList).length === 0 ? (
                            <SearchNoResult />
                        ) : (



                            Object.entries(filteredAlphabetList).map(([letter, contacts]) => (
                                <>
                                    <GroupByAlphabetContact
                                        nameList={nameList}
                                        key={letter}
                                        title={letter}
                                        contactData={contacts.filter((contact) =>
                                            dataStorage[contact.user.idOther || ""]
                                                ? dataStorage[contact.user.idOther || ""].toLowerCase()
                                                    .includes(searchTerm.toLowerCase())
                                                : contact?.user?.name
                                                    .toLowerCase()
                                                    .includes(searchTerm.toLowerCase())
                                        )}
                                    />
                                </>
                            ))
                        )}
                    </div>
                </div>

            )}
            {showModalAddContact && (<div className="fixed w-screen flex items-center justify-center top-0 left-0 h-full bg-[#00000080] z-50">

                <div ref={modalAddContactRef}>
                    <ModalAddContact
                        onClose={() => {
                            setShowModalAddContact(false);

                        }}
                    />
                </div>


            </div>)}


            {/* modalGroupChat */}
            {showModalCreateGroup && (
                <div className="fixed w-screen flex items-center justify-center top-0 h-full bg-[#00000080] left-0 z-50">

                    <div ref={modalCreateGroupRef} >
                        <ModalCreateGroup
                            onClose={() => {
                                setShowModalCreateGroup(false);
                            }}
                        />
                    </div>
                </div>
            )}

        </>

    );

};

export default ListContact;
