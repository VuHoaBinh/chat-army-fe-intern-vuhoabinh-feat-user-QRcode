import React from "react";

const RoundCheckbox: React.FC<{ checked: boolean; onChange: () => void }> = ({
    checked,
    onChange
}) => (
    <div
        className={`w-5 h-5 flex items-center justify-center min-w-[20px]
                ${
    checked ? "bg-blue-500" : "bg-white border-2 border-gray-300"
    } rounded-full`}
        onClick={onChange}
    >
        {checked && (
            <svg className="fill-current text-white w-4 h-4" viewBox="0 0 20 20">
                <path d="M0 11l2-2 5 5L18 3l2 2L7 18z" />
            </svg>
        )}
    </div>
);

export default RoundCheckbox;
