import React, { useState, ReactElement, useRef, useEffect } from "react";
import { HiArrowsUpDown, HiOutlineChevronDown } from "react-icons/hi2";
import { BsCheck2 } from "react-icons/bs";

type SortOrder = "A-Z" | "Z-A";

interface SortSelectBoxProps {
    sortType: SortOrder;
    setSortType: (sortType: SortOrder) => void;
}

const SortSelectBox = ({
    sortType,
    setSortType
}: SortSelectBoxProps): ReactElement => {
    const [isDropdownOpen, setIsDropdownOpen] = useState(false);
    const selectRef = useRef<HTMLDivElement>(null);

    const toggleDropdown = () => {
        setIsDropdownOpen(true);
    };

    const handleChangeSort = (newSort: SortOrder) => {
        setSortType(newSort);
        setIsDropdownOpen(false);
    };

    // close when click outside
    const closeOpenMenus = (e: MouseEvent): void => {
        if (selectRef.current && !selectRef.current.contains(e.target as Node)) {
            setIsDropdownOpen(false);
        }
    };
    useEffect(() => {
        document.addEventListener("mousedown", closeOpenMenus);
        return () => {
            document.removeEventListener("mousedown", closeOpenMenus);
        };
    }, []);

    return (
        <div className="relative inline-block w-80 md:w-72" ref={selectRef}>
            <div
                className={`flex items-center dark:bg-thNewtral1 bg-light-thNewtral1 hover:bg-thPrimaryHover w-full md:w-72 rounded-md cursor-pointer h-12 ${isDropdownOpen && "bg-thPrimaryHover dark:text-thPrimary text-light-thPrimary "
                }`}
                onClick={toggleDropdown}
            >
                <div className="flex items-center px-4">
                    <HiArrowsUpDown className="mr-3" />
                    <span className="mr-14">Tên ({sortType})</span>
                    <HiOutlineChevronDown className="absolute right-3" />
                </div>
            </div>
            <div
                className={`absolute right-0 mt-2 py-2 origin-top-right rounded-md w-full shadow-md dark:bg-thNewtral1 bg-light-thNewtral1 z-10 ${isDropdownOpen ? "" : "hidden"
                }`}
            >
                <div
                    className="hover:bg-thPrimaryHover py-2 cursor-pointer"
                    onClick={() => handleChangeSort("A-Z")}
                >
                    <div className="flex items-center gap-4 px-5">
                        <div className="w-6 dark:text-thPrimary text-light-thPrimary">
                            {sortType === "A-Z" && <BsCheck2 size={24} />}
                        </div>
                        <span>Tên (A-Z)</span>
                    </div>
                </div>
                <div
                    className="hover:bg-thPrimaryHover py-2 cursor-pointer"
                    onClick={() => handleChangeSort("Z-A")}
                >
                    <div className="flex items-center gap-4 px-5">
                        <div className="w-6 dark:text-thPrimary text-light-thPrimary">
                            {sortType === "Z-A" && <BsCheck2 size={24} />}
                        </div>
                        <span>Tên (Z-A)</span>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SortSelectBox;
