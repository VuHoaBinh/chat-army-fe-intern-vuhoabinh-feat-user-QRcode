/* eslint-disable @typescript-eslint/no-unused-vars */
"use client";
import ChatCallIcon from "@/public/call.svg";
import CloseIcon from "@/public/trash.svg";
import EditIcon from "@/public/edit.svg";
import VideoCallIcon from "@/public/video-call.svg";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import {
    setListConversations,
    setOtherUser
} from "@/src/redux/slices/conversationSlice";
import { IUserProfile } from "@/src/types/User";
import axiosClient from "@/src/utils/axios/axiosClient";
import { filterConversation } from "@/src/utils/conversations/filterConversation";
import Image from "next/image";
import { useRouter } from "next/navigation";
import { ReactElement, useEffect, useRef, useState } from "react";
import EditUserName from "../../../chat/components/Modal/EditUserName";
import { Avatar } from "../../components/Avatar";
import RoundCheckbox from "./RoundedChecbox";
import { updateUserCreateGroup } from "@/src/redux/slices/listUserCreateGroupSlice";
import { BiEditAlt } from "react-icons/bi";

type ListUser = {
    user: IUserProfile;
};

interface GroupByAlphabetContactProps {
    title: string;
    contactData: ListUser[];
    nameList?: string;
}

const GroupByAlphabetContact = ({
    title,
    contactData,
    nameList
}: GroupByAlphabetContactProps): ReactElement => {

    const actions = [
        {
            name: "chat call",
            icon: ChatCallIcon,
            onClick: (e: any, contact: any) => {
                e.stopPropagation();
                setIsCalling(true);
                setSelectedContact(contact);
            }
        },
        {
            name: "video call",
            icon: VideoCallIcon,
            onClick: (e: any, contact: any) => {
                e.stopPropagation();
                setIsCalling(true);
                setSelectedContact(contact);
            }
        },
        {
            name: "delete contact",
            icon: CloseIcon,
            onClick: (e: any, contact?: any) => {
                e.stopPropagation();

                if (window.confirm("Are you sure to delete this contact?")) {
                    const deleteContact = async () => {
                        const res = await axiosClient().delete(
                            `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/${contact.user.id}`
                        );
                        if (res.status === 200) {
                            alert("Delete contact successfully!!!");
                            const refreshListConversation = async () => {
                                const { data } = await axiosClient().get(
                                    `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations?offset=0&status=[2]&limit=10`
                                );
                                const listConversation = filterConversation(
                                    data,
                                    contact.user.directUserId
                                );

                                dispatch(setListConversations(listConversation));
                            };
                            refreshListConversation();
                        }
                    };
                    deleteContact();
                }
            }
        }
    ];
    const [isCalling, setIsCalling] = useState(false);
    const [isEditUserName, setIsEditUserName] = useState(false);
    const { listUserCreateGroup } = useAppSelector((state) => state.user) as any;
    const [otherUsername, setOtherUsername] = useState<any>("");
    const [selectedContact, setSelectedContact] = useState<any>();
    const [dataStorage, setDataStorage] = useState<any>([]);
    const modalEditUserRef = useRef<HTMLDivElement>(null);
    const localStorageData = useAppSelector((state: any) => state.user);
    const handleItemClick = (item: any) => {
        if (
            listUserCreateGroup.find((user: any) => user.user.id === item.user.id)
        ) {
            dispatch(
                updateUserCreateGroup(
                    listUserCreateGroup.filter(
                        (checkedItem: any) => checkedItem.user.id !== item.user.id
                    )
                )
            );
        } else {
            dispatch(updateUserCreateGroup([...listUserCreateGroup, item]));
        }
    };
    const handleEditUsername = (e: any, contact: any) => {
        e.stopPropagation();
        setSelectedContact(contact);
        setOtherUsername(
            dataStorage[contact.user.idOther]
                ? dataStorage[contact.user.idOther]
                : contact.user.name
        );
        setIsEditUserName(!isEditUserName);
    };
    useEffect(() => {
        const dataString = localStorage.getItem("Othername");
        if (dataString) {
            setDataStorage(JSON.parse(dataString));
        }
    }, [localStorageData]);
    const handleClickOutside = (event: { target: any }) => {
        if (
            modalEditUserRef.current &&
            !modalEditUserRef.current.contains(event.target)
        ) {
            setIsEditUserName(false);
        }
    };
    useEffect(() => {
        document.addEventListener("mousedown", handleClickOutside);

        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, []);

    const { push } = useRouter();
    const dispatch = useAppDispatch();
    const { conversation } = useAppSelector((state) => state.user);

    const directConversation = (contact: any) => {
        const selectedConversation: any = conversation?.listConversation?.find(
            (conver: any) => conver?.id === contact?.user?.id
        );
        dispatch(
            setOtherUser({
                otherUsername: selectedConversation?.name,
                otherAvatar: selectedConversation?.avatar,
                lastMessage: conversation.lastMessage || {}
            })
        );
        push(
            `/chat/conversation/${selectedConversation?._id}?isCalling=${isCalling}`
        );
    };

    useEffect(() => {
        if (isCalling) {
            directConversation(selectedContact);
        }
    }, [isCalling]);

    return (
        <>

            {nameList && nameList === "listFriend" ? (
                <div className="flex flex-col  ">
                    <span className="font-bold leading-6 mb-3 pl-8">{title}</span>

                    {contactData.map((contact, index) => (
                        // contact item wrapper

                        <div
                            key={contact.user.avatar + index}
                            className="hover:bg-thNewtral1  cursor-pointer"
                            onClick={() => handleItemClick(contact)}
                        >
                            {/* contact item */}

                            <div className="flex justify-between items-center py-3 md:px-8 px-2 dark:hover:bg-thNewtral2 hover:bg-light-thNewtral2 hover:cursor-pointer">
                                {/* user infor */}
                                <div className="flex items-center gap-3 truncate">
                                    <RoundCheckbox
                                        checked={listUserCreateGroup.find(
                                            (usergr: any) => usergr.user.id === contact.user.id
                                        )}
                                        onChange={() => { }}
                                    />
                                    <div className="relative w-10 h-10 overflow-hidden rounded-full min-w-[40px]">
                                        <Avatar
                                            url={
                                                contact.user.avatar === "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg"
                                                    ? "/default_avatar.jpg"
                                                    : contact.user.avatar
                                            }
                                        />
                                    </div>
                                    <span className="font-bold leading-6 truncate">
                                        {dataStorage[contact.user.idOther || ""] || contact.user.name}
                                    </span>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            ) : (
                <div className="flex flex-col">
                    <span className="font-bold leading-6 mb-3 pl-8">{title}</span>

                    {contactData.map((contact, index) => (
                        // contact item wrapper

                        <div
                            onClick={() => directConversation(contact)}
                            key={contact.user.name + index}
                            className="dark:hover:bg-thNewtral1 hover:bg-light-thNewtral1 cursor-pointer"
                        >
                            <></>
                            {/* contact item */}
                            <div className="flex justify-between items-center py-3 px-8">
                                {/* user infor */}
                                <div className="flex-1 flex items-center  md:gap-3 mr-2">
                                    <div className="flex items-center">
                                        <div className="relative w-10 h-10 md:w-14 md:h-14 overflow-hidden rounded-full">
                                            <Avatar url={contact.user.avatar == "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg" ? "/default_avatar.jpg" : contact.user.avatar} />
                                        </div>
                                        <span className="font-bold leading-6 px-3 truncate flex-1 max-w-[30vw] min-w-[30vw] md:max-w-[9rem] md:min-w-[9rem] lg:max-w-[30vw] lg:min-w-[30vw]">
                                            {dataStorage[contact.user?.idOther || ""] || contact.user.name}
                                        </span>
                                    </div>
                                    <div
                                        className=" rounded-md md:pr-0 w-[1.5rem]"
                                        onClick={(e) => handleEditUsername(e, contact)}
                                    >
                                        {/* <Image
                                            src={EditIcon}
                                            width={28}
                                            alt="edit icon"
                                            className="m-2"
                                        /> */}
                                        <BiEditAlt size={28} className="m-2 text-black dark:text-white" />
                                    </div>
                                </div>

                                {/* action */}
                                <div className="flex items-center gap-2 md:gap-2 w-[6rem]">
                                    {actions.map((action) => (
                                        <div
                                            key={`index-${action.name}`}
                                            className="rounded-md"
                                            onClick={(e) => action.onClick(e, contact)}
                                        >
                                            <Image
                                                src={action.icon}
                                                alt={action.name}
                                                width={28}
                                                className="m-2"
                                            />
                                        </div>
                                    ))}
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            )}
            {isEditUserName &&


                <div className="fixed top-[-90px] bottom-0  w-full h-[calc(100%+110px)]`} bg-[#00000080] min-h-screen left-0  z-50 ">

                    <div ref={modalEditUserRef} >


                        <EditUserName
                            contact={selectedContact}
                            onClose={() => {
                                setIsEditUserName(false);
                            }}
                            onSubmit={(otherUsername) => {
                                setOtherUsername(otherUsername);
                                setIsEditUserName(false);
                            }}
                            otherUsername={otherUsername}

                        />
                    </div>
                </div>


            }
        </>
    );
};

export default GroupByAlphabetContact;
