"use client";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { updateUserCreateGroup } from "@/src/redux/slices/listUserCreateGroupSlice";
import axiosClient from "@/src/utils/axios/axiosClient";
import { uploadFile } from "@/src/utils/upload/getUploadToken";
import Image from "next/image";
import { ReactElement, useRef, useState } from "react";
import { AiOutlineCloseCircle } from "react-icons/ai";
import { BiSearch } from "react-icons/bi";
import { BsCameraFill } from "react-icons/bs";
import { LiaTimesSolid } from "react-icons/lia";
import ListContact from "../ListContact";
import ListUserCreateGroup from "../ListUserCreateGroup";

interface EditUserNameProps {
    //   contact: any;
    onClose: () => void;
    //   onSubmit: (otherUsername: string) => void;
    //   otherUsername: string;
}

const ModalCreateGroup = ({ onClose }: EditUserNameProps): ReactElement => {
    const [inputValue, setInputValue] = useState("");
    const [nameGroup, setNameGroup] = useState("");
    const [avatar, setAvatar] = useState<File>();

    const dispatch = useAppDispatch();
    const { listUserCreateGroup } = useAppSelector((state) => state.user) as any;

    const handleCloseCreateGroup = () => {
        if (listUserCreateGroup.length > 0) {
            if (window.confirm("Bạn có muốn hủy nhóm đang tạo không?")) {
                onClose();
                dispatch(updateUserCreateGroup([]));
            } else return;
        }
        onClose();
    };
    const fetchCreateGroup = async (nameGroup: string, listId: any) => {
        let linkAvatar = "";

        if (avatar) {
            const formData = new FormData();
            formData.append("files", avatar);
            const data = await uploadFile(formData);
            linkAvatar = data[0].link;
        }

        const body: {
            type: number;
            name: string;
            memberIds: string[];
            avatar?: string; // Make avatar property optional
        } = {
            type: 2,
            name: nameGroup,
            memberIds: listId
        };

        if (linkAvatar !== "") {
            body.avatar = linkAvatar;
        }

        try {
            const response = await axiosClient().post(
                `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations`,
                body
            );
            // Handle the response here
            if (response.status === 200) {
                alert("Tạo nhóm thành công");
                // axiosClient()
                //   .get(
                //     `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations?offset=0&status=[2]`
                //   )
                //   .then((response: any) => {
                //     const { data } = response;
                //     let listGroupChat = data?.filter((user: any) => user?.type !== 1);
                //     const listConversation = sortConversationByNewest(listGroupChat);
                //     dispatch(setListConversations(listConversation));
                //   });
                dispatch(updateUserCreateGroup([]));

                onClose();
            }
        } catch (error) {
            // Handle any errors that occur during the request
            console.error(error);
        }
    };

    const inputRef = useRef<HTMLInputElement>(null);
    const focusTextInput = () => inputRef?.current?.focus();
    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setInputValue(e.target.value);
    };
    const handleNameGroup = (e: React.ChangeEvent<HTMLInputElement>) => {
        setNameGroup(e.target.value);
    };

    const handleCreateGroup = (nameGroup: string, listUserCreateGroup: any) => {
        if (nameGroup.trim() === "") {
            alert("Bạn chưa nhập tên nhóm");
            focusTextInput();
            return;
        }

        const listId = [] as any;
        listUserCreateGroup.map((user: any) => {
            listId.push(user.user.idOther ? user.user.idOther : user.user.id);
        });

        // ("listId", listId, listUserCreateGroup);
        fetchCreateGroup(nameGroup, listId);
    };

    return (
        <div
            className="
            fixed 
        
           top-1/2 left-1/2  transform -translate-x-1/2 -translate-y-1/2
           dark:bg-thNewtral1 bg-light-thNewtral1
            rounded-lg 
            overflow-hidden
            shadow-2xl
            transition-all
            duration-200
            ease-in-out
            md:max-w-[600px]
            lg:max-w-[600px]
            w-[90%]
            p-3
            z-50
           
          
            min-h-[500px]
            flex gap-4
            flex-col
            "
        >
            <div className=" flex justify-between items-center">
                <span>Tạo Nhóm</span>
                <LiaTimesSolid
                    size={28}
                    className="hover:cursor-pointer hover:opacity-70"
                    onClick={handleCloseCreateGroup}
                />
            </div>
            <hr className="bg-black dark:bg-white h-[1px] w-full" />

            <div className="text-sm flex flex-col max-h-[300px] mb- ">
                <div className="flex items-center  rounded-md relative w-full h-full mb-2 ">
                    {/*           
          <AiOutlineCamera
            size={40}
            className=" text-[#b5b5b5] rounded-full border-[1px] p-2 cursor-pointer"
          /> */}
                    {avatar ? (
                        <div className="relative mb-2">
                            <Image
                                className="w-[60px] h-[54px] rounded-full border-solid border-2"
                                width={60}
                                height={54}
                                src={URL.createObjectURL(avatar)}
                                alt=""
                            />
                            <span className="top-0 right-0 absolute  w-3.5 h-3.5 rounded-full cursor-pointer">
                                <AiOutlineCloseCircle
                                    onClick={() => setAvatar(undefined)}
                                    size={18}
                                    color="red"
                                />
                            </span>
                        </div>
                    ) : (
                        <label
                            htmlFor="avatar"
                            className="block mb-2 text-sm font-medium text-gray-300 border-solid border-2 p-2 rounded-full dark:border-inherit border-gray-500"
                        >
                            <BsCameraFill size={35} />
                        </label>
                    )}
                    <input
                        type="file"
                        name="file"
                        className="hidden"
                        id="avatar"
                        onChange={(e: any) => setAvatar(e.target.files[0])}
                    />
                    <input
                        ref={inputRef}
                        type="text"
                        placeholder="Nhập Tên Nhóm..."
                        className="bg-transparent outline-none p-2 w-full pr-12 pl-3"
                        value={nameGroup}
                        onChange={handleNameGroup}
                    // onKeyDown={handleKeyDown}
                    />
                </div>
                <div className="flex items-center rounded-md relative w-full h-full mb-3 border-[1px] dark:border-inherit border-black">
                    <BiSearch className="ml-3 text-[#b5b5b5]" />
                    <input
                        type="text"
                        placeholder="Nhập Tên,Số điện thoại"
                        className="bg-transparent outline-none p-2 w-full pr-12 pl-3"
                        value={inputValue}
                        onChange={handleInputChange}
                    // onKeyDown={handleKeyDown}
                    />
                </div>
            </div>
            <hr className="bg-black dark:bg-white h-[1px] w-full" />
            <div className="h-[340px] flex gap-x-6">
                <div
                    className={`overflow-y-auto ${listUserCreateGroup.length > 0 ? "md:w-[70%] w-[55%]" : "w-full"
                    }  h-full overflow-hidden`}
                >
                    <ListContact nameList="listFriend" serchUserGroup={inputValue} />
                </div>

                <div
                    className={` ${listUserCreateGroup.length > 0
                        ? "md:w-[40%] block w-[42%]"
                        : "w-0 hidden"
                    }  md:ml-4`}
                >
                    <ListUserCreateGroup />
                </div>
            </div>
            <div className="flex justify-end gap-x-4">
                <button
                    className="bg-gray-400 p-3 rounded-md"
                    onClick={() => {
                        handleCloseCreateGroup();
                    }}
                >
                    Hủy
                </button>
                <button
                    type="button"
                    className=" dark:bg-thPrimary bg-light-thPrimary p-3 rounded-md hover:opacity-75 disabled:opacity-50"
                    disabled={listUserCreateGroup.length < 2}
                    onClick={(e) => {
                        e.stopPropagation();
                        handleCreateGroup(nameGroup, listUserCreateGroup);
                    }}
                >
                    Tạo Nhóm
                </button>
            </div>
        </div>
    );
};

export default ModalCreateGroup;
