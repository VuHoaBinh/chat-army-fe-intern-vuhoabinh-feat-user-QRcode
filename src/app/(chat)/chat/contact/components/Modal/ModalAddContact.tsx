import axiosClient from "@/src/utils/axios/axiosClient";
import { ReactElement, useState } from "react";
import { BiSearch } from "react-icons/bi";
import ListUser from "../../../components/ListUser";
interface EditUserNameProps {
    //   contact: any;
    onClose: () => void;
    //   onSubmit: (otherUsername: string) => void;
    //   otherUsername: string;

}
const fetchListUser = (inputValue: string) =>
    axiosClient().get(
        `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/users?page=1&limit=20&keyword=${inputValue}`
    );

const ModalAddContact = ({ onClose }: EditUserNameProps): ReactElement => {
    const [inputValue, setInputValue] = useState("");

    const [dataFilter, setDatafilter] = useState<any>([]);

    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setInputValue(e.target.value);
    };
    const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === "Enter") {
            handleSearchContact(inputValue);
        }
    };
    const handleSearchContact = async (inputValue: string) => {
        const response = await fetchListUser(inputValue);

        const data = response.data;
        if (inputValue === "") {
            setDatafilter([]);
            return;
        }
        setDatafilter(
            data?.filter((c: any) => {
                return (
                    c?.username?.toLowerCase().includes(inputValue.toLowerCase()) ||
                    c?.email?.toLowerCase().includes(inputValue.toLowerCase()) ||
                    c?.phonenumber?.toLowerCase().includes(inputValue.toLowerCase())
                );
            })
        );
    };

    return (

        <div
            className="
            fixed 
            m-auto
            lg:m-0
            top-1/2 left-1/2   transform -translate-x-1/2 -translate-y-1/2
           dark:bg-thNewtral1 bg-light-thNewtral1
            rounded-lg 
            overflow-hidden
            shadow-2xl
            transition-all
            duration-200
            ease-in-out
            lg:min-w-[300px]
            lg:min-h-[300px]
            min-w-[350px]
            min-h-[250px]
            
            p-3
            z-50
           
            origin-center
            lg:origin-top-right
            flex gap-4
            flex-col
            "
        >
            <div className="">Thêm bạn</div>
            <hr className="bg-black dark:bg-white h-[1px] w-full" />

            <div className="text-sm flex flex-col max-h-[300px] overflow-y-scroll ">
                <div className="flex items-center border border-thNewtral1 rounded-md relative w-full">
                    <BiSearch className="ml-3 text-[#b5b5b5]" />
                    <input
                        type="text"
                        placeholder="Tìm Bạn"
                        className="bg-transparent outline-none p-2 w-full pr-12 pl-3"
                        value={inputValue}
                        onChange={handleInputChange}
                        onKeyDown={handleKeyDown}
                    />
                </div>
                <hr className="bg-black dark:bg-white h-[1px] w-full" />
                <ListUser dataSearch={dataFilter} />
            </div>

            <div className="flex justify-end gap-x-4">
                <button
                    className="bg-gray-400 p-3 rounded-md"
                    onClick={(e) => {
                        e.stopPropagation();
                        onClose();
                    }}
                >
                    Hủy
                </button>
                <button
                    className=" dark:bg-thPrimary bg-light-thPrimary p-3 rounded-md hover:opacity-75"
                    onClick={(e) => {
                        e.stopPropagation();
                        handleSearchContact(inputValue);
                    }}
                >
                    Tìm Kiếm
                </button>
            </div>
        </div>
    );
};

export default ModalAddContact;
