import Send from "@/public/send-2.svg";
import { useAppDispatch, useAppSelector } from "@/src/redux/hook";
import { updateUserCreateGroup } from "@/src/redux/slices/listUserCreateGroupSlice";
import { IInforGroup } from "@/src/types/Conversation";
import { IUserProfile } from "@/src/types/User";
import getAxiosClient from "@/src/utils/axios/axiosClient";
import { QRCode } from "antd";
import Image from "next/image";
import { SyntheticEvent, cache, memo, useEffect, useState } from "react";
import { FaTimes } from "react-icons/fa";
import userAvatar from "@/public/user.png";

type ListUser = {
  user: IUserProfile;
};
interface ListUserCreateProps {
  url?: string;
  handleShareLink?: (urlGroup: string, listUserCreateGroup: any) => void;
}
const fetchInforGroup = cache(async (id: string) => {
  try {
    const response = await getAxiosClient().get(
      `${process.env.NEXT_PUBLIC_DAK_CHAT_API}/conversations/group/invite/${id
        .split("/")
        .pop()}`
    );
    return response.data;
  } catch (err) {
    console.log(err);
  }
});

const ListUserCreateGroup = ({ url, handleShareLink }: ListUserCreateProps) => {
  const { listUserCreateGroup } = useAppSelector((state) => state.user) as any;
  const [inforGroup, setInforGroup] = useState<IInforGroup>();

  const dispatch = useAppDispatch();
  useEffect(() => {
    (async () => {
      const data: IInforGroup = await fetchInforGroup(url || "");
      setInforGroup(data);
    })();
  }, [url]);
  const handleRemoveUser = (userClick: ListUser) => {
    if (
      listUserCreateGroup.find(
        (user: any) => user.user.id === userClick.user.id
      )
    ) {
      dispatch(
        updateUserCreateGroup(
          listUserCreateGroup.filter(
            (checkedItem: any) => checkedItem.user.id !== userClick.user.id
          )
        )
      );
    }
  };
  const handleImageError = (event: SyntheticEvent<HTMLImageElement, Event>) => {
    const imgElement = event.target as HTMLImageElement;
    imgElement.src = userAvatar.src;
  };

  return (
    <div className="md:h-[96%] h-full w-[104%] mx-[-12px] rounded-md border-[1px] border-neutral-400  p-3  gap-y-4 relative  md:overflow-hidden">
      <div className="flex md:flex-col flex-row overflow-x-scroll md:gap-y-2">
        <span className="hidden md:block">
          Đã chọn {listUserCreateGroup.length}/100
        </span>
        {listUserCreateGroup.map((contact: any) => (
          <div
            key={contact.user.id}
            className="rounded-3xl text-sm px-2 md:bg-[#d5eaf5] flex  md:items-center gap-x-1 justify-between"
          >
            <div className="flex md:gap-x-3 gap-x-1 md:items-center">
              <div className="relative w-12 h-12 rounded-full md:w-5 md:h-5 flex items-center">
                <Image
                  src={
                    contact.user.avatar ===
                    "https://test3.stechvn.org/api/file/318323afa-8a24-11ee-a1eb-0242c0a83003.Circle_user.svg"
                      ? "/default_avatar.jpg"
                      : contact.user.avatar
                  }
                  alt="avt"
                  onError={handleImageError}
                  className="object-cover rounded-full overflow-hidden"
                  width={50}
                  height={50}
                />
                <FaTimes
                  size={16}
                  color={"#ccc"}
                  className="font-bold hover:cursor-pointer min-w-[12px] bg-thNewtral2 md:hidden absolute top-0 rounded-full  right-0 z-20"
                  onClick={() => handleRemoveUser(contact)}
                />
              </div>
              <span className="hidden md:block font-medium leading-6 text-blue-500 truncate w-16 md:w-[90px] md:truncate">
                {contact?.user?.name}
              </span>
            </div>

            <FaTimes
              size={12}
              color={"black"}
              className="font-bold hover:cursor-pointer min-w-[10px] hidden md:block"
              onClick={() => handleRemoveUser(contact)}
            />
          </div>
        ))}
      </div>
      <div className="flex md:hidden flex-1 items-center gap-x-2 justify-between px-2">
        <div className="flex items-center gap-x-4">
          <div className=" w-[60px] h-[50px] relative overflow-x-hidden mt-2">
            <div className="bg-gradient-to-r from-yellow-300 to-yellow-500 rounded-md h-full w-full flex flex-col p-1">
              <div className="flex items-center">
                <div className="text-xs truncate text-white">
                  {inforGroup?.name}
                </div>
              </div>
              <div className="absolute right-[-15px]  bottom-1">
                <QRCode
                  value={`${url}`}
                  className="w-full h-full p-0 border-none"
                  size={20}
                  bordered={false}
                  type="svg"
                  bgColor="white"
                />
              </div>
            </div>
          </div>
          <div className="flex flex-col max-w-[60%] sm:max-w-[90%] items-center justify-start">
            <div className=" text-sm w-full truncate">
              DAC - {inforGroup?.name}
            </div>
            <span className="text-neutral-300 text-xs w-full">DAC.me</span>
          </div>
        </div>
        <Image
          src={Send}
          sizes="(max-width: 40px) 40px, 40px"
          alt={"avt"}
          loading="eager"
          priority
          onError={handleImageError}
          width={40}
          height={40}
          className="rounded-full h-10 cursor-pointer rotate-45"
          onClick={() =>
            handleShareLink && handleShareLink(url || "", listUserCreateGroup)
          }
        />
      </div>
    </div>
  );
};

export default memo(ListUserCreateGroup);
