import React, { ReactElement } from "react";
import ListContact from "./components/ListContact";

const Contact = (): ReactElement => {
    return (
        <div className="flex flex-col gap-8 py-8 text-base lg:text-lg">
            {/* <h1 className="font-bold text-2xl pl-8">Contact</h1> */}
            <ListContact />
        </div>
    );
};

export default Contact;
