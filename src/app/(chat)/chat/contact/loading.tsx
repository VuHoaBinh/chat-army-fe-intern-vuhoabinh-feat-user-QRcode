import React, { ReactElement } from "react";
import "./styles.css";

const Loading = (): ReactElement => {
    const spanCount = 12;
    const spanElements = [];

    for (let i: number = 1; i <= spanCount; i++) {
        const spanStyle: React.CSSProperties = {
            ["--i" as any]: i
        };
        spanElements.push(<span style={spanStyle}></span>);
    }

    return (
        <div className="wrapper__loader">
            <div className="loader">{spanElements}</div>
        </div>
    );
};

export default Loading;
