"use client";
import React, { ReactElement } from "react";
import { CookiesProvider } from "react-cookie";
import { Provider } from "react-redux";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { persistStore } from "redux-persist";
import "../app/(joinGroupLink)/style.css";
import { store } from "../redux/store";
import "./globals.css";

persistStore(store);
export default function RootLayout({
    children
}: {
    children: React.ReactNode;
}): ReactElement {



    return (



        <CookiesProvider>
            <Provider store={store}>
                <html lang="en" >

                    <head>
                        <script dangerouslySetInnerHTML={{
                            __html: `
        try {
            if(!('theme' in localStorage)){
                localStorage.theme = 'dark'
                document.documentElement.classList.add('dark')
              }
            if (localStorage.theme === 'dark' || (!('theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
                document.documentElement.classList.add('dark')

              }
               else{
                document.documentElement.classList.remove('dark')
                
              }
        } catch (_) {}
      `
                        }} />
                        <title>DAC</title>
                        <link rel='icon' href='/logoDAK.jpg' />
                    </head>

                    <body className="relative dark:bg-black dark:text-white bg-white text-black">{children}

                        <ToastContainer />
                    </body>
                </html>
            </Provider>
        </CookiesProvider>



    );
}
