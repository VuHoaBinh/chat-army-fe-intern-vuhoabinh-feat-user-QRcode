import { useEffect, useState } from "react";
import { Socket, io } from "socket.io-client";
import { getLoginCookies } from "../utils/auth/handleCookies";

const useCreateSocket = (idConversation: string): any => {
    const [socket, setSocket] = useState<Socket<any, any> | any>(null);
    const cookieData = getLoginCookies();

    useEffect(() => {
        (() => {
            try {
                // const response = await axios.get(`/api/auth/chat-token`);
                // if(!response) return;
                // (process.env.NEXT_PUBLIC_DAK_CHAT_SOCKET);

                const socketConversation = io(
                    `${process.env.NEXT_PUBLIC_DAK_CHAT_SOCKET}/chat-${idConversation}`,
                    {
                        auth: { token: `Bearer ${cookieData?.token_chat}` },
                        autoConnect: true,
                        transports: ["websocket"],
                        withCredentials: true
                    }
                );
                setSocket(socketConversation);
            } catch (error: any) {
                throw new Error(error);
            }
        })();
    }, [idConversation]);
    return socket;
};

export default useCreateSocket;
