import { ISeenUser, IUser } from "./User";
export interface IMessage {
  _id: string;
  conversationId: string;
  type: number;
  content: string;
  seen: ISeenUser[];
  reactions: IUser[];
  isDeleted: boolean;
  createdAt: string;
  updatedAt: string;
  createdBy: ISeenUser | any;
  replyTo?: IMessage;
  isCompressed?: boolean;
  id: string;
}

export interface IMessageData {
  content: string;
  replyTo: any;
  conversationId: string;
  type: number;
  isCompressed?: boolean;
  exceptSocketId?: string;
}

export interface IForwardMessageData {
  content: string;
  forward_conversation_id: string;
  type: number;
  isCompressed?: boolean;
}
export interface IStatusSeened {
  value: boolean;
  type: number;
}

export type TMessageCallback = () => void;
