import { IMessage } from "./Message";
import { IUser } from "./User";

export interface IConversation {
  _id: string;
  type: number;
  createdBy: string;
  directUserId: string;
  isPinned: boolean;
  createdAt: string;
  updatedAt: string;
  latestMessage: IMessage[];
  createdByUser: IUser;
  unSeenMessageTotal: number;
  directUser: IUser;
  isConversationHidden?: any
}
export interface IInforGroup {
  avatar: string;
  name: string;
  reviewMember: boolean;
}
