import { ConversationVoteAnswer, INote, IVote } from "../redux/slices/newsletterSlice";
import { IMessage } from "./Message";

export interface IMessageMerge extends IMessage {
        itemType: "message"
    };
export interface INoteMerge extends INote {
        itemType: "note"
    };

export interface IVoteMerge extends IVote {
        itemType: "vote"
    };
export interface IActivityVote extends ConversationVoteAnswer{
        itemType: "voteAnswers",
        voteItem: IVote,
        id: string
    };