import { IMessage } from "./Message";
import { IUser } from "./User";

export interface Invitation {
  inviter: string;
  createdAt: Date;
  content: string;
}

type ChatTab = "1:1" | "group";

interface InvitationListItem extends Invitation {
  accept: () => void;
  decline: () => void;
  viewAll: () => void;
  chatTab: ChatTab;
}
export interface UserRequestFriend {
  _id: string;
  type: number;
  createdBy: string;
  directUserId: string;
  isPinned: boolean;
  createdAt: string;
  updatedAt: string;
  latestMessage: IMessage;
  createdByUser: IUser;
  unSeenMessageTotal: number;
  directUser: IUser;
  name: string;
  avatar: string;
}
export type InvitationList = InvitationListItem[];
