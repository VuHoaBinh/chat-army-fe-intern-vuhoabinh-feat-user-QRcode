export interface IUserCall {
  avatar: string;
  conversationId: string;
  device: string;
  type: number;
  userId: string;
  username: string;
}

export interface ICallState {
  isCalling: boolean;
  conversationId: string;
  deviceCall?: string;
  deviceAcceptCall?: string;
  userCreatCall?: string;
  userAcceptCall?: string;
}
