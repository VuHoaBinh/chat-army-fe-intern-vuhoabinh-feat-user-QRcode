export interface IBaseUser {
  _id?: string;
  username: string;
  avatar: string;
  id: string;
}

export interface IUserProfile extends IBaseUser {
  mode?: string;
  language: string;
  background_img: string;
  last_login: number;
  name: string;
  active?: number;
  bio?: string;
  email?: string;
  birthday?: string;
  gender?: number;
  is_calling?: boolean;
  createdBy?: string;
  idOther?: string;
}

export interface IUserSocket extends IUserProfile {
  socket?: any;
  token_chat?: number;
  is_watting_call?: boolean;
}

export interface ISeenUser extends IBaseUser {
  time: string;
}

export interface IUser extends IBaseUser {
  email: string;
  status: number;
  isActiveMember: boolean;
  isBlockStranger: boolean;
  blockUserIds: [];
  lastLogin: string;
  createdAt: string;
  updatedAt: string;
}

export interface IUserReact extends IUser {
  icon: number;
}

export interface IUserCookie {
  refresh_token: string;
  xsrf_token: string;
  chat_token: string;
  token: string;
  userAgent: string;
}
