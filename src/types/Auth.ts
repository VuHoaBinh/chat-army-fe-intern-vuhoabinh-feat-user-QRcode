export interface ApiRegisterResponse {
  success: boolean;
  message: string;
  error?: ErrorResponseRegister[];
}

export interface ErrorResponseRegister {
  field: string;
  message: string;
}
