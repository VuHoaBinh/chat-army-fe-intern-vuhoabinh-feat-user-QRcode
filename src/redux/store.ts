/* eslint-disable @typescript-eslint/no-unused-vars */
import { combineReducers, configureStore } from "@reduxjs/toolkit";
import {
    FLUSH,
    PAUSE,
    PERSIST,
    PURGE,
    REGISTER,
    REHYDRATE,
    persistReducer
} from "redux-persist";
import createWebStorage from "redux-persist/lib/storage/createWebStorage";
import conversationReducer from "./slices/conversationSlice";
import localStorageReducer from "./slices/localStorageSlice ";
import userReducer from "./slices/userSlice";
import { searchReducer } from "./slices/searchSlice";
import listUserCreateGroupReducer from "./slices/listUserCreateGroupSlice";
import callReducer from "./slices/callSlice";
import detailChatSlice from "./slices/detailChatSlice";
import newsletterSlice from "./slices/newsletterSlice";
import { endToEndEncryptionReducer } from "./slices/endToEndEncryptionSlice";
import { notificationReducer } from "./slices/NotifyOfConversationSlice";
import statusSeenReducer from "./slices/statusSeenedSlice";
const reducers = combineReducers({
    user: userReducer,
    conversation: conversationReducer,
    localStorage: localStorageReducer,
    search: searchReducer,
    listUserCreateGroup: listUserCreateGroupReducer,
    call: callReducer,
    statusSeen: statusSeenReducer,
    detailChat: detailChatSlice,
    newsletter: newsletterSlice,
    endToEndEncryption: endToEndEncryptionReducer,
    notify: notificationReducer
});
const createNoopStorage = () => {
    return {
        getItem(_key: any) {
            return Promise.resolve(null);
        },
        setItem(_key: any, value: any) {
            return Promise.resolve(value);
        },
        removeItem(_key: any) {
            return Promise.resolve();
        }
    };
};
const persistConfig = {
    key: "root",
    storage:
    typeof window !== "undefined"
        ? createWebStorage("local")
        : createNoopStorage(),
    blacklist: ["search", "detailChat", "newsletter", "notify"]
};
const persistedReducer = persistReducer(persistConfig, reducers);
export const store = configureStore({
    reducer: {
        user: persistedReducer
    },
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: {
                ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER]
            }
        })
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
