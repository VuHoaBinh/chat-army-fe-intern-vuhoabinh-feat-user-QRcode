import { IConversation } from "@/src/types/Conversation";
import { IMessage } from "@/src/types/Message";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface conversationState {
    otherUsername: string;
    otherAvatar: string;
    lastMessage?: IMessage | any;
    listConversation?: IConversation[];
    totalUnseenMessage?: number;
    messageReaction?: any;
    messageForward?: any
    listHiddenConversation?: IConversation[];
    ListIdPinnedConversation?: string[] | any;
}

const initialState: conversationState = {
    otherUsername: "",
    otherAvatar: "",
    lastMessage: {},
    listConversation: [],
    totalUnseenMessage: 0,
    messageReaction: undefined,
    messageForward: undefined,
    listHiddenConversation:[],
    ListIdPinnedConversation: []
};
const conversationSlice = createSlice({
    name: "conversation",
    initialState,
    reducers: {
        setOtherUser: (state, action: PayloadAction<conversationState>) => {
            return {
                ...state,
                ...action.payload
            };
        },
        setLastMessage: (state, action: PayloadAction<IMessage | any>) => {
            state.lastMessage = action.payload;
            return state;
        },
        setListConversations: (state, action: PayloadAction<IConversation[]>) => {
            state.listConversation = action.payload;
            return state;
        },
        setTotalUnseenMessage: (state, action: PayloadAction<number>) => {
            state.totalUnseenMessage = action.payload;
            return state;
        },
        setMessageReaction: (state, action: PayloadAction<object>) => {
            state.messageReaction = action.payload;
            return state;
        },
        setMessageForward: (state, action: PayloadAction<object>) => {
            state.messageForward = action.payload;
            return state;
        },
        setListHiddenConversation: (state, action: PayloadAction<IConversation[]>) => {
            state.listHiddenConversation = action.payload;
            return state;
        },
        setListIdPinnedConversation:(state, action: PayloadAction<string>) => {
            if (!state.ListIdPinnedConversation) {
                state.ListIdPinnedConversation = [];
            } else {
                const id: string = state.ListIdPinnedConversation.find(
                    (id: string) => id === action.payload
                );
                if(id) {
                    // remove
                    state.ListIdPinnedConversation = state.ListIdPinnedConversation.filter(
                        (id: string) => id !== action.payload
                    );
                } else {
                    // add
                    state.ListIdPinnedConversation.push(action.payload);
                }
            }
            return state;
        }
    }
});
export const { setOtherUser, setLastMessage, setListConversations, setTotalUnseenMessage, setMessageReaction, setListIdPinnedConversation, setMessageForward, setListHiddenConversation } = conversationSlice.actions;
export default conversationSlice.reducer;