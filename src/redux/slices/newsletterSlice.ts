
import { IMessage } from "@/src/types/Message";
import { IBaseUser } from "@/src/types/User";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface INote{
    id: string,
    content: string,
    createdBy: IBaseUser,
    isPinned: boolean,
    createdAt: string,
    updatedAt: string
}
interface IPinMessage{
    _id: string;
    conversationId: string;
    createdAt: string;
    updatedAt: string;
    id: string;
    message: IMessage;
}
export interface IVote {
    id: string;
    createdBy: {
        avatar: string;
        id: string;
        username: string;
    };
    question: string;
    isPinned: boolean;
    allowMultipleAnswers: boolean;
    allowAddOption: boolean;
    hideResultBeforeAnswers: boolean;
    hideMemberAnswers: boolean;
    allowChangeAnswers: boolean;
    status: number;
    duration: number;
    createdAt: string;
    updatedAt: string;
    conversationVoteOptions: ConversationVoteOption[];
}

interface ConversationVoteOption {
    id: string;
    option: string;
    createdAt: string;
    updatedAt: string;
    conversationVoteAnswers: ConversationVoteAnswer[];
}

export interface ConversationVoteAnswer {
    user: IBaseUser;
    createdAt: string;
    updatedAt: string;
}

interface newsletterState {
    messagePinned: IPinMessage[]
    noteList: INote[];
    voteList: IVote[];
    isShowModalAnswersVote: boolean
    answersVoteItem?: IVote,
    actionForNote: {
        type: string
        isShowEditNote: boolean
        noteItem?: INote,
    }
}

const initialState: newsletterState = {
    messagePinned:[],
    noteList: [],
    voteList: [],
    isShowModalAnswersVote: false,
    answersVoteItem: undefined,
    actionForNote: {
        type: "CREATE",
        isShowEditNote: false,
        noteItem: undefined
    }
};
const newsletterSlice = createSlice({
    name: "newsletterSlice",
    initialState,
    reducers: {
        setPinMessages: (state, action: PayloadAction<IPinMessage[]>) => {
            state.messagePinned = action.payload;
        },
        setNotes: (state, action: PayloadAction<INote[]>) => {
            state.noteList = action.payload;
        },
        setVotes: (state, action: PayloadAction<IVote[]>) => {
            state.voteList = action.payload;
        },
        unPinMessageItem: (state, action: PayloadAction<string>) => {
            state.messagePinned = state.messagePinned?.filter((item: any) => item._id !== action.payload);
        },
        setStartAnswersVote:(state, action: PayloadAction<IVote>) => {
            state.isShowModalAnswersVote = true;
            state.answersVoteItem = action.payload;
        },
        setEndAnswersVote:(state) => {
            state.isShowModalAnswersVote = true;
            state.answersVoteItem = undefined;
        },
        setStartEditNote:(state, action: PayloadAction<{typeAction: string, note?: INote}>) => {
            // Check if state.actionForNote is undefined and initialize it as an object
            if (!state.actionForNote) {
                state.actionForNote = {
                    type: "",
                    isShowEditNote: false,
                    noteItem: undefined
                };
            }
            state.actionForNote.isShowEditNote = true;
            state.actionForNote.noteItem = action.payload.note;
            state.actionForNote.type = action.payload.typeAction;
        },
        setEndEditNote:(state) =>{
            state.actionForNote.isShowEditNote = false;
            state.actionForNote.noteItem = undefined;
            state.actionForNote.type = "";
        }
    }
});
export const { setPinMessages, setNotes, setVotes, unPinMessageItem, setStartAnswersVote, setEndAnswersVote, setStartEditNote, setEndEditNote } = newsletterSlice.actions;
export default newsletterSlice.reducer;