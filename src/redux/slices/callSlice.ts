import { ICallState } from "@/src/types/Call";
import { PayloadAction, createSlice } from "@reduxjs/toolkit";

const initialState: ICallState = {
    isCalling: false,
    conversationId: "",
    deviceCall: "",
    deviceAcceptCall: "",
    userCreatCall: "",
    userAcceptCall: ""
};
const callSlice = createSlice({
    name: "call",
    initialState,
    reducers: {
        updateCalling: (state, action: PayloadAction<ICallState>) => {
            // const { isCalling, conversationId } = action.payload;
            // state.isCalling = isCalling;
            // state.conversationId = conversationId;

            return { ...state, ...action.payload };
        }
    }
});

export const { updateCalling } = callSlice.actions;
export default callSlice.reducer;
