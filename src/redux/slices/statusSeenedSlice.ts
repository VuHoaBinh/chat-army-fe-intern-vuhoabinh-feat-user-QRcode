import { IStatusSeened } from "@/src/types/Message";
import { PayloadAction, createSlice } from "@reduxjs/toolkit";

const initialState: IStatusSeened = {
    value: true,
    type: 0
};
const statusSeen = createSlice({
    name: "call",
    initialState,
    reducers: {
        updateStatusSeen: (state, action: PayloadAction<IStatusSeened>) => {
            return action.payload;
        }
    }
});

export const { updateStatusSeen } = statusSeen.actions;
export default statusSeen.reducer;
