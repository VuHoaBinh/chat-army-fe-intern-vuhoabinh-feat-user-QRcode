import { createSlice } from "@reduxjs/toolkit";

const listUserCreateGroup = createSlice({
    name: "listUserCreateGroup",
    initialState: [],
    reducers: {
        updateUserCreateGroup: (state, action) => {
            return action.payload;
        }
    }
});

export const { updateUserCreateGroup } = listUserCreateGroup.actions;
export default listUserCreateGroup.reducer;
