// searchSlice.js
import { IConversation } from '@/src/types/Conversation';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface NotificationState {
  isOpenModalTurnOffNotification: boolean;
  listIdConversationTurnOffNotify: IConversation[] | any;
}

const initialState: NotificationState = {
    isOpenModalTurnOffNotification: false,
    listIdConversationTurnOffNotify: []
};

const notificationSlice = createSlice({
    name: 'notification',
    initialState,
    reducers: {
        setOpenModalTurnOffNotifications: (state, action: PayloadAction<boolean>) => {
            state.isOpenModalTurnOffNotification = action.payload;
        },
        addListIdToListIdConversationTurnOffNotify: (state, action: PayloadAction<string[] | any>) => {
            state.listIdConversationTurnOffNotify = [...state.listIdConversationTurnOffNotify, ...action.payload];
        },
        addIdToListIdConversationTurnOffNotify: (state, action: PayloadAction<string>) => {
            if(state.listIdConversationTurnOffNotify.includes(action.payload)) {
                return;
            }
            if (!state.listIdConversationTurnOffNotify) {
                state.listIdConversationTurnOffNotify = [];
            } else {
                state.listIdConversationTurnOffNotify.push(action.payload);
            }
        },
        removeIdToListIdConversationTurnOffNotify: (state, action: PayloadAction<string>) => {
            if(!state.listIdConversationTurnOffNotify.includes(action.payload)) {
                return;
            }
            state.listIdConversationTurnOffNotify = state.listIdConversationTurnOffNotify.filter(
                (id: string) => id !== action.payload
            );
        }
    }
});

export const { setOpenModalTurnOffNotifications, addIdToListIdConversationTurnOffNotify, removeIdToListIdConversationTurnOffNotify, addListIdToListIdConversationTurnOffNotify } =
  notificationSlice.actions;

export const notificationReducer = notificationSlice.reducer;
