// searchSlice.js
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IMessage } from '@/src/types/Message';

export interface SearchState {
    isOpenSearch: boolean;
    isOpenHeaderSearch: boolean;
    idConversation: string;
    arrayValueToSearch: any[];
    searchItem: IMessage | any;
}

const initialState: SearchState = {
    isOpenSearch: false,
    isOpenHeaderSearch: false,
    idConversation: '',
    arrayValueToSearch: [],
    searchItem: null
};

const searchSlice = createSlice({
    name: 'search',
    initialState,
    reducers: {
        toggleOpenSearch: (state) => {
            state.isOpenSearch = !state.isOpenSearch;
        },
        toggleOpenHeaderSearch: (state) => {
            state.isOpenHeaderSearch = !state.isOpenHeaderSearch;
        },
        setIdConversation: (state, action: PayloadAction<string>) => {
            state.idConversation = action.payload;
        },
        setSearchItem: (state, action: PayloadAction<IMessage | null>) => {
            state.searchItem = action.payload;
        },
        setArrayValueToSearch: (state, action: PayloadAction<any[]>) => {
            state.arrayValueToSearch = action.payload;
        }
    }
});

export const {
    toggleOpenSearch,
    toggleOpenHeaderSearch,
    setIdConversation,
    setSearchItem,
    setArrayValueToSearch
} = searchSlice.actions;

export const searchReducer = searchSlice.reducer;

// export const selectSearch = (state: RootState) => state.search; // Nếu có
