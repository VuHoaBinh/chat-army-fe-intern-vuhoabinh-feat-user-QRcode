import { IUserSocket } from "@/src/types/User";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

const initialState: IUserSocket = {
    active: 1,
    avatar: "",
    background_img: "",
    id: "",
    last_login: 0,
    name: "",
    username: "",
    language: "vn",
    is_calling: false,
    is_watting_call:false
};

const userSlice = createSlice({
    name: "user",
    initialState,
    reducers: {
        setUserLogin: (state, action: PayloadAction<IUserSocket>) => {
            const {
                active,
                avatar,
                background_img,
                id,
                last_login,
                name,
                username,
                language,
                socket,
                is_calling
            } = action.payload;
            state.active = active;
            state.avatar = avatar;
            state.background_img = background_img;
            state.id = id;
            state.last_login = last_login;
            state.name = name;
            state.username = username;
            state.language = language;
            state.socket = socket;
            state.is_calling = is_calling;
            return state;
        },
        updateAvatar: (state, action: PayloadAction<string>) => {
            state.avatar = action.payload;
        },
        removeUser: (state) => {
            state.active = 0;
            state.avatar = "";
            state.background_img = "";
            state.id = "";
            state.last_login = 0;
            state.name = "";
            state.username = "";
            state.language = "vn";
            return state;
        },
        setUserIsCalling: (state, action: PayloadAction<boolean>) => {
            state.is_calling = action.payload;
        },
        setWaitCalling: (state, action: PayloadAction<boolean>) => {
            state.is_watting_call = action.payload;
        }
    }
});

export const { setUserLogin, updateAvatar, removeUser, setUserIsCalling, setWaitCalling } =
  userSlice.actions;
export default userSlice.reducer;
