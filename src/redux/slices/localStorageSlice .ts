import { createSlice } from "@reduxjs/toolkit";

const localStorageSlice = createSlice({
    name: "localStorage",
    initialState: null,
    reducers: {
        updateLocalStorageData: (state, action) => {
            return action.payload;
        }
    }
});

export const { updateLocalStorageData } = localStorageSlice.actions;
export default localStorageSlice.reducer;
