import { createSlice, PayloadAction } from '@reduxjs/toolkit';

type OptionProps = "enterPassword" | "forgotPassword" | "changePassword" | "none";

export interface EncryptionConversationProp {
    idConversation: string | any,
    hashEncryptionPassword: string | any,
    encryptionPassword: string | any,
    isDecoded: boolean
}

interface EndToEndEncryptionSliceState {
    isOpenModalGeneratesEncryptedPasswords: boolean;
    typeOfMofalHandlesPasswordsEncryption: OptionProps;
    listIdConversationDecoded: EncryptionConversationProp[];
}

const initialState: EndToEndEncryptionSliceState = {
    isOpenModalGeneratesEncryptedPasswords: false,
    listIdConversationDecoded: [],
    typeOfMofalHandlesPasswordsEncryption: "none"
};

export function findEncryptionConversationByIdConversation(targetId: string, list: EncryptionConversationProp[]): EncryptionConversationProp | null {
    return list.find(item => item.idConversation === targetId) || null;
}

const endToEndEncryptionSlice = createSlice({
    name: 'endToEndEncryption',
    initialState,
    reducers: {
        setOpenModalGeneratesEncryptedPasswords: (state, action: PayloadAction<boolean>) => {
            state.isOpenModalGeneratesEncryptedPasswords = action.payload;
        },
        setTypeOfMofalHandlesPasswordsEncryption: (state, action: PayloadAction<OptionProps>) => {
            state.typeOfMofalHandlesPasswordsEncryption = action.payload;
        },
        addListIdToListIdConversationDecoded: (state, action: PayloadAction<EncryptionConversationProp[] | any>) => {
            state.listIdConversationDecoded = [...action.payload];
        },
        addIdToListIdConversationDecoded: (state, action: PayloadAction<EncryptionConversationProp>) => {
            if(findEncryptionConversationByIdConversation(action.payload.idConversation, state.listIdConversationDecoded)) {
                return;
            }
            
            let newConversationDecoded: any = [];

            if (!state.listIdConversationDecoded) {
                state.listIdConversationDecoded = [];
                newConversationDecoded = [];
            } else {
                newConversationDecoded = [...state.listIdConversationDecoded, action.payload];
                state.listIdConversationDecoded.push(action.payload);
            }

            const listConversationDecoded = newConversationDecoded.map((c: any) => {
                return {
                    idConversation: c.idConversation,
                    hashEncryptionPassword: c.hashEncryptionPassword,
                    isDecoded: c.isDecoded
                };
            });

            if(localStorage.getItem("listEndToEndEncryptionConversation")) {
                localStorage.removeItem("listEndToEndEncryptionConversation");
            }
            localStorage.setItem("listEndToEndEncryptionConversation", JSON.stringify(listConversationDecoded));
        },
        removeIdToListIdConversationDecoded: (state, action: PayloadAction<string>) => {
            if(!findEncryptionConversationByIdConversation(action.payload, state.listIdConversationDecoded)) {
                return;
            }
            const newConversationDecoded = state.listIdConversationDecoded.filter(
                (c: EncryptionConversationProp) => c.idConversation !== action.payload
            );
            state.listIdConversationDecoded = newConversationDecoded;

            const listConversationDecoded = newConversationDecoded.map((c: any) => {
                return {
                    idConversation: c.idConversation,
                    hashEncryptionPassword: c.hashEncryptionPassword,
                    isDecoded: c.isDecoded
                };
            });

            if(localStorage.getItem("listEndToEndEncryptionConversation")) {
                localStorage.removeItem("listEndToEndEncryptionConversation");
            }
            localStorage.setItem("listEndToEndEncryptionConversation", JSON.stringify(listConversationDecoded));
        },
        updateIsDecodedOfEncryptionConversationById: (state, action: PayloadAction<{ idConversation: string; newIsDecoded: boolean, newHashedPassword: string }>) => {
            const { idConversation, newIsDecoded, newHashedPassword } = action.payload;
            const newConversationDecoded = state.listIdConversationDecoded.map((c: any) => {
                if(c.idConversation === idConversation) {
                    return {
                        ...c,
                        hashEncryptionPassword: newHashedPassword,
                        isDecoded: newIsDecoded
                    };
                }
                return c;
            });

            const listConversationDecoded = newConversationDecoded.map((c: any) => {
                return {
                    idConversation: c.idConversation,
                    hashEncryptionPassword: c.hashEncryptionPassword,
                    isDecoded: c.isDecoded
                };
            });

            if(localStorage.getItem("listEndToEndEncryptionConversation")) {
                localStorage.removeItem("listEndToEndEncryptionConversation");
            }
            localStorage.setItem("listEndToEndEncryptionConversation", JSON.stringify(listConversationDecoded));

            return { ...state, listIdConversationDecoded: newConversationDecoded };
        },
        updateHashEncryptionPasswordById: (state, action: PayloadAction<{ idConversation: string; newHashPassword: string }>) => {
            const { idConversation, newHashPassword } = action.payload;
            const newConversationDecoded = state.listIdConversationDecoded.map((c: any) => {
                if(c.idConversation === idConversation) {
                    return {
                        ...c,
                        hashEncryptionPassword: newHashPassword
                    };
                }
                return c;
            });

            const listConversationDecoded = newConversationDecoded.map((c: any) => {
                return {
                    idConversation: c.idConversation,
                    hashEncryptionPassword: c.hashEncryptionPassword,
                    isDecoded: c.isDecoded
                };
            });

            if(localStorage.getItem("listEndToEndEncryptionConversation")) {
                localStorage.removeItem("listEndToEndEncryptionConversation");
            }
            localStorage.setItem("listEndToEndEncryptionConversation", JSON.stringify(listConversationDecoded));

            return { ...state, listIdConversationDecoded: newConversationDecoded };
        },
        updateEncryptionPasswordById: (state, action: PayloadAction<{ idConversation: string; newPassword: string }>) => {
            const { idConversation, newPassword } = action.payload;
            const newConversationDecoded = state.listIdConversationDecoded.map((c: any) => {
                if(c.idConversation === idConversation) {
                    return {
                        ...c,
                        encryptionPassword: newPassword
                    };
                }
                return c;
            });

            const listConversationDecoded = newConversationDecoded.map((c: any) => {
                return {
                    idConversation: c.idConversation,
                    hashEncryptionPassword: c.hashEncryptionPassword,
                    isDecoded: c.isDecoded
                };
            });

            if(localStorage.getItem("listEndToEndEncryptionConversation")) {
                localStorage.removeItem("listEndToEndEncryptionConversation");
            }
            localStorage.setItem("listEndToEndEncryptionConversation", JSON.stringify(listConversationDecoded));

            return { ...state, listIdConversationDecoded: newConversationDecoded };
        }
    }
});

export const { setOpenModalGeneratesEncryptedPasswords, setTypeOfMofalHandlesPasswordsEncryption, updateEncryptionPasswordById, addListIdToListIdConversationDecoded, updateIsDecodedOfEncryptionConversationById, updateHashEncryptionPasswordById, addIdToListIdConversationDecoded, removeIdToListIdConversationDecoded } = endToEndEncryptionSlice.actions;

export const endToEndEncryptionReducer = endToEndEncryptionSlice.reducer;
