import { IBaseUser } from "@/src/types/User";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
interface IMember extends IBaseUser{
    type?: number
}
interface IPrevent{
    id: string
    userPrevented: {
        id: string,
        username: string,
        avatar: string
    }
}
interface ISetting {
    type: number,
    value: string|boolean|IPrevent[]
}
interface INote{
    id: string,
    content: string,
    createdBy: IBaseUser,
    isPinned: boolean,
    createdAt: string,
    updatedAt: string
}

interface detailChatState {
    _id: string;
    type: number;
    avatar: string;
    name: string;
    isPinned: boolean;
    status: number;
    lastMessageCreated: string;
    isDeleted: boolean;
    createdAt: string;
    updatedAt: string;
    id: string;
    members: IMember[];
    conversationSetting?: ISetting[];
    notePinned?: INote[];
    votePinned?: INote[];
    inviteId?: string;
}

const initialState: detailChatState = {
    _id: "",
    type: 0,
    avatar: "",
    name: "",
    isPinned: false,
    status: 0,
    lastMessageCreated: "",
    isDeleted: false,
    createdAt: "",
    updatedAt: "",
    id: "",
    members: [],
    conversationSetting: [
        {
            "type": 3,
            "value": true
        },
        {
            "type": 4,
            "value": true
        },
        {
            "type": 5,
            "value": true
        },
        {
            "type": 6,
            "value": true
        },
        {
            "type": 7,
            "value": true
        },
        {
            "type": 9,
            "value": "true"
        },
        {
            "type": 8,
            "value": []
        }
    ],
    notePinned: [],
    votePinned: [],
    inviteId: ""
};
const detailChatSlice = createSlice({
    name: "detailChat",
    initialState,
    reducers: {
        setDetailChat: (state, action: PayloadAction<detailChatState>) => {
            return {
                ...state,
                ...action.payload
            };
        }
    }
});
export const { setDetailChat } = detailChatSlice.actions;
export default detailChatSlice.reducer;